﻿using Emerge.Core;
using Leap;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomOVRGrabbable : OVRGrabbable
{
    //[Range(0, 0.25f)]
    //public float influenceZoneSize = 0.15f;
    private Vector3 deltaGrabPos;
    private TactileMesh tm;
    private Collider currentGrabber;
    public bool grabbed;

    public OVRSkeleton.SkeletonType skeletontype;

    public OVRSkeleton skeleton_right;
    public OVRSkeleton skeleton_left;

    public GameObject Vibration_pouce_offset;
    public GameObject Vibration_index_offset;


    // Start is called before the first frame update
    new void Start()
    {
        tm = GetComponent<TactileMesh>();
        base.Start();
        skeletontype = OVRSkeleton.SkeletonType.None;
    }

    public void Update()
    {

        if (grabbed)
        {
            //Si la louttre est grab on fait vibrer la main
            switch (skeletontype)
            {
                case OVRSkeleton.SkeletonType.HandLeft:
                    VibrationLeft(skeleton_left);
                    break;
                case OVRSkeleton.SkeletonType.HandRight:
                    VibrationRight(skeleton_right);
                    break;
                default:
                    break;

            }
        }
        else
        {
            Vibration_pouce_offset.SetActive(false);
            Vibration_index_offset.SetActive(false);
        }
    }


    public override void GrabBegin(OVRGrabber hand, Collider grabPoint)
    {
        
        currentGrabber = grabPoint;
        deltaGrabPos = this.transform.position;
        GetComponent<MeshRenderer>().material.color = Color.red;
        grabbed = true;
        base.GrabBegin(hand, grabPoint);
        


    }

    public override void GrabEnd(Vector3 linearVelocity, Vector3 angularVelocity)
    {

        //SPECIFIC CODE FOR HANDLING INTERFACES / TACTILE MESH MODIFICATIONS ....
        if (tm)
        {
            tm.Strength = 0;
            // For all type of targets slots (Sphere, Pillar ...) needing to be notified of the grab release
            MySceneManager.Instance.NotifyDrop();
        }
        GetComponent<MeshRenderer>().material.color = Color.white;
        base.GrabEnd(linearVelocity, angularVelocity);

        grabbed = false;
    }

    public void OnTriggerEnter(Collider other)
    {
        //Avec quel main on attrape la louttre
        if (other.gameObject.GetComponent<OVRSkeleton>() != null)
        {
            if (other.gameObject.GetComponent<OVRSkeleton>().GetSkeletonType() != OVRSkeleton.SkeletonType.None)
            {
                skeletontype = other.gameObject.GetComponent<OVRSkeleton>().GetSkeletonType();
            }
        }
    }

    public void OnTriggerStay(Collider other)
    {
        // Detection des doigts avec le Leap est encore problématique pour le Quest c'est ok
        // Si on passe la barriere, le reset de la position de la louttre est ok
        // Mais le reset de la vibration est incompréhensible
        // Et parfois on ne peut pas attraper la louttre après qu'elle soit désincarner
        if (this.gameObject.name == "Star Otter")
        {
            
            if(GameObject.Find("SkeletalPhysicsHand(Clone)(LEFT)") != null)
            {
                if (GameObject.Find("SkeletalPhysicsHand(Clone)(LEFT)").activeSelf && other.transform.IsChildOf(GameObject.Find("SkeletalPhysicsHand(Clone)(LEFT)/Index").transform))
                {
                    
                    Debug.Log("ok ca detecte la gauche");
                    if (Vector3.Distance(GameObject.Find("SkeletalPhysicsHand(Clone)(LEFT)/Thumb/Distal/Bone").transform.position, GameObject.Find("SkeletalPhysicsHand(Clone)(LEFT)/Index/Distal/Bone").transform.position) < .02f)
                    {
                        Debug.Log("test ok distance : " + Vector3.Distance(GameObject.Find("SkeletalPhysicsHand(Clone)(LEFT)/Thumb/Distal/Bone").transform.position, GameObject.Find("SkeletalPhysicsHand(Clone)(LEFT)/Index/Distal/Bone").transform.position));
                        grabbed = true;
                        if (grabbed == true)
                        {
                            this.transform.position = GameObject.Find("SkeletalPhysicsHand(Clone)(LEFT)/Thumb/Distal/Bone").transform.position;
                            Vibration_pouce_offset.SetActive(true);
                            Vibration_pouce_offset.transform.position = GameObject.Find("SkeletalPhysicsHand(Clone)(LEFT)/Thumb/Distal/Bone").transform.position;
                            Vibration_pouce_offset.transform.rotation = transform.rotation;

                            Vibration_index_offset.SetActive(true);
                            Vibration_index_offset.transform.position = GameObject.Find("SkeletalPhysicsHand(Clone)(LEFT)/Index/Distal/Bone").transform.position;
                            Vibration_index_offset.transform.rotation = transform.rotation;
                        }
                    }
                }
                if (Vector3.Distance(GameObject.Find("SkeletalPhysicsHand(Clone)(LEFT)/Thumb/Distal/Bone").transform.position, GameObject.Find("SkeletalPhysicsHand(Clone)(LEFT)/Index/Distal/Bone").transform.position) > .02f)
                {
                    grabbed = false;
                    Vibration_pouce_offset.SetActive(false);
                    Vibration_index_offset.SetActive(false);
                }

            }
            if(GameObject.Find("SkeletalPhysicsHand(Clone)(RIGHT)") != null)
            {
                if (GameObject.Find("SkeletalPhysicsHand(Clone)(RIGHT)").activeSelf && other.transform.IsChildOf(GameObject.Find("SkeletalPhysicsHand(Clone)(RIGHT)/Index").transform))
                {
                    
                    Debug.Log("ok ca detecte la droite");
                    if (Vector3.Distance(GameObject.Find("SkeletalPhysicsHand(Clone)(RIGHT)/Thumb/Distal/Bone").transform.position, GameObject.Find("SkeletalPhysicsHand(Clone)(RIGHT)/Index/Distal/Bone").transform.position) < .02f)
                    {
                        grabbed = true;
                        if (grabbed == true)
                        {
                            this.transform.position = GameObject.Find("SkeletalPhysicsHand(Clone)(RIGHT)/Thumb/Distal/Bone").transform.position;
                            Vibration_pouce_offset.SetActive(true);
                            Vibration_pouce_offset.transform.position = GameObject.Find("SkeletalPhysicsHand(Clone)(RIGHT)/Thumb/Distal/Bone").transform.position;
                            Vibration_pouce_offset.transform.rotation = transform.rotation;

                            Vibration_index_offset.SetActive(true);
                            Vibration_index_offset.transform.position = GameObject.Find("SkeletalPhysicsHand(Clone)(RIGHT)/Index/Distal/Bone").transform.position;
                            Vibration_index_offset.transform.rotation = transform.rotation;
                        }
                    }
                    if (Vector3.Distance(GameObject.Find("SkeletalPhysicsHand(Clone)(RIGHT)/Thumb/Distal/Bone").transform.position, GameObject.Find("SkeletalPhysicsHand(Clone)(RIGHT)/Index/Distal/Bone").transform.position) > .02f)
                    {
                        grabbed = false;
                        Vibration_pouce_offset.SetActive(false);
                        Vibration_index_offset.SetActive(false);
                    }
                }
            }
        }
    }

    public void OnTriggerExit(Collider other)
    {
        //grabbed = false;
    }

    public void VibrationRight(OVRSkeleton skeleton_right)
    {
        foreach (OVRBone bone in skeleton_right.Bones)
        {
            if (bone.Id == OVRSkeleton.BoneId.Hand_Thumb3)
            {
                Debug.Log("Pouce droit trouvé : " + bone.Transform.position);

                Vibration_pouce_offset.SetActive(true);
                Vibration_pouce_offset.transform.position = bone.Transform.position;
                Vibration_pouce_offset.transform.rotation = transform.rotation;

            }
            if (bone.Id == OVRSkeleton.BoneId.Hand_Index3)
            {
                Vibration_index_offset.SetActive(true);
                Vibration_index_offset.transform.position = bone.Transform.position;
                Vibration_index_offset.transform.rotation = transform.rotation;
            }
        }
    }

    public void VibrationLeft(OVRSkeleton skeleton_left)
    {
        foreach (OVRBone bone in skeleton_left.Bones)
        {
            if (bone.Id == OVRSkeleton.BoneId.Hand_Thumb3)
            {
                Debug.Log("Pouce gauche trouvé : " + bone.Transform.position);

                Vibration_pouce_offset.SetActive(true);
                Vibration_pouce_offset.transform.position = bone.Transform.position;
                Vibration_pouce_offset.transform.rotation = transform.rotation;
            }

            if (bone.Id == OVRSkeleton.BoneId.Hand_Index3)
            {
                Vibration_index_offset.SetActive(true);
                Vibration_index_offset.transform.position = bone.Transform.position;
                Vibration_index_offset.transform.rotation = transform.rotation;
            }
        }
    }
}
