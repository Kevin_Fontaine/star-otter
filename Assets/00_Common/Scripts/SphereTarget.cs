﻿using Emerge.Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereTarget : MonoBehaviour , IDroppable
{
    private SphereCollider col;
    private TactileMesh tm;
    private MeshRenderer mr;
    private bool isItemInside = false ;
    private float score = 0;

    // Start is called before the first frame update
    void Start()
    {
        col = GetComponent<SphereCollider>();
        mr = GetComponent<MeshRenderer>();

        mr.enabled = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Item")
        {
            isItemInside = true;
            tm = other.GetComponent<TactileMesh>();
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Item")
        {
            isItemInside = true;
            if (tm)
            {
                float deltaPos = (other.transform.position - this.transform.position).magnitude;
                //TODO "0.10m to 0m" progressive snap zone for intensity
                score = 100 * Mathf.Max(0, 1 - (Mathf.Abs(deltaPos) / (col.radius)));

                //Snap zone for testing
                if (score > 70)
                    score = 100;
                tm.Strength = score;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Item")
        {
            isItemInside = false;
            if (tm)
            {
                tm.Strength = 0;
            }
        }
    }

    public void OnDrop()
    {
        if (!isItemInside) return;
        if (mr)
        {
            if (score > 70)
            {
                mr.enabled = true;
                mr.material.color = Color.red;
                //POSSIBLE SNAP ITEM POSITION TO SET
            }
        }

    }
}
