﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plateform : MonoBehaviour
{

    public LevelGenerator lg;

    // Start is called before the first frame update
    void Start()
    {
        if(lg == null)
        {
            lg = GameObject.Find("Level Editor").GetComponent<LevelGenerator>();
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        if (lg.Instances.Count > 0)
        {
            foreach(GameObject obj in lg.Instances)
            {
                if(obj.name == "star(Clone)")
                {
                    obj.transform.Rotate(0.0f, 1.0f, 0.0f, Space.World);
                }
            }
            

        }
    }
}
