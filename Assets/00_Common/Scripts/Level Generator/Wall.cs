﻿using Emerge.Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wall : MonoBehaviour
{

    public GameObject otter;

    private void Start()
    {
        if(otter.gameObject == null)
        {
            otter = GameObject.FindGameObjectWithTag("Item");
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        WallState();

        
    }

    void WallState()
    {
        
        if (!otter.GetComponent<CustomOVRGrabbable>().grabbed)
        {
            Debug.Log("wall Haptic feedback activated");
            Debug.Log("Wall child count " + this.transform.childCount);
            int value = this.transform.childCount;
            for (int i = 0; i < value; i++)
            {
                Debug.Log("Wall i: " + i);
                Debug.Log("Wall Child access " + transform.GetChild(i).name);
                GameObject walltmp = transform.GetChild(i).gameObject;
                Debug.Log("Wall tmp: " + walltmp.name);
                walltmp.GetComponent<BoxCollider>().enabled = true;
                if(walltmp.GetComponent<TactilePoint>() != null)
                {
                    walltmp.GetComponent<TactilePoint>().enabled = true;
                }
                
            }
        }
        else
        {
            Debug.Log("wall Haptic feedback desactivated");
            int value = this.transform.childCount;
            for (int i = 0; i < value; i++)
            {
                GameObject walltmp = transform.GetChild(i).gameObject;
                walltmp.GetComponent<BoxCollider>().enabled = false;
                if (walltmp.GetComponent<TactilePoint>() != null)
                {
                    walltmp.GetComponent<TactilePoint>().enabled = false;
                }
            }
        }
    }


}
