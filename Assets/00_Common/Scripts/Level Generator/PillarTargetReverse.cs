﻿using DG.Tweening;
using Emerge.Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PillarTargetReverse : MonoBehaviour
{
    private TactileMesh tm;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Item")
        {
            tm = other.GetComponent<TactileMesh>();
            Debug.Log("Puissance appliqué quand avant le mur : " + tm.Strength);
        }

        Debug.Log(other.name + " est entré en collision avec le mur");

    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Item")
        {
            Vector3 pillar = other.transform.position - this.transform.position;
            float magnitude = Vector3.Distance(other.transform.position, this.transform.position);

            tm.Strength = 0;
            Debug.Log("Puissance appliqué quand dans le mur : " + tm.Strength);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Item")
        {
            Debug.Log("Puissance appliqué quand apres le mur : " + tm.Strength);
            tm.Strength = 100;
        }
        
    }

}
