﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetLevel : MonoBehaviour
{

    public LevelGenerator lg;
    public GameObject otter;
    public IEnumerator restart;
    public Level level;


    private void Start()
    {
        level = GameObject.Find("Level Editor").GetComponent<Level>();

    }

    public void Launchreset()
    {
        Debug.Log("Clear Old Level");
        foreach (GameObject ToDestroy in lg.Instances)
        {
            Destroy(ToDestroy);
        }
        lg.Instances.Clear();

        Debug.Log("Generate next Level");
        lg.LevelSelector += 1; ;
        if(lg.LevelSelector > level.LevelList.Count)
        {
            lg.LevelSelector = 0;
        }

        lg.LevelGen();
        lg.GeneratePlateform();
        lg.GenerateWall();
        lg.SetPositionOtter(lg.Instances, otter);
        Debug.Log("Reset in progress");
        /*
        restart = Restart();
        StartCoroutine(restart);
        */

    }

    public IEnumerator Restart()
    {
        Debug.Log("Coroutine reset");
        yield return new WaitForSeconds(2);
        //otter.transform.position = GameObject.FindGameObjectWithTag("Start").transform.position;
        //lg.SetPositionOtter();
        Debug.Log("Reset ok");
    }


}
