﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level : MonoBehaviour
{
    // Start is called before the first frame update

    public List<int[,,]> LevelList;
    public LevelGenerator lg;

    public int height_0;
    public int height_1;
    public int height_2;

    private void Awake()
    {
        LevelList = new List<int[,,]>();
        Debug.Log("Generate level array");
        int[,,] level0 = new int[3, 3, 3] {
                        { { 1, 1, 1 }, { 4, 3, 2 }, { 1, 1, 1 } },
                        { { 1, 1, 1 }, { 1, 1, 1 }, { 1, 1, 1 } },
                        { { 1, 1, 1 }, { 1, 1, 1 }, { 1, 1, 1 } }
                    };
        LevelList.Add(level0);
        int[,,] level1 = new int[3, 3, 3] {
                        { { 1, 1, 1 }, { 4, 0, 2 }, { 1, 1, 1 } },
                        { { 1, 1, 1 }, { 1, 1, 1 }, { 1, 1, 1 } },
                        { { 1, 1, 1 }, { 1, 1, 1 }, { 1, 1, 1 } }
                    };
        LevelList.Add(level1);
        int[,,] level2 = new int[3, 3, 3] {
                        { { 1, 0, 1 }, { 4, 0, 2 }, { 1, 0, 1 } },
                        { { 1, 1, 1 }, { 1, 1, 1 }, { 1, 1, 1 } },
                        { { 1, 1, 1 }, { 1, 1, 1 }, { 1, 1, 1 } }
                    };
        LevelList.Add(level2);
        int[,,] level3 = new int[3, 3, 3] {
                        { { 4, 0, 3 }, { 3, 0, 2 }, { 3, 0, 3 } },
                        { { 1, 0, 1 }, { 1, 0, 1 }, { 1, 0, 1 } },
                        { { 1, 1, 1 }, { 1, 1, 1 }, { 1, 1, 1 } }
                    };
        LevelList.Add(level3);
        int[,,] level4 = new int[2, 3, 2] {
                        { { 1, 4 }, { 1, 1 }, { 0, 1 } },
                        { { 1, 1 }, { 1, 1 }, { 2, 1 } }
                    };
        LevelList.Add(level4);
        int[,,] level5 = new int[2, 3, 2] {
                        { { 1, 4 }, { 5, 5 }, { 5, 1 } },
                        { { 1, 1 }, { 5, 5 }, { 2, 1 } }
                    };
        LevelList.Add(level5);
        int[,,] level6 = new int[3, 3, 2] {
                        { { 6, 4 }, { 5, 5 }, { 5, 5 } },
                        { { 1, 1 }, { 6, 3 }, { 5, 1 } },
                        { { 1, 1 }, { 1, 2 }, { 3, 3 } }
                    };
        LevelList.Add(level6);


        // Esprit Bloc = 0
        // Vide = 1
        // Plateforme Finish = 2
        // Plateforme Normal = 3
        // Plateforme Start = 4;
        // Esprit Bloc 90 = 5
        // Pente = 6

    }

    void Start()
    {

        lg = GetComponent<LevelGenerator>();



    }

    // Update is called once per frame
    void Update()
    {
        height_0 = LevelList[lg.LevelSelector].GetLength(0);
        height_1 = LevelList[lg.LevelSelector].GetLength(1);
        height_2 = LevelList[lg.LevelSelector].GetLength(2);
    }
}
