﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Emerge.Core;
using OVRTouchSample;
using System.Diagnostics.Tracing;
using JetBrains.Annotations;
using Leap.Unity;
using Emerge.Tracking;

public class Otter : MonoBehaviour
{

    private TactileMesh tm;
    private TactilePoint tpp;
    private TactilePoint tpi;
    [Range(0, 100)]
    public float StrengthMesh = 50.0f;
    [Range(0, 100)]
    public float Threshold_z1;

    [Range(0, 100)]
    public float Threshold_z2;
    [Range(0, 100)]
    public float Threshold_z2_start;
    [Range(0.0f, 5.0f)]
    public float multiplier;




    public ResetLevel reset;
    [Range(0, 2.5f)]
    public float resettime;





    private void Start()
    {
        GetComponent<Renderer>().material.color = Color.grey;
        tm = this.GetComponent<TactileMesh>();
        tpp = GetComponent<CustomOVRGrabbable>().Vibration_pouce_offset.GetComponent<TactilePoint>();
        tpi = GetComponent<CustomOVRGrabbable>().Vibration_index_offset.GetComponent<TactilePoint>();

        
    }

    private void Update()
    {
        tm.Strength = tpp.Strength = tpi.Strength = StrengthMesh;

       

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Target"))
        {
            GetComponent<Renderer>().material.color = Color.red;
            float score = 70;
            StrengthMesh = score;

        }



        
    }

    private void OnTriggerStay(Collider other)
    {
        if(other.CompareTag("Target"))
        {
            Debug.Log("la louttre est dans le mur, je répète la louttre est dans le mur");

            Transform deadzone = other.transform.GetChild(0);
            Vector3 point = new Vector3(deadzone.position.x, this.transform.position.y, this.transform.position.z);
            float DecreaseStrength = Vector3.Distance(this.transform.position, point);

            Debug.Log("Distance entre la louttre et la deadzone: " + DecreaseStrength);
            Debug.Log("Deadzone distance *1000: " + DecreaseStrength * 1000);
            Debug.Log("Deadzone distance clamp: " + Mathf.Clamp(DecreaseStrength * 1000, 30.0f, 70.0f));

            float score = Mathf.Clamp(DecreaseStrength * 1000, 0.0f, Threshold_z2_start);

            if(score < Threshold_z2)
            {
                score /= multiplier;
            }
            StrengthMesh = score;
            Debug.Log("Deazone strength value: " + score);

            //Si la louttre est trop loin dans le cube, on recommence
            if (tm.Strength < Threshold_z1)
            {
                ForceReleaseOtter();
                LevelGenerator leveleditor = GameObject.Find("Level Editor").GetComponent<LevelGenerator>();
                leveleditor.SetPositionOtter(leveleditor.Instances, this.gameObject);
                StrengthMesh = 100;
            }

            if(!GetComponent<CustomOVRGrabbable>().grabbed)
            {
                StrengthMesh = Threshold_z1 - 1.0f;
            }
        }
        Debug.Log("La louttre est entré en collision avec " + other.name);

        Debug.Log("Grabbed" + GetComponent<CustomOVRGrabbable>().grabbed);
        if (other.CompareTag("Finish"))
        {
            if(GetComponent<CustomOVRGrabbable>().grabbed == false)
            {
                StrengthMesh = 0;
                other.gameObject.GetComponent<ResetLevel>().Launchreset();
                Debug.Log("player at the end");
                StrengthMesh = 100;
            }


        }

    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Target"))
        {
            StrengthMesh = 100;
        }
    }

    public void ForceReleaseOtter()
    {
        CustomOVRGrabbable lache = GetComponent<CustomOVRGrabbable>();
        Rigidbody rb = gameObject.GetComponent<Rigidbody>();
        lache.GrabEnd(rb.velocity = new Vector3(0.0f, 0.0f, 0.0f), rb.angularVelocity = new Vector3(0.0f, 0.0f, 0.0f));
        lache.grabbed = false;

        // Force release hand with leap motion 
        // get the gameobject and disable the grabble bool in the script i've forgot the name
        if (GameObject.Find("SkeletalPhysicsHand(Clone)(LEFT)") != null)
        {
            GameObject Hand = GameObject.Find("SkeletalPhysicsHand(Clone)(LEFT)");
            Hand.GetComponent<InteractableHand>().AllowsGrabbing = false;
        }
        if (GameObject.Find("SkeletalPhysicsHand(Clone)(RIGHT)") != null)
        {
            GameObject Hand = GameObject.Find("SkeletalPhysicsHand(Clone)(RIGHT)");
            Hand.GetComponent<InteractableHand>().AllowsGrabbing = false;
        }

        IEnumerator resetallowgrabbing = test();
        StartCoroutine(test());
    }

    IEnumerator test()
    {

        yield return new WaitForSeconds(5);
        Debug.Log("ok pour le test");
        if (GameObject.Find("SkeletalPhysicsHand(Clone)(LEFT)") != null)
        {
            GameObject Hand = GameObject.Find("SkeletalPhysicsHand(Clone)(LEFT)");
            Hand.GetComponent<InteractableHand>().AllowsGrabbing = true;
        }
        if (GameObject.Find("SkeletalPhysicsHand(Clone)(RIGHT)") != null)
        {
            GameObject Hand = GameObject.Find("SkeletalPhysicsHand(Clone)(RIGHT)");
            Hand.GetComponent<InteractableHand>().AllowsGrabbing = true;
        }
    }

}

