﻿using System;
using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;
using UnityEngine.UI;

public class LevelGenerator : MonoBehaviour
{
    public int[,,] array3D;

    public int height_0;
    public int height_1;
    public int height_2;


    public GameObject Player;

    public GameObject ParentClone;
    public GameObject CubeClone;
    public GameObject SphereClone;
    public GameObject PlateformeClone;
    public GameObject PenteClone;
    public GameObject StarClone;

    public GameObject[] Limites;
    public Level level;

    public int LevelSelector;

    public List<GameObject> Instances;

    public float Distance01;
    public float Distance02;
    public float Distance03;

    public Text leveltext;


    // Start is called before the first frame update
    public void Start()
    {
        //LevelSelector = 0;
        level = GetComponent<Level>();
        Debug.Log("Start level generation");

        

        LevelGen();
        GenerateWall();
        GeneratePlateform();
        SetPositionOtter(Instances, Player);



    }



    // Update is called once per frame
    void Update()
    {
        leveltext.text = "Tutorial level " + LevelSelector + "/ " + this.GetComponent<Level>().LevelList.Count.ToString() ;
        Debug.Log("Position Update Origine 0: " + Limites[0].transform.position + " 1: " + Limites[1].transform.position + " 2: " + Limites[2].transform.position + " 3: " + Limites[3].transform.position);

    }

    public void LevelGen()
    {
        Debug.Log("Level Genenration in progress");

        //Delimitation de la zone de jeu
        Distance01 = Vector3.Distance(Limites[0].transform.position, Limites[1].transform.position);
        Distance02 = Vector3.Distance(Limites[0].transform.position, Limites[2].transform.position);
        Distance03 = Vector3.Distance(Limites[0].transform.position, Limites[3].transform.position);

        Debug.Log("Distance origine 01: " + Distance01 + " 02: " + Distance02 + " 03: " + Distance03);

        height_0 = level.LevelList[LevelSelector].GetLength(0);
        height_1 = level.LevelList[LevelSelector].GetLength(1);
        height_2 = level.LevelList[LevelSelector].GetLength(2);

        array3D = new int[height_0, height_1, height_2];

        // Generate Level Base
        for (int i = 0; i < height_0; i++)
        {
            //axe y
            for (int j = 0; j < height_1; j++)
            {
                //axe z
                for (int k = 0; k < height_2; k++)
                {
                    // axe x
                    Debug.Log("Level selector: " + LevelSelector);
                    Debug.Log("Level list count : " + level.LevelList.Count);
                    int[,,] levelselected = level.LevelList[LevelSelector];
                    Debug.Log("test level value " + levelselected[i, j, k]);

                    array3D[i, j, k] = levelselected[i, j, k];
                    //array3D[i, j, k] = Random.Range(0, 2);

                    // Cube = 0
                    // Sphere = 1
                    // Finish = 2
                    // Plateforme Normal = 3
                    // Start = 4;
                    // Cube 90 = 5
                }
            }
        }
    }

    public void GeneratePlateform()
    {
        Debug.Log("Position Gen Origine 0: " + Limites[0].transform.position + " 1: " + Limites[1].transform.position + " 2: " + Limites[2].transform.position + " 3: " + Limites[3].transform.position);
        //Delimitation de la zone de jeu
        Distance01 = Vector3.Distance(Limites[0].transform.position, Limites[1].transform.position);
        Distance02 = Vector3.Distance(Limites[0].transform.position, Limites[2].transform.position);
        Distance03 = Vector3.Distance(Limites[0].transform.position, Limites[3].transform.position);

        height_0 = level.LevelList[LevelSelector].GetLength(0);
        height_1 = level.LevelList[LevelSelector].GetLength(1);
        height_2 = level.LevelList[LevelSelector].GetLength(2);

        Debug.Log("Generate start end plateform");
        //Generate Start-End plateforme
        PlateformeClone.SetActive(true);
        StarClone.SetActive(true);
        for (int i = 0; i < height_0; i++)
        {
            //axe y
            for (int j = 0; j < height_1; j++)
            {
                //axe z
                for (int k = 0; k < height_2; k++)
                {
                    // axe x

                    float y = Limites[0].transform.position.y + (i * (Distance03 / (height_0 - 1)));
                    float z = Limites[0].transform.position.z + (j * (Distance02 / (height_1 - 1)));
                    float x = Limites[0].transform.position.x + (k * (Distance01 / (height_2 - 1)));

                    
                    
                    if (array3D[i, j, k] == 4)
                    {
                        //Position de la plateforme de depart
                        GameObject StartPlateforme = Instantiate(PlateformeClone, new Vector3(x, y, z), Quaternion.identity, ParentClone.transform);
                        StartPlateforme.tag = "Start";
                        Debug.Log("Plateforme de départ: " + new Vector3(x, y, z));
                        StartPlateforme.GetComponent<MeshRenderer>().enabled = true;
                        Instances.Add(StartPlateforme);
                    }

                    if ((array3D[i, j, k] == 3))
                    {
                        //Position de la plateforme normal
                        GameObject NormalPlateforme = Instantiate(PlateformeClone, new Vector3(x, y, z), Quaternion.identity, ParentClone.transform);
                        NormalPlateforme.GetComponent<MeshRenderer>().enabled = true;
                        Instances.Add(NormalPlateforme);
                    }

                    if ((array3D[i, j, k] == 2))
                    {
                        //Position de la plateforme d'arrive
                        GameObject EndPlateforme = Instantiate(PlateformeClone, new Vector3(x, y, z), Quaternion.identity, ParentClone.transform);
                        EndPlateforme.tag = "Finish";
                        EndPlateforme.GetComponent<MeshRenderer>().enabled = true;
                        Instances.Add(EndPlateforme);

                        GameObject EndStar = Instantiate(StarClone, new Vector3(x, y + 0.03f, z), new Quaternion(30.0f, 90.0f, 90.0f, 1.0f), ParentClone.transform);
                        EndStar.transform.GetChild(0).GetComponent<MeshRenderer>().enabled = true;
                        Instances.Add(EndStar);
                    }

                }
            }
        }
        PlateformeClone.SetActive(false);
        StarClone.SetActive(false);
    }

    public void GenerateWall()
    {
        //Delimitation de la zone de jeu
        Distance01 = Vector3.Distance(Limites[0].transform.position, Limites[1].transform.position);
        Distance02 = Vector3.Distance(Limites[0].transform.position, Limites[2].transform.position);
        Distance03 = Vector3.Distance(Limites[0].transform.position, Limites[3].transform.position);

        height_0 = level.LevelList[LevelSelector].GetLength(0);
        height_1 = level.LevelList[LevelSelector].GetLength(1);
        height_2 = level.LevelList[LevelSelector].GetLength(2);

        Debug.Log("Generation spirit ");
        // Generate Spirit Wall
        CubeClone.SetActive(true);
        for (int i = 0; i < height_0; i++)
        {
            //axe y
            for (int j = 0; j < height_1; j++)
            {
                //axe z
                for (int k = 0; k < height_2; k++)
                {
                    // axe x

                    float y = Limites[0].transform.position.y + (i * (Distance03 / (height_0 - 1)));
                    float z = Limites[0].transform.position.z + (j * (Distance02 / (height_1 - 1)));
                    float x = Limites[0].transform.position.x + (k * (Distance01 / (height_2 - 1)));

                    
                    if (array3D[i, j, k] == 0)
                    {
                        // Sinon on peut mettre un cube
                        GameObject NormalCube = Instantiate(CubeClone, new Vector3(x, y + 0.05f, z), Quaternion.identity , ParentClone.transform);
                            //Le scale auto des murs, c'est pas encore ça
                        //NormalCube.transform.localScale = new Vector3(NormalCube.transform.localScale.x, NormalCube.transform.localScale.y / height_1 , NormalCube.transform.localScale.z);
                        Instances.Add(NormalCube);
                    }

                    if (array3D[i, j, k] == 5)
                    {
                        // Sinon on peut mettre un cube
                        GameObject Normal90Cube = Instantiate(CubeClone, new Vector3(x, y + 0.05f, z), Quaternion.identity, ParentClone.transform);
                        Normal90Cube.transform.Rotate(new Vector3(0.0f, 90.0f, 0.0f));
                        Instances.Add(Normal90Cube);
                    }

                    if (array3D[i, j, k] == 6)
                    {
                        // Sinon on peut mettre un cube
                        GameObject PenteCube = Instantiate(PenteClone, new Vector3(x, y, z), Quaternion.identity, ParentClone.transform);
                        //Le scale auto des murs, c'est pas encore ça
                        //PenteCube.transform.localScale = new Vector3(PenteCube.transform.localScale.x, PenteCube.transform.localScale.y / height_1, PenteCube.transform.localScale.z);
                        Instances.Add(PenteCube);
                    }

                    if (array3D[i, j, k] != 1)
                    {
                        // On doit mettre un sphere
                        GameObject NormalSphere = Instantiate(SphereClone, new Vector3(x, y, z), Quaternion.identity, ParentClone.transform);
                        //NormalSphere.GetComponent<Renderer>().material.color = Color.yellow;
                        Instances.Add(NormalSphere);
                    }
                    
                }
            }
        }

        CubeClone.SetActive(false);
        PenteClone.SetActive(false);
    }

    public void SetPositionOtter(List<GameObject> Instance, GameObject Otter )
    {
        foreach(GameObject obj in Instances)
        {
            if(obj.CompareTag("Start"))
            {
                Debug.Log("Set start position of otter");
                //Position Star Otter
                Otter.transform.position = obj.transform.position;
            }
        }
    }

}
