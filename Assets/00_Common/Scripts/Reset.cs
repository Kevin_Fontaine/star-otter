﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class Reset : MonoBehaviour
{

    public GameObject Cubes;
    public List<Vector3> Savepos;
    public List<Vector3> Saverot;
    public int ResetLenght;


    // Start is called before the first frame update
    void Start()
    {
        Cubes = GameObject.FindGameObjectWithTag("Keys");
        Debug.Log("ChildCount Cube 2 : " + Cubes.transform.childCount);

        ResetLenght = Cubes.transform.childCount;

        for(int i = 0; i < ResetLenght; i++)
        {
            Savepos.Add(Cubes.transform.GetChild(i).gameObject.transform.localPosition);
            Saverot.Add(Cubes.transform.GetChild(i).gameObject.transform.localEulerAngles);

        }



    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(!other.CompareTag("Item"))
        {
            for (int i = 0; i < ResetLenght; i++)
            {
                Cubes.transform.GetChild(i).localPosition = Savepos[i];
                Cubes.transform.GetChild(i).localEulerAngles = Saverot[i];
            }
        }
    }
}
