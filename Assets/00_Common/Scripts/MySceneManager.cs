﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using System.IO;
using System;
using UnityEngine.UI;
using Emerge.Core;

public class MySceneManager : MonoBehaviour {
	
	//SINGLETON
	private static MySceneManager _instance;
	public static MySceneManager Instance
	{
		get
		{
			if (_instance == null) 
				_instance = FindObjectOfType<MySceneManager>();
			 return _instance;
		}
	}

    [Tooltip("Are we using Leap or Quest ?")]
    public bool isOculusQuest = false;

	public GameObject pcCam;
	public GameObject ovrCamRig;
	public List<IDroppable> droppables = new List<IDroppable>();

    void Awake()
    {
        _instance = this;
		ovrCamRig = GameObject.Find("OVRCameraRig");
		GameObject[] targets = GameObject.FindGameObjectsWithTag("Target");
		foreach (GameObject go in targets)
        {
			IDroppable droppable = go.GetComponent(typeof(IDroppable)) as IDroppable;
			if (droppable != null)
				droppables.Add(droppable);
        }
    }

    void Start () {

		TactileConfig.Instance.TargetPlatform = isOculusQuest ? Platforms.Oculus_Quest : Platforms.PC_Mac_Linux;
		TactileConfig.Instance.AnchoringMethod = isOculusQuest ? Anchoring.OculusControllers : Anchoring.None;
		TactileConfig.Instance.EditorHandTrackingSource = isOculusQuest ? HandHardware.Oculus : HandHardware.LeapMotion;
		TactileConfig.Instance.EditorAnchoringMethod = isOculusQuest ? Anchoring.OculusControllers : Anchoring.None;

		pcCam.SetActive(!isOculusQuest);
		ovrCamRig.SetActive(isOculusQuest);
	}

	public void NotifyDrop()
    {
		foreach (IDroppable droppable in droppables)
			droppable.OnDrop();
    }

	public void NotifyRightHandPinched()
    {

	}

	public void NotifyLeftHandPinched()
	{

	}

	public void NotifyRightHandReleased()
    {
	}

	public void NotifyLeftHandReleased()
	{

	}
}
