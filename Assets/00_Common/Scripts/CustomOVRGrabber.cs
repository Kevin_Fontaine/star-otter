﻿using OVRTouchSample;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomOVRGrabber : OVRGrabber
{
    float pinchStrength = 0;
    OVRHand ovrHand;
    [SerializeField]
    private OVRHand.Hand m_handtype;

    // Start is called before the first frame update
    new void Start()
    {
        base.Start();
        ovrHand = GetComponent<OVRHand>();
    }

    // Update is called once per frame
    new void Update()
    {
        base.Update();
        
        if (ovrHand)
            pinchStrength = ovrHand.GetFingerPinchStrength(OVRHand.HandFinger.Thumb);

        if (pinchStrength > grabBegin)
        {
            GrabBegin();
        }
        else if (pinchStrength < grabEnd)
        {
            GrabEnd();
        }
    }
}
