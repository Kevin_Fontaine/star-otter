﻿using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleDraw : MonoBehaviour
{

    [Range(0, 0.5f)]
    public float radius;

    [Range(0, 32.0f)]
    public int resolution;

    private Vector3 centerPos;
    private GameObject pos;
    public Transform parent;
    private List<GameObject> Clones;

    // Start is called before the first frame update
    void Start()
    {
        pos = new GameObject
        {
            name = "CircleCenter"
        };
        pos.transform.position = new Vector3(0.0f, 0.175f, 0.0f);
        Clones = new List<GameObject>();
        InstantiateCircle();

    }

    void InstantiateCircle()
    {

        float angle = 360f / (float)resolution;
        for (int i = 0; i < resolution; i++)
        {
            Quaternion rotation = Quaternion.AngleAxis(i * angle, Vector3.up);
            Vector3 direction = rotation * Vector3.forward;

            centerPos = pos.transform.position;
            Vector3 position = centerPos + (direction * radius);
            GameObject clone = Instantiate(pos, position, rotation, parent);
            Clones.Add(clone);
        }
    }
}
