﻿using Emerge.Core;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Labyrinth : MonoBehaviour
{

    public GameObject EmergeSphere;
    public GameObject Player;

    public Color Neutral;
    public Color Bad;
    public Color Fin;

    [Range(0, 1.0f)]
    public float TimeBeforeDeath = 0.0f;

    public float Timer = 0.0f;
    public bool death = false;
    // Start is called before the first frame update
    void Start()
    {
        GameObject Maze = GameObject.Find("Maze");
        for (int i = 0; i < Maze.transform.childCount; i++)
        {
            Maze.transform.GetChild(i).GetComponent<MeshRenderer>().enabled = false;
        }


        Player.transform.GetComponent<Renderer>().material.color = Neutral;
        EmergeSphere = GameObject.Find("EmergeMeshObject");
        transform.position = new Vector3(EmergeSphere.transform.position.x, .015f, EmergeSphere.transform.position.z);
        EmergeSphere.GetComponent<TactileMesh>().Strength = 10;
    }

    // Update is called once per frame
    void Update()
    {
        if(death)
        {
            Timer += Time.deltaTime;
        }

        if (Timer > TimeBeforeDeath)
        {
            // Kill player
            KillPlayer();
        }

        Vector3 pos = new Vector3(EmergeSphere.transform.position.x, .015f, EmergeSphere.transform.position.z);
        GetComponent<Rigidbody>().MovePosition(pos);
        EmergeSphere.transform.rotation = new Quaternion(0.0f, 0.0f, 0.0f, 0.0f);
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Trigger enter :" + other);
        if(other.transform.parent.name == "Maze")
        {
            // Collision avec les murs 
            death = true;

            Debug.Log("Object Detected" + other.gameObject.name);
            Player.transform.GetComponent<Renderer>().material.color = Bad;
            EmergeSphere.GetComponent<TactileMesh>().Strength = 40;
        }

        if(other.name == "End")
        {
            EmergeSphere.SetActive(false);
            Player.transform.GetComponent<Renderer>().material.color = Fin;
            GameObject Maze = GameObject.Find("Maze");
            for(int i = 0; i < Maze.transform.childCount; i++)
            {
                Maze.transform.GetChild(i).GetComponent<MeshRenderer>().enabled = true;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.transform.parent.name == "Maze")
        {
            death = false;
            EmergeSphere.GetComponent<TactileMesh>().Strength = 10;
        }
        Player.transform.GetComponent<Renderer>().material.color = Neutral;

    }

    public void KillPlayer()
    {

        GetComponent<MeshRenderer>().enabled = false;
        GetComponent<SphereCollider>().enabled = false;
        EmergeSphere.GetComponent<BoxCollider>().enabled = false;
        EmergeSphere.GetComponent<CustomOVRGrabbable>().enabled = false;
        EmergeSphere.SetActive(false);
       
        EmergeSphere.transform.localPosition = new Vector3(0.0f, 0.0f, 0.0f);

        death = false;
        EmergeSphere.GetComponent<BoxCollider>().enabled = true;
        EmergeSphere.GetComponent<CustomOVRGrabbable>().enabled = true;
        EmergeSphere.SetActive(true);
        GetComponent<MeshRenderer>().enabled = true;
        GetComponent<SphereCollider>().enabled = true;
        Timer = 0.0f;

    }



}
