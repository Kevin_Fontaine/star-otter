﻿using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Keyhole : MonoBehaviour
{
    public Color Good;
    public Color Bad;
    public Color Neutral;

    [Range(0, 10.0f)]
    public float WaitFor;

    private IEnumerator couroutine;

    public int StartValue;

    public GameObject[] Cubes;
    public GameObject[] Logo;

    public bool Reponse = false;

    private void Start()
    {
        this.gameObject.transform.GetComponent<Renderer>().material.color = Neutral;
        Cubes = GameObject.FindGameObjectsWithTag("Keys");
        Logo = GameObject.FindGameObjectsWithTag("Logo");

        //Debug.Log("Cubes " + Cubes.Length);
        //Debug.Log("Logo " + Logo.Length);
        
        for(int i = 0; i < Cubes[0].transform.childCount; i++)
        {
            Cubes[0].transform.GetChild(i).gameObject.tag = "Bad_Item";
        }
        
        for (int i = 0; i < Logo[0].transform.childCount; i++)
        {
            Logo[0].transform.GetChild(i).gameObject.SetActive(false);
        }

        StartValue = (int)Random.Range(0.0f, Cubes[0].transform.childCount);

        Cubes[0].transform.GetChild(StartValue).gameObject.tag = "Item";
        Logo[0].transform.GetChild(StartValue).gameObject.SetActive(true);

        Debug.Log("ChildCount Cube: " + Cubes[0].transform.childCount);
    }

    private void Update()
    {
        for (int i = 0; i < Cubes[0].transform.childCount; i++)
        {
            if(Cubes[0].transform.GetChild(i).transform.position.y < -30f)
            {
                Cubes[0].transform.GetChild(i).transform.position = Logo[0].transform.GetChild(StartValue).gameObject.transform.position;
                Cubes[0].transform.GetChild(i).GetComponent<Rigidbody>().velocity = new Vector3(0.0f, 0.0f, 0.0f); 
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Object detected: " + other);
        if(other.gameObject.CompareTag("Item"))
        {
            int OldValue = StartValue;
            Debug.Log("The key is here");
            GameObject.Find("Bucket").gameObject.transform.GetComponent<Renderer>().material.color = Good;
            other.gameObject.tag = "Bad_Item";
            Cubes[0].transform.GetChild(StartValue).transform.position = Logo[0].transform.GetChild(StartValue).gameObject.transform.position;
            Logo[0].transform.GetChild(StartValue).gameObject.SetActive(false);

            do
            {
                StartValue = (int)Random.Range(0.0f, Cubes[0].transform.childCount);
            } while (StartValue == OldValue);


            Logo[0].transform.GetChild(StartValue).gameObject.SetActive(true);
            Cubes[0].transform.GetChild(StartValue).gameObject.tag = "Item";

            Reponse = true;

            couroutine = Bravo(WaitFor);
            StartCoroutine(couroutine);
        }
        /*
        else
        {
            if(other.gameObject.CompareTag("Bad_Item"))
            {
                GameObject.Find("Bucket").gameObject.transform.GetComponent<Renderer>().material.color = Bad;
                Reponse = false;
                couroutine = Bravo(WaitFor);
                StartCoroutine(couroutine);
            }
            
        }*/
    }
    /*
    private void OnTriggerExit(Collider other)
    {
        Debug.Log(other + "exit");
        GameObject.Find("Bucket").gameObject.transform.GetComponent<Renderer>().material.color = Neutral;
    }
    */

    IEnumerator Bravo(float waitfor)
    {
        

        yield return new WaitForSeconds(waitfor);
        
        GameObject.Find("Bucket").gameObject.transform.GetComponent<Renderer>().material.color = Neutral;
        Reponse = false;

    }



}
