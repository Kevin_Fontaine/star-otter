﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;
using UnityEngine.UIElements;

public class RandomDraw : MonoBehaviour
{
    public int resolution;
    public Transform parent;
    private GameObject pos;

    // Start is called before the first frame update
    void Start()
    {
        pos = new GameObject
        {
            name = "RandomName"
        };
        pos.transform.position = new Vector3(0.0f, 0.125f, 0.0f);
        InstantiateCircle();

    }

    void InstantiateCircle()
    {

        float angle = 360f / (float)resolution;
        for (int i = 0; i < resolution; i++)
        {
            Quaternion rotation = Quaternion.AngleAxis(i * angle, Vector3.up);

            float x = Random.Range(-0.125f, 0.125f);
            float y = Random.Range(0.1f, 0.2f);
            float z = Random.Range(-0.125f, 0.125f);

            Vector3 position = new Vector3(x, y, z);
            Instantiate(pos, position, rotation, parent);
        }
    }
}
