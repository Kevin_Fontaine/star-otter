﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecognitionForm : MonoBehaviour
{

    public GameObject[] Cubes;
    public GameObject[] Logos;

    // Start is called before the first frame update
    void Start()
    {
        Cubes = GameObject.FindGameObjectsWithTag("Cubes");
        Logos = GameObject.FindGameObjectsWithTag("Logo");
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
