﻿using System.Collections.Generic;
using Emerge.Core;
using UnityEngine;
using DG.Tweening;

public class Draw : MonoBehaviour
{
    // Start is called before the first frame update

    [Range(0, 10.0f)]
    public float speed;

    private int current;

    [Range(0, 10.0f)]
    public float damping;

    public GameObject[] PathToFollow;
    public List<Transform> autopath;

    private void Start()
    {
        int RandomDraw = PathToFollow.Length;
        int randomnumber = Random.Range(0, RandomDraw);
        ChildCount(PathToFollow[randomnumber]);
        // To Do : Start the brush at the first point location
        Debug.Log("Selected Element: " + randomnumber);
    }

    // Update is called once per frame
    void Update()
    {

        if (transform.position != autopath[current].position)
        {
            Vector3 pos = Vector3.MoveTowards(transform.position, autopath[current].position, speed * Time.deltaTime);
            GetComponent<Rigidbody>().MovePosition(pos);
            var rotation = Quaternion.LookRotation(autopath[current].position - transform.position);
            transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * damping);

        }
        else current = (current + 1) % autopath.Count;
    }

    void ChildCount(GameObject pathtofollow)
    {
        int children = pathtofollow.transform.childCount;
        for (int i = 0; i < children; i++)
        {
            autopath.Add(pathtofollow.transform.GetChild(i).transform);
        }

    }
}
