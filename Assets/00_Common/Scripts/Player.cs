﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Player : MonoBehaviour
{

    public GameObject Bucket;
    public Keyhole keyhole;
    public int var;
    public float speed;
    public GameObject Obstacle;

    private IEnumerator couroutine;

    // Start is called before the first frame update
    void Start()
    {
        var = 0;
        Bucket = GameObject.Find("Bucket");

        Obstacle = GameObject.Find("Obstacle");

        couroutine = PlayerPause();
    }


    // Update is called once per frame
    void Update()
    {
        //Debug.Log("value of reponse: " + keyhole.Reponse);
        if(keyhole.Reponse)
        {
            StartCoroutine(couroutine);
            


            /*
            Vector3 pos = Vector3.MoveTowards(transform.position, Obstacle.transform.GetChild(var).position, speed * Time.deltaTime);
            GetComponent<Rigidbody>().MovePosition(pos);
            var += 1;
            
            */
        }
        

        
    }

    IEnumerator PlayerPause()
    {
        string mur = "Mur " + var;
        Obstacle.transform.GetChild(var).GetComponent<DOTweenPath>().GetTween().Play();
        var += 1;
        Debug.Log("Le mur " + mur + "est levé");

        //DOTween.Play(mur);

        Tween tween = this.GetComponent<DOTweenPath>().GetTween();
        tween.OnWaypointChange(WaypointChangeCallback);
        //tween.GotoWaypoint(var);
        tween.Goto(var);
        Debug.Log("Player reponse avant" + keyhole.Reponse);
        //yield return new WaitForSeconds(keyhole.WaitFor);
        //this.GetComponent<DOTweenPath>().GetTween().Pause();
        //keyhole.Reponse = false;
        Debug.Log("Player reponse apres" + keyhole.Reponse);
        yield return null; 
    }

    private void WaypointChangeCallback(int waypointIndex)
    {
        Debug.Log("Waypoint index changed to " + waypointIndex);
        this.GetComponent<DOTweenPath>().GetTween().Pause();
    }


}
