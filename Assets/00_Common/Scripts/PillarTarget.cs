﻿using DG.Tweening;
using Emerge.Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PillarTarget : MonoBehaviour , IDroppable
{
    private BoxCollider col;
    private MeshRenderer mr;
    private TactileMesh tm;
    private CustomOVRGrabbable grabbable;
    private float pillarHeight;
    private bool isItemInside = false;
    private float score = 0;
    public bool isShaking = false; 

    // Start is called before the first frame update
    void Start()
    {
        col = GetComponent<BoxCollider>();
        mr = GetComponent<MeshRenderer>();
        mr.enabled = false;

        if (isShaking)
            this.transform.DOShakeScale(1000, Vector3.up * 0.15f, 1, 10, true);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Item")
        {
            isItemInside = true;
            tm = other.GetComponent<TactileMesh>();
            grabbable = other.GetComponent<CustomOVRGrabbable>();
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Item")
        {
            isItemInside = true;
            if (tm)
            {
                pillarHeight = transform.localScale.y;
                float otherRelativeAltitude =  other.transform.position.y - this.transform.position.y;
                if (otherRelativeAltitude < 0 || otherRelativeAltitude > pillarHeight)  return;

                //Strength max is reached when at top
                tm.Strength = score = 100 * (otherRelativeAltitude / pillarHeight);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Item")
        {
            isItemInside = false;
            if (tm)
            {
                tm.Strength = 0;
            }
        }
    }

    private void StopAnimation()
    {
        DOTween.Kill(this.transform);
    }

    public void OnDrop()
    {
        if (!isItemInside) return;

        Debug.Log("Ondropped !");

        if (score > 75)
        {
            StopAnimation();
            mr.enabled = true;
            mr.material.color = Color.red;
        }
    }
}
