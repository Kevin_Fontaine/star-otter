﻿using UnityEngine;
using UnityEditor;


/// <summary>
/// Convert from the deprecated PrefabType in Unity < 2018.3 (new nested prefab system)
/// 
/// </summary>
/// 

public enum OldUnityPrefabType
{

    DisconnectedModelPrefabInstance,
    //The object is an instance of an imported 3D model, but the connection is broken.

    DisconnectedPrefabInstance,
    //The object is an instance of a user created Prefab, but the connection is broken.

    MissingPrefabInstance,
    //The object was an instance of a Prefab, but the original Prefab could not be found.

    ModelPrefab,
    //The object is an imported 3D model asset.

    ModelPrefabInstance,
    //The object is an instance of an imported 3D model.

    None,
    //The object is not a Prefab nor an instance of a Prefab.

    Prefab,
    //The object is a user created Prefab asset.

    PrefabInstance
    //The object is an instance of a user created Prefab.

}

public static class DeprecatedPrefabTypeConversion
{
    public static bool IsPrefabType(OldUnityPrefabType t, Object o)
    {
        if (t == OldUnityPrefabType.None)
        {
            return !IsPrefab(o) && !IsInstance(o);
        }

        if (t == OldUnityPrefabType.Prefab)
        {
            return IsPrefab(o) && !IsInstance(o);
        }

        if (t == OldUnityPrefabType.PrefabInstance)
        {
            return IsPrefab(o) && IsInstance(o);
        }

        if (t == OldUnityPrefabType.ModelPrefab)
        {
            return IsModel(o) && !IsInstance(o);
        }

        if (t == OldUnityPrefabType.ModelPrefabInstance)
        {
            return IsModel(o) && IsInstance(o);
        }


        //---missing and disconnected
        if (t == OldUnityPrefabType.MissingPrefabInstance)
        {
            return IsPrefabMissing(o) | IsMissing(o);
        }

        if (t == OldUnityPrefabType.DisconnectedModelPrefabInstance)
        {
            return IsModel(o) && IsDisconnected(o);
        }

        if (t == OldUnityPrefabType.DisconnectedPrefabInstance)
        {
            return IsPrefab(o) && IsInstance(o) && IsDisconnected(o);
        }

        else return false;
    }







    public static bool IsRegularPrefab(Object o)
    {
        return IsPrefab(o) && IsInstance(o);
    }


    public static bool IsPrefabAndIsInstantiated(Object o)
    {
        return IsPrefab(o) && !IsInstance(o);
    }

    //====================================================================================
    private static bool IsPrefab(Object o)
    {
#if !UNITY_WSA && UNITY_EDITOR
        return (PrefabUtility.GetPrefabAssetType(o) != PrefabAssetType.NotAPrefab);
#else
        return false;
#endif
    }

    private static bool IsPrefabInstance(Object o)
    {
#if !UNITY_WSA && UNITY_EDITOR
        return (PrefabUtility.GetPrefabAssetType(o) != PrefabAssetType.NotAPrefab);
#else
        return false;
#endif
    }

    private static bool IsModel(Object o)
    {
#if !UNITY_WSA && UNITY_EDITOR
        return (PrefabUtility.GetPrefabAssetType(o) == PrefabAssetType.Model);
#else
        return false;
#endif
    }

    private static bool IsInstance(Object o)
    {
#if !UNITY_WSA && UNITY_EDITOR
        return (PrefabUtility.GetPrefabInstanceStatus(o) != PrefabInstanceStatus.NotAPrefab);
#else
        return false;
#endif
    }

    //missing + disconnected
    private static bool IsPrefabMissing(Object o)
    {
#if !UNITY_WSA && UNITY_EDITOR
        return (PrefabUtility.GetPrefabAssetType(o) == PrefabAssetType.MissingAsset);
#else
        return false;
#endif
    }

    private static bool IsMissing(Object o)
    {
#if !UNITY_WSA && UNITY_EDITOR
        return (PrefabUtility.GetPrefabInstanceStatus(o) == PrefabInstanceStatus.MissingAsset);
#else
        return false;
#endif
    }

    private static bool IsDisconnected(Object o)
    {
#if !UNITY_WSA && UNITY_EDITOR
        return (PrefabUtility.GetPrefabInstanceStatus(o) == PrefabInstanceStatus.Disconnected);
#else
        return false;
#endif
    }
}

