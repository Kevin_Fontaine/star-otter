﻿/******************************************************************************
 * Copyright (C) Emerge Now, Inc. 2016-2019.                                  *
 * Emerge Now Inc proprietary and confidential.                               *
 *                                                                            *
 * Use subject to the terms of the Emerge Exploration Program Agreement       *
 * and/or terms of limited, non-exclusive, non-sublicensable license to       *
 * Emerge SDK between Emerge and you, your company or any other organization. *
 ******************************************************************************/

using UnityEngine;

namespace Emerge.Core
{
    public class SeatManager : Singleton<SeatManager>
    {
#if !UNITY_WSA
        private Transform interactionObj_root = null;
        // Use this for initialization
          

        void Awake()
        {
            Debug.Log("Seat Manager Awake called"); 
        }

        void Start()
        {
            interactionObj_root = TactileCore.Instance.GetInteractableObjectRoot()?.transform;
            if (interactionObj_root != null)
            {
                if (TactileConfig.Instance.SeatNumber == 0)
                {
                    interactionObj_root.eulerAngles = new Vector3(0.0f, 0.0f, 0.0f);
                    //remoteHands.transform.eulerAngles = new Vector3(0.0f, 0.0f, 0.0f);
                }
                else if (TactileConfig.Instance.SeatNumber == 1)
                {
                    interactionObj_root.eulerAngles = new Vector3(0.0f, 180.0f, 0.0f);
                    //remoteHands.transform.eulerAngles = new Vector3(0.0f, 180.0f, 0.0f);
                }
            }
        }

        // Update is called once per frame
        void Update()
        {

        }
#endif

    }
}
