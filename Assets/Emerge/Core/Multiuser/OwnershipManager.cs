﻿/******************************************************************************
 * Copyright (C) Emerge Now, Inc. 2016-2019.                                  *
 * Emerge Now Inc proprietary and confidential.                               *
 *                                                                            *
 * Use subject to the terms of the Emerge Exploration Program Agreement       *
 * and/or terms of limited, non-exclusive, non-sublicensable license to       *
 * Emerge SDK between Emerge and you, your company or any other organization. *
 ******************************************************************************/

using UnityEngine;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;

namespace Emerge.Communication
{
    using Core;

    /// <summary>
    /// Transfer ownership in an Emerge multi user setup
    /// This class acts like a Singleton but does not extend the Emerge.Core.Singleton class 
    /// because it inherits from a base class already.
    /// </summary>
    public class OwnershipManager : MonoBehaviourPunCallbacks, IPunOwnershipCallbacks
    {
        /// <summary>
        /// True if the current player is the owner.
        /// </summary>
        public bool IsOwner { get; set; } = false;

        public static OwnershipManager Instance;

        private void Awake()
        {
            Instance = this;
        }

        private void RegisterIEBallInteractions(bool b)
        {
            foreach (AbstractTactileBehavior interactionObjectContoller in TactileManager.Instance.EmergeObjects)
            {
                if(interactionObjectContoller.InteractionBehaviour == null)
                {
                    continue;
                } 
                if (b)
                {
                    Leap.Unity.Interaction.InteractionManager.instance.RegisterInteractionBehaviour(interactionObjectContoller.InteractionBehaviour);
                }
                else
                {
                    Leap.Unity.Interaction.InteractionManager.instance.UnregisterInteractionBehaviour(interactionObjectContoller.InteractionBehaviour);
                }
            }
        }

        /// <summary>
        /// Call this function to have your user take control of an EmergeObject. 
        /// This means your user will broadcast position data and other connected users will receive it and are unable to control the object. Connected players will still receive tactile sensations for that object.
        /// </summary>
        public void TakeOwnership()
        {
            List<AbstractTactileBehavior> emergeObjects = TactileManager.Instance.EmergeObjects;
            foreach (AbstractTactileBehavior eo in emergeObjects)
            {
                if (!eo.NetworkObjectMultiUserComponent.photonView.IsOwnerActive || eo.NetworkObjectMultiUserComponent.photonView.Owner.ActorNumber != PhotonNetwork.LocalPlayer.ActorNumber)
                {
#if !UNITY_WSA
                    //Debug.Log("Take ownership of network ball");
                    eo.NetworkObjectMultiUserComponent.RequestOwnership(PhotonNetwork.LocalPlayer.ActorNumber);
#endif
                }
            }
        }

        #region PUN
        public bool TransferOwnershipOnRequest = true;
        public void OnOwnershipRequest(PhotonView view, Player requestingPlayer)
        {
            Debug.Log("OnOwnershipRequest(): Player " + requestingPlayer + " requests ownership of: " + view + ".");
            if (this.TransferOwnershipOnRequest)
            {
                view.TransferOwnership(requestingPlayer.ActorNumber);
            }
        }

        public void OnOwnershipTransfered(PhotonView view, Player oldOwner)
        {
            Player newOwner = view.Owner;
            Debug.Log("OnOwnershipTransfered for PhotonView" + view.ToString() + " from " + oldOwner + " to " + newOwner);
            if (newOwner.ActorNumber == PhotonNetwork.LocalPlayer.ActorNumber)
            {
                IsOwner = true;
                RegisterIEBallInteractions(true);
            }
            else
            {
                IsOwner = false;
                RegisterIEBallInteractions(false);
            }
        }
        #endregion
    }
}