﻿/******************************************************************************
 * Copyright (C) Emerge Now, Inc. 2016-2019.                                  *
 * Emerge Now Inc proprietary and confidential.                               *
 *                                                                            *
 * Use subject to the terms of the Emerge Exploration Program Agreement       *
 * and/or terms of limited, non-exclusive, non-sublicensable license to       *
 * Emerge SDK between Emerge and you, your company or any other organization. *
 ******************************************************************************/
using System;
using UnityEngine;
using System.Collections;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;


namespace Emerge.Communication
{
    using Core;
    /// <summary>
    /// This script automatically connects to Photon (using the settings file),
    /// tries to join a random room and creates one if none was found (which is ok).
    /// </summary>
    public class PhotonConnection : MonoBehaviourPunCallbacks
    {
        /// <summary>Connect automatically? If false you can set this to true later on or call ConnectUsingSettings in your own scripts.</summary>
        public bool AutoConnect = true;

        public byte Version = 1;

        /// <summary>if we don't want to connect in Start(), we have to "remember" if we called ConnectUsingSettings()</summary>
        private bool ConnectInUpdate = true;


        public virtual void Start() { }

        public virtual void Update()
        {
            if (ConnectInUpdate && AutoConnect && !PhotonNetwork.IsConnected)
            {
                Debug.Log("Update() was called by Unity. Scene is loaded. Let's connect to the Photon Master Server. Calling: PhotonNetwork.ConnectUsingSettings();");
                
                ConnectInUpdate = false;

                // Clients must set a unique user ID when using On Premise Server
                if (PhotonNetwork.AuthValues == null)
                {
                    PhotonNetwork.AuthValues = new AuthenticationValues(Guid.NewGuid().ToString());
                }
                else if (string.IsNullOrEmpty(PhotonNetwork.AuthValues.UserId))
                {
                    PhotonNetwork.AuthValues.UserId = Guid.NewGuid().ToString();
                }

                // On Premise Servers don't support the default GpBinaryV18 yet
                PhotonNetwork.NetworkingClient.LoadBalancingPeer.SerializationProtocolType = SerializationProtocol.GpBinaryV16;
                PhotonNetwork.ConnectToMaster(TactileConfig.Instance.PhotonServerAddress, TactileConfig.Instance.PhotonPort, TactileConfig.Instance.PhotonAppId);

                PhotonNetwork.NickName = "RelayApplication" + "_" + UnityEngine.Random.Range(0, 9999);
            }
        }


        #region PUN Overrides

        public override void OnConnectedToMaster()
        {
            Debug.Log("OnConnectedToMaster() was called by PUN. Now this client is connected and could join a room. Calling: PhotonNetwork.JoinRandomRoom();");
            PhotonNetwork.JoinRandomRoom();
        }

        public override void OnJoinedLobby()
        {
            Debug.Log("OnJoinedLobby(). This client is connected and does get a room-list, which gets stored as PhotonNetwork.GetRoomList(). This script now calls: PhotonNetwork.JoinRandomRoom();");
            PhotonNetwork.JoinRandomRoom();
        }

        public override void OnJoinRandomFailed(short returnCode, string message)
        {
            Debug.Log("OnPhotonRandomJoinFailed() was called by PUN. No random room available, so we create one. Calling: PhotonNetwork.CreateRoom(null, new RoomOptions() {maxPlayers = 4}, null);");
            PhotonNetwork.CreateRoom(null, new RoomOptions() { MaxPlayers = 4 }, null);
        }

        // the following methods are implemented to give you some context. re-implement them as needed.

        public override void OnDisconnected(DisconnectCause cause)
        {
            Debug.LogWarning("Photon Connection disconnected. Cause: " + cause + ". Please make sure photon server is running!");
        }

        public override void OnJoinedRoom()
        {
            Debug.Log("OnJoinedRoom() called by PUN. Now this client is in a room. From here on, your game would be running. For reference, all callbacks are listed in enum: PhotonNetworkingMessage");
        }

        #endregion
    }
}