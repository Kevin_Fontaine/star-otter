﻿/******************************************************************************
 * Copyright (C) Emerge Now, Inc. 2016-2019.                                  *
 * Emerge Now Inc proprietary and confidential.                               *
 *                                                                            *
 * Use subject to the terms of the Emerge Exploration Program Agreement       *
 * and/or terms of limited, non-exclusive, non-sublicensable license to       *
 * Emerge SDK between Emerge and you, your company or any other organization. *
 ******************************************************************************/
using System;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

namespace Emerge.Core
{

    public class PhotonRoomManager : MonoBehaviourPunCallbacks
    {
        //players
        public GameObject playerPrefab;

        public Action WeJoinedTheRoom;
        public Action OnPlayerConnected;
        public Action OnPlayerDisconnected;


        //Photon Messages    ======================================================================================

        /// <summary>
        /// Called when a Photon Player is connected.  
        /// </summary>
        /// <param name="other">Other.</param>
        public override void OnPlayerEnteredRoom(Player other)
        {
            Debug.Log("OnPhotonPlayerConnected() " + other.NickName); // not seen if you're the player connecting

            OnPlayerConnected.Dispatch();
        }

        /// <summary>
        /// Called when a Photon Player is disconnected.  
        /// </summary>
        /// <param name="other">Other.</param>
        public override void OnPlayerLeftRoom(Player other)
        {
            Debug.Log("OnPhotonPlayerDisconnected() " + other.NickName); // seen when other disconnects

            OnPlayerDisconnected.Dispatch();
        }


        public override void OnJoinedRoom()
        {
            byte hands_group = (byte)1;
            Debug.Log("group = " + hands_group);

            PhotonNetwork.SetInterestGroups(hands_group, true);
            PhotonNetwork.Instantiate(this.playerPrefab.name, new Vector3(0f, 0f, 0f), Quaternion.identity, hands_group);

            WeJoinedTheRoom.Dispatch();

        }


    }

}