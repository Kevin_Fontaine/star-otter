﻿/******************************************************************************
 * Copyright (C) Emerge Now, Inc. 2016-2019.                                  *
 * Emerge Now Inc proprietary and confidential.                               *
 *                                                                            *
 * Use subject to the terms of the Emerge Exploration Program Agreement       *
 * and/or terms of limited, non-exclusive, non-sublicensable license to       *
 * Emerge SDK between Emerge and you, your company or any other organization. *
 ******************************************************************************/
using UnityEngine;
using System;
using Photon.Pun;
using ExitGames.Client.Photon;

///---------------------------------------------------------------------------------
/// CLASS WILL BE DEPRECATED BECAUSE OF THE USE OF UNIVERSAL NETWORK OBJECT FOR HANDS
///---------------------------------------------------------------------------------

namespace Emerge.Communication
{
    using HandTracking;
    using Core;

    /// <summary>
    /// Leap network player is responsible for handing network sync for Avatar Hands
    /// </summary>
    public class LEAPNetworkPlayer : NetworkPlayer, IComparable<LEAPNetworkPlayer>, Tracking.IHandTrackingDataSource
    {

#if !UNITY_WSA
        //TODO: need to remove this for HL build
        //public BaseLeapConnector leapDataService;////////////
#endif

        //TODO: sync this over photonView? so we know which remote hand to send to Hololens?
        public Handedness currentHandUsed;
#if !UNITY_WSA
        private Handedness lastHandedness = Handedness.Right;
#endif

        public PuppetHand_riggedHand hand_LEFT;
        public PuppetHand_riggedHand hand_RIGHT;

        //photon views collection
        public PhotonView[] leftHandPhotonViews;
        public PhotonView[] rightHandPhotonViews;
        private ViewSynchronization originalViewSynchronization;

        public void Awake()
        {
            //if (NetworkPlayers.Exists)
            //{
            //    NetworkPlayers.Instance.AddLEAPPlayer(this);
            //}
            NetworkPlayers.Instance.AddLEAPPlayer(this);
             
            //this is a REMOTE Instance (because if we are a HL then all LEAPNetworkPlayers are remote)
            //if (TactileConfig.Instance.Mode == Mode.XRPlatforms)
            //{
            //    //on a Hololens, all remotely instantiated objects (LEAP hands) need to be
            //    //parented to the hologram root
            //    //this works because all passed positions use localPosition/Rotation etc. internally
            //    //
            //    transform.parent = GameObject.Find("LEAPspaceCenter").transform;

            //    transform.localPosition = Vector3.zero;
            //    transform.localRotation = Quaternion.identity;

            //    hand_LEFT.InitialSetup(false);
            //    hand_RIGHT.InitialSetup(false);

            //    return;
            //} 

            if (photonView.IsMine)
            {
                //local
                /////////////////AvatarManager.Instance.AddLocalNetworkAvatar(transform);

                //local hands
                hand_LEFT.InitialSetup(true);
                hand_RIGHT.InitialSetup(true);
            }
            else
            {
                //remote
                /////////////////AvatarManager.Instance.AddRemoteNetworkAvatar(transform);

                Transform newParent = new GameObject("RemoteHands").transform;
                newParent.position = Vector3.zero;
                newParent.rotation = Quaternion.identity;
                transform.parent = newParent;

                //TODO: removed offset here - needs to be tested in Headset
                transform.localPosition = Vector3.zero;
                transform.localRotation = Quaternion.Euler(new Vector3(0, 180, 0)); // TODO update here to support more than one remote player

                //this are remote hands
                hand_LEFT.InitialSetup(false);
                hand_RIGHT.InitialSetup(false);

                if (Tracking.HandTracking.HasDataSourceForId(EmergeHandDataSourceIds.REMOTE))
                {
                    // Right now we only support one set of remote hands at a time
                    Tracking.HandTracking.UnregisterDataSource(EmergeHandDataSourceIds.REMOTE);
                }

                Tracking.HandTracking.RegisterDataSource(EmergeHandDataSourceIds.REMOTE, this, false);

                //EmergeObjectRemoteHandInitializer remoteHandInitializer = FindObjectOfType<EmergeObjectRemoteHandInitializer>();
                //remoteHandInitializer.SetupHands(newParent, hand_LEFT, hand_RIGHT);
            }

            //default values
            currentHandUsed = Handedness.Right;

            //photon instantiate -should- set group interest for all children photonviews (fingers), but it doesnt, so do it here
            byte LEAPgroup = (byte)1;
            PhotonView[] allViews = transform.GetComponentsInChildren<PhotonView>();
            for (int i = 0; i < allViews.Length; i++)
            {
                allViews[i].Group = LEAPgroup;
            }

            //TODO - create list of PhotonViews for each hand
            leftHandPhotonViews = hand_LEFT.transform.GetComponentsInChildren<PhotonView>();
            rightHandPhotonViews = hand_RIGHT.transform.GetComponentsInChildren<PhotonView>();
            originalViewSynchronization = leftHandPhotonViews[0].Synchronization; //get the defualt/original setting, although it will likely never
                                                                                  //be anything but `Unreliable on change`
        }

        private void OnDestroy()
        {
            if (!NetworkPlayers.Instance.IsNullOrDestroyed())
            {
                ////////////////////AvatarManager.Instance.DestroyRemoteAvatar(transform);
                NetworkPlayers.Instance.RemoveLEAPPlayer(this);
            }
        }

        //sort by photon ID
        public int CompareTo(LEAPNetworkPlayer other)
        {
            return photonView.Owner.ActorNumber.CompareTo(other.photonView.Owner.ActorNumber);
        }

        public void RemoteHideHands()
        {
            hand_LEFT.RemoveFromSimulation();
            hand_RIGHT.RemoveFromSimulation();
        }

        public void RemoteShowHand()
        {
            hand_LEFT.AddToSimulation();
            hand_RIGHT.RemoveFromSimulation();
        }

        public PuppetHand_riggedHand[] GetHand()
        {
            return new PuppetHand_riggedHand[] { hand_LEFT, hand_RIGHT };
        }

        private void SwitchHands(Handedness h)
        {
            currentHandUsed = h;

            if (h == Handedness.Right)
            {
                hand_RIGHT.AddToSimulation();
                SetPhotonSync(Handedness.Right, true);

                //hand_LEFT.RemoveFromSimulation();
                //SetPhotonSync(Handedness.Left, false);
            }
            else
            {
                hand_LEFT.AddToSimulation();
                SetPhotonSync(Handedness.Left, true);

                //hand_RIGHT.RemoveFromSimulation();
                //SetPhotonSync(Handedness.Right, false);
            }
        }

        private void SetPhotonSync(Handedness h, bool b)
        {
            ViewSynchronization syncToSet;
            if (b)
                syncToSet = originalViewSynchronization;
            else
                syncToSet = ViewSynchronization.Off;

            if (h == Handedness.Left)
            {

                //leftHandPhotonViews
                for (int i = 0; i < leftHandPhotonViews.Length; i++)
                {
                    leftHandPhotonViews[i].Synchronization = syncToSet;
                }
            }
            else
            {

                //leftHandPhotonViews
                for (int i = 0; i < rightHandPhotonViews.Length; i++)
                {
                    rightHandPhotonViews[i].Synchronization = syncToSet;
                }
            }

        }





#if !UNITY_WSA
        bool oneHand = false;
        public void Update()
        {
            if (photonView.IsMine)
            {
                // Handle not tracking
                if (!Tracking.HandTracking.HasDefaultDataSource || 
                    !Tracking.HandTracking.Default.IsRunning || 
                    !Tracking.HandTracking.Default.HasData)
                {
                    lastHandedness = Handedness.Other;
                    return;
                }

                Tracking.HandSet handSet = Tracking.HandTracking.Default.MostRecentHandSet;
                hand_RIGHT.gameObject.SetActive(handSet.right.active);
                hand_LEFT.gameObject.SetActive(handSet.left.active);

                Handedness currentHandedness;
                if (handSet.left.active)
                {
                    currentHandedness = Handedness.Left;
                }
                else if (handSet.right.active)
                {
                    currentHandedness = Handedness.Right;
                }
                else
                {
                    lastHandedness = Handedness.Other;
                    return;
                }

                if (lastHandedness != currentHandedness)
                {
                    SwitchHands(currentHandedness);
                }

                if (oneHand)
                {
                    if (currentHandedness == Handedness.Right)
                    {
                        hand_RIGHT.UpdateHandState(handSet.right);
                    }
                    else
                    {
                        hand_LEFT.UpdateHandState(handSet.left);
                    }
                }
                else
                {
                    hand_RIGHT.UpdateHandState(handSet.right);
                    hand_LEFT.UpdateHandState(handSet.left);
                }

                lastHandedness = currentHandedness;
            }
            else if (handBuffer != null && hand_RIGHT != null && hand_LEFT != null)
            {
                handBuffer.Push((Tracking.HandSet handSetToUpdate) =>
                {
                    if (!hand_LEFT.PopulateHandData(handSetToUpdate.left) || 
                        !hand_RIGHT.PopulateHandData(handSetToUpdate.right))
                    {
                        Debug.LogError("LEAPNetworkPlayer: could not get Hand data from remote puppet hands");
                        return CircularReuseBuffer<Tracking.HandSet>.PushAction.CancelPush;
                    }
                    
                    return CircularReuseBuffer<Tracking.HandSet>.PushAction.FinishPush;
                });
            }
            /*
            if (photonView.IsMine)
            {

                ////Simulate hololens behaviour for testing on PC ------------------------------
                //if (EmergeConfig.Instance.isHololens) {
                //    HololensUpdate();
                //    return;
                //}
                ////-----------------------------------------------------------------------------

                if (leapDataService == null)
                {
                    leapDataService = GameObject.FindObjectOfType<BaseLeapConnector>();
                    if (leapDataService == null) return;
                }


                //DETECT IF NOT TRACKING
                if (leapDataService.CurrentHandData == null)
                {
                    lastHandedness = Handedness.Other;
                    return;
                }


                Handedness currentHandedness = leapDataService.CurrentHandData.handedness;


                if (lastHandedness != currentHandedness)
                {
                    SwitchHands(currentHandedness);
                }

                if (oneHand)
                {
                    if (currentHandedness == Handedness.Right)
                    {
                        hand_RIGHT.UpdateHandState(leapDataService.CurrentHandData, leapDataService.IsTracking);
                    }
                    else
                    {
                        hand_LEFT.UpdateHandState(leapDataService.CurrentHandData, leapDataService.IsTracking);
                    }
                }
                else
                {
                    hand_RIGHT.UpdateHandState(leapDataService.rightHandData, leapDataService.IsTracking);
                    hand_LEFT.UpdateHandState(leapDataService.leftHandData, leapDataService.IsTracking);
                }

                lastHandedness = currentHandedness;
            }
        */
        }
#endif




        public override void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
        {
            if (stream.IsWriting)
            {
                stream.SendNext((byte)this.currentHandUsed);
            }
            else
            {
                this.currentHandUsed = (Handedness)((byte)stream.ReceiveNext());
            }
        }


        //===================================================================
        // Photon Event registration
        //===================================================================
        public override void OnEnable()
        {
            base.OnEnable();
            PhotonNetwork.NetworkingClient.EventReceived += OnEvent;
        }

        public override void OnDisable()
        {
            base.OnDisable();
            PhotonNetwork.NetworkingClient.EventReceived -= OnEvent;
        }


        //===================================================================
        // RPCs, external events/messages from Photon
        //===================================================================
        public void OnEvent(EventData eventData)
        {
            if (eventData.Code == 0)
            {
                object[] data = (object[])eventData.CustomData;

                string ip = (string)data[0];

                Debug.Log("############ GOT IP FROM RAISEEVENT, ip = " + ip + " sender id = " + eventData.Sender);

#if !UNITY_WSA
                if (HeadsetUDPConnection.Instance != null)
                {
                    HeadsetUDPConnection.Instance.Disconnect();
                    HeadsetUDPConnection.Instance.Connect(ip);
                }
                else
                {
                    Debug.LogError("HeadsetUDPConnection must be present in scene to support automatic headset connection");
                }
#endif
            }
        }


        #region IHandTrackingDataSource

        private CircularReuseBuffer<Tracking.HandSet> handBuffer;

        public void SetHandsBuffer(CircularReuseBuffer<Tracking.HandSet> buffer)
        {
            handBuffer = buffer;
        }

        public bool StartTracking()
        {
            // noop, controlled by connection
            return true;
        }

        public void StopTracking()
        {
            // noop, controlled by connection
        }

        public void SetTrackingFrequency(Tracking.TrackingFrequency frequency)
        {
            // noop
        }

        #endregion
    }
}