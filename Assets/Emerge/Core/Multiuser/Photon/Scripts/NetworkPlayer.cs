﻿/******************************************************************************
 * Copyright (C) Emerge Now, Inc. 2016-2019.                                  *
 * Emerge Now Inc proprietary and confidential.                               *
 *                                                                            *
 * Use subject to the terms of the Emerge Exploration Program Agreement       *
 * and/or terms of limited, non-exclusive, non-sublicensable license to       *
 * Emerge SDK between Emerge and you, your company or any other organization. *
 ******************************************************************************/
using UnityEngine;
using Photon.Pun;

namespace Emerge.Communication {

    /// <summary>
    /// Emerge Network Player which is responsible for photon Instantiate
    /// </summary>
    public class NetworkPlayer : MonoBehaviourPunCallbacks, IPunObservable, IPunInstantiateMagicCallback
    {
        private int playerIndex;


        public int GetPlayerIndex()
        {
            return playerIndex;
        }

        public void SetPlayerIndex(int value)
        {
            playerIndex = value;
        }

        //NOTE: can get player that instantiated here:
        //OnPhotonInstantiate(PhotonMessageInfo info)
        public void OnPhotonInstantiate(PhotonMessageInfo info)
        {
            if (info.Sender != null)
                Debug.Log("Photon Instantiate: gameobject.name = " + gameObject.name + " Called by = " + info.Sender.NickName + " my NickName is:" + PhotonNetwork.LocalPlayer.NickName);
        }

        public bool AmILocal()
        {
            return photonView.IsMine;
        }

       



        #region IPunObservable implementation

        public virtual void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
        {
            // noop
        }

        //public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
        //{
        //    if (stream.isWriting)
        //    {
        //        // We own this player: send the others our data
        //        stream.SendNext(this.IsFiring);
        //        stream.SendNext(this.Health);
        //    }
        //    else
        //    {
        //        // Network player, receive data
        //        this.IsFiring = (bool)stream.ReceiveNext();
        //        this.Health = (float)stream.ReceiveNext();
        //    }
        //}

        #endregion







    }
}