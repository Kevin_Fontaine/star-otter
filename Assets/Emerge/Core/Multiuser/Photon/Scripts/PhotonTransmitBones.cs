﻿using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using System;

public class PhotonTransmitBones : MonoBehaviourPunCallbacks, IPunObservable
{
    [SerializeField] List<Transform> boneTransforms;
    [SerializeField] float posLerpSpeed;
    Vector3[] recievedLocPos;
    List<Vector3> transmissionList = new List<Vector3>();
    bool photonDataRecieved = false;

    private void Awake()
    {
        recievedLocPos = new Vector3[boneTransforms.Count];
    }

    void Update()
    {
        ApplyRecievedTransforms();
    }

    private void ApplyRecievedTransforms()
    {
        if (!photonView.IsMine && photonDataRecieved)
        {
            photonDataRecieved = false;
            for (int i = 0; i < boneTransforms.Count; i++)
            {
                boneTransforms[i].localPosition = Vector3.Lerp(boneTransforms[i].localPosition, recievedLocPos[i], Time.deltaTime * posLerpSpeed);
            }
        }
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if(stream.IsWriting)
        {
            if (photonView.IsMine)
            {
                Vector3[] sentPositions = CollateDataForTransmission().ToArray();
                for (int i = 0; i < sentPositions.Length; i++)
                {
                    stream.SendNext(sentPositions[i]);
                }
            }
        }
        else
        {
            if (!photonView.IsMine)
            {
                Array.Clear(recievedLocPos, 0, boneTransforms.Count);
                for (int i = 0; i < boneTransforms.Count; i++)
                {
                    recievedLocPos[i] = (Vector3)stream.ReceiveNext();
                }
                photonDataRecieved = true;
            }
        }
    }

    List<Vector3> CollateDataForTransmission()
    {
        transmissionList.Clear();
        if (boneTransforms == null || boneTransforms.Count == 0)
        {
            Debug.LogError("No Bone Transforms declared");
            return new List<Vector3>();
        }

        for (int i = 0; i < boneTransforms.Count; i++)
        {
            transmissionList.Add(boneTransforms[i].localPosition);
        }

        return transmissionList;
    }
}
