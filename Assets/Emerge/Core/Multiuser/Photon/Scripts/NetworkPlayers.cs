﻿/******************************************************************************
 * Copyright (C) Emerge Now, Inc. 2016-2019.                                  *
 * Emerge Now Inc proprietary and confidential.                               *
 *                                                                            *
 * Use subject to the terms of the Emerge Exploration Program Agreement       *
 * and/or terms of limited, non-exclusive, non-sublicensable license to       *
 * Emerge SDK between Emerge and you, your company or any other organization. *
 ******************************************************************************/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Emerge.Core;
using Photon.Realtime;

namespace Emerge.Communication {

    /// <summary>
    /// Access to all photon players connected, both LEAP (PC, handtracking player) and Hololens (AR headset or device)
    /// </summary>
    public class NetworkPlayers : Singleton<NetworkPlayers> 
    {
        //------------------------------------
        private readonly List<LEAPNetworkPlayer> leapPlayers = new List<LEAPNetworkPlayer>();
        private readonly List<HololensNetworkPlayer> hololensPlayers = new List<HololensNetworkPlayer>();
        //private readonly List<Transform> playerHeadEffectors = new List<Transform>();

        public Action<LEAPNetworkPlayer> OnPlayerJoin = (player) => { };
        public Action<LEAPNetworkPlayer> OnPlayerLeave = (player) => { };
        //------------------------------------

        //use instead of photons OnPlayerConnected when you need the actual prefab - 
        //photon method takes an unknown amount of time to complete registration and instantiation etc.
        //WARNING: this will fire many times, once for each player in the room, when you join a room with players already in it
        public Action OnLeapPlayerJoined;

        //public static bool Exists
        //{
        //    get
        //    {
        //        return Instance != null;
        //    }
        //}
         
        //public static bool IsMultiplayer()
        //{
        //    return Instance != null && Instance.LEAPPlayers.Count > 1;
        //}


        //========================================== LEAP PLAYERS =======================================
        public List<LEAPNetworkPlayer> LEAPPlayers
        {
            get
            {
                return leapPlayers;
            }
        }
  
        public void AddLEAPPlayer(LEAPNetworkPlayer player)
        {
            Debug.Log("LEAP player added to the master list " + player.photonView.Owner.NickName);
            LEAPPlayers.Add(player);

            LEAPPlayers.Sort(); //Ensure ordering across clients. Sorts by PhotonID;

            OnPlayerJoin.Invoke(player);

            ReassignPlayerIndexes();

            OnLeapPlayerJoined.Dispatch();
        }

        public void RemoveLEAPPlayer(LEAPNetworkPlayer player)
        {
            LEAPPlayers.Remove(player);

            OnPlayerLeave.Invoke(player);

            ReassignPlayerIndexes();
        }

        private void ReassignPlayerIndexes()
        {
            for (int i = 0; i < LEAPPlayers.Count; i++) {
                LEAPPlayers[i].SetPlayerIndex(i);
            }
        }

        public LEAPNetworkPlayer GetPlayerAtIndex(int index)
        {
            if (index < LEAPPlayers.Count)
                return LEAPPlayers[index];
            else
                return null;
        }



        //========================================== HOLOLENS PLAYERS =======================================
        public List<HololensNetworkPlayer> HololensPlayers
        {
            get
            {
                return hololensPlayers;
            }
        }

        public void AddHololensPlayer(HololensNetworkPlayer player)
        {
            HololensPlayers.Add(player);
        }

        public void RemoveHololensPlayer(HololensNetworkPlayer player)
        {
            HololensPlayers.Remove(player);
        }

        //========================================== HOLOLENS PLAYERS =======================================




        //========================================== GENERAL PHOTONPLAYER UTILS (not HL or LEAP specific) =======================================
        public bool IsHololensPlayer(Player p)
        {
            return p.NickName.Contains("_hololens");
        }


        public NetworkPlayer GetLEAPPlayerfromPhotonPlayer(Player photonPlayer)
        {
            foreach (NetworkPlayer player in LEAPPlayers)
                if (player.photonView.Owner == photonPlayer)
                    return player;

            //foreach (HololensNetworkPlayer player in HololensPlayers)
            //    if (player.photonView.owner == photonPlayer)
            //        return player;

            return null;
        }

    }
}



//==============================================================
//      EXAMPLE USAGE of this class
//==============================================================

//debugging Photon information
//private void Update()
//{
//    if (Input.GetKeyDown(KeyCode.A)) { 
//        //NetworkPlayers.Instance.PrintAllPlayers();
//        foreach (LEAPNetworkPlayer p in NetworkPlayers.Instance.LEAPPlayers) {
//            //PhotonPlayer pp = NetworkPlayers.Instance.GetPlayerfromPhotonPlayer();

//            Debug.Log("player photonView.Owner.ActorNumber = " + p.photonView.Owner.ActorNumber + " photonView.name = " + p.photonView.name);
//            Debug.Log("player photonView.player.NickName = " + p.photonView.Owner.NickName);
//        }
//        foreach (HololensNetworkPlayer p in NetworkPlayers.Instance.HololensPlayers) {
//            //PhotonPlayer pp = NetworkPlayers.Instance.GetPlayerfromPhotonPlayer();

//            Debug.Log("player photonView.Owner.ActorNumber = " + p.photonView.Owner.ActorNumber + " photonView.name = " + p.photonView.name);
//            Debug.Log("player photonView.player.NickName = " + p.photonView.Owner.NickName);
//        }
//    } 
//}



