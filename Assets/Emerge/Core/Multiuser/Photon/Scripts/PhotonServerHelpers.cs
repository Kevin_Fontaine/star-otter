﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.ComponentModel;

namespace Emerge.Communication
{


    public enum PhotonServerBookmarks
    {
        none,
        [Description("192.168.2.118")]
        Emerge,
        [Description("192.168.10.125 ")]
        MikeOffice
    }



    public static class EnumExtensions
    {
        public static string ToDescriptionString(this PhotonServerBookmarks val)
        {
            DescriptionAttribute[] attributes = (DescriptionAttribute[])val
               .GetType()
               .GetField(val.ToString())
               .GetCustomAttributes(typeof(DescriptionAttribute), false);
            return attributes.Length > 0 ? attributes[0].Description : string.Empty;
        }
    }

}