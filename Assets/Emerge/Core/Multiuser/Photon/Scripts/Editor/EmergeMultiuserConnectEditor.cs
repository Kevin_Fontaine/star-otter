﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

using Emerge.Core;
using EditorHelperUtils;

namespace Emerge.Communication
{


    [CustomEditor(typeof(MultiuserConnectAndJoin), true)]
    public class MultiuserConnectInspector : Editor
    {
        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            serializedObject.ApplyModifiedProperties();
            base.OnInspectorGUI();
        }
    }

}