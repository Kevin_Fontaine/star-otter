﻿/******************************************************************************
 * Copyright (C) Emerge Now, Inc. 2016-2019.                                  *
 * Emerge Now Inc proprietary and confidential.                               *
 *                                                                            *
 * Use subject to the terms of the Emerge Exploration Program Agreement       *
 * and/or terms of limited, non-exclusive, non-sublicensable license to       *
 * Emerge SDK between Emerge and you, your company or any other organization. *
 ******************************************************************************/
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;
using Emerge.HandTracking;

namespace Emerge.Communication {

    /// <summary>
    /// Photon Player for HMD
    /// </summary>
    public class HololensNetworkPlayer : NetworkPlayer {

        public void Awake()
        {
            //if (NetworkPlayers.Exists)
            //{
            //    NetworkPlayers.Instance.AddHololensPlayer(this);
            //}
            NetworkPlayers.Instance.AddHololensPlayer(this);  
        }

        private void OnDestroy()
        {
            //Debug.Log("PhotonPlayer Destroy called");
            //if (NetworkPlayers.Exists) {
            //    NetworkPlayers.Instance.RemoveHololensPlayer(this);
            //} 
                NetworkPlayers.Instance.RemoveHololensPlayer(this); 
        }




        // network/socketComms RPCs
        //============================================================================================================================

        //usage example
        //HololensNetworkPlayer p = NetworkPlayers.Instance.HololensPlayers[0];
        //Debug.Log("hololens player: " + p.photonView.owner.NickName);
        ////if (IsHololensPlayer(e.photonView.owner)) {
        //p.ConnectToClientPC("192.168.1.125", p.photonView.owner);
        //p.DisconnectFromClientPC(p.photonView.owner);


        [PunRPC]
        public void RPC_HololensIP(int PCphotonPlayerID, byte[] UDPserver_ip)
        {

            string leapPC_ip = IPAddressUtils.ConvertIPAddressBytestoString(UDPserver_ip);

            //get the IP of this hololens
            string hololensIP_string = IPAddressUtils.GetIP();

            Debug.Log("RPC - RPC_HololensIP(ip address = " + hololensIP_string + ") called on " + photonView.Owner.NickName);

            Debug.Log("sending my ip (" + hololensIP_string + ") to leap pc with photonNetwork player ID of: " + PCphotonPlayerID);
            SendIPToLeapPC(hololensIP_string, PCphotonPlayerID);
        }

        //trigger a Photon RaiseEvent targeted at the PC that wants to know the IP
        private void SendIPToLeapPC(string ip, int targetID)
        {
            byte evCode = 0; // Custom Event 0:  
            object[] content = new object[] { ip };
            RaiseEventOptions raiseEventOptions = new RaiseEventOptions { TargetActors = new int[] { targetID } }; // You would have to set the Receivers to All in order to receive this event on the local client as well
            PhotonNetwork.RaiseEvent(evCode, content, raiseEventOptions, SendOptions.SendReliable);
        }


        //ask the HL to send its IP address back to the Leap PC
        public void SendHololensIP(Player player, int returnPhotonNetworkID, byte[] returnIP)
        //public void ConnectToClientPC(string ip, PhotonPlayer player)
        {
            Debug.Log(photonView.Owner.NickName + "with viewid = " + photonView.ViewID + " SendHololensIP(returnID = " + returnPhotonNetworkID + ")");
            if (PhotonNetwork.InRoom) {
                Debug.Log("photonView.RPC(RPC_HololensIP) sent to player with Nickname: " + player.NickName);
                photonView.RPC("RPC_HololensIP", player, returnPhotonNetworkID, returnIP);
            } else {
                Debug.LogWarning("WARNING : tried to broadcast RPC while not in room");
            }
        }


        


        //-----------------------------------------------------------------------------------
        [PunRPC]
        public void RPC_ToggleHandGraphics()
        {
            Debug.Log("RPC - RPC_ToggleHandGraphics() called on " + photonView.Owner.NickName);
          
 #if UNITY_WSA
            // TODO REVISIT ///////////////
            //AvatarManager.Instance.ToggleLocalAvatarVisibility();
 #endif
        }

        public void ToggleHandGraphics(Player player)
        {
            photonView.RPC("RPC_ToggleHandGraphics", player);
        }




        //-----------------------------------------------------------------------------------
        [PunRPC]
        public void RPC_ResetTimer()
        {
            Debug.Log("RPC - RPC_ResetTimer() called on " + photonView.Owner.NickName);
        }

        public void ResetTimer(Player player)
        {
            photonView.RPC("RPC_ResetTimer", player);
        }



    }

}
