﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Emerge.Communication
{
    using Emerge.Tracking;
    using HandTracking;
    using Photon.Pun;
    using UnityEngine.Rendering;

    //Universal class to control all hand data transmission between two end-to-end devices (Eg: PC, Oculus Quest, Hololens)
    public class UniversalNetworkPlayer : NetworkPlayer
    {
        [Header("Hand Data")]
        [SerializeField] PuppetHand_riggedHand leftPuppetRiggedHand; //Left rigged hand
        [SerializeField] PuppetHand_riggedHand rightPuppetRiggedHand; //Right rigged hand
        bool rotatedAboutAnchor = false;

        //Hide hands 
        enum VisibleHand
        {
            LEFT,
            RIGHT,
            BOTH,
            NONE
        };
        VisibleHand visibleHand = VisibleHand.NONE;

        private void Awake()
        {
            InitialHandSetup();
        }

        void Update()
        {
            UpdateHandState();  //Update the bone data for each of the hands
            RotateAboutAnchor(); //Rotate the remote hands about the anchor position in a poker table setup
            UpdateVisibleHand(); //Hides the hands which are inactive 
        }

        #region Init Methods
        //Initial Hand setup (to setup the material of local and remote hands)
        void InitialHandSetup()
        {
            if (photonView.IsMine)
            {
                leftPuppetRiggedHand.InitialSetup(true); //Set coloring for local networked hands
                rightPuppetRiggedHand.InitialSetup(true);
            }
            else
            {
                leftPuppetRiggedHand.InitialSetup(false); //Set coloring for remote networked hands
                rightPuppetRiggedHand.InitialSetup(false);
            }
        }
        #endregion

        #region Update Methods
        //Update the bone data for each of the hands from the local hand data to be transmitted over the network
        void UpdateHandState()
        {
            if (photonView.IsMine)
            {
                Tracking.HandSet handSet = Tracking.HandTracking.Default.MostRecentHandSet; //Get most recent hand set data
                leftPuppetRiggedHand.UpdateHandState(handSet.left);
                rightPuppetRiggedHand.UpdateHandState(handSet.right);
            }
        }

        //Rotate the remote hands about the anchor position in a poker table setup
        void RotateAboutAnchor()
        {
            if (!rotatedAboutAnchor)
            {
                rotatedAboutAnchor = true;
                if (!photonView.IsMine)
                {
                    Transform newParent = new GameObject("RemoteHands").transform; //Create a new Game Object to act as the parent for all remote hands
                    newParent.position = Vector3.zero;
                    newParent.rotation = Quaternion.identity;
                    transform.parent = newParent;

                    //TODO: removed offset here - needs to be tested in Headset
                    transform.localPosition = Vector3.zero;
                    transform.localRotation = Quaternion.Euler(new Vector3(0, 180, 0));
                }
            }
        }

        //Hide inactive hands if this is the local hand object and transmit that info
        void UpdateVisibleHand()
        {
            if (photonView.IsMine)
            {
                if (!leftPuppetRiggedHand.gameObject.activeSelf && !rightPuppetRiggedHand.gameObject.activeSelf)
                    visibleHand = VisibleHand.NONE;
                else if (!leftPuppetRiggedHand.gameObject.activeSelf)
                    visibleHand = VisibleHand.RIGHT;
                else if (!rightPuppetRiggedHand.gameObject.activeSelf)
                    visibleHand = VisibleHand.LEFT;
                else
                    visibleHand = VisibleHand.BOTH;
            }

            switch (visibleHand)
            {
                case VisibleHand.LEFT:
                    leftPuppetRiggedHand.gameObject.SetActive(true);
                    rightPuppetRiggedHand.gameObject.SetActive(false);
                    break;
                case VisibleHand.RIGHT:
                    leftPuppetRiggedHand.gameObject.SetActive(false);
                    rightPuppetRiggedHand.gameObject.SetActive(true);
                    break;
                case VisibleHand.BOTH:
                    leftPuppetRiggedHand.gameObject.SetActive(true);
                    rightPuppetRiggedHand.gameObject.SetActive(true);
                    break;
                case VisibleHand.NONE:
                    leftPuppetRiggedHand.gameObject.SetActive(false);
                    rightPuppetRiggedHand.gameObject.SetActive(false);
                    break;
            }
        }
        #endregion

        #region Photon Methods
        //Send data for the visible hand to other clients 
        override public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
        {
            if (stream.IsWriting)
            {
                if (photonView.IsMine)
                    stream.SendNext(visibleHand);
            }
            else
            {
                if (!photonView.IsMine)
                    this.visibleHand = (VisibleHand)stream.ReceiveNext();
            }
        }
        #endregion
    }
}
