﻿/******************************************************************************
 * Copyright (C) Emerge Now, Inc. 2016-2019.                                  *
 * Emerge Now Inc proprietary and confidential.                               *
 *                                                                            *
 * Use subject to the terms of the Emerge Exploration Program Agreement       *
 * and/or terms of limited, non-exclusive, non-sublicensable license to       *
 * Emerge SDK between Emerge and you, your company or any other organization. *
 ******************************************************************************/
using System.IO;
using System.Collections.Generic;

namespace Emerge.Core
{
    public interface ISyncedData<T> where T : struct 
	{

		void WriteToBinary(BinaryWriter writer);
		T ReadFromBinary(BinaryReader reader);

		string ToJson();
		T FromJson(string json);
	}
}