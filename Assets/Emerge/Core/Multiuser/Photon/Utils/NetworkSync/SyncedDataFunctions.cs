﻿/******************************************************************************
 * Copyright (C) Emerge Now, Inc. 2016-2019.                                  *
 * Emerge Now Inc proprietary and confidential.                               *
 *                                                                            *
 * Use subject to the terms of the Emerge Exploration Program Agreement       *
 * and/or terms of limited, non-exclusive, non-sublicensable license to       *
 * Emerge SDK between Emerge and you, your company or any other organization. *
 ******************************************************************************/
using UnityEngine;
using System.IO;

namespace Emerge.Core
{
    public static class SyncedDataFunctions
	{
		public static byte[] DataToBinary<T>(ISyncedData<T> data) where T : struct
		{
			MemoryStream memStream = new MemoryStream();
			BinaryWriter writer = new BinaryWriter(memStream);

			data.WriteToBinary(writer);

			return memStream.ToArray();
		}

		public static T DataFromBinary<T>(byte[] binary) where T : struct, ISyncedData<T>
		{
			T data = default(T);

			using (MemoryStream stream = new MemoryStream(binary))
			{
				using (BinaryReader reader = new BinaryReader(stream))
				{
					return data.ReadFromBinary(reader);
				}
			}
		}
	}
}