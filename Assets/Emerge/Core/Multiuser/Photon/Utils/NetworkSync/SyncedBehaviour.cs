﻿/******************************************************************************
 * Copyright (C) Emerge Now, Inc. 2016-2019.                                  *
 * Emerge Now Inc proprietary and confidential.                               *
 *                                                                            *
 * Use subject to the terms of the Emerge Exploration Program Agreement       *
 * and/or terms of limited, non-exclusive, non-sublicensable license to       *
 * Emerge SDK between Emerge and you, your company or any other organization. *
 ******************************************************************************/
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

namespace Emerge.Core {
    [RequireComponent(typeof(PhotonView))]
    public abstract class SyncedBehaviour<T> : MonoBehaviourPunCallbacks, IPunObservable, IUndoObject where T : struct, ISyncedData<T> {
        private T data = default(T);
        private T previousData;
        private bool isBusySynching = false;

        public event System.Action DataChanged;

        public T PreviousData
        {
            get
            {
                return previousData;
            }
        }

        public T Data
        {
            get
            {
                return data;
            }
            set
            {
                SetDataNoSync(value);
                BroadcastData();
            }
        }

        protected abstract T GetDefaultData();
        protected abstract void OnDataChanged();

        public void SetDataNoSync(T value)
        {
            previousData = data;
            data = value;
            OnDataChanged();
            DataChanged.Dispatch();
        }


        #region IPunObservable

        public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
        {
            // noop
        }

        #endregion


        #region Networking

        [PunRPC]
        public void SyncData(byte[] binary)
        {
             
            previousData = data;
            data = SyncedDataFunctions.DataFromBinary<T>(binary);
            OnDataChanged();
            isBusySynching = false;
        }

        public void BroadcastData()
        {
            if (PhotonNetwork.InRoom) {
                //Convert data to binary
                byte[] binary = SyncedDataFunctions.DataToBinary(Data);
 
                photonView.RPC("SyncData", RpcTarget.Others, binary);
            } else {
                Debug.LogWarning("WARNING : tried to broadcast current state while not in room");
            }
        }

        public override void OnJoinedRoom()
        {
            base.OnJoinedRoom();

            if (photonView.IsMine)// && NetworkManager.IsMultiplayerMode())
            {
                // TODO: on joined room, broadcast the current state to everybody if we're the owner
                BroadcastData();
            }
        }

        public override void OnPlayerEnteredRoom(Player newPlayer)
        {
            if (photonView.IsMine)// && NetworkManager.IsMultiplayerMode())
            {
                BroadcastData();
            }
        }
        #endregion

        #region Saving/Loading
        public void Reset()
        {
            SetDataNoSync(GetDefaultData());
        }

        public bool SerializingBusy
        {
            get { return isBusySynching; }
        }
         
        #endregion
        public void Undo()
        {
            Data = previousData;
        }

    }
}  