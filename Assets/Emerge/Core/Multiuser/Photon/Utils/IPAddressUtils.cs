﻿using System;
using System.Net;
using System.Net.Sockets;

#if !UNITY_EDITOR && UNITY_WSA
using Windows.Networking.Connectivity;
using Windows.Networking;
#endif

public static class IPAddressUtils {


    public static string GetIP()
    {
#if !UNITY_EDITOR && UNITY_WSA

        foreach (HostName localHostName in NetworkInformation.GetHostNames()) {
            if (localHostName.IPInformation != null) {
                if (localHostName.Type == HostNameType.Ipv4) {
                   return localHostName.ToString();
                   
                }
            }
        }

        //Debug.Log("NO IP FOUND! returning 127.0.0.1");
        return "127.0.0.1";

#else
        string strHostName = "";
        strHostName = Dns.GetHostName();

        IPHostEntry ipEntry = Dns.GetHostEntry(strHostName);

        IPAddress[] addr = ipEntry.AddressList;

        foreach (var address in addr)
        {
            if (address.AddressFamily == AddressFamily.InterNetwork)
                return address.ToString();
            //Debug.Log("IPv4 Address:  = " + address.ToString());
        }

        return "no ipv4 available";
#endif

    }

    /// <summary>
    /// Convert a ip string to a uint (must be uint to handle values all the way up to 255.255.255.255
    /// </summary>
    /// <param name="s">a ipv4 string like "255.255.255.255"</param>
    public static uint ConvertIPAddressStringtoInt(string s)
    {
        IPAddress address = IPAddress.Parse(s);
        byte[] bytes = address.GetAddressBytes();
        Array.Reverse(bytes); // flip big-endian(network order) to little-endian
        uint intAddress = BitConverter.ToUInt32(bytes, 0);
        return intAddress;
    }


    public static string ConvertIPAddressIntToString(uint s)
    {

        byte[] bytes = BitConverter.GetBytes(s);
        Array.Reverse(bytes); // flip little-endian to big-endian(network order)
        string ipAddress = new IPAddress(bytes).ToString();
        return ipAddress;
    }


    public static byte[] ConvertIPAddressStringtoBytes(string s)
    {
        IPAddress address = IPAddress.Parse(s);
        byte[] bytes = address.GetAddressBytes();
        Array.Reverse(bytes); // flip big-endian(network order) to little-endian
        return bytes;
    }

    public static string ConvertIPAddressBytestoString(byte[] bytes)
    { 
        Array.Reverse(bytes); // flip little-endian to big-endian(network order)
        string ipAddress = new IPAddress(bytes).ToString();
        return ipAddress;
    }

}
