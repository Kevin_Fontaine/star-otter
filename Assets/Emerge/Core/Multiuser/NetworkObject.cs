﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using System;

namespace Emerge.Communication {

    using Core;

    [RequireComponent(typeof(PhotonView))]
    public class NetworkObject : MonoBehaviourPun, IPunObservable
    {
        private void Start()
        {
            byte LEAP_group = (byte)1;
            photonView.Group = LEAP_group;
            photonView.Synchronization = ViewSynchronization.UnreliableOnChange;
        }
        public void OnEnable()
        {
            PhotonNetwork.AddCallbackTarget(this);
        }

        public void OnDisable()
        {
            PhotonNetwork.RemoveCallbackTarget(this);
        }

        private Transform interactableObjectRoot;

        public void SetInteractableObjectRoot(Transform trans)
        {
            interactableObjectRoot = trans;
        }

        public bool IsOwner()
        {
            return photonView.IsMine;
        }
#if !UNITY_WSA
        public void RequestOwnership(int playerID)
        {
            photonView.TransferOwnership(playerID);
        }
#endif


        #region IPunObservable

        public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
        {
            if (stream.IsWriting)
            {
                if (IsOwner())
                {
                    stream.SendNext(interactableObjectRoot.localPosition);
                    stream.SendNext(interactableObjectRoot.localEulerAngles);
                }
            }
            else
            {
                if (!IsOwner())
                {
                    Vector3 pos = (Vector3)stream.ReceiveNext();
                    interactableObjectRoot.localPosition = Vector3.Lerp(interactableObjectRoot.localPosition, pos, 30 * Time.deltaTime);
                    interactableObjectRoot.localEulerAngles = (Vector3)stream.ReceiveNext();
                }
            }
        }

    #endregion
    }
}
