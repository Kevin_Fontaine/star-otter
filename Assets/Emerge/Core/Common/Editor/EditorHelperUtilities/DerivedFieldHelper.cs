﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;



namespace EditorHelperUtils
{
    /// <summary>
    /// Collection of helper functions to work with derived types in SerializedObjects
    /// </summary>


    public static class DerivedFieldHelper
    {
        public static object GetValue(object source, string name)
        {
            if (source == null)
                return null;

            var type = source.GetType();

            var f = FindFieldInTypeHierarchy(type, name);

            if (f == null)
            {
                var p = FindPropertyInTypeHierarchy(type, name);
                if (p == null)
                    return null;
                return p.GetValue(source, null);
            }
            return f.GetValue(source);
        }

        public static FieldInfo FindFieldInTypeHierarchy(Type providedType, string fieldName)
        {
            FieldInfo field = providedType.GetField(fieldName, (BindingFlags)(-1));


            while (field == null && providedType.BaseType != null)
            {
                providedType = providedType.BaseType;
                field = providedType.GetField(fieldName, (BindingFlags)(-1));
            }

            return field;
        }

        public static PropertyInfo FindPropertyInTypeHierarchy(Type providedType, string propertyName)
        {
            PropertyInfo property = providedType.GetProperty(propertyName, (BindingFlags)(-1));


            while (property == null && providedType.BaseType != null)
            {
                providedType = providedType.BaseType;
                property = providedType.GetProperty(propertyName, (BindingFlags)(-1));
            }

            return property;
        }
    }
}
