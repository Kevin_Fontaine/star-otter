﻿using System.Collections;
using System.Collections.Generic;
using Emerge.Tracking;
using UnityEngine;
using UnityEditor;

namespace Emerge.Core
{
    [CustomEditor(typeof(TactileConfig))]
    public class TactileConfigInspector : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            TactileConfig tactileConfig = (TactileConfig)target;

            EditorGUILayout.Space();
            EditorGUILayout.LabelField("Editor only settings", EditorStyles.boldLabel);
            EditorGUI.indentLevel++;
            tactileConfig.EditorHandTrackingSource = (HandHardware)EditorGUILayout.EnumPopup(nameof(tactileConfig.EditorHandTrackingSource),
                tactileConfig.EditorHandTrackingSource);

            if (tactileConfig.EditorHandTrackingSource == HandHardware.Oculus)
            {
                tactileConfig.EditorAnchoringMethod = (Anchoring)EditorGUILayout.EnumPopup(nameof(tactileConfig.EditorAnchoringMethod),
                tactileConfig.EditorAnchoringMethod);
                EditorGUILayout.HelpBox("Oculus Hands and anchoring works in editor only if you are using Oculus link", MessageType.Info);
            }
            EditorGUI.indentLevel--;
            EditorGUILayout.LabelField("Target devices", EditorStyles.boldLabel);
            EditorGUI.indentLevel++;
            tactileConfig.TargetPlatform = (Platforms)EditorGUILayout.EnumPopup(nameof(tactileConfig.TargetPlatform),
                tactileConfig.TargetPlatform);
            if (tactileConfig.TargetPlatform != Platforms.PC_Mac_Linux)
            {
                GUI.enabled = false;
                EditorGUILayout.TextField("Hand Tracking Source", string.Format("{0}'s native hand tracking will be used", tactileConfig.TargetPlatform.ToString("G")));
                GUI.enabled = true;
                tactileConfig.AnchoringMethod = (Anchoring)EditorGUILayout.EnumPopup(nameof(tactileConfig.AnchoringMethod),
                tactileConfig.AnchoringMethod);
            }
            else
            {
                tactileConfig.HandTrackingSource = (BaseStationHandHardware)EditorGUILayout.EnumPopup(nameof(tactileConfig.HandTrackingSource),
                    tactileConfig.HandTrackingSource);
            }

            EditorGUI.indentLevel--;
            EditorGUILayout.LabelField("Tactile settings", EditorStyles.boldLabel);
            EditorGUI.indentLevel++;
            tactileConfig.UseSoloPointAmplification = EditorGUILayout.Toggle("Amplify solo point", tactileConfig.UseSoloPointAmplification);
            EditorGUI.indentLevel--;
            EditorGUILayout.LabelField("Multiuser settings", EditorStyles.boldLabel);
            EditorGUI.indentLevel++;
            tactileConfig.AutoConnect = EditorGUILayout.Toggle("Photon auto connect", tactileConfig.AutoConnect);
            tactileConfig.photonServerMode = (PhotonServerMode)EditorGUILayout.EnumPopup("Photon server mode", tactileConfig.photonServerMode);

            EditorGUILayout.HelpBox("Photon settings must be set for multiplayer under \"PhotonServerSettings\" asset." +
                                    "\n1. Set Photon App ID under \"App Id Realtime\" for cloud server. By default photon connects to best cloud server" +
                                    "\n2. Set server IP and port under \"Server\" and \"Port\" for local server. Default port is 5055.", MessageType.Info);

            EditorGUI.indentLevel--;
            if (GUI.changed)
            {
                EditorUtility.SetDirty(target);
            }

        }
    }
}
