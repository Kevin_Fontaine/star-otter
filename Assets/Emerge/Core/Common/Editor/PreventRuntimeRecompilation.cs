/******************************************************************************
 * Copyright (C) Emerge Now, Inc. 2016-2019.                                  *
 * Emerge Now Inc proprietary and confidential.                               *
 *                                                                            *
 * Use subject to the terms of the Emerge Exploration Program Agreement       *
 * and/or terms of limited, non-exclusive, non-sublicensable license to       *
 * Emerge SDK between Emerge and you, your company or any other organization. *
 ******************************************************************************/
using UnityEditor;
using UnityEngine;

namespace Emerge.Communication
{
    [InitializeOnLoad]
    public class CompilerOptionsEditorScript
    {
        static bool waitingForStop = false;

        static CompilerOptionsEditorScript()
        {
            EditorApplication.update += OnEditorUpdate;
        }

        static void OnEditorUpdate()
        {
            if (!waitingForStop
                && EditorApplication.isCompiling
                && EditorApplication.isPlaying)
            {
                EditorApplication.LockReloadAssemblies();
                EditorApplication.playModeStateChanged
                     += PlaymodeChanged;
                waitingForStop = true;
            }
        }

        static void PlaymodeChanged(PlayModeStateChange playModeStateChange)
        {
            if (EditorApplication.isPlaying)
                return;

            EditorApplication.UnlockReloadAssemblies();
            EditorApplication.playModeStateChanged
                 -= PlaymodeChanged;
            waitingForStop = false;
        }
    }
}