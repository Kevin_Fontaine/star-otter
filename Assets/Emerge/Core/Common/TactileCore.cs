﻿/******************************************************************************
 * Copyright (C) Emerge Now, Inc. 2016-2019.                                  *
 * Emerge Now Inc proprietary and confidential.                               *
 *                                                                            *
 * Use subject to the terms of the Emerge Exploration Program Agreement       *
 * and/or terms of limited, non-exclusive, non-sublicensable license to       *
 * Emerge SDK between Emerge and you, your company or any other organization. *
 ******************************************************************************/
 // CleanPass

using Emerge.HandTracking;
using UnityEngine;

namespace Emerge.Core
{
    public class TactileCore : Singleton<TactileCore>
    {
        private GameObject interactableObjectGo;

        public static readonly string InteractableObjectsString = "InteractableObjects";

        //[HideInInspector] 
        //public AvatarManager AvatarManager { get; set; }

        [HideInInspector]
        public PhotonRoomManager PhotonRoomManager;

#if !UNITY_WSA
        // ToDo: remove, Only reference is below
        //[HideInInspector] 
        //public EmergeObjectsManager EmergeObjectsManager { get; set; }

        //// ToDo: remove, Only reference is below
        //[HideInInspector] 
        //public SeatManager SeatingManager { get; set; }

        // ToDo: Ask Naveen why this is not done on a HoloLens build. Prefer to merge GetInteractableObjectRoot
        public GameObject InteractableObjectGo
        {
            get => interactableObjectGo;
            set => interactableObjectGo = value;
        }

        public GameObject GetInteractableObjectRoot()
        {
            if (InteractableObjectGo == null) { InteractableObjectGo = GameObject.Find(InteractableObjectsString); }

            return InteractableObjectGo;
        }
#endif

        private void Awake()
        {
#if !UNITY_WSA
            // ToDo: These are singletons now.
            //EmergeObjectsManager = FindObjectOfType<EmergeObjectsManager>();
            //SeatingManager = FindObjectOfType<SeatManager>();
#endif
            //AvatarManager = FindObjectOfType<AvatarManager>();

            // TODO: we can subscribe to Join events for photon player prefab creation. 
            // That way room manager is just events, and all player info can be removed and moved into more appropriate class (like AvatarManager?)
            // PhotonRoomManager = FindObjectOfType<PhotonRoomManager>();
            // PhotonRoomManager.WeJoinedTheRoom += avatarManager.playerJoined;
#if !UNITY_WSA

            // Debug.Log("Init called on EmergeCore");
            InteractableObjectGo = GameObject.Find(InteractableObjectsString);
#endif

        }
    }
}