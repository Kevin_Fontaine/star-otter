﻿/******************************************************************************
 * Copyright (C) Emerge Now, Inc. 2016-2019.                                  *
 * Emerge Now Inc proprietary and confidential.                               *
 *                                                                            *
 * Use subject to the terms of the Emerge Exploration Program Agreement       *
 * and/or terms of limited, non-exclusive, non-sublicensable license to       *
 * Emerge SDK between Emerge and you, your company or any other organization. *
 ******************************************************************************/
using UnityEngine;
using System.Collections;
namespace Emerge.Core
{
    public class CameraMovement : MonoBehaviour
    {
        private float speed = 1.0f;

        public float minX = -360.0f;
        public float maxX = 360.0f;

        public float minY = -95.0f;
        public float maxY = 95.0f;

        public float sensX = 90.0f;
        public float sensY = 90.0f;

        float rotationY = 0.0f;
        float rotationX = 0.0f;

        public Transform target;

        private void Start()
        {
            //transform.LookAt(target);
            transform.localEulerAngles = new Vector3(0f, 0f, 0f);
            transform.position = new Vector3(0f, .23f, -0.5f);
            rotationX = transform.localEulerAngles.x;
            rotationY = transform.localEulerAngles.y;
        }

        void Update()
        {
            float scroll = Input.GetAxis("Mouse ScrollWheel");
            //transform.Translate(0, scroll * zoomSpeed, scroll * zoomSpeed, Space.Self);

            if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
            {
                transform.position += transform.right * speed * Time.deltaTime;
            }
            if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
            {
                transform.position += -transform.right * speed * Time.deltaTime;
            }
            if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W))
            {
                transform.position += transform.forward * speed * Time.deltaTime;
            }
            if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S))
            {
                transform.position += -transform.forward * speed * Time.deltaTime;
            }

            if (Input.GetMouseButton(1))
            {
                rotationX += Input.GetAxis("Mouse X") * sensX * Time.deltaTime;
                rotationY += Input.GetAxis("Mouse Y") * sensY * Time.deltaTime;
                rotationY = Mathf.Clamp(rotationY, minY, maxY);
                transform.localEulerAngles = new Vector3(-rotationY, rotationX, 0);
            }

            if (Input.GetKeyUp(KeyCode.T))
            {
                transform.LookAt(target);
            }
        }
    }
}