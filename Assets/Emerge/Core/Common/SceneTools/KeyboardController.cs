﻿/******************************************************************************
 * Copyright (C) Emerge Now, Inc. 2016-2019.                                  *
 * Emerge Now Inc proprietary and confidential.                               *
 *                                                                            *
 * Use subject to the terms of the Emerge Exploration Program Agreement       *
 * and/or terms of limited, non-exclusive, non-sublicensable license to       *
 * Emerge SDK between Emerge and you, your company or any other organization. *
 ******************************************************************************/
using UnityEngine;

namespace Emerge.Core
{
    public class KeyboardController : MonoBehaviour
    {

        public float moveIncrement = 0.0025f;
        public float rotateIncrement = 0.25f;

        void Update()
        {
            if (Input.GetKey(KeyCode.A))
            {
                transform.position += transform.right * moveIncrement;
            }
            if (Input.GetKey(KeyCode.S))
            {
                transform.position += transform.forward * -moveIncrement;
            }
            if (Input.GetKey(KeyCode.D))
            {
                transform.position += transform.right * -moveIncrement;
            }
            if (Input.GetKey(KeyCode.Keypad2))
            {
                transform.position += transform.up * moveIncrement;
            }
            if (Input.GetKey(KeyCode.Keypad0))
            {
                transform.position += transform.up * -moveIncrement;
            }
        }
    }
}