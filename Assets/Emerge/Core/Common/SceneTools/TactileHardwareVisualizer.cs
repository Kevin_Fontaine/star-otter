﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TactileHardwareVisualizer : MonoBehaviour
{
    [SerializeField]
    private bool hideHardwareVisualizerOnAwake;

    /// <summary>
    /// Optional Transform to hide on Awake. If nothing is specified, the Transform on which this 
    /// Component is placed will be used. 
    /// </summary>
    private string HardwareConfigurationString = "HardwareConfiguration";
    [SerializeField]
    private Transform hardwareConfiguration;
    public Transform HardwareConfiguration
    {
        get => hardwareConfiguration;
        set => hardwareConfiguration = value;
    } 

    void Start()
    {
        if(HardwareConfiguration == null)
        {
            HardwareConfiguration = transform.Find(HardwareConfigurationString);
        }

        if(hideHardwareVisualizerOnAwake)
        {
            ShowHideHardwareConfiguration(false);
        }
    }

    public void ShowHideHardwareConfiguration(bool show)
    {
        HardwareConfiguration?.gameObject.SetActive(show);
    }
}
