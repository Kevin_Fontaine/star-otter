﻿/******************************************************************************
 * Copyright (C) Emerge Now, Inc. 2016-2019.                                  *
 * Emerge Now Inc proprietary and confidential.                               *
 *                                                                            *
 * Use subject to the terms of the Emerge Exploration Program Agreement       *
 * and/or terms of limited, non-exclusive, non-sublicensable license to       *
 * Emerge SDK between Emerge and you, your company or any other organization. *
 ******************************************************************************/
// CleanPass

using System.IO;
using UnityEngine;

namespace Emerge.Core
{
    public enum HandHardware
    {
        LeapMotion,
        Realsense,
        Oculus
    }

    public enum Mode
    {
        BaseStation,
        XRPlatforms,
    }

    public enum BaseStationHandHardware
    {
        LeapMotion,
        Realsense
    }

    public enum Platforms
    {
        Oculus_Quest,
        PC_Mac_Linux,
    }

    public enum Anchoring
    {
        None,
        OculusControllers,
    }

    public enum PhotonServerMode //Photon server mode for local or cloud server connection 
    {
        LOCAL_SERVER,
        CLOUD_SERVER
    }

    public class TactileConfig : Singleton<TactileConfig>
    {
        private static readonly bool DefaultLoadFromFile = false;
        private static readonly string DefaultApplicationPersistentDataPath = "/Emerge.config";
        private static readonly string DefaultApplicationDataPath = "/../Emerge.config";
        public static readonly string ConfigString = "Config";

        // ToDo: should this be 192.168.10.125?
        private static readonly string DefaultPhotonServerAddress = "192.168.1.119";
        private static readonly string DefaultAppId = "0677f51f-6242-474b-a69f-fa13182e192c";
        private static readonly int DefaultPort = 5055;
        private static readonly int DefaultNumberOfSeats = 2;
        private static readonly int DefaultSeatNumber = 0;
        private static readonly bool DefaultIsLocalHosted = true;

        [HideInInspector, Tooltip("This is forced to true in a build. Can be set to true for testing in editor. \n" +
                                  "See Documentation for location of config file within the file system.")]
        public bool LoadFromFile = DefaultLoadFromFile;

        [HideInInspector, Tooltip("Hand tracking data source in editor")]
        public HandHardware EditorHandTrackingSource;
        
        [HideInInspector, Tooltip("Hand tracking data source in the build")]
        public BaseStationHandHardware HandTrackingSource;

        [HideInInspector, Tooltip("Anchoring data source in editor")]
        public Anchoring EditorAnchoringMethod;

        [HideInInspector, Tooltip("Anchoring data source in the build")]
        public Anchoring AnchoringMethod;

        [HideInInspector]
        public Platforms TargetPlatform = Platforms.PC_Mac_Linux;

        public Mode Mode { get; set; } = Mode.BaseStation;

        //Multiuser settings
        [HideInInspector, Tooltip("Local or cloud photon server")]
        public PhotonServerMode photonServerMode = PhotonServerMode.CLOUD_SERVER;

        /// <summary>Connect automatically? If false you can set this to true later on or call ConnectUsingSettings in your own scripts.</summary>
        [HideInInspector, Tooltip("Auto connect to Photon")]
        public bool AutoConnect = true;

        [HideInInspector]
        public int SeatNumber = DefaultSeatNumber;

        [HideInInspector, Tooltip("Photon Unity Networking App ID")]
        public string PhotonAppId = DefaultAppId;

        [HideInInspector, Tooltip("Photon Unity Networking port number")]
        public int PhotonPort = DefaultPort;

        [HideInInspector, Tooltip("Photon Unity Networking server address")]
        public string PhotonServerAddress = DefaultPhotonServerAddress;


        /// <summary>
        /// When the SG1 is only projecting a single TactilePoint the sensation is a very smooth sensation that feels unlike the sensation
        /// when the SG1 is projecting multiple TactilePoints.
        /// When set to True, this setting causes the SG1 to project all TactilePoints with the same sensation even when there is only one of them.
        /// When set to False, this setting causes the SG1 to project a smooth TactilePoint when it is only projecting one TactilePoint.
        /// The hardware does not support projecting multiple TactilePoints in the same frame while using the smooth setting.  
        /// </summary>
        [HideInInspector, Tooltip("When set to True, this setting causes the SG1 to project all TactilePoints with the same sensation even when there is only one of them. \n" +
        "When set to False, this setting causes the SG1 to project a smooth TactilePoint when it is only projecting one TactilePoint.")]
        public bool UseSoloPointAmplification = true;
        void Awake()
        {
#if !UNITY_EDITOR
            LoadFromFile = true;
#endif
#if UNITY_EDITOR || UNITY_STANDALONE
            Mode = Mode.BaseStation;
#else
            Mode = Mode.XRPlatforms;
#endif

            if (LoadFromFile) { LoadConfigFromFile(); }
        }

        private void LoadConfigFromFile()
        {
            Debug.Log("Loading config from file");
            // Get the config file path - for HoloLens it's located here:
            // Go to Device Portal/File Explorer -> LocalAppData/YourApp/LocalState
#if UNITY_WSA
            string pathToConfigFile = Application.persistentDataPath + DefaultApplicationPersistentDataPath;
#else
            string pathToConfigFile = Application.dataPath + DefaultApplicationDataPath;
#endif

            if (File.Exists(pathToConfigFile))
            {
                // config file found at that path - overwrite our existing one with the one we loaded
                string configFromJson = System.IO.File.ReadAllText(pathToConfigFile);
                TactileConfig tactileConfig = JsonUtility.FromJson<TactileConfig>(configFromJson);
                //PhotonServerAddress = tactileConfig.PhotonServerAddress;

            } 
        } 
    }
}
