﻿using System;
using UnityEngine;

namespace Emerge.Core.Shared.Utils
{
    [Serializable]
    public class ModifiedInt : AbstractModifiedPrimitive
    {
        [HideInInspector]
        public string description;
        private readonly int min;
        private readonly int max;
        private int defaultValue;
        [HideInInspector]
        public int currentValue;

        public ModifiedInt(int _min, int _max, int _default, string _description, Action t = null)
        {
            description = _description;
            defaultValue = _default;
            max = _max;
            min = _min;
            currentValue = defaultValue;
            base.RegisterAction(t);
        }

        public int Min => min;

        public int Max => max;

        public int Value
        {
            get => currentValue;
            set
            {
                currentValue = Unity.Mathematics.math.clamp(value, min, max);
                ValueChanged();
            }
        }

        public void Reset()
        {
            currentValue = defaultValue;
        }
    }
}