﻿/******************************************************************************
 * Copyright (C) Emerge Now, Inc. 2016-2019.                                  *
 * Emerge Now Inc proprietary and confidential.                               *
 *                                                                            *
 * Use subject to the terms of the Emerge Exploration Program Agreement       *
 * and/or terms of limited, non-exclusive, non-sublicensable license to       *
 * Emerge SDK between Emerge and you, your company or any other organization. *
 ******************************************************************************/
using UnityEngine;

namespace Emerge.Core
{
    public static class InterfaceUtils
    {
        /// <summary>
        /// This method checks if an object is null or has been destroyed.
        /// Interfaces do not equate to null after their game object is destroyed, unlike checking the component directly.
        /// This method should be used to check if an interface is truly null.
        /// </summary>
        /// <returns><c>true</c>, if the object is null or has been destroyed, <c>false</c> otherwise.</returns>
        /// <param name="obj">Object.</param>
        public static bool IsNullOrDestroyed(this object obj)
        {
            if (obj is Component)
            {
                return (obj as Component) == null;
            }

            return (obj == null);
        }
    }
}
