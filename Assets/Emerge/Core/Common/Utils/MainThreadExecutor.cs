﻿/******************************************************************************
 * Copyright (C) Emerge Now, Inc. 2016-2019.                                  *
 * Emerge Now Inc proprietary and confidential.                               *
 *                                                                            *
 * Use subject to the terms of the Emerge Exploration Program Agreement       *
 * and/or terms of limited, non-exclusive, non-sublicensable license to       *
 * Emerge SDK between Emerge and you, your company or any other organization. *
 ******************************************************************************/
using System;
using System.Threading;
using System.Collections.Generic;
using UnityEngine;

namespace Emerge.Core
{
    /// <summary>
    /// Enqueues actions to be invoked on the main thread.
    /// </summary>
    public class MainThreadExecutor : Singleton<MainThreadExecutor>
    {
        /// <summary>
        /// For thread safety, please lock this stack before changing it.
        /// </summary>
        private readonly Stack<Action> stack = new Stack<Action>();

        private Thread mainThread; 

        /// <summary>
        /// If true, then an action enqueued while on the actual main thread will be executed synchronously,
        /// instead of being added to the queue. Use this if order does not matter.
        /// If this is false, all actions are executed in the order they're enqueued. 
        /// </summary>
        public bool synchronousIfPossible = true;
          
        #region Unity Methods

        private void Awake()
        {
            mainThread = Thread.CurrentThread; 
        }

        private void Update()
        {
            InvokePendingActions();
        }

        private void LateUpdate()
        {
            InvokePendingActions();
        }

        #endregion


        #region Public

        public void Enqueue(Action action)
        {
            if (action == null)
            {
                return;
            }

            if (synchronousIfPossible && mainThread != null && Thread.CurrentThread == mainThread)
            {
                action.Invoke();
                return;
            }

            lock (stack)
            {
                stack.Push(action);
            }
        }

        #endregion


        #region Execution

        private void InvokePendingActions()
        {
            while (stack.Count > 0)
            {
                Action action = null;
                lock (stack)
                {
                    action = stack.Pop();
                }

                action?.Invoke();
            }
        }

        #endregion
    }
}
