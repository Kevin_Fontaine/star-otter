﻿using System;
using UnityEngine;

namespace Emerge.Core.Shared.Utils
{
    [Serializable]
    public class ModifiedFloat : AbstractModifiedPrimitive
    {
        [HideInInspector]
        public string description;
        private readonly float min;
        private readonly float max;
        private float defaultValue;
        [HideInInspector]
        public float currentValue;


        public ModifiedFloat(float _min, float _max, float _default, string _description, Action t = null)
        {
            description = _description;
            defaultValue = _default;
            max = _max;
            min = _min;
            currentValue = defaultValue;
            base.RegisterAction(t);
        }

        public float Min => min;

        public float Max => max;

        public float Value
        {
            get
            {
                return (float)Math.Round(currentValue, 3);
            }
            set
            {
                currentValue = Unity.Mathematics.math.clamp(value, min, max);
                ValueChanged();
            }
        }

        public void Reset() { currentValue = defaultValue; }

    }
}