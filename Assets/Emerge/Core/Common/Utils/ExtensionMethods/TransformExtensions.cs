﻿/******************************************************************************
 * Copyright (C) Emerge Now, Inc. 2016-2019.                                  *
 * Emerge Now Inc proprietary and confidential.                               *
 *                                                                            *
 * Use subject to the terms of the Emerge Exploration Program Agreement       *
 * and/or terms of limited, non-exclusive, non-sublicensable license to       *
 * Emerge SDK between Emerge and you, your company or any other organization. *
 ******************************************************************************/
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class TransformExtensions {
    //Breadth-first search
    public static Transform FindDeepChild(this Transform aParent, string aName)
    {
        if(aParent == null) {
            Debug.Log("root object is null!");
            return null;
        }
        Debug.Log("FindDeepChild = aParent " + aParent.name + " string = " + aName);
        var result = aParent.Find(aName);
        if (result != null)
            return result;
        foreach (Transform child in aParent) {
            result = child.FindDeepChild(aName);
            if (result != null)
                return result;
        }
        Debug.Log("Child not found with this name: " + aName);
        return null;
    }


    /*
    //Depth-first search
    public static Transform FindDeepChild(this Transform aParent, string aName)
    {
        foreach(Transform child in aParent)
        {
            if(child.name == aName )
                return child;
            var result = child.FindDeepChild(aName);
            if (result != null)
                return result;
        }
        return null;
    }
    */


    public static Transform FindChildContainingName(this Transform aParent, string aName)
    {
        foreach (Transform child in aParent) {
            if (child.name.Contains(aName))
                return child;
        }
        return null;
    }

    public static Transform[] FindChildrenContainingName(this Transform aParent, string aName)
    {
        List<Transform>  list = new List<Transform>();
        foreach (Transform child in aParent) {
            if (child.name.Contains(aName))
                list.Add(child);
        }
        return list.ToArray();
    }


    //list the path of a transform or component, useful for debugging. 
    public static string GetPath(this Transform current)
    {
        if (current.parent == null)
            return "/" + current.name;
        return current.parent.GetPath() + "/" + current.name;
    }


    public static string GetPath(this Component component)
    {
        return component.transform.GetPath() + "/" + component.GetType().ToString();
    }

}