﻿/******************************************************************************
 * Copyright (C) Emerge Now, Inc. 2016-2019.                                  *
 * Emerge Now Inc proprietary and confidential.                               *
 *                                                                            *
 * Use subject to the terms of the Emerge Exploration Program Agreement       *
 * and/or terms of limited, non-exclusive, non-sublicensable license to       *
 * Emerge SDK between Emerge and you, your company or any other organization. *
 ******************************************************************************/
using System;

public static class EventExtensions 
{
	public static void Dispatch(this Action eventHandler)
	{
		if(eventHandler != null)
		{
			eventHandler();
		}
	}

	public static void Dispatch<T>(this Action<T> eventHandler, T payload)
	{
		if(eventHandler != null)
		{
			eventHandler(payload);
		}
	}

	public static void Dispatch<T, U>(this Action<T, U> eventHandler, T payloadA, U payloadB)
	{
		if (eventHandler != null)
		{
			eventHandler(payloadA, payloadB);
		}
	}

	public static void Dispatch<T, Uone, Utwo>(this Action<T, Uone, Utwo> eventHandler, T payloadA, Uone payloadB, Utwo payloadC)
	{
		if (eventHandler != null)
		{
			eventHandler(payloadA, payloadB, payloadC);
		}
	}
}
