﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



namespace Emerge
{

    public static class VectorExtensions  
    {
        /// <summary>
		/// Returns a copy of this vector with the given x-coordinate.
		/// </summary>
		 
        public static Vector3 WithX(this Vector3 vector, float x)
        {
            return new Vector3(x, vector.y, vector.z);
        }

        /// <summary>
        /// Returns a copy of this vector with the given y-coordinate.
        /// </summary>
      
        public static Vector3 WithY(this Vector3 vector, float y)
        {
            return new Vector3(vector.x, y, vector.z);
        }

        /// <summary>
        /// Returns a copy of this vector with the given z-coordinate.
        /// </summary>
       
        public static Vector3 WithZ(this Vector3 vector, float z)
        {
            return new Vector3(vector.x, vector.y, z);
        }
    }

}
