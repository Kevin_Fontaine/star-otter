﻿using Emerge.Core;
using System;
using UnityEngine;

namespace Emerge.Core.Shared.Utils
{
    [Serializable]
    public class ModifiedBool : AbstractModifiedPrimitive
    {
        [HideInInspector]
        public string description;
        private bool defaultValue;
        [HideInInspector]
        public bool currentValue;

        public ModifiedBool(bool _default, string _description, Action t = null)
        {
            description = _description;
            defaultValue = _default;
            currentValue = defaultValue;
            base.RegisterAction(t);
        }

        public bool Value
        {
            get => currentValue;
            set
            {
                currentValue = value;
                ValueChanged();
            }
        }

        public void Reset()
        {
            currentValue = defaultValue;
        }
    }
}