﻿using UnityEngine;

namespace Emerge.Tracking
{
    public class TipsAndPalmHandModel : AbstractHandModel
    {
        public Transform thumbPoint;
        public Transform indexPoint;
        public Transform middlePoint;
        public Transform ringPoint;
        public Transform littlePoint;
        public Transform palmPoint;


        #region Overrides

        override public void SetHandData(Hand hand)
        {
            SetPosition(thumbPoint, hand.fingers.thumb.Tip);
            SetPosition(indexPoint, hand.fingers.index.Tip);
            SetPosition(middlePoint, hand.fingers.middle.Tip);
            SetPosition(ringPoint, hand.fingers.ring.Tip);
            SetPosition(littlePoint, hand.fingers.little.Tip);
            SetPosition(palmPoint, hand.palm.center);
        }

        #endregion

        private static void SetPosition(Transform transform, Vector3 position)
        {
            if (transform == null)
            {
                return;
            }

            transform.position = position;
        }
    }
}
