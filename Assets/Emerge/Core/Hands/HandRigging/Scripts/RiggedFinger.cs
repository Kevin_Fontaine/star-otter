﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Emerge.Tracking
{
    public class RiggedFinger : MonoBehaviour
    {
        public Transform bottomBone;
        public Transform middleBone;
        public Transform topBone;

        public Transform baseJoint;
        public Transform middleJoint;
        public Transform topJoint;
        public Transform tip;

        public float boneScaleFactor = 1f;

        public void SetFinger(Digit finger)
        {
            ConfigureBone(bottomBone, finger.bottomBone);
            ConfigureBone(middleBone, finger.middleBone);
            ConfigureBone(topBone, finger.topBone);

            ConfigureJoint(baseJoint, finger.BaseJoint);
            ConfigureJoint(middleJoint, finger.MiddleJoint);
            ConfigureJoint(topJoint, finger.TopJoint);
            ConfigureJoint(tip, finger.Tip);
        }

        private void ConfigureBone(Transform boneTransform, Bone boneData)
        {
            if (boneTransform == null)
            {
                return;
            }

            boneTransform.position = boneData.Center;
            boneTransform.rotation = Quaternion.FromToRotation(Vector3.up, boneData.Direction);
            boneTransform.localScale = new Vector3(boneTransform.localScale.x, boneData.Length * boneScaleFactor, boneTransform.localScale.z);
        }

        private static void ConfigureJoint(Transform jointTransfrom, Vector3 jointPosition)
        {
            if (jointTransfrom == null)
            {
                return;
            }

            jointTransfrom.position = jointPosition;
        }
    }
}
