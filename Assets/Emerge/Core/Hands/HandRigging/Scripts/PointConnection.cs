﻿using Photon.Pun;
using UnityEngine;

namespace Emerge
{
    [ExecuteInEditMode]
    public class PointConnection : MonoBehaviour
    {
        public Transform point1;
        public Transform point2;

        public float scaleFactor = 1f;
        //Lerp transform
        [SerializeField] bool isLerping = true;
        [SerializeField] float positionLerpSpeed = 5f;
        [SerializeField] float rotationLerpSpeed = 5f;
        [SerializeField] PhotonView photonView;

        private void Awake()
        {
            photonView = transform.root.GetComponent<PhotonView>();
        }

        private void Update()
        {
            if (point1 == null || point2 == null)
            {
                return;
            }

            Vector3 center = (point1.position + point2.position) / 2f;
            Vector3 direction = (point1.position - point2.position).normalized;
            float length = (point1.position - point2.position).magnitude;

            if (!isLerping || (photonView && photonView.IsMine) || !photonView)
            {
                transform.position = center;
                transform.rotation = Quaternion.FromToRotation(Vector3.up, direction);
            }
            else
            {
                transform.position = Vector3.Lerp(transform.position, center, positionLerpSpeed * Time.deltaTime);
                transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.FromToRotation(Vector3.up, direction), rotationLerpSpeed * Time.deltaTime);
            }
            transform.localScale = new Vector3(transform.localScale.x, length * scaleFactor, transform.localScale.z);
        }
    }
}
