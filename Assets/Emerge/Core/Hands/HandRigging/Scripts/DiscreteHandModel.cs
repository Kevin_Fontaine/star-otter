﻿using System;
using UnityEngine;

namespace Emerge.Tracking
{
    public class DiscreteHandModel : AbstractHandModel
    {
        public GameObject wristInner;
        public GameObject wristCenter;
        public GameObject wristOuter;
        public GameObject palmCenter;

        public RiggedFinger thumb;
        public RiggedFinger index;
        public RiggedFinger middle;
        public RiggedFinger ring;
        public RiggedFinger little;

        public bool scalePalm;


        #region Overrides

        override public void SetHandData(Hand hand)
        {
            if (hand == null)
            {
                return;
            }

            if (palmCenter != null)
            {
                palmCenter.transform.position = hand.palm.center;
                palmCenter.transform.rotation = hand.GetRotation();

                if (scalePalm)
                {
                    palmCenter.transform.localScale = new Vector3(
                        hand.palm.radius,
                        hand.palm.radius,
                        palmCenter.transform.localScale.z
                    );
                }
            }

            SetPosition(wristInner, hand.wrist.innerEdge);
            SetPosition(wristCenter, hand.wrist.center);
            SetPosition(wristOuter, hand.wrist.outerEdge);

            SetupFinger(thumb, hand.fingers.thumb);
            SetupFinger(index, hand.fingers.index);
            SetupFinger(middle, hand.fingers.middle);
            SetupFinger(ring, hand.fingers.ring);
            SetupFinger(little, hand.fingers.little);
        }

        #endregion

        private static void SetPosition(GameObject gameObject, Vector3 position)
        {
            if (gameObject == null)
            {
                return;
            }

            gameObject.transform.position = position;
        }

        private static void SetupFinger(RiggedFinger riggedFinger, Digit digit)
        {
            if (riggedFinger == null)
            {
                return;
            }

            riggedFinger.SetFinger(digit);
        }
    }
}
