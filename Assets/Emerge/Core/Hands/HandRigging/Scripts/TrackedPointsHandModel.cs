﻿using UnityEngine;

namespace Emerge.Tracking
{
    public class TrackedPointsHandModel : AbstractHandModel
    {
        private const int NUMBER_OF_POINTS = 24;

        [SerializeField]
        private GameObject pointPrefab;

        private readonly GameObject[] pointGameObjects = new GameObject[NUMBER_OF_POINTS];


        virtual protected void Awake()
        {
            for (int i = 0; i < NUMBER_OF_POINTS; i++)
            {
                pointGameObjects[i] = Instantiate(pointPrefab, transform);
            }
        }

        #region Overrides

        override public void SetHandData(Hand hand)
        {
            int index = 0;
            foreach (Vector3 point in hand)
            {
                pointGameObjects[index].transform.position = point;
                index++;
            }
        }

        #endregion
    }
}
