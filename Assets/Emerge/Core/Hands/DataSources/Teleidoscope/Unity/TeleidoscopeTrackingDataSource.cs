﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Emerge.Tracking
{
    [RequireComponent(typeof(SpaceTransformation))]
    public class TeleidoscopeTrackingDataSource : MonoBehaviour, IHandTrackingDataSource
    {
        private const string ID = "teleidoscope";


        private CircularReuseBuffer<HandSet> handSetBuffer;
        private TrackingFrequency trackingFrequency;

        [SerializeField]
        private bool startTrackingOnAwake = true;


        #region Unity Methods        

        public void RegisterDataSource(Type grabGestureType)
        {
            HandTracking.RegisterDataSource(ID, this, isDefault: true, grabGestureType : grabGestureType);

            if (startTrackingOnAwake)
            {
                HandTracking.Default.StartTracking();
            }
        }

        private void LateUpdate()
        {
            if (trackingFrequency == TrackingFrequency.GraphicsFrame ||
                trackingFrequency == TrackingFrequency.GraphicsAndPhysicsFrames)
            {
                GetNewHandSet();
            }
        }

        private void FixedUpdate()
        {
            if (trackingFrequency == TrackingFrequency.PhysicsFrame ||
                trackingFrequency == TrackingFrequency.GraphicsAndPhysicsFrames)
            {
                GetNewHandSet();
            }
        }

        #endregion


        #region IHandTrackingDataSource

        public void SetHandsBuffer(CircularReuseBuffer<HandSet> buffer)
        {
            handSetBuffer = buffer;
        }

        public bool StartTracking()
        {
            Debug.LogFormat("Teleidoscope: starting hand tracking");
            TioCV.NativeHandTracker.Start();
            return true;
        }

        public void StopTracking()
        {
            Debug.LogFormat("Teleidoscope: stopping hand tracking");
            TioCV.NativeHandTracker.Stop();
        }

        public void SetTrackingFrequency(TrackingFrequency frequency)
        {
            trackingFrequency = frequency;
        }

        #endregion


        private void GetNewHandSet()
        {
            if (handSetBuffer == null)
            {
                return;
            }

            handSetBuffer.Push(BufferUpdateAction);
        }

        private CircularReuseBuffer<HandSet>.PushAction BufferUpdateAction(HandSet handSetToUpdate)
        {
            TioCV.NativeHandTrackerTypes.NativeHandSet nativeSet = TioCV.NativeHandTracker.FetchLatest3D();
            handSetToUpdate.UpdateFromNativeData(nativeSet);
            handSetToUpdate.ConvertToLocalSpace(GetComponent<SpaceTransformation>());

            return CircularReuseBuffer<HandSet>.PushAction.FinishPush;
        }        
    }
}
