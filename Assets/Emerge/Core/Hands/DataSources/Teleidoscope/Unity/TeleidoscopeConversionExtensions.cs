﻿using UnityEngine;
using TioCV;

namespace Emerge.Tracking
{
    public static class TeleidoscopeConversionExtensions
    {
        public static void UpdateFromNativeData(this HandSet handSet, NativeHandTrackerTypes.NativeHandSet nativeData)
        {
            handSet.left.UpdateFromNativeData(nativeData.left);
            handSet.right.UpdateFromNativeData(nativeData.right);
        }

        public static void UpdateFromNativeData(this Hand hand, NativeHandTrackerTypes.NativeHand nativeHand)
        {
            hand.fingers.UpdateFromNativeData(nativeHand.fingers);
            hand.palm.UpdateFromNativeData(nativeHand.palm);
            hand.wrist.UpdateFromNativeData(nativeHand.wrist);
            hand.bounds = nativeHand.bounds.ToBounds();
            hand.type = nativeHand.type.ToUnityChirality();
            hand.uid = nativeHand.uid;
            hand.rotation = nativeHand.rotation;
            hand.active = nativeHand.active;
        }

        public static void UpdateFromNativeData(this Fingers fingers, NativeHandTrackerTypes.NativeFingers nativeFingers)
        {
            fingers.index.UpdateFromNativeData(nativeFingers.index);
            fingers.middle.UpdateFromNativeData(nativeFingers.middle);
            fingers.ring.UpdateFromNativeData(nativeFingers.ring);
            fingers.little.UpdateFromNativeData(nativeFingers.little);
            fingers.thumb.UpdateFromNativeData(nativeFingers.thumb);
        }

        public static void UpdateFromNativeData(this Wrist wrist, NativeHandTrackerTypes.NativeWrist nativeWrist)
        {
            wrist.innerEdge = nativeWrist.innerEdge.ToVector3();
            wrist.center = nativeWrist.center.ToVector3();
            wrist.outerEdge = nativeWrist.outerEdge.ToVector3();
        }

        public static void UpdateFromNativeData(this Palm palm, NativeHandTrackerTypes.NativePalm nativePalm)
        {
            palm.radius = nativePalm.radius;
            palm.center = nativePalm.center.ToVector3();
        }

        public static void UpdateFromNativeData(this Digit digit, NativeHandTrackerTypes.NativeDigit nativeDigit)
        {
            digit.Configure(
                nativeDigit.baseJoint.ToVector3(), 
                nativeDigit.middleJoint.ToVector3(), 
                nativeDigit.topJoint.ToVector3(), 
                nativeDigit.tip.ToVector3()
            );
        }

        public static Vector3 ToVector3(this NativeHandTrackerTypes.NativeVec3D nativeVector)
        {
            return new Vector3(nativeVector.x, nativeVector.y, nativeVector.z);
        }

        public static Bounds ToBounds(this NativeHandTrackerTypes.NativeBox3D nativeBox)
        {
            return new Bounds(
                nativeBox.center.ToVector3(),
                new Vector3(nativeBox.width, nativeBox.height, nativeBox.length) // TODO VALIDATE
            );
        }

        public static Chirality ToUnityChirality(this NativeHandTrackerTypes.NativeHandType nativeType)
        {
            Chirality unityValue = Chirality.Unknown;
            switch (nativeType)
            {
                case NativeHandTrackerTypes.NativeHandType.LEFT_HAND:
                    unityValue = Chirality.Left;
                    break;

                case NativeHandTrackerTypes.NativeHandType.UNKNOWN_HAND:
                    unityValue = Chirality.Unknown;
                    break;

                case NativeHandTrackerTypes.NativeHandType.RIGHT_HAND:
                    unityValue = Chirality.Right;
                    break;
            }

            return unityValue;
        }
    }
}
