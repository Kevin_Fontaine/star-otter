﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;
using System;
namespace TioCV
{
    public static class NativeHandTracker
    {
        /** @brief Starts the hand tracking engine
         */
     public static void Start()
     {

         StartHandTracker();
            SetModelUpdateInterval(3);
            SetUseRigidTransforms(true);
            SetOverPredictionEnabled(true);
     }
        /** @brief Stops the hand tracking engine
        */
        public static void Stop()
     {
         StopHandTracker();
     }
     

     /** @brief Sets the prediction mode on the tracker.
      * 
      * Prediction modes effect the frequency updates can be generated.
      * NativeHandTrackerPredictionMode.ACCURATE_PREDICTION means process the latest frame possible and do not forward predict on new frames until the model has finished. 
      * This mode will always wait for the model to finish before making the results available to unity.
      *  
      * NativeHandTrackerPredictionMode.FAST_PREDICTION means process the latest frame possible and forward predict before attempting to sync with the model before timing out.
      *  If a time out occurs the forward predicted result will be returned unless new frames have arrived in which case another forward prediction is performed.
      *  
      * NativeHandTrackerPredictionMode.FASTER_PREDICTION is similiar to FAST_PREDICTION but with a shorter model sync timeout
      * 
      *  NativeHandTrackerPredictionMode.FASTEST_PREDICTION means always forward predict an never wait for model sync.
      *  If a model sync occurs at the same time as a prediction, the results will be merged. This results in very smooth update at the cost of accuracy in between model updates.
      *  
      *  This mode will also enable frame interpolation allowing it to produce results above the device frame rate. 
      *
      * After changing the prediction mode, results may be unreliable for a few frames.
      * In most cases the prediction mode should only be set once based on your applications needs.
      *
      */

        public static void SetPredictionMode(NativeHandTrackerTypes.NativeHandTrackerPredictionMode optimizationMode)
        {
            SetOptimizationMode(optimizationMode);
        }
        public static NativeHandTrackerTypes.NativeHandTrackerPredictionMode GetPredictionMode()
        {
            return GetOptimizationMode();
        }
        /** @brief Fetches the current activity status for each hand.
        * This is identical to checking the active status of each hand after a call to FetchLatest()
        * In most cases it is better to just call GetLatestHandSet() and check TIOHandSet.[left/right].active status 
        * however this function can be used to check activity status without marshalling the full hand update.
        * @note Calling GetHandActivityEvents() immediately prior to GetLatestHandSet().
        */
        public static NativeHandTrackerTypes.NativeHandActivityEvent GetHandActivityEvent()
        {
            return GetHandActivityEvents();
        }
        /** @brief Fetches the latest NativeHandSet produced by the hand tracking engine in screen space.
         * 
         * This function will return the latest  status and pose for each hand.
         * The hand tracking engine runs parallel to Unitys render loop, multiple calls to this function in same update frame may result in different values if new data has arrived. 
         * @note This function will attempt to mimimize blocking but it can not be guarenteed. It is best to call in a background thread or in LateUpdate()  
         * @note This function returns normalized screenspace coordinates for x and y. Z is mapped to depth.
         * Call this function to retrieve the latest results produced from the engine. 
         */
        public static NativeHandTrackerTypes.NativeHandSet FetchLatest()
        {
            return GetLatestHandSet();
        }

        /** @brief Identical to the results of FetchLatest but mapped into a 3D coordinate space relative to the depth camera. 
         * 
         * 
         */
        public static NativeHandTrackerTypes.NativeHandSet FetchLatest3D()
        {
            return GetLatestHandSet3D();
        }

        /** @brief Enables expirimental smoothing 
        * 
        * 
        */
        public static void EnableSmoothing()
        {
            EnableLateralSmoothing();
        }

        /** @brief Disables expirimental smoothing 
        * 
        * 
        */
        public static void DisableSmoothing()
        {
            DisableLateralSmoothing();
        }

        [DllImport("tiounity")]
        private static extern void StartHandTracker();
        [DllImport("tiounity")]
        private static extern void StopHandTracker();

        [DllImport("tiounity")]
        private static extern void SetOptimizationMode(NativeHandTrackerTypes.NativeHandTrackerPredictionMode mode);

        [DllImport("tiounity")]
        private static extern NativeHandTrackerTypes.NativeHandTrackerPredictionMode GetOptimizationMode();

        [DllImport("tiounity")]
        private static extern NativeHandTrackerTypes.NativeHandActivityEvent GetHandActivityEvents();
       
        [DllImport("tiounity")]
        private static extern NativeHandTrackerTypes.NativeHandSet GetLatestHandSet();

      
        [DllImport("tiounity")]
        private static extern NativeHandTrackerTypes.NativeHandSet GetLatestHandSet3D();

 
        [DllImport("tiounity")]
        private static extern void EnableAutoCalibration();
        
        [DllImport("tiounity")]
        private static extern void DisableAutoCalibration();

 
        [DllImport("tiounity")]
        private static extern void SetModelUpdateInterval(int interval);

        [DllImport("tiounity")]
        private static extern int GetModelUpdateInterval();

        [DllImport("tiounity")]
        private static extern void SetCalibrationValues(float min_brightness, float max_brightness,float min_dark_clip, float max_dark_clip);



        [DllImport("tiounity")]
        private static extern int EnableLateralSmoothing();


        [DllImport("tiounity")]
        private static extern int DisableLateralSmoothing();
 
        [DllImport("tiounity")]
        private static extern void SetUseRigidTransforms(bool val);
        
        [DllImport("tiounity")]
        private static extern bool UseRigidTransforms();

        [DllImport("tiounity")]
        private static extern void SetOverPredictionEnabled(bool val);

        [DllImport("tiounity")]
        private static extern bool OverPredictionEnabled();
    }
}