﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;
using System;
 
public class HandTrackerBridge : MonoBehaviour
{
   

    void Start()
    {
        TioCV.NativeHandTracker.Start();
    }
    void Stop() 
    {
        TioCV.NativeHandTracker.Stop();
    }
    // Update is called once per frame
    void Update()
    {
      
        var hands = TioCV.NativeHandTracker.FetchLatest3D();
        // var hands2d = TioCV.NativeHandTracker.FetchLatest();
   
        if (hands.left.active)
        {
            Debug.Log("Left Hand Active");
        }
        if (hands.right.active)
        {
            Debug.Log("Right Hand Active");
        }
        
        Debug.Log("hands:" + hands.ToString());

    }
     
     
}
