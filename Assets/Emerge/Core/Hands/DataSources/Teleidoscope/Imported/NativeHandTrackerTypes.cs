using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;
using System;
namespace TioCV
{
    public static class NativeHandTrackerTypes
    {
        public enum NativeHandTrackerPredictionMode : int
        {
            ACCURATE_PREDICTION = 0,
            FAST_PREDICTION = 1,
            FASTER_PREDICTION = 2,
            FASTEST_PREDICTION = 3,
        };

        /*Native
        NOTE: These are the raw native values relative to the depth feed.
        I.e: X and Y are normalized relative to depth.width and depth.height and Z is millimeters
        */
        public enum NativeHandType
        {
            LEFT_HAND = -1,
            UNKNOWN_HAND = 0,//Set when the hand type can not be determined (i.e only one hand visible and balled into a fist)
            RIGHT_HAND = 1,

        };

        public enum NativeHandActivityEvent
        {
            NO_HANDS_ACTIVE = 0,
            LEFT_HAND_ACTIVE = 1,
            RIGHT_HAND_ACTIVE = 2,
            BOTH_HANDS_ACTIVE = 3,
        };
        public struct NativeVec3D
        {
            public float x;
            public float y;
            public float z;
            public override string ToString()
            {
                return "[" + x + ", " + y + ", " + z + "]";
            }
        }

        public struct NativeDepthRange
        {
            public float min;
            public float max;
            public float units;  // units per meter  (i.e 1000 means that min and max are in  millimeters)
            public override string ToString()
            {
                return "{ \"min\" : " + min + ", \"max\" : " + max + ", \"units\" : " + max + "}";
            }
        }

        public struct NativeBox3D
        {
            public NativeVec3D center;
            public float width;
            public float height;
            public float length;
            public override string ToString()
            {
                return "{ \"center\": " + center.ToString() + ", \"WxHxL\" : [" + width + ", " + height + ", " + length + "]" + "}";
            }
        }

        public struct NativePalm
        {
            /** @brief Center of the palm */
            public NativeVec3D center;

            /** @brief Radius of the circle enclosing the palm, normalized to the hypot of the image */
            public float radius;

            public override string ToString()
            {
                return "{ \"center\" : " + center.ToString() + ", \"radius\" : " + radius + "}";
            }
        }
        public struct NativeWrist
        {
            /** @brief A point on the edge of the wrist nearest to the body*/
            public NativeVec3D innerEdge;
            /** @brief The center of the wrist */

            public NativeVec3D center;
            /** @brief A point on the edge of the wrist furthest from the body */

            public NativeVec3D outerEdge;

            public override string ToString()
            {
                return "{ \"innerEdge\" : " + innerEdge.ToString() + ", \"center\" : " + center.ToString() + ", \"outerEdge\" : " + outerEdge.ToString() + "}";
            }
        }
        public struct NativeDigit
        {
            /**@brief Tip of the finger*/
            public NativeVec3D tip;
            /**@brief Joint nearest the tip (DIP)*/
            public NativeVec3D topJoint;
            /**@brief Joint nearest the base (PIP)*/
            public NativeVec3D middleJoint;
            /**@brief Joint nearest the base (MCP)*/
            public NativeVec3D baseJoint;
            public override string ToString()
            {
                return "{ \"tip\": " + tip.ToString() + ", \"topJoint\" : " + topJoint.ToString() + ", \"middleJoint\" : " + middleJoint.ToString() + ", \"baseJoint\" : " + baseJoint.ToString() + "}";
            }
        }
        public struct NativeFingers
        {
            /**@brief The index/pointer finger*/
            public NativeDigit index;

            /**@brief The middle finger*/
            public NativeDigit middle;

            /**@brief The ring/fourth finger*/
            public NativeDigit ring;

            /**@brief The little/pinky finger*/
            public NativeDigit little;

            /**@brief The thumb*/
            public NativeDigit thumb;

            public override string ToString()
            {
                return "{ \"index\" : " + index.ToString() + ", \"middle\" : " + middle.ToString() + ", \"ring\" : " + ring.ToString() + ", \"little\" : " + little.ToString() + ", \"thumb\" : " + thumb.ToString() + "}";
            }
        }

        public struct NativeHand
        {
            /**@brief Fingers and thumb of the the hand*/
            public NativeFingers fingers;

            /**@brief The palm of the hand*/
            public NativePalm palm;

            /**@brief The wrist the hand is connect to.*/
            public NativeWrist wrist;

            /**@brief The box enclosing the hand*/
            public NativeBox3D bounds;

            /**@brief Specifies the hand type (left or right)*/
            public NativeHandType type;

            /**@brief A unique id for the hand. */
            public int uid;

            /**@brief 2d rotation of the hand in radians relative to the wrist and finger base joints*/
            public float rotation;

            /**@brief Hand is active. */
            [MarshalAs(UnmanagedType.Bool)] public bool active;

            public override string ToString()
            {
                return "{ \"type\" : \"" + type.ToString() + "\", \"fingers\" : " + fingers.ToString() + ", \"palm\" : " + palm.ToString() + ", \"wrist\" : " + wrist.ToString() + ", \"bounds\" : " + bounds.ToString() + ", \"uid\" : " + uid + ", \"active\" : " + ((active == true) ? "true" : "false") + "}";
            }
        }

        public struct NativeCoordinateSpace
        {
            /**@brief width of the depth map dimensions*/
            public int width;

            /**@brief height of the depth map dimensions*/
            public int height;

            /**@brief range of depth map values*/
            public NativeDepthRange zRange;

            public override string ToString()
            {
                return "{ \"width\" : " + width + ", \"height\" : " + height + ", \"zRange\" : " + zRange.ToString() + "}";
            }
        }

        public struct NativeHandSet
        {
            public NativeHand left;
            public NativeHand right;
            public NativeCoordinateSpace coordinateSpace;
            public int updateFrameNumber;
            public override string ToString()
            {
                return "{ \"updateFrameNumber\" : " + updateFrameNumber + ", \"left\" : " + left.ToString() + ", \"right\" : " + right.ToString() + ", \"coordinateSpace\" : " + coordinateSpace.ToString() + "}";
            }
        }

    }

}
