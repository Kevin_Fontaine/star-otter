﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Emerge;
using Emerge.Tracking;

public class UDPMessageHandDataSource : IHandTrackingDataSource
{
    private bool ignoreMessages = false;
    private CircularReuseBuffer<HandSet> handSetBuffer;


    #region Lifecycle

    public UDPMessageHandDataSource(string id, bool isDefault)
    {
        HandTracking.RegisterDataSource(id, this, isDefault);
    }

    #endregion


    #region UDP

    public void DecodeMessage(Ceras.GeneratedFormatters.Hand leftHandMessage, Ceras.GeneratedFormatters.Hand rightHandMessage, SpaceTransformation transformation)
    {
        if (ignoreMessages)
        {
            return;
        }

        handSetBuffer.Push((HandSet handSetToUpdate) =>
        {
            leftHandMessage.DecompressData(handSetToUpdate.left, transformation);
            rightHandMessage.DecompressData(handSetToUpdate.right, transformation);

            return CircularReuseBuffer<HandSet>.PushAction.FinishPush;
        });
    }

    #endregion


    #region IHandTrackingDataSource

    public void SetHandsBuffer(CircularReuseBuffer<HandSet> buffer)
    {
        handSetBuffer = buffer;
    }

    public bool StartTracking()
    {
        ignoreMessages = false;
        return true;
    }

    public void StopTracking()
    {
        ignoreMessages = true;
    }

    public void SetTrackingFrequency(TrackingFrequency frequency)
    {
        // noop - data frequency is driven by when a message is received
    }

    #endregion
}
