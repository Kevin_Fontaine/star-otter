﻿
using UnityEngine;
using Leap;

namespace Emerge.Tracking
{
    public static class HandSetLeapExtensions
    {
        public static void UpdateFromLeapFrame(this HandSet handSet, Frame leapFrame)
        {
            handSet.left.active = false;
            handSet.right.active = false;

            if (leapFrame.Hands == null || leapFrame.Hands.Count == 0)
            {
                return;
            }

            foreach (Leap.Hand leapHand in leapFrame.Hands)
            {
                Hand hand;
                if (leapHand.IsLeft)
                {
                    hand = handSet.left;
                }
                else
                {
                    hand = handSet.right;
                }

                try
                {
                    UpdateHand(hand, leapHand);
                }
                catch
                {
                    hand.active = false;
                }
            }
        }

        private static void UpdateHand(Hand hand, Leap.Hand leapHand)
        {
            hand.active = true;

            hand.wrist.center = leapHand.WristPosition.ToUnityVector3();
            hand.wrist.outerEdge = hand.wrist.center; // TODO REVISIT//////////////
            hand.wrist.innerEdge = hand.wrist.center; // TODO REVISIT//////////////


            // Moves the palm position from the default leap motion palm tracking, to a position in between the wrist center and palm center
            hand.palm.center = (leapHand.WristPosition.ToUnityVector3() + leapHand.PalmPosition.ToUnityVector3()) * 0.5f;
            hand.palm.radius = Vector3.Magnitude(hand.palm.center - hand.wrist.center);

            foreach (Finger leapFinger in leapHand.Fingers)
            {
                switch (leapFinger.Type)
                {
                    case Finger.FingerType.TYPE_INDEX:
                        UpdateFinger(hand.fingers.index, leapFinger);
                        break;

                    case Finger.FingerType.TYPE_MIDDLE:
                        UpdateFinger(hand.fingers.middle, leapFinger);
                        break;

                    case Finger.FingerType.TYPE_PINKY:
                        UpdateFinger(hand.fingers.little, leapFinger);
                        break;

                    case Finger.FingerType.TYPE_RING:
                        UpdateFinger(hand.fingers.ring, leapFinger);
                        break;

                    case Finger.FingerType.TYPE_THUMB:
                        UpdateFinger(hand.fingers.thumb, leapFinger);
                        break;

                    default:
                        break;
                }
            }
        }

        private static void UpdateFinger(Digit finger, Leap.Finger leapFinger)
        {
            finger.Tip = leapFinger.TipPosition.ToUnityVector3();
            finger.TopJoint = leapFinger.Bone(Leap.Bone.BoneType.TYPE_DISTAL).PrevJoint.ToUnityVector3();
            finger.MiddleJoint = leapFinger.Bone(Leap.Bone.BoneType.TYPE_INTERMEDIATE).PrevJoint.ToUnityVector3();
            finger.BaseJoint = leapFinger.Bone(Leap.Bone.BoneType.TYPE_PROXIMAL).PrevJoint.ToUnityVector3();
        }

        private static Vector3 ToUnityVector3(this Leap.Vector leapVector)
        {
            // Convert mm to meters here too
            return new Vector3(leapVector.x / 1000f, leapVector.y / 1000f, leapVector.z / 1000f);
        }
    }
}
