﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Leap;

namespace Emerge.Tracking
{
    [RequireComponent(typeof(SpaceTransformation))]
    public class LeapTrackingDataSource : MonoBehaviour, IHandTrackingDataSource
    {
        private const string ID = "leap";

        private Controller leapCtrl;
        private CircularReuseBuffer<HandSet> handSetBuffer;
        private TrackingFrequency trackingFrequency;


        #region Unity Methods

        private void Awake()
        {
            leapCtrl = new Controller();
        }

        public void RegisterDataSource(Type grabGestureType)
        {
            HandTracking.RegisterDataSource(ID, this, isDefault: true, grabGestureType : grabGestureType);
            HandTracking.Default.StartTracking();
        }

        private void LateUpdate()
        {
            if (trackingFrequency == TrackingFrequency.GraphicsFrame ||
                trackingFrequency == TrackingFrequency.GraphicsAndPhysicsFrames)
            {
                GetNewHandSet();
            }
        }

        private void FixedUpdate()
        {
            if (trackingFrequency == TrackingFrequency.PhysicsFrame ||
                trackingFrequency == TrackingFrequency.GraphicsAndPhysicsFrames)
            {
                GetNewHandSet();
            }
        }

        #endregion


        #region IHandTrackingDataSource

        public void SetHandsBuffer(CircularReuseBuffer<HandSet> buffer)
        {
            handSetBuffer = buffer;
        }

        public bool StartTracking()
        {
            Debug.LogFormat("LeapMotion: starting hand tracking");
            leapCtrl.StartConnection();
            return true;
        }

        public void StopTracking()
        {
            leapCtrl.StopConnection();
            Debug.LogFormat("LeapMotion: stopping hand tracking");
        }

        public void SetTrackingFrequency(TrackingFrequency frequency)
        {
            trackingFrequency = frequency;
        }

        #endregion


        private void GetNewHandSet()
        {
            if (handSetBuffer == null)
            {
                return;
            }

            handSetBuffer.Push(BufferUpdateAction);
        }

        private CircularReuseBuffer<HandSet>.PushAction BufferUpdateAction(HandSet handSetToUpdate)
        {
            Frame leapFrame = leapCtrl.Frame();
            if (leapFrame == null)
            {
                return CircularReuseBuffer<HandSet>.PushAction.CancelPush;
            }

            handSetToUpdate.UpdateFromLeapFrame(leapFrame);
            handSetToUpdate.ConvertToLocalSpace(GetComponent<SpaceTransformation>());

            return CircularReuseBuffer<HandSet>.PushAction.FinishPush;
        }
    }
}
