﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Emerge.Tracking
{
    public static class HandSetOculusExtensions
    {
        public static void UpdateFromOculusFrame(this HandSet handSet, OVRSkeleton left, OVRSkeleton right)
        {
            handSet.left.active = false;
            handSet.right.active = false;

            if (left == null || right == null)
            {
                return;
            }

            Hand hand;
            if (left.isActiveAndEnabled)
            {
                hand = handSet.left;
                try
                {
                    UpdateHand(hand, left);
                }
                catch
                {
                    hand.active = false;
                }
            }
            if (right.isActiveAndEnabled)
            {
                hand = handSet.right;
                try
                {
                    UpdateHand(hand, right);
                }
                catch
                {
                    hand.active = false;
                }
            }
        }

        private static void UpdateHand(Hand hand, OVRSkeleton oculusHand)
        {
            hand.active = true;

            hand.wrist.center = oculusHand.Bones[(int)OVRSkeleton.BoneId.Hand_WristRoot].Transform.position;
            hand.wrist.outerEdge = hand.wrist.center; // TODO REVISIT//////////////
            hand.wrist.innerEdge = hand.wrist.center; // TODO REVISIT//////////////
            hand.palm.center = (oculusHand.Bones[(int)OVRSkeleton.BoneId.Hand_Pinky1].Transform.position + oculusHand.Bones[(int)OVRSkeleton.BoneId.Hand_Thumb1].Transform.position) * 0.5f;// oculusHand.Bones[(int)OVRSkeleton.BoneId.Hand_Thumb1].Transform.position;// + pinkyToThumbDist.normalized * distance;
            hand.palm.radius = Vector3.Magnitude(hand.palm.center - hand.wrist.center);
            UpdateFinger(hand.fingers.index, oculusHand, OVRSkeleton.BoneId.Hand_IndexTip);
            UpdateFinger(hand.fingers.middle, oculusHand, OVRSkeleton.BoneId.Hand_MiddleTip);
            UpdateFinger(hand.fingers.little, oculusHand, OVRSkeleton.BoneId.Hand_PinkyTip);
            UpdateFinger(hand.fingers.ring, oculusHand, OVRSkeleton.BoneId.Hand_RingTip);
            UpdateFinger(hand.fingers.thumb, oculusHand, OVRSkeleton.BoneId.Hand_ThumbTip);
        }

        private static void UpdateFinger(Digit finger, OVRSkeleton oculusHand, OVRSkeleton.BoneId boneId)
        {
            OVRBone ovrBone = oculusHand.Bones[(int)boneId];
            finger.Tip = ovrBone.Transform.position;
            ovrBone = oculusHand.Bones[ovrBone.ParentBoneIndex];
            finger.TopJoint = ovrBone.Transform.position;
            ovrBone = oculusHand.Bones[ovrBone.ParentBoneIndex];
            finger.MiddleJoint = ovrBone.Transform.position;
            ovrBone = oculusHand.Bones[ovrBone.ParentBoneIndex];
            finger.BaseJoint = ovrBone.Transform.position;
        }

        private static Vector3 ToUnityVector3(this Leap.Vector leapVector)
        {
            // Convert mm to meters here too
            return new Vector3(leapVector.x / 1000f, leapVector.y / 1000f, leapVector.z / 1000f);
        }
    }
}
