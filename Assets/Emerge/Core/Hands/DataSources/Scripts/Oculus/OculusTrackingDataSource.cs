﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static OVRHand;

namespace Emerge.Tracking
{
    [RequireComponent(typeof(SpaceTransformation))]
    public class OculusTrackingDataSource : MonoBehaviour, IHandTrackingDataSource
    {
        private const string ID = "oculus";

        [SerializeField]
        private OVRSkeleton leftHandSkeleton;
        [SerializeField]
        private OVRSkeleton rightHandSkeleton;
        private CircularReuseBuffer<HandSet> handSetBuffer;
        private TrackingFrequency trackingFrequency;


        #region Unity Methods

        private void Awake()
        {
            leftHandSkeleton = GameObject.Find("OVRCameraRig/TrackingSpace/LeftHandAnchor/OVRHandPrefab").GetComponent<OVRSkeleton>();
            rightHandSkeleton = GameObject.Find("OVRCameraRig/TrackingSpace/RightHandAnchor/OVRHandPrefab").GetComponent<OVRSkeleton>();
        }

        public void RegisterDataSource(Type grabGestureType)
        {
            HandTracking.RegisterDataSource(ID, this, isDefault: true, grabGestureType: grabGestureType);
            HandTracking.Default.StartTracking();
        }

        private void LateUpdate()
        {
            if (trackingFrequency == TrackingFrequency.GraphicsFrame ||
                trackingFrequency == TrackingFrequency.GraphicsAndPhysicsFrames)
            {
                GetNewHandSet();
            }
        }

        private void FixedUpdate()
        {
            if (trackingFrequency == TrackingFrequency.PhysicsFrame ||
                trackingFrequency == TrackingFrequency.GraphicsAndPhysicsFrames)
            {
                GetNewHandSet();
            }
        }

        #endregion


        #region IHandTrackingDataSource

        public void SetHandsBuffer(CircularReuseBuffer<HandSet> buffer)
        {
            handSetBuffer = buffer;
        }

        public bool StartTracking()
        {
            Debug.LogFormat("Oculus: starting hand tracking");
            return true;
        }

        public void StopTracking()
        {
            Debug.LogFormat("Oculus: stopping hand tracking");
        }

        public void SetTrackingFrequency(TrackingFrequency frequency)
        {
            trackingFrequency = frequency;
        }

        #endregion


        private void GetNewHandSet()
        {
            if (handSetBuffer == null)
            {
                return;
            }

            handSetBuffer.Push(BufferUpdateAction);
        }

        private CircularReuseBuffer<HandSet>.PushAction BufferUpdateAction(HandSet handSetToUpdate)
        {
            handSetToUpdate.UpdateFromOculusFrame(leftHandSkeleton, rightHandSkeleton);
            //handSetToUpdate.ConvertToLocalSpace(GetComponent<SpaceTransformation>());

            return CircularReuseBuffer<HandSet>.PushAction.FinishPush;
        }
    }
}
