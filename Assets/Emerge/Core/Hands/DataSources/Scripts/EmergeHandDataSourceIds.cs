﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class EmergeHandDataSourceIds
{
    public const string REMOTE = "Emerge.RemoteHands";
    public const string UDP_LOCAL = "Emerge.UDPLocalHands";
    public const string UDP_REMOTE = "Emerge.UDPRemoteHands";
}
