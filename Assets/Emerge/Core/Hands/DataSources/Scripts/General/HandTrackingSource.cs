﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Emerge.Tracking
{
    using Core;

    public class HandTrackingSource : MonoBehaviour
    {
        public enum GestureSources
        {
            LeapMotion,
            Default
        }
#if !UNITY_WSA
        [SerializeField]
        private bool useGestures = true;

        private GestureSources handGestureSource;

        private void Start()
        {
        }
        private void Awake()
        {
            SetSource();

            if (useGestures)
            {
                HandTracking.Default.Gestures.StartListeningForAllGestures();
            }
        }

        private void SetSource()
        {
#if UNITY_EDITOR
            SetHandTracking(TactileConfig.Instance.EditorHandTrackingSource);
#else
            if (TactileConfig.Instance.TargetPlatform == Platforms.PC_Mac_Linux)
            {
                SetBaseStationHandTracking(TactileConfig.Instance.HandTrackingSource);
            }
            else if (TactileConfig.Instance.TargetPlatform == Platforms.Oculus_Quest)
            {
                SetHandTracking(HandHardware.Oculus);
            }
#endif
        }
        private void SetHandTracking(HandHardware handHardware)
        {
            switch (handHardware)
            {
                case HandHardware.LeapMotion:
                    InitLeapmotionHandTracking();
                    break;
                case HandHardware.Realsense:
                    InitRealsenseHandTracking();
                    break;
                case HandHardware.Oculus:
                    InitOculusHandTracking();
                    break;
            }
        }

        private void SetBaseStationHandTracking(BaseStationHandHardware handHardware)
        {
            switch (handHardware)
            {
                case BaseStationHandHardware.LeapMotion:
                    InitLeapmotionHandTracking();
                    break;
                case BaseStationHandHardware.Realsense:
                    InitRealsenseHandTracking();
                    break;
            }
        }

        void InitLeapmotionHandTracking()
        {
            handGestureSource = GestureSources.LeapMotion;
            AddLeapSource();
        }

        void InitRealsenseHandTracking()
        {
            handGestureSource = GestureSources.Default;
            AddRealsenseSource();
        }

        void InitOculusHandTracking()
        {
            handGestureSource = GestureSources.Default;
            AddOculusSource();
        }

        private void AddLeapSource()
        {
            GameObject child = new GameObject("LEAP Motion");
            child.transform.SetParent(transform);
            SpaceTransformation spaceTransformation = child.AddComponent<SpaceTransformation>();
            spaceTransformation.sourceLocalSpace = FindObjectOfType<HardwareMarkerLeap>().transform;
            spaceTransformation.destinationLocalSpace = FindObjectOfType<HardwareMarkerCenter>().transform;

            LeapTrackingDataSource leapTrackingDataSource = child.AddComponent<LeapTrackingDataSource>();
            leapTrackingDataSource.RegisterDataSource(GetGestureType());
            child.AddComponent<Leap.Unity.LeapServiceProvider>();
        }

        private void AddRealsenseSource()
        {
            GameObject child = new GameObject("Realsense");
            child.transform.SetParent(transform);
            SpaceTransformation spaceTransformation = child.AddComponent<SpaceTransformation>();
            spaceTransformation.sourceLocalSpace = FindObjectOfType<HardwareMarkerRealsense>().transform;
            spaceTransformation.destinationLocalSpace = FindObjectOfType<HardwareMarkerCenter>().transform;

            TeleidoscopeTrackingDataSource teleidoscopeTrackingDataSource = child.AddComponent<TeleidoscopeTrackingDataSource>();
            teleidoscopeTrackingDataSource.RegisterDataSource((GetGestureType()));
        }

        private Type GetGestureType()
        {
            if (useGestures)
            {
                switch (handGestureSource)
                {
                    case GestureSources.LeapMotion:
                        return typeof(LeapMotionGrabGesture);
                    case GestureSources.Default:
                        return typeof(DefaultGrabGesture);
                    default:
                        return null;
                }
            }
            else
            { 
                // If useGestures is disabled at compile time, disable gestures. 
                // ToDo: Make a more dynamic system that controls individual objects' responsiveness
                // to gestures at runtime. 
                return null; 
            }
        }

        private void AddOculusSource()
        {
            GameObject child = new GameObject("Oculus");
            child.transform.SetParent(transform);
            SpaceTransformation spaceTransformation = child.AddComponent<SpaceTransformation>();
            spaceTransformation.sourceLocalSpace = FindObjectOfType<HardwareMarkerOculus>().transform;
            spaceTransformation.destinationLocalSpace = FindObjectOfType<HardwareMarkerCenter>().transform;

            OculusTrackingDataSource oculusTrackingDataSource = child.AddComponent<OculusTrackingDataSource>();
            oculusTrackingDataSource.RegisterDataSource(GetGestureType());
        }
#endif
        }
}
