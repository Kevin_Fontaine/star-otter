﻿using System;
using UnityEngine;
using Leap;

namespace Emerge.Tracking
{
    internal class DefaultGrabGesture : MonoBehaviour, IGesture
    {
        private bool isListening;

        #region Unity Methods

        private void OnGUI()
        {           
            GetGuiGrab();
        }
        #endregion

        #region IGesture

        public event Action<Chirality> OnGrabGestureStart;
        public event Action<Chirality> OnGrabGestureFinished;

        public bool IsLeftMakingGesture { get; private set; }

        public bool IsRightMakingGesture { get; private set; }

        public void StartListening()
        {
            isListening = true;
        }

        public void StopListening()
        {
            isListening = false;
        }

        #endregion


        #region Helpers
        
        private void GetGuiGrab()
        {
            IsLeftMakingGesture = GUILayout.Toggle(IsLeftMakingGesture, "Is Grabbing");
            IsRightMakingGesture = IsLeftMakingGesture;
        }

        #endregion
    }
}