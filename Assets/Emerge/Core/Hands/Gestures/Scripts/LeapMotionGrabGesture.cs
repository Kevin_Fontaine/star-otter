﻿using System;
using UnityEngine;
using Leap;

namespace Emerge.Tracking
{
    internal class LeapMotionGrabGesture : MonoBehaviour, IGesture
    {
        private const float LEAP_GRAB_THRESHOLD = 0.8f;

        private Controller leapController;
        private bool isListening;
        private bool useGui;
        private bool privateIsLeftMakingGesture;
        private bool privateIsRightMakingGesture;


        #region Unity Methods

        private void Awake()
        {
            leapController = new Controller();
        }

        private void Update()
        {
            useGui = !TryGetLeapGrab();
        }

        private void OnGUI()
        {
            if (!isListening || !useGui)
            {
                return;
            }

            GetGuiGrab();
        }

        #endregion


        #region IGesture

        public event Action<Chirality> OnGrabGestureStart;
        public event Action<Chirality> OnGrabGestureFinished;

        public bool IsLeftMakingGesture
        {
            get
            {
                return privateIsLeftMakingGesture;
            }
            private set
            {
                if (privateIsLeftMakingGesture == value)
                {
                    return;
                }

                privateIsLeftMakingGesture = value;
                if (privateIsLeftMakingGesture)
                {
                    // Debug.Log("Grab gesture started left hand");
                    OnGrabGestureStart?.Invoke(Chirality.Left);
                }
                else
                {
                    // Debug.Log("Grab gesture finished left hand");
                    OnGrabGestureFinished?.Invoke(Chirality.Left);
                }
            }
        }

        public bool IsRightMakingGesture
        {
            get
            {
                return privateIsRightMakingGesture;
            }
            private set
            {
                if (privateIsRightMakingGesture == value)
                {
                    return;
                }

                privateIsRightMakingGesture = value;
                if (privateIsRightMakingGesture)
                {
                    // Debug.Log("Grab gesture started right hand");
                    OnGrabGestureStart?.Invoke(Chirality.Right);
                }
                else
                {
                    // Debug.Log("Grab gesture finished right hand");
                    OnGrabGestureFinished?.Invoke(Chirality.Right);
                }
            }
        }

        public void StartListening()
        {
            isListening = true;
        }

        public void StopListening()
        {
            isListening = false;
        }

        #endregion


        #region Helpers

        private bool TryGetLeapGrab()
        {
            if (!leapController.IsConnected)
            {
                return false;
            }

            Frame leapFrame = leapController.Frame();
            if (leapFrame == null)
            {
                return false;
            }

            bool? leftIsGrabbing = null;
            bool? rightIsGrabbing = null;
            foreach (Leap.Hand leapHand in leapFrame.Hands)
            {
                if (leapHand.IsLeft)
                {
                    leftIsGrabbing = (leapHand.GrabStrength > LEAP_GRAB_THRESHOLD);
                }
                else if (leapHand.IsRight)
                {
                    rightIsGrabbing = (leapHand.GrabStrength > LEAP_GRAB_THRESHOLD);
                }
            }

            IsLeftMakingGesture = leftIsGrabbing ?? false;
            IsRightMakingGesture = rightIsGrabbing ?? false;

            return true;
        }

        private void GetGuiGrab()
        {
            IsLeftMakingGesture = GUILayout.Toggle(IsLeftMakingGesture, "Is Grabbing");
            IsRightMakingGesture = IsLeftMakingGesture;
        }

        #endregion
    }
}