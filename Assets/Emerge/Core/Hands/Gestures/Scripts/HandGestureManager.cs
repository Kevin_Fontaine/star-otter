﻿using System;
using UnityEngine;
using UnityEngine.Events;


namespace Emerge.Tracking
{
    /// <summary>
    /// Use this class to configure gestures inside the inspector window.
    /// This is optional and not required to be present in the scene.
    /// </summary>
    public class HandGestureManager : MonoBehaviour
    {
        #region Definitions

        [Serializable]
        public class GestureUnityEvent : UnityEvent<Chirality> { }

        #endregion


        #region Serialized Fields

        /// <summary>
        /// This event will be invoked when a grab gesture is first detected.
        /// </summary>
        [SerializeField]
        private GestureUnityEvent OnGrabGestureStart = new GestureUnityEvent();

        /// <summary>
        /// This event will be invoked when a previously detected grab gesture has finished.
        /// </summary>
        [SerializeField]
        private GestureUnityEvent OnGrabGestureStopped = new GestureUnityEvent();

        #endregion


        #region Unity Methods

        private void OnDestroy()
        {
            if (HandTracking.HasDefaultDataSource)
            {
                HandTracking.Default.Gestures.Grab.OnGrabGestureFinished -= HandleGrabStopped;
                HandTracking.Default.Gestures.Grab.OnGrabGestureStart -= HandleGrabStarted;
            }
        }

        private void Awake()
        {
            HandTracking.Default.Gestures.Grab.OnGrabGestureStart += HandleGrabStarted;
            HandTracking.Default.Gestures.Grab.OnGrabGestureFinished += HandleGrabStopped;
        }

        #endregion


        #region Event Handlers

        private void HandleGrabStarted(Chirality chirality)
        {
            OnGrabGestureStart?.Invoke(chirality);
        }

        private void HandleGrabStopped(Chirality chirality)
        {
            OnGrabGestureStopped?.Invoke(chirality);
        }

        #endregion
    }
}
