﻿/******************************************************************************
 * Copyright (C) Emerge Now, Inc. 2016-2019.                                  *
 * Emerge Now Inc proprietary and confidential.                               *
 *                                                                            *
 * Use subject to the terms of the Emerge Exploration Program Agreement       *
 * and/or terms of limited, non-exclusive, non-sublicensable license to       *
 * Emerge SDK between Emerge and you, your company or any other organization. *
 ******************************************************************************/
using UnityEngine;

namespace Emerge.HandTracking
{
    /*
    /// <summary>
    /// Each Avatar instance in the scene has Avatar controller which controls the behaviour of the avatar.
    /// </summary>
    public class AvatarController : MonoBehaviour
    {
        public bool isRemote = false;
        public PuppetHand leftHand;
        public PuppetHand rightHand;
        /// <summary>
        /// set this to true for showing the hand renderer, false for hiding
        /// </summary>
        public bool showHandMesh = true;
        private bool prevShowHandMesh;
        private void Start()
        {
            prevShowHandMesh = showHandMesh = true;
            if (isRemote)
            {
                DestroyColliders();
            }
            else
            {
                ActivateDeactivateHand(Handedness.Left, false);
                ActivateDeactivateHand(Handedness.Right, false);
            }
        }

        Collider[] childColliders;

        /// <summary>
        /// Removes the collider from the Avatar hand
        /// </summary>
        public void DestroyColliders()
        {
            childColliders = transform.GetComponentsInChildren<Collider>();
            foreach (Collider collider in childColliders)
            {
#if !UNITY_WSA
                //special case - if its an EmergeObject, then ignore. those are colliders that we want to keep
                if (collider.transform.GetComponent<Emerge.Core.BaseEmergeObject>() == null)
                    DestroyImmediate(collider);
#endif
            }
        }

        /// <summary>
        /// Returns the transform of hand part
        /// </summary>
        /// <param name="handedness">left or right</param>
        /// <param name="handPart">hand part</param>
        /// <returns></returns>
        public Transform GetHandPart(Handedness handedness, HandParts handPart)
        {
            PuppetHand hand = handedness == Handedness.Left ? leftHand : rightHand;

            if (hand == null) return null;

            return hand.GetHandTransforms(handPart);
        }


        /// <summary>
        /// Returns the transform of hand part
        /// </summary>
        /// <param name="handedness">left or right</param>
        /// <param name="handPart">hand part</param>
        /// <returns></returns>
        public Transform GetPalmCenter(Handedness handedness)
        {
            //special case for palm transform used by MeshInteraction. we need the child of the palm root called "PalmCenter"
            PuppetHand hand = handedness == Handedness.Left ? leftHand : rightHand;
            if (hand == null) return null;
            return hand.GetHandTransforms(HandParts.PalmCenter);
        }


        /// <summary>
        /// return the Puppet Hand
        /// </summary>
        /// <param name="handedness">left or right</param>
        /// <returns>puppet hand</returns>
        public PuppetHand GetHand(Handedness handedness)
        {
            if (handedness == Handedness.Left)
            {
                return leftHand;
            }
            else if (handedness == Handedness.Right)
            {
                return rightHand;
            }
            return null;
        }

        /// <summary>
        /// Activate or Deactivate the hand
        /// </summary>
        /// <param name="handedness">left or right</param>
        /// <param name="value">true for activate, false for deactivate</param>
        public void ActivateDeactivateHand(Handedness handedness, bool value)
        {
            if (handedness == Handedness.Left)
            {
                leftHand.gameObject.SetActive(value);
            }
            else if (handedness == Handedness.Right)
            {
                rightHand.gameObject.SetActive(value);
            }
        }

        /// <summary>
        /// Hide or unhide hand renderer
        /// </summary>
        /// <param name="handedness">left or right</param>
        /// <param name="value">true for unhiding the renderer, false for hide the hand renderer</param>
        public void SetHandVisibility(Handedness handedness, bool value)
        {
            if (handedness == Handedness.Left)
            {
                if (leftHand.handRenderer != null)
                {
                    leftHand.handRenderer.enabled = value;
                }
            }
            if (handedness == Handedness.Right)
            {
                if (rightHand.handRenderer != null)
                {
                    rightHand.handRenderer.enabled = value;
                }
            }
        }

        public void Update()
        {
            if (Input.GetKeyUp(KeyCode.H))
            {
                if (!isRemote)
                {
                    showHandMesh = !showHandMesh;
                }
            }
            if (prevShowHandMesh != showHandMesh)
            {
                SetHandVisibility(Handedness.Left, showHandMesh);
                SetHandVisibility(Handedness.Right, showHandMesh);
            }
            prevShowHandMesh = showHandMesh;
        }
    }
    */
}
