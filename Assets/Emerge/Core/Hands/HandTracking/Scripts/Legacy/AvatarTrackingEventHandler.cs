﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Emerge.HandTracking
{
    /*
    /// <summary>
    /// Implement this interface to subscribe to local hand's tracking status
    /// </summary>
    public interface IAvatarTrackingEventHandler
    {
        void OnLeftHandActive();
        void OnLeftHandInActive();
        void OnRightHandActive();
        void OnRightHandInActive();
    }
    */
}
