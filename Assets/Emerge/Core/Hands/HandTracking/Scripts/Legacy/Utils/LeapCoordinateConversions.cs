﻿/******************************************************************************
 * Copyright (C) Emerge Now, Inc. 2016-2019.                                  *
 * Emerge Now Inc proprietary and confidential.                               *
 *                                                                            *
 * Use subject to the terms of the Emerge Exploration Program Agreement       *
 * and/or terms of limited, non-exclusive, non-sublicensable license to       *
 * Emerge SDK between Emerge and you, your company or any other organization. *
 ******************************************************************************/
using Leap;
using UnityEngine;

namespace Emerge.HandTracking
{
     
    public static class LeapCoordinateConversions {

        public static readonly float MM_TO_M = 1e-3f;

         

        //---------------------------------------
        public static Vector3 ConvertLeapVectorIntoUnityWorldpositionVector(Transform leapSensorCenter, Vector leapVector, bool flipZ)
        {
            Vector3 finalVector = ConvertToUnityVector3(leapVector);
            finalVector = LeapCoordinateConversions.ReCenterLeapCoordinatesInWorld(leapSensorCenter, finalVector);
            if (flipZ)
                finalVector = ConvertToLeapSpace(finalVector);
            return finalVector;
        }

        public static Vector3 ConvertToLeapSpace(Vector3 unityVector)
        {
            return new Vector3(unityVector.x, unityVector.y, -unityVector.z);
        }

        /// <summary>
        /// When the LEAP sensor is not at world center, the LEAP coordinates need to be offset\n
        /// in order to be used in Unity's world space
        /// </summary>
        /// <param name="leapPosition"></param>
        /// <returns>A position in world space</returns>
        public static Vector3 ReCenterLeapCoordinatesInWorld(Transform leapSensorCenter, Vector3 leapPosition)
        {
            leapPosition -= leapSensorCenter.position;
            //unityVector = LeapSensorCenter.InverseTransformPoint(unityVector);
            return leapPosition;
        }

        public static Vector3 ConvertToUnityVector3(Vector originalValue)
        {
            //Debug.Log("Vector conversion" + );
            Vector3 convertedValue;
            convertedValue.x = originalValue.x;
            convertedValue.y = originalValue.y;
            convertedValue.z = originalValue.z;

            ////adjust coordinates to match our scene (leap is in mm)

            convertedValue = convertedValue * MM_TO_M;

            return convertedValue;
        }
    }
   


}

