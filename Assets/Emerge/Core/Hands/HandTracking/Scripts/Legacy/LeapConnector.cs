﻿/******************************************************************************
 * Copyright (C) Emerge Now, Inc. 2016-2019.                                  *
 * Emerge Now Inc proprietary and confidential.                               *
 *                                                                            *
 * Use subject to the terms of the Emerge Exploration Program Agreement       *
 * and/or terms of limited, non-exclusive, non-sublicensable license to       *
 * Emerge SDK between Emerge and you, your company or any other organization. *
 ******************************************************************************/
using UnityEngine;

namespace Emerge.HandTracking
{
    using Communication;
    // this class needs renaming...
    public class LeapConnector : BaseLeapConnector
    {
        // TODO delete this and rely on rigged hand for networking functionality//////////////////
        /*
        [Space(20)]
        [Header("local hands used as sources for transmission to Hololens")]
        private PuppetHand_riggedHand network_puppetHand_local_RIGHT;
        private PuppetHand_riggedHand network_puppetHand_local_LEFT;

        [Space(20)]
        [Header("Remote hands used as sources for transmission to Hololens")]
        private PuppetHand_riggedHand network_puppetHand_remote_LEFT;
        private PuppetHand_riggedHand network_puppetHand_remote_RIGHT;

        private Handedness currentHandedness;

        private void Awake()
        {
            network_puppetHand_local_LEFT = AvatarManager.Instance.GetLocalAvatar().GetHand(Handedness.Left) as PuppetHand_riggedHand;
            network_puppetHand_local_RIGHT = AvatarManager.Instance.GetLocalAvatar().GetHand(Handedness.Right) as PuppetHand_riggedHand;
            network_puppetHand_remote_LEFT = AvatarManager.Instance.GetRemoteAvatar().GetHand(Handedness.Left) as PuppetHand_riggedHand;
            network_puppetHand_remote_RIGHT = AvatarManager.Instance.GetRemoteAvatar().GetHand(Handedness.Right) as PuppetHand_riggedHand;
        }

        public override void Step()
        {
            SendLEAPDataToPuppetHands();
        }


        public void SendLEAPDataToPuppetHands()
        {
            if (NetworkPlayers.Exists && NetworkPlayers.Instance.LEAPPlayers.Count > 1)
            {
                //MULTI PLAYER 
                for (int i = 0; i < NetworkPlayers.Instance.LEAPPlayers.Count; i++)
                {
                    if (NetworkPlayers.Instance.LEAPPlayers[i].AmILocal() == true)
                        UpdateLocalHand();
                    else
                    {

                        //OLD CODE - not sure why hand would ever be null? test before deleting
                        //if (NetworkPlayers.Instance.LEAPPlayers[i].hand_LEFT == null) {
                        //    Debug.Log("NO HAND AVAILABLE for " + NetworkPlayers.Instance.LEAPPlayers[i].photonView.name);
                        //}

                        Transform[] leftHandTransforms = NetworkPlayers.Instance.LEAPPlayers[i].hand_LEFT.currentHandData.all;
                        Transform[] rightHandTransforms = NetworkPlayers.Instance.LEAPPlayers[i].hand_RIGHT.currentHandData.all;

                        network_puppetHand_remote_LEFT.SetHandPositions(leftHandTransforms);
                        network_puppetHand_remote_RIGHT.SetHandPositions(rightHandTransforms);
                    }

                }


            }
            else
            {
                //SINGLE PLAYER

                UpdateLocalHand();
            }
        }

        private void UpdateLocalHand()
        {
            if (riggedHand_RIGHT.currentHandData != null)
            {
                network_puppetHand_local_RIGHT.UpdateHandState(riggedHand_RIGHT.currentHandData, IsTracking);
            }
            if (riggedHand_LEFT.currentHandData != null)
            {
                network_puppetHand_local_LEFT.UpdateHandState(riggedHand_LEFT.currentHandData, IsTracking);
            }
        }
        */
    }
}
