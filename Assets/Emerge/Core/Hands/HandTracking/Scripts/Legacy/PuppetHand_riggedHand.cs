﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Emerge.HandTracking {
    //TODO: this class should really be deleted - this can be moved into PuppetHand.
    //right now the Avatar-Lowres prefab uses this class and its -not- a rigged hand...
    public class PuppetHand_riggedHand : PuppetHand {
        /// <summary>
        /// used for graphics and other components where all hand data is not needed
        /// </summary>
        /// <returns></returns>
        public override Transform[] GetFingerTipsAndPalm()
        {
            Transform[] fingerTipsAndPalm = new Transform[6];
            fingerTipsAndPalm[0] = currentHandData.all[7];//palm //note: using the middle knuckle instead, 
                                                          //since palm is really at the wrist in this hand model
            fingerTipsAndPalm[1] = currentHandData.all[5];//index
            fingerTipsAndPalm[2] = currentHandData.all[10];//middle
            fingerTipsAndPalm[3] = currentHandData.all[15];//pinky
            fingerTipsAndPalm[4] = currentHandData.all[20];//ring
            fingerTipsAndPalm[5] = currentHandData.all[24];//thumb

            return fingerTipsAndPalm;
        }
    }
}
