﻿/******************************************************************************
 * Copyright (C) Emerge Now, Inc. 2016-2019.                                  *
 * Emerge Now Inc proprietary and confidential.                               *
 *                                                                            *
 * Use subject to the terms of the Emerge Exploration Program Agreement       *
 * and/or terms of limited, non-exclusive, non-sublicensable license to       *
 * Emerge SDK between Emerge and you, your company or any other organization. *
 ******************************************************************************/
using Leap;
using UnityEngine;
using System.Collections.Generic;

namespace Emerge.HandTracking {
    using Core;

    /// <summary>
    /// Serves as a bridge that connects the LEAP internals to other custom data structures, such as
    /// such as HandData (a collection of transforms that other objects can use to define a visual hand,
    /// or send over network, etc.)
    /// - Also provides callbacks for Tracking started/stopped and simple Grabbed/Released gesture
    /// </summary>
    public class BaseLeapConnector : MonoBehaviour {
        // TODO DELETE THIS IN FAVOR OF HAND RIGGING API///////////////////
        /*
        [Header("these are the SOURCE hands, driven by the LEAP sensor")]
        public PuppetHand riggedHand_LEFT;
        public PuppetHand riggedHand_RIGHT;

        //LEAP internals
        private Controller leapCtrl;
        private Frame leapFrame;
        private List<Leap.Hand> handList;

        //tracked properties 
        private bool isTracking;
		
        private Vector3 pointOfInterest;

        //hand data
        private HandData currentHandData;
		public HandData CurrentHandData
        {
            get { return currentHandData; }
        }
		
        public HandData leftHandData
        {
            get { return riggedHand_LEFT.currentHandData; }
        }
        public HandData rightHandData
        {
            get { return riggedHand_RIGHT.currentHandData; }
        }

        public bool IsTracking
        {
            get { return isTracking; }
        }

        private void Start()
        {
            //riggedHand_LEFT = HandBehaviour.Instance.GetLocalHand(Handedness.Left) as PuppetHand_riggedHand;
            //riggedHand_RIGHT = HandBehaviour.Instance.GetLocalHand(Handedness.Right) as PuppetHand_riggedHand;
            currentHandData = riggedHand_RIGHT.currentHandData;
            // Connect to LeapMotion when launching App.
            leapCtrl = new Controller();
            leapCtrl.StartConnection();

            isTracking = false;
        }

        private void Update()
        {
           
            leapFrame = leapCtrl.Frame();
            handList = leapFrame.Hands;

            isTracking = (handList.Count > 0);

            if (!isTracking) {
                currentHandData = null;
                // no tracking, so dont update hands
                //return;
            } else {
                UpdateFingerPositionsUsingRiggedHands();
            }
             
            Step();
        }

        public virtual void Step()
        {
            //allow others to extend this class w/o using Unity Update()
        }

        private void OnApplicationQuit()
        {
            // Disconnect at the end of App
            leapCtrl.StopConnection();
            leapCtrl.Dispose();
        }


        public void UpdateFingerPositionsUsingRiggedHands()
        {
            // If tracking is in progress, consider the first hand in the list to be 'the' hand
            Leap.Hand leapHand = handList[0];
            PuppetHand currentHand;
            if (leapHand.IsLeft) {
                currentHand = riggedHand_LEFT;
            } else {
                currentHand = riggedHand_RIGHT;
            }

            //source data to use:
            currentHandData = currentHand.currentHandData;
 
        }
        */
    }


}

