﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Emerge.HandTracking
{
    /*
    /// <summary>
    /// Events are called when Avatar hand's status gets changed.
    /// </summary>
    public class AvatarTrackingEvents : MonoBehaviour
    {
        /// <summary>
        /// Dispatched when left hand become active
        /// </summary>
        public UnityEvent OnLeftHandActive;
        /// <summary>
        /// Dispatched when left hand become inactive
        /// </summary>
        public UnityEvent OnLeftHandInActive;
        /// <summary>        
        /// Dispatched when right hand become inactive
        /// </summary>
        public UnityEvent OnRightHandActive;
        /// <summary>
        /// Dispatched when right hand become inactive
        /// </summary>
        public UnityEvent OnRightHandInActive;

        List<IAvatarTrackingEventHandler> avatarTrackingEventsList;

        private void Awake()
        {
            Init();
        }

        private void Init()
        {
            if (avatarTrackingEventsList == null)
            {
                avatarTrackingEventsList = new List<IAvatarTrackingEventHandler>();
            }
        }

        /// <summary>
        /// Register the AvatarTracking Event handler to subscribe for Avatar tracking events
        /// </summary>
        /// <param name="avatarTrackingEventHandler"></param>
        public void RegisterAvatarTrackingEventHandler(IAvatarTrackingEventHandler avatarTrackingEventHandler)
        {
            Init();
            avatarTrackingEventsList.Add(avatarTrackingEventHandler);
        }

        /// <summary>
        /// unRegister the AvatarTracking Event handler to unsubscribe from Avatar tracking events
        /// </summary>
        /// <param name="avatarTrackingEventHandler"></param>
        /// <returns></returns>
        public bool UnregisterAvatarTrackingEventHandler(IAvatarTrackingEventHandler avatarTrackingEventHandler)
        {
            Init();
            return avatarTrackingEventsList.Remove(avatarTrackingEventHandler);
        }

        /// <summary>
        /// called by handtracking module when the left hand become active
        /// </summary>
        public void LeftHandActive()
        {
            if (avatarTrackingEventsList != null)
            {
                foreach (IAvatarTrackingEventHandler avatarTrackingEventHandler in avatarTrackingEventsList)
                {
                    avatarTrackingEventHandler.OnLeftHandActive();
                }
            }
            if (OnLeftHandActive != null)
            {
                OnLeftHandActive.Invoke();
            }
        }

        /// <summary>
        /// called by handtracking module when the left hand become inactive
        /// </summary>
        public void LeftHandInActive()
        {
            if (avatarTrackingEventsList != null)
            {
                foreach (IAvatarTrackingEventHandler avatarTrackingEventHandler in avatarTrackingEventsList)
                {
                    avatarTrackingEventHandler.OnLeftHandInActive();
                }
            }
            if (OnLeftHandInActive != null)
            {
                OnLeftHandInActive.Invoke();
            }
        }

        /// <summary>
        /// called by handtracking module when the right hand become active
        /// </summary>
        public void RightHandActive()
        {
            if (avatarTrackingEventsList != null)
            {
                foreach (IAvatarTrackingEventHandler avatarTrackingEventHandler in avatarTrackingEventsList)
                {
                    avatarTrackingEventHandler.OnRightHandActive();
                }
            }
            if (OnRightHandActive != null)
            {
                OnRightHandActive.Invoke();
            }
        }

        /// <summary>
        /// called by handtracking module when the right hand become inactive
        /// </summary>
        public void RightHandInActive()
        {
            if (avatarTrackingEventsList != null)
            {
                foreach (IAvatarTrackingEventHandler avatarTrackingEventHandler in avatarTrackingEventsList)
                {
                    avatarTrackingEventHandler.OnRightHandInActive();
                }
            }
            if (OnRightHandInActive != null)
            {
                OnRightHandInActive.Invoke();
            }
        }
    }
    */
}
