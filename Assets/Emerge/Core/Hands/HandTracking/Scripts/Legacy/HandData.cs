﻿using System.Collections.Generic;
using UnityEngine;


namespace Emerge.HandTracking
{
    public class HandData : MonoBehaviour
    {
        public Handedness handedness; //convenience flag to id L/R when this class is passed

        public Transform[] all;
        private Vector3[] allPositions;
        private Quaternion[] allRotations;

        private int handArraySize;
        public int HandArraySize => handArraySize;

        private Transform[] fingerTipsAndPalm;
        public const int primitiveHandArraySize = 6; //5fingers + 1palm 
        public const int riggedHandArraySize = 25;

        public Transform palmCenter;

        public static int GetHandArraySize(AvatarType h)
        {
            if (h == AvatarType.Primitive)
                return primitiveHandArraySize;
            else
                return riggedHandArraySize;
        }


        void Awake()
        {
            handArraySize = all.Length;
            allPositions = new Vector3[handArraySize];
            allRotations = new Quaternion[handArraySize];
            fingerTipsAndPalm = GetFingerTipsAndPalm();
        }



        internal Transform[] GetFingerTipsAndPalm()
        {
            if (handArraySize != primitiveHandArraySize) //this prefab is a AvatarType.Primitive prefab TODO: assign this some other way?
                return all;

            return new Transform[] { palmCenter, GetHandTransform(HandParts.Index), GetHandTransform(HandParts.Middle),
                GetHandTransform(HandParts.Pinky), GetHandTransform(HandParts.Ring), GetHandTransform(HandParts.Thumb)};
        }

        public Transform[] GetFingerList()
        {
            return all;
        }


        public Transform GetHandTransform(HandParts handPart)
        {
            switch (handPart)
            {
                case HandParts.Palm: //root of the palm, so actually more like the wrist
                    return all[0];
                case HandParts.PalmCenter:
                    if (palmCenter == null)
                        return all[7]; //this is the middle knuckle on the edge of the palm
                    else
                        return palmCenter;
                case HandParts.Index: //these are the first joint from the tip, not the tip itself
                    return all[4];
                case HandParts.Middle:
                    return all[9];
                case HandParts.Pinky:
                    return all[14];
                case HandParts.Ring:
                    return all[19];
                case HandParts.Thumb:
                    return all[23];
                case HandParts.Index_knuckle: //these are the middle of the finger, not the knuckles on the hand
                    return all[3];
                case HandParts.Middle_knuckle:
                    return all[8];
                case HandParts.Pinky_knuckle:
                    return all[13];
                case HandParts.Ring_knuckle:
                    return all[18];
                case HandParts.Thumb_knuckle:
                    return all[22];
            }
            return null;
        }





        //===================================================================================
        // GET poses
        //===================================================================================
        //   
        public Vector3[] GetCurrentPose()
        {
            for (int i = 0; i < handArraySize; i++)
            {
                allPositions[i] = all[i].localPosition;
            }

            return allPositions;
        }


        public void GetCurrentPose(ref List<Vector3> list)
        {
            if (list.Count < handArraySize)
                return;

            for (int i = 0; i < handArraySize; i++)
            {
                list[i] = all[i].localPosition;
            }
        }

        public void GetCurrentPose(ref Vector3[] list)
        {
            if (list.Length < handArraySize)
                return;

            for (int i = 0; i < handArraySize; i++)
            {
                list[i] = all[i].localPosition;
            }
        }


        public Quaternion[] GetCurrentPoseRotations()
        {
            for (int i = 0; i < handArraySize; i++)
            {
                allRotations[i] = all[i].localRotation;
            }

            return allRotations;
        }


        public void GetCurrentPoseRotations(ref List<Quaternion> list)
        {
            if (list.Count < handArraySize)
                return;

            for (int i = 0; i < handArraySize; i++)
            {
                list[i] = all[i].localRotation;
            }
        }


        //===================================================================================
        // GET poses - with 'primitive' hand - fingertips and palm objects only
        public Vector3[] GetCurrentPose_fingerTipsAndPalmOnly(Transform root)
        {
            Vector3[] totalPositions = new Vector3[primitiveHandArraySize];

            for (int i = 0; i < primitiveHandArraySize; i++)
            {
                Vector3 pos = fingerTipsAndPalm[i].position;
                totalPositions[i] = root.InverseTransformPoint(pos); //localPosition relative to the hand root
            }

            return totalPositions;

        }


        public void GetCurrentPose_fingerTipsAndPalmOnly(ref List<Vector3> positions, Transform root)
        {
            for (int i = 0; i < primitiveHandArraySize; i++)
                positions[i] = root.InverseTransformPoint(fingerTipsAndPalm[i].position); //localPosition relative to the hand root
        }

        public void GetCurrentPose_fingerTipsAndPalmOnly(ref Vector3[] positions, Transform root)
        {
            for (int i = 0; i < primitiveHandArraySize; i++)
                positions[i] = root.InverseTransformPoint(fingerTipsAndPalm[i].position); //localPosition relative to the hand root
        }


        public Quaternion[] GetCurrentPoseRotations_fingerTipsAndPalmOnly(Transform root)
        {
            Quaternion[] totalPositions = new Quaternion[primitiveHandArraySize];

            for (int i = 0; i < primitiveHandArraySize; i++)
            {
                totalPositions[i] = fingerTipsAndPalm[i].localRotation;
            }

            return totalPositions;

        }


        //===================================================================================
        // SET poses
        //===================================================================================
        //   
        public void SetHandPose(List<Quaternion> totalRotations)
        {
            int handArraySize = all.Length;
            for (int i = 0; i < handArraySize; i++)
            {
                all[i].localRotation = totalRotations[i];
            }
        }

        public void SetHandPose(Quaternion[] totalRotations)
        {
            int handArraySize = all.Length;
            for (int i = 0; i < handArraySize; i++)
            {
                all[i].localRotation = totalRotations[i];
            }
        }

        public void SetHandPose(List<Vector3> totalPositions)
        {
            int handArraySize = all.Length;
            for (int i = 0; i < handArraySize; i++)
            {

                all[i].localPosition = totalPositions[i];
            }
            //--------------------------------------
        }

        public void SetHandPose(Vector3[] totalPositions)
        {
            int handArraySize = all.Length;
            for (int i = 0; i < handArraySize; i++)
            {
                all[i].localPosition = totalPositions[i];
            }
            //--------------------------------------
        }

        public void SetHandPose(Tracking.Hand handData)
        {
            if (handData.active != gameObject.activeSelf)
            {
                gameObject.SetActive(handData.active);
            }
            
            if (!handData.active)
            {
                return;
            }

            Tracking.AbstractHandModel handModel = GetComponent<Tracking.AbstractHandModel>();
            if (handModel == null)
            {
                Debug.LogErrorFormat("HandData: AbstractHandModel component is required when setting the pose with Tracking.Hand data");
                return;
            }

            handModel.SetHandData(handData);
        }

        public bool PopulateHandData(Tracking.Hand handData)
        {
            handData.active = gameObject.activeSelf;
            if (!gameObject.activeSelf)
            {
                return true;
            }

            Tracking.DiscreteHandModel handModel = GetComponent<Tracking.DiscreteHandModel>();
            if (handModel == null)
            {
                Debug.LogErrorFormat("HandData: DiscreteHandModel component is required when using the PopulateHandData method");
                return false;
            }

            // TODO DOES CHIRALITY MATTER

            handData.wrist.center = GetPosition(handModel.wristCenter);
            handData.wrist.innerEdge = GetPosition(handModel.wristInner);
            handData.wrist.outerEdge = GetPosition(handModel.wristOuter);
            handData.palm.center = GetPosition(handModel.palmCenter);

            PopulateFingerData(handData.fingers.thumb, handModel.thumb);
            PopulateFingerData(handData.fingers.index, handModel.index);
            PopulateFingerData(handData.fingers.middle, handModel.middle);
            PopulateFingerData(handData.fingers.ring, handModel.ring);
            PopulateFingerData(handData.fingers.little, handModel.little);

            return true;
        }

        private static Vector3 GetPosition(GameObject gameObject)
        {
            if (gameObject == null)
            {
                return Vector3.zero;
            }

            return gameObject.transform.position;
        }

        private static Vector3 GetPosition(Transform transform)
        {
            if (transform == null)
            {
                return Vector3.zero;
            }

            return transform.position;
        }

        private static void PopulateFingerData(Tracking.Digit data, Tracking.RiggedFinger rig)
        {
            if (rig == null)
            {
                data.Configure(Vector3.zero, Vector3.zero, Vector3.zero, Vector3.zero);
                return;
            }

            data.Configure(
                GetPosition(rig.baseJoint),
                GetPosition(rig.middleJoint),
                GetPosition(rig.topJoint),
                GetPosition(rig.tip)
            );
        }
    }
}
