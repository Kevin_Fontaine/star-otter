﻿﻿/******************************************************************************
 * Copyright (C) Emerge Now, Inc. 2016-2019.                                  *
 * Emerge Now Inc proprietary and confidential.                               *
 *                                                                            *
 * Use subject to the terms of the Emerge Exploration Program Agreement       *
 * and/or terms of limited, non-exclusive, non-sublicensable license to       *
 * Emerge SDK between Emerge and you, your company or any other organization. *
 ******************************************************************************/
using UnityEngine;
using UnityEngine.Events;

namespace Emerge.HandTracking
{
    /*
    using System;
    using Core;

    public class OnHandCollisionDetection : UnityEvent<Handedness> { }

    /// <summary>
    /// Manages the avatar Instance in the scene
    /// Every user that gets connected in a multi user setup gets an avatar Instance
    /// </summary>
    [RequireComponent(typeof(AvatarTrackingEvents))]
    public class AvatarManager : Singleton<AvatarManager>, IAvatarTrackingEventHandler
    {
        [SerializeField]
        private GameObject avatarPrefab = null;
        [SerializeField]
        private GameObject avatarLowResPrefab = null;

        public AvatarType localAvatarType = AvatarType.Rigged;
        public AvatarType remoteAvatarType = AvatarType.Rigged;

        private AvatarController localAvatar;
        private AvatarController remoteAvatar;
        private AvatarController localNetworkAvatar;
        private AvatarController remoteNetworkAvatar;

        private Transform localRoot;
        private Transform remoteRoot;

        public Action OnLeftHandActive;
        public Action OnLeftHandInactive;
        public Action OnRightHandActive;
        public Action OnRightHandInactive;

        public Action OnLeftHandCollisionStart;
        public Action OnLeftHandCollisionEnd;

        public Action OnRightHandCollisionStart;
        public Action OnRightHandCollisionEnd;

        private bool initialized = false;

        private AvatarTrackingEvents avatarTrackingEvents;
        private bool isLeftHandActive = false;
        private bool isRightHandActive = false;
        #region Unity

        private void Awake()
        {
            Init();
        }

        #endregion

        #region Private
        private void OnDestroy()
        {
            if (avatarTrackingEvents != null)
            {
                UnRegisterAvatarTrackingEventHandler(this);
            }
        }

        private new void Init()
        {
            if (initialized)
            {
                return;
            }
            Debug.Log("Avatar Manager Init called");
            avatarTrackingEvents = GetComponent<AvatarTrackingEvents>();
            if (avatarTrackingEvents != null)
            {
                RegisterAvatarTrackingEventHandler(this);
            }
            localRoot = new GameObject("Local").transform;
            localRoot.parent = transform;
            localRoot.localPosition = Vector3.zero;
            remoteRoot = new GameObject("Remote").transform;
            remoteRoot.parent = transform;
            remoteRoot.localPosition = Vector3.zero;

            GameObject tempLocalAvatar = null;

            if (localAvatarType == AvatarType.Primitive)
            {
                tempLocalAvatar = (Instantiate(avatarLowResPrefab) as GameObject);
            }
            else
            {
                tempLocalAvatar = (Instantiate(avatarPrefab) as GameObject);
            }

            //Local Avatar
            localAvatar = tempLocalAvatar.GetComponent<AvatarController>();
            localAvatar.name = "Avatar-Visual";
            localAvatar.transform.parent = localRoot;
            localAvatar.transform.localPosition = Vector3.zero;

            GameObject tempRemoteAvatar = null;
            if (remoteAvatarType == AvatarType.Primitive)
            {
                tempRemoteAvatar = (Instantiate(avatarLowResPrefab) as GameObject);
            }
            else
            {
                tempRemoteAvatar = (Instantiate(avatarPrefab) as GameObject);
            }

            //Remote Avatar
            remoteAvatar = tempRemoteAvatar.GetComponent<AvatarController>();
            remoteAvatar.name = "Avatar-Visual";
            remoteAvatar.isRemote = true;
            remoteAvatar.transform.parent = remoteRoot;
            //remoteAvatar.SetHandVisibility(Handedness.Left, false);
            //remoteAvatar.SetHandVisibility(Handedness.Right, false);

            remoteAvatar.transform.localPosition = Vector3.zero;
            //rotate 180 for two seat setup. currently not set up for >2 players
            remoteRoot.Rotate(Vector3.up, 180.00f);

            initialized = true;
        }

        #endregion

        #region Public

        /// <summary>
        /// Implement IAvatarTrackingHandler and register it here to subscribe for Avatar tracking events
        /// </summary>
        /// <param name="avatarTrackingEventHandler">IAvatarTrackingHandler</param>
        /// <returns>true when register is a success, false otherwise</returns>
        public bool RegisterAvatarTrackingEventHandler(IAvatarTrackingEventHandler avatarTrackingEventHandler)
        {
            if (avatarTrackingEvents == null || avatarTrackingEventHandler == null)
            {
                return false;
            }
            avatarTrackingEvents.RegisterAvatarTrackingEventHandler(avatarTrackingEventHandler);
            return true;
        }

        /// <summary>
        /// Unregister IAvatarTrackingHandler
        /// </summary>
        /// <param name="avatarTrackingEventHandler">IAvatarTrackingHandler</param>
        /// <returns>true when unregister is a success, false otherwise</returns>
        public bool UnRegisterAvatarTrackingEventHandler(IAvatarTrackingEventHandler avatarTrackingEventHandler)
        {
            if (avatarTrackingEvents == null || avatarTrackingEventHandler == null)
            {
                return false;
            }
            avatarTrackingEvents.UnregisterAvatarTrackingEventHandler(avatarTrackingEventHandler);
            return true;
        }

        /// <summary>
        /// returns true if the local left hand is active
        /// </summary>
        public bool IsLeftHandActive
        {
            get { return isLeftHandActive; }
        }
        /// <summary>
        /// returns true if the local right hand is active
        /// </summary>
        public bool IsRightHandActive
        {
            get { return isRightHandActive; }
        }

        public bool addTactileResponseToRemoteHands;
        public bool AddTactileResponseToRemoteHands => addTactileResponseToRemoteHands;

        /// <summary>
        /// Returns local avatar
        /// </summary>
        /// <returns></returns>
        public AvatarController GetLocalAvatar()
        {
            return localAvatar;
        }

        /// <summary>
        /// Returns remote avatar
        /// </summary>
        /// <param name="id">id of the remote avatar, default value is -1 and returns the first remote avatar connected</param>
        /// <returns></returns>
        public AvatarController GetRemoteAvatar(int id = -1)
        {
            return remoteAvatar;
        }

        /// <summary>
        /// Returns local network avatar
        /// </summary>
        /// <returns></returns>
        public AvatarController GetLocalNetworkAvatar()
        {
            return localNetworkAvatar;
        }

        /// <summary>
        /// Returns remote network avatar
        /// </summary>
        /// <param name="id">id of the remote avatar, default value is -1 and returns the first remote avatar connected</param>
        /// <returns></returns>
        public AvatarController GetRemoteNetworkAvatar(int id = -1)
        {
            return remoteNetworkAvatar;
        }

        /// <summary>
        /// call this to add local network avatar to avatar manager
        /// </summary>
        /// <param name="avatar">avatar controller</param>
        public void AddLocalNetworkAvatar(Transform t)
        {
            localNetworkAvatar = t.GetComponent<AvatarController>();
            t.parent = localRoot;
            localNetworkAvatar.name = "Hands-Network-local";
            localNetworkAvatar.SetHandVisibility(Handedness.Right, false);
            localNetworkAvatar.SetHandVisibility(Handedness.Left, false);
        }

        /// <summary>
        /// call this to add remote network avatar to avatar manager
        /// </summary>
        /// <param name="avatar">avatar controller</param>
        public void AddRemoteNetworkAvatar(Transform t)
        {
            remoteNetworkAvatar = t.GetComponent<AvatarController>();
            t.parent = remoteRoot;
            remoteNetworkAvatar.name = "Hands-Network-Remote" + UnityEngine.Random.Range(0,100);
            remoteNetworkAvatar.isRemote = true;
            remoteNetworkAvatar.ActivateDeactivateHand(Handedness.Left, true);
            remoteNetworkAvatar.ActivateDeactivateHand(Handedness.Right, true);
            remoteNetworkAvatar.SetHandVisibility(Handedness.Left, true);
            remoteNetworkAvatar.SetHandVisibility(Handedness.Right, true);

#if !UNITY_WSA
            if (this.AddTactileResponseToRemoteHands)
            {
                EmergeObjectsManager.Instance.AddTactileMeshToRemoteHands(t, remoteNetworkAvatar.GetHand(Handedness.Left), remoteNetworkAvatar.GetHand(Handedness.Right));
                //EmergeObjectsManager.Instance.AddTactileMeshToRemoteHands(t, remoteNetworkAvatar.GetHand(Handedness.Right));
            }
#endif
        }


        public void DestroyRemoteAvatar(Transform t)
        {
 
            //Debug.Log("DestroyRemoteAvatar" + t.name );
            //if (this.AddTactileResponseToRemoteHands)
            //    EmergeObjectsManager.Instance.RemoteHandsDestroyed(t);
 
        }

        /// <summary>
        /// Returns the local root of local avatars //local root? -MO
        /// </summary>
        /// <returns></returns>
        public Transform GetLocalRoot()
        {
            return localRoot;
        }

        /// <summary>
        /// Returns the remote root of remote avatars
        /// </summary>
        /// <returns></returns>
        public Transform GetRemoteRoot()
        {
            return remoteRoot;
        }


        public void ToggleLocalAvatarVisibility()
        {
            GetLocalAvatar().GetHand(Handedness.Left).ToggleRenderers();
            GetLocalAvatar().GetHand(Handedness.Right).ToggleRenderers();
        }



        void IAvatarTrackingEventHandler.OnLeftHandActive()
        {
            isLeftHandActive = true;
            localAvatar.ActivateDeactivateHand(Handedness.Left, true);
            if (localNetworkAvatar != null)
            {
                localNetworkAvatar.ActivateDeactivateHand(Handedness.Left, true);
            }
        }

        void IAvatarTrackingEventHandler.OnLeftHandInActive()
        {
            isLeftHandActive = false;
            localAvatar.ActivateDeactivateHand(Handedness.Left, false);
            if (localNetworkAvatar != null)
            {
                localNetworkAvatar.ActivateDeactivateHand(Handedness.Left, false);
            }
        }

        void IAvatarTrackingEventHandler.OnRightHandActive()
        {
            isRightHandActive = true;
            localAvatar.ActivateDeactivateHand(Handedness.Right, true);
            if (localNetworkAvatar != null)
            {
                localNetworkAvatar.ActivateDeactivateHand(Handedness.Right, true);
            }
        }

        void IAvatarTrackingEventHandler.OnRightHandInActive()
        {
            isRightHandActive = false;
            localAvatar.ActivateDeactivateHand(Handedness.Right, false);
            if (localNetworkAvatar != null)
            {
                localNetworkAvatar.ActivateDeactivateHand(Handedness.Right, false);
            }
        }
        #endregion

    }
    */
}
