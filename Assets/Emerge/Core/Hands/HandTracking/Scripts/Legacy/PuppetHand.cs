﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;




namespace Emerge.HandTracking
{

    using Core;
    /// <summary>
    /// Puppet hand class - used for Photon but also used in Hololens - might need a name change, but its also used in the actual
    /// network instantiated photon prefab, so...
    /// 
    /// First part of this class is position data set/get/modify
    /// Second part is visuals, hiding, showing, graphics, etc.
    /// </summary>


    //TODO: context menu to get all transforms, in order
    public partial class PuppetHand : MonoBehaviour
    {
        public HandData currentHandData;

        public SkinnedMeshRenderer handRenderer;
        [HideInInspector]
        public Handedness handedness;


        public Transform[] GetHandTransforms()
        {
            return currentHandData.all;
        }

        public Transform GetHandTransform(HandParts handParts)
        {
            return currentHandData.GetHandTransform(handPart: handParts);
        }

        public virtual Transform GetRoot()
        {
            return currentHandData.all[0].root; //NOTE: all[0] for the puppet hand is ok to move around (with root being at 0,0,0) -
                                                //its the palm, which is right at wrist
                                                // for the fingers only hand, you need the root
        }

        public Vector3 GetHandRootPosition()
        {
            if (currentHandData == null)
            {
                return Vector3.zero;
            }
            return currentHandData.all[0].position;
        }

        /// <summary>
        /// used to update hand data from LEAP data source
        /// </summary>
        private bool lastTracking;
        public void UpdateHandState(HandData h)
        {
            //position update
            //if (isTracking)
            //{
                SetHandPositions(h.all);
            //}
        }

        public void UpdateHandState(Tracking.Hand handData)
        {
            //if (isTracking)
            //{
                SetHandPositions(handData);
            //}
        }


        internal void SetHandPositions(Transform[] _all)
        {
            if (_all == null || currentHandData == null)
            {
                return;
            }
            for (int i = 0; i < _all.Length; i++)
            {
                currentHandData.all[i].localPosition = _all[i].localPosition;
                currentHandData.all[i].localRotation = _all[i].localRotation;
            }
        }

        internal void SetHandPositions(Tracking.Hand handData)
        {
            currentHandData.SetHandPose(handData);
        }

        internal bool PopulateHandData(Tracking.Hand handData)
        {
            return currentHandData.PopulateHandData(handData);
        }


        public virtual Transform[] GetFingerTipsAndPalm()
        {
            return new Transform[6];
            //return currentHandData.GetFingerTipsAndPalm();
        }

        public virtual void OffsetTransform(Transform t)
        {
            //update position transforms

            for (int i = 0; i < currentHandData.all.Length; i++)
            {
                Vector3 pos = t.TransformPoint(currentHandData.all[i].position);
                currentHandData.all[i].position = pos;
                //Quaternion LocalRotation = Quaternion.Inverse(t.rotation) * all[i].rotation;
                Quaternion LocalRotation = Quaternion.Inverse(currentHandData.all[i].rotation) * t.rotation;
                currentHandData.all[i].rotation = LocalRotation;
            }

        }

        public Transform GetHandTransforms(HandParts handPart)
        {
            //Debug.Log("currentHandData = " + currentHandData);
            return currentHandData.GetHandTransform(handPart);
        }

    }



    /// <summary>
    /// GRAPHICS and VISUALS section of this class
    /// </summary>
    public partial class PuppetHand
    {


        private Renderer[] allRenderers;

        [Space(20)]
        [Header("Graphics")]
        public Material material_Mine;
        public Material material_Theirs;
        public Material material_Disabled;


        [Space(20)]
        [Header("Hand Visuals")]
        [InspectorButton("GetAllRenderers")]
        private bool DoGetAllRenderers;
        private bool isMine = true;
        private bool readyToUpdate = false;
        public void GetAllRenderers()
        {
            if (allRenderers == null || allRenderers.Length == 0)
                allRenderers = GetComponentsInChildren<Renderer>();
        }

        public void Update()
        {
            if (readyToUpdate)
            {
                StartCoroutine(WaitToUpdateColors(isMine));
                readyToUpdate = false;
            }
        }

        public void ShowHand(bool b)
        {
            //for (int i = 0; i < all.Length; i++) {
            //    all[i].gameObject.SetActive(b);
            //}
            EnableRenderers(b);
        }

        private void LazyInitRenderersList()
        {
            if (allRenderers == null || allRenderers.Length == 0)
                allRenderers = GetComponentsInChildren<Renderer>();
        }

        public void SetMaterial(Material m)
        {
            LazyInitRenderersList();

            for (int i = 0; i < allRenderers.Length; i++)
            {
                allRenderers[i].material = m;
            }
        }

        public void EnableRenderers(bool b)
        {
            LazyInitRenderersList();

            for (int i = 0; i < allRenderers.Length; i++)
            {
                allRenderers[i].enabled = b;
            }
        }

        public void ToggleRenderers()
        {
            LazyInitRenderersList();

            for (int i = 0; i < allRenderers.Length; i++)
            {
                allRenderers[i].enabled = !allRenderers[i].enabled;
            }
        }


        internal void InitialSetup(bool isMine)
        {
            this.isMine = isMine;
            Debug.Log("InitialSetup, hand isMine = " + isMine);
            readyToUpdate = true;
        }


        IEnumerator WaitToUpdateColors(bool isMine)
        {
            yield return new WaitForSeconds(.1f);
            if (isMine)
            {
                this.SetMaterial(material_Mine);
            }
            else
            {
                this.SetMaterial(material_Theirs);
                //scoot over so we can see it 
                //transform.Translate(0, 0, 0.1f);//TEMP
            }
            readyToUpdate = true;
        }

        //--------------------------------------------------
        public void RemoveFromSimulation()
        {
            //stop sending data? //NOTE: data sync is handled in the photon player prefab

            //disable visuals
            EnableRenderers(false);
        }

        public void AddToSimulation()
        {
            //resume sending data
            //enable visuals
            EnableRenderers(true);
        }
    }
}