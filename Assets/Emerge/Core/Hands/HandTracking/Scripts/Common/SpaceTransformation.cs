﻿using UnityEngine;

namespace Emerge
{
    public class SpaceTransformation : MonoBehaviour
    {
        public Transform sourceLocalSpace;
        public Transform destinationLocalSpace;

        public Vector3 TransformFromSourceToDestinationLocalPoint(Vector3 sourcePoint)
        {
            if (sourceLocalSpace == null || destinationLocalSpace == null)
            {
                Debug.LogErrorFormat("SpaceTransformation: space transform cannot be null");
                return sourcePoint;
            }

            Vector3 worldSpace = sourceLocalSpace.TransformPoint(sourcePoint);
            return destinationLocalSpace.InverseTransformPoint(worldSpace);
        }
    }
}
