﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Emerge.Tracking
{
    public static class HandDataSpaceExtensions
    {
        public static void ConvertToLocalSpace(this HandSet handSet, SpaceTransformation transformation)
        {
            handSet.left.ConvertToLocalSpace(transformation);
            handSet.right.ConvertToLocalSpace(transformation);
        }

        public static void ConvertToLocalSpace(this Hand hand, SpaceTransformation transformation)
        {
            hand.fingers.ConvertToLocalSpace(transformation);
            hand.wrist.ConvertToLocalSpace(transformation);
            hand.palm.ConvertToLocalSpace(transformation);
            hand.bounds.center = transformation.TransformFromSourceToDestinationLocalPoint(hand.bounds.center);
        }

        public static void ConvertToLocalSpace(this Wrist wrist, SpaceTransformation transformation)
        {
            wrist.innerEdge = transformation.TransformFromSourceToDestinationLocalPoint(wrist.innerEdge);
            wrist.center = transformation.TransformFromSourceToDestinationLocalPoint(wrist.center);
            wrist.outerEdge = transformation.TransformFromSourceToDestinationLocalPoint(wrist.outerEdge);
        }

        public static void ConvertToLocalSpace(this Palm palm, SpaceTransformation transformation)
        {
            palm.center = transformation.TransformFromSourceToDestinationLocalPoint(palm.center);
        }

        public static void ConvertToLocalSpace(this Fingers fingers, SpaceTransformation transformation)
        {
            fingers.index.ConvertToLocalSpace(transformation);
            fingers.middle.ConvertToLocalSpace(transformation);
            fingers.ring.ConvertToLocalSpace(transformation);
            fingers.little.ConvertToLocalSpace(transformation);
            fingers.thumb.ConvertToLocalSpace(transformation);
        }

        public static void ConvertToLocalSpace(this Digit digit, SpaceTransformation transformation)
        {
            digit.Tip = transformation.TransformFromSourceToDestinationLocalPoint(digit.Tip);
            digit.TopJoint = transformation.TransformFromSourceToDestinationLocalPoint(digit.TopJoint);
            digit.MiddleJoint = transformation.TransformFromSourceToDestinationLocalPoint(digit.MiddleJoint);
            digit.BaseJoint = transformation.TransformFromSourceToDestinationLocalPoint(digit.BaseJoint);
        }
    }
}
