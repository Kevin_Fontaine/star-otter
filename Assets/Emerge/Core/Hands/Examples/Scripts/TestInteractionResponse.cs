﻿using UnityEngine;
using Emerge.Tracking;

[RequireComponent(typeof(InteractableObject))]
public class TestInteractionResponse : MonoBehaviour
{
    private void Start()
    {
        InteractableObject interactableObject = GetComponent<InteractableObject>();
        interactableObject.OnGrabbed.AddListener(HandleGrabStart);
        interactableObject.OnGrabReleased.AddListener(HandleGrabEnd);
        interactableObject.OnTouchStart.AddListener(HandleTouchStart);
        interactableObject.OnTouchEnded.AddListener(HandleTouchEnd);
    }

    private void Update()
    {
        if (GetComponent<InteractableObject>().TouchingHandComponents == null)
        {
            return;
        }

        //Debug.LogFormat("{0} touching hand components", GetComponent<InteractableObject>().TouchingHandComponents.Count);
    }

    private void HandleGrabStart(Rigidbody hand)
    {
        Debug.LogFormat("GRABBED '{0}'", gameObject.name);
    }

    private void HandleGrabEnd(Rigidbody hand)
    {
        Debug.LogFormat("RELEASED '{0}'", gameObject.name);
    }

    private void HandleTouchStart(Rigidbody hand, AbstractInteractionComponent handComponent)
    {
        Debug.LogFormat("TOUCH START '{0}' WITH '{1}'", gameObject.name, handComponent.type);
    }

    private void HandleTouchEnd(Rigidbody hand, AbstractInteractionComponent handComponent)
    {
        Debug.LogFormat("TOUCH END '{0}' WITH '{1}'", gameObject.name, handComponent.type);
    }
}
