﻿using UnityEngine;
using System.Collections.Generic;

namespace Emerge.Tracking
{
    /// <summary>
    /// This component should be added to each interactable part of a hand model.
    /// This is a required part of a physics hand rig.
    /// </summary>
    public class InteractionComponent : AbstractInteractionComponent
    {
    }
}
