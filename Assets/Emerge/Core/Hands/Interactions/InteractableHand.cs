﻿using System.Collections.Generic;
using UnityEngine;

namespace Emerge.Tracking
{
    /// <summary>
    /// This object manages a physics hand model to be used for interactions.
    /// This is required in order to use the Emerge hand interaction API.
    /// This hand must contain InteractionComponents as children.
    /// </summary>
    public class InteractableHand : AbstractInteractableHand
    {
    }
}
