﻿using System;
using System.Collections.ObjectModel;
using UnityEngine;
using UnityEngine.Events;

namespace Emerge.Tracking
{
    /// <summary>
    /// Attach this component to get information about hand interactions with this object.
    /// This component is not required for interactions;
    /// it only provides additional information.
    /// This must be added to the same GameObject as the Rigidbody.
    /// </summary>
    [RequireComponent(typeof(Rigidbody))]
    public class InteractableObject : MonoBehaviour
    {
        #region Definitions

        [Serializable]
        public class GestureUnityEvent : UnityEvent<Rigidbody> { }

        [Serializable]
        public class TouchUnityEvent : UnityEvent<Rigidbody, AbstractInteractionComponent> { }

        #endregion


        #region Public

        /// <summary>
        /// This event will be invoked when this object is grabbed.
        /// </summary>
        public GestureUnityEvent OnGrabbed = new GestureUnityEvent();

        /// <summary>
        /// This event will be invoked when this object is released from a grab.
        /// </summary>
        public GestureUnityEvent OnGrabReleased = new GestureUnityEvent();

        /// <summary>
        /// This event will be invoked when a new InteractionComponent begins touching this object.
        /// Note that touches are not mutually exclusive;
        /// multiple InteractionComponents may be touching this object at any given time.
        /// </summary>
        public TouchUnityEvent OnTouchStart = new TouchUnityEvent();

        /// <summary>
        /// This event will be invoked when an InteractionComponent stops touching this object.
        /// Note that touches are not mutually exclusive;
        /// multiple InteractionComponents may be touching this object at any given time.
        /// </summary>
        public TouchUnityEvent OnTouchEnded = new TouchUnityEvent();

        /// <summary>
        /// All InteractionComponents currently touching this object.
        /// </summary>
        public ReadOnlyCollection<AbstractInteractionComponent> TouchingHandComponents
        {
            get
            {
                return HandInteractions.CurrentInteractionsForGameObjectId(gameObject.GetInstanceID());
            }
        }

        #endregion


        #region Unity Methods

        private void OnDestroy()
        {
            HandInteractions.OnHandTouchEnd -= HandleTouchEnded;
            HandInteractions.OnHandTouchStart -= HandleTouchStarted;
            HandInteractions.OnObjectGrabReleased -= HandleGrabReleased;
            HandInteractions.OnObjectGrabbed -= HandleGrabStart;
        }

        private void Awake()
        {
            HandInteractions.OnObjectGrabbed += HandleGrabStart;
            HandInteractions.OnObjectGrabReleased += HandleGrabReleased;
            HandInteractions.OnHandTouchStart += HandleTouchStarted;
            HandInteractions.OnHandTouchEnd += HandleTouchEnded;
        }

        #endregion


        #region Event Handlers

        private void HandleGrabStart(Rigidbody target, Rigidbody hand)
        {
            if (!target.SameGameObject(this))
            {
                return;
            }

            OnGrabbed?.Invoke(hand);
        }

        private void HandleGrabReleased (Rigidbody target, Rigidbody hand)
        {
            if (!target.SameGameObject(this))
            {
                return;
            }

            OnGrabReleased?.Invoke(hand);
        }

        private void HandleTouchStarted(Collider target, Rigidbody hand, AbstractInteractionComponent handComponent)
        {
            if (target == null || !target.attachedRigidbody.SameGameObject(this))
            {
                return;
            }

            OnTouchStart?.Invoke(hand, handComponent);
        }

        private void HandleTouchEnded(Collider target, Rigidbody hand, AbstractInteractionComponent handComponent)
        {
            if (target == null || !target.attachedRigidbody.SameGameObject(this))
            {
                return;
            }

            OnTouchEnded?.Invoke(hand, handComponent);
        }

        #endregion
    }
}
