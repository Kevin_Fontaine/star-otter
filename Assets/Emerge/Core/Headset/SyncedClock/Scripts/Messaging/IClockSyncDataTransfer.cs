﻿/******************************************************************************
 * Copyright (C) Emerge Now, Inc. 2016-2019.                                  *
 * Emerge Now Inc proprietary and confidential.                               *
 *                                                                            *
 * Use subject to the terms of the Emerge Exploration Program Agreement       *
 * and/or terms of limited, non-exclusive, non-sublicensable license to       *
 * Emerge SDK between Emerge and you, your company or any other organization. *
 ******************************************************************************/

using System;

namespace Emerge.Communication
{
    public delegate void ClockSyncInfoDelegate(UInt16 messageType, string clockId, long timestamp);
    public delegate void ClockSyncConnectionDelegate(bool isConnected);

    public interface IClockSyncDataTransfer
    {
        event ClockSyncInfoDelegate OnDataReceived;

        IUDPConnection Connection { get; }

        bool SendData(UInt16 messageType, string clockId, long timestamp);
    }
}
