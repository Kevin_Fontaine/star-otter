﻿/******************************************************************************
 * Copyright (C) Emerge Now, Inc. 2016-2019.                                  *
 * Emerge Now Inc proprietary and confidential.                               *
 *                                                                            *
 * Use subject to the terms of the Emerge Exploration Program Agreement       *
 * and/or terms of limited, non-exclusive, non-sublicensable license to       *
 * Emerge SDK between Emerge and you, your company or any other organization. *
 ******************************************************************************/
using System;
using UnityEngine;

namespace Emerge.Communication
{
    /// <summary>
    /// This class is responsible for sending, receiving, and parsing all clock sync messages.
    /// This class acts like a Singleton but does not extend the Emerge.Core.Singleton class 
    /// because it inherits from a base class already.
    /// </summary>
    public class ClockSyncMessageHub : AbstractUDPMessageHandler, IClockSyncDataTransfer
    {
        private static ClockSyncMessageHub instance;

        public static bool InstanceExists
        {
            get
            {
                return instance == null;
            }
        }

        /// <summary>
        /// If an instance doesn't exist, one will be created.
        /// That creation must happen on the main thread.
        /// </summary>
        public static ClockSyncMessageHub Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = FindObjectOfType<ClockSyncMessageHub>();
                    if (instance == null)
                    {
                        instance = new GameObject("ClockSyncMessageHub").AddComponent<ClockSyncMessageHub>();
                        DontDestroyOnLoad(instance.gameObject);
                    }
                }

                return instance;
            }
        }

        #region Overrides

        public override void HandleIncomingMessage(UInt16 messageId, byte[] msg)
        {
            if (!IsClockSyncMessageId(messageId) || !ParseInfoFromData(msg, out string clockId, out long timestamp))
            {
                return;
            }

            OnDataReceived?.Invoke(messageId, clockId, timestamp);
        }

        #endregion


        #region IClockSyncDataTransfer

        public event ClockSyncInfoDelegate OnDataReceived;

        public IUDPConnection Connection
        {
            get
            {
                return HeadsetUDPConnection.Instance;
            }
        }

        public bool SendData(UInt16 messageId, string clockId, long timestamp)
        {
            if (HeadsetUDPConnection.Instance == null)
            {
                Debug.LogError("ClockSyncMessageHub: HeadsetUDPConnection singleton cannot be null - no data will be sent");
                return false;
            }

            return HeadsetUDPConnection.Instance.SendData(messageId, DataFromInfo(clockId, timestamp));
        }

        #endregion


        #region Parsing

        private static bool IsClockSyncMessageId(int messageId)
        {
            return (
                messageId == TactileMessageIdConstants.clockSyncMasterStartSync ||
                messageId == TactileMessageIdConstants.clockSyncSlaveRequestingTimestamp ||
                messageId == TactileMessageIdConstants.clockSyncMasterSendingTimestamp ||
                messageId == TactileMessageIdConstants.clockSyncStartClock ||
                messageId == TactileMessageIdConstants.clockSyncStopClock ||
                messageId == TactileMessageIdConstants.clockSyncResetClock ||
                messageId == TactileMessageIdConstants.clockSyncRestartClock ||
                messageId == TactileMessageIdConstants.clockSyncAbort
            );
        }

        internal static byte[] DataFromInfo(string clockId, long timestamp)
        {
            byte[] timestampData = BitConverter.GetBytes(timestamp);
            if (string.IsNullOrEmpty(clockId))
            {
                return timestampData;
            }

            byte[] clockIdData = System.Text.Encoding.UTF8.GetBytes(clockId);
            byte[] finalData = new byte[timestampData.Length + clockIdData.Length];
            Buffer.BlockCopy(timestampData, 0, finalData, 0, timestampData.Length);
            Buffer.BlockCopy(clockIdData, 0, finalData, timestampData.Length, clockIdData.Length);
            return finalData;
        }

        internal static bool ParseInfoFromData(byte[] data, out string clockId, out long timestamp)
        {
            timestamp = -1;
            clockId = null;
            if (data == null)
            {
                return false;
            }

            try
            {
                timestamp = BitConverter.ToInt64(data, 0);
                if (data.Length == sizeof(Int64))
                {
                    return true;
                }

                byte[] clockIdData = new byte[data.Length - sizeof(Int64)];
                Buffer.BlockCopy(data, sizeof(Int64), clockIdData, 0, clockIdData.Length);
                clockId = System.Text.Encoding.UTF8.GetString(clockIdData);
                return true;
            }
            catch (Exception e)
            {
                Debug.LogErrorFormat("ClockSyncMessageHub: error parsing message: {0}", e);
                return false;
            }
        }

        #endregion
    }
}
