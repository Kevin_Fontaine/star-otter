﻿/******************************************************************************
 * Copyright (C) Emerge Now, Inc. 2016-2019.                                  *
 * Emerge Now Inc proprietary and confidential.                               *
 *                                                                            *
 * Use subject to the terms of the Emerge Exploration Program Agreement       *
 * and/or terms of limited, non-exclusive, non-sublicensable license to       *
 * Emerge SDK between Emerge and you, your company or any other organization. *
 ******************************************************************************/
using System.Timers;
using UnityEngine;
using Emerge.Core;

namespace Emerge.Communication
{
    /// <summary>
    /// This class will automatically retry syncing a master clock if a failure occurs.
    /// </summary>
    public class SyncedClockRetry
    {
        #region Constants

        private const double RETRY_DELAY_MILLI = 2 * 1000;

        #endregion


        private readonly ISyncedClockMaster clock;
        private readonly IClockSyncDataTransfer messanger;
        private readonly Timer timer;


        #region Lifecycle

        ~SyncedClockRetry()
        {
            if (!clock.IsNullOrDestroyed())
            {
                clock.OnSyncTimedOut -= HandleSyncFailed;
            }

            if (!messanger.IsNullOrDestroyed() && !messanger.Connection.IsNullOrDestroyed())
            {
                messanger.Connection.OnDisconnected -= HandleDidDisconnect;
            }
        }

        public SyncedClockRetry(ISyncedClockMaster clockToSync, IClockSyncDataTransfer clockMessanger)
        {
            clock = clockToSync;
            messanger = clockMessanger;
            timer = new Timer(RETRY_DELAY_MILLI)
            {
                AutoReset = true
            };

            timer.Elapsed += HandleTimerElapsed;

            messanger.Connection.OnDisconnected += HandleDidDisconnect;
            clock.OnSyncTimedOut += HandleSyncFailed;
        }

        #endregion


        #region Event Handlers

        private void HandleTimerElapsed(object source, ElapsedEventArgs e)
        {
            timer.Stop();
            clock.SyncSlaveClocks();
        }

        private void HandleDidDisconnect(string ip, int port)
        {
            timer.Stop();
        }

        private void HandleSyncFailed()
        {
            timer.Stop();
            timer.Start();
        }

        #endregion
    }
}
