﻿/******************************************************************************
 * Copyright (C) Emerge Now, Inc. 2016-2019.                                  *
 * Emerge Now Inc proprietary and confidential.                               *
 *                                                                            *
 * Use subject to the terms of the Emerge Exploration Program Agreement       *
 * and/or terms of limited, non-exclusive, non-sublicensable license to       *
 * Emerge SDK between Emerge and you, your company or any other organization. *
 ******************************************************************************/
using System;
using System.Timers;
using UnityEngine;
using Emerge.Core;

namespace Emerge.Communication
{
    /// <summary>
    /// This clock defines the absolute time that all slave clocks will sync to.
    /// At this point in time, a master clock can only support one slave clock.
    /// </summary>
    public class SyncedClockMaster : BaseSyncedClock, ISyncedClockMaster
    {
        #region Constants

        private const double TIMEOUT_MILLI = 5 * 1000;

        #endregion


        private readonly AutoClockSync autoSync;
        private readonly SyncedClockRetry autoRetry;

        private ClockSyncState_Master currentState = ClockSyncState_Master.idle;
        private enum ClockSyncState_Master
        {
            idle,
            waitForDelayRequest
        }


        #region Lifecycle

        public SyncedClockMaster(string clockId, IClockSyncDataTransfer clockMessanger) : base(clockId, clockMessanger)
        {
            autoSync = new AutoClockSync(this, clockMessanger);
            autoRetry = new SyncedClockRetry(this, clockMessanger);
            timeout = new Timer(TIMEOUT_MILLI)
            {
                AutoReset = true
            };

            timeout.Elapsed += HandleTimeoutElapsed;
        }

        #endregion


        #region Public

        public event Action OnSyncTimedOut;

        public bool IsSyncingSlaves
        {
            get
            {
                return currentState != ClockSyncState_Master.idle;
            }
        }

        public bool SyncSlaveClocks()
        {
            if (IsSyncingSlaves)
            {
                Debug.LogWarning("SyncedClockMaster: sync is already in progress");
                return false;
            }

            if (!SendMesssageType(TactileMessageIdConstants.clockSyncMasterStartSync))
            {
                Debug.LogWarning("SyncedClockMaster: sync could not be started");
                return false;
            }

            Debug.LogFormat("SyncedClockMaster: starting sync");
            StartWaitingForTimeout();
            currentState = ClockSyncState_Master.waitForDelayRequest;
            return true;
        }

        public void StartClock()
        {
            if (currentState != ClockSyncState_Master.idle)
            {
                SendMesssageType(TactileMessageIdConstants.clockSyncAbort);
                currentState = ClockSyncState_Master.idle;
            }

            stopwatch.Start();

            if (!SendMesssageType(TactileMessageIdConstants.clockSyncStartClock))
            {
                return;
            }

            StartWaitingForTimeout();
            currentState = ClockSyncState_Master.waitForDelayRequest;
        }

        public void RestartClock()
        {
            if (currentState != ClockSyncState_Master.idle)
            {
                SendMesssageType(TactileMessageIdConstants.clockSyncAbort);
                currentState = ClockSyncState_Master.idle;
            }

            stopwatch.Restart();
            if (!SendMesssageType(TactileMessageIdConstants.clockSyncRestartClock, 0))
            {
                return;
            }

            StartWaitingForTimeout();
            currentState = ClockSyncState_Master.waitForDelayRequest;
        }

        public void StopClock()
        {
            stopwatch.Stop();
            SendMesssageType(TactileMessageIdConstants.clockSyncStopClock);
        }

        public void ResetClock()
        {
            stopwatch.Reset();
            SendMesssageType(TactileMessageIdConstants.clockSyncResetClock, 0);
        }

        #endregion


        #region Overrides

        // Note this is called on a background thread
        override protected void HandleDataReceived(UInt16 messageType, long timestamp)
        {
            if (currentState == ClockSyncState_Master.waitForDelayRequest && messageType == TactileMessageIdConstants.clockSyncSlaveRequestingTimestamp)
            {
                Debug.Log("SyncedClockMaster: sending timestamp to slave");
                currentState = ClockSyncState_Master.idle;
                SendMesssageType(TactileMessageIdConstants.clockSyncMasterSendingTimestamp);

                MainThreadExecutor.Instance.Enqueue(StopWaitingForTimeout);
            }
        }

        #endregion


        #region Timeout

        private readonly Timer timeout;

        private void StartWaitingForTimeout()
        {
            timeout.Stop();
            timeout.Start();
        }

        private void StopWaitingForTimeout()
        {
            timeout.Stop();
        }

        private void HandleTimeoutElapsed(object source, ElapsedEventArgs e)
        {
            Debug.LogFormat("SyncedClockMaster: timed out");
            timeout.Stop();

            SendMesssageType(TactileMessageIdConstants.clockSyncAbort);
            currentState = ClockSyncState_Master.idle;
            OnSyncTimedOut?.Invoke();
        }

        #endregion
    }
}
