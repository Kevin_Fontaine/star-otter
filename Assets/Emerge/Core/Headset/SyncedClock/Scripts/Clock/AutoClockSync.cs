﻿/******************************************************************************
 * Copyright (C) Emerge Now, Inc. 2016-2019.                                  *
 * Emerge Now Inc proprietary and confidential.                               *
 *                                                                            *
 * Use subject to the terms of the Emerge Exploration Program Agreement       *
 * and/or terms of limited, non-exclusive, non-sublicensable license to       *
 * Emerge SDK between Emerge and you, your company or any other organization. *
 ******************************************************************************/
using System.Timers;
using UnityEngine;
using Emerge.Core;

namespace Emerge.Communication
{
    /// <summary>
    /// This class will start a master clock sync whenever a connection is made.
    /// </summary>
    public class AutoClockSync
    {
        #region Constants

        private const double SYNC_DELAY_MILLI = 2 * 1000;

        #endregion


        private readonly ISyncedClockMaster clock;
        private readonly IClockSyncDataTransfer messanger;
        private readonly Timer timer;


        #region Lifecycle

        ~AutoClockSync()
        {
            if (!messanger.IsNullOrDestroyed() && !messanger.Connection.IsNullOrDestroyed())
            {
                messanger.Connection.OnDisconnected -= HandleDidDisconnect;
                messanger.Connection.OnConnected -= HandleDidConnect;
            }
        }

        public AutoClockSync(ISyncedClockMaster clockToSync, IClockSyncDataTransfer clockMessanger)
        {
            clock = clockToSync;
            messanger = clockMessanger;
            timer = new Timer(SYNC_DELAY_MILLI)
            {
                AutoReset = true
            };

            timer.Elapsed += HandleTimerElapsed;

            clockMessanger.Connection.OnConnected += HandleDidConnect;
            clockMessanger.Connection.OnDisconnected += HandleDidDisconnect;
        }

        #endregion


        private void HandleTimerElapsed(object source, ElapsedEventArgs e)
        {
            timer.Stop();
            clock.SyncSlaveClocks();
        }

        private void HandleDidConnect(string ip, int port)
        {
            timer.Stop();
            timer.Start();
        }

        private void HandleDidDisconnect(string ip, int port)
        {
            timer.Stop();
        }
    }
}
