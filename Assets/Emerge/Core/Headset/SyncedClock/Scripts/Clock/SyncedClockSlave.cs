﻿/******************************************************************************
 * Copyright (C) Emerge Now, Inc. 2016-2019.                                  *
 * Emerge Now Inc proprietary and confidential.                               *
 *                                                                            *
 * Use subject to the terms of the Emerge Exploration Program Agreement       *
 * and/or terms of limited, non-exclusive, non-sublicensable license to       *
 * Emerge SDK between Emerge and you, your company or any other organization. *
 ******************************************************************************/
using System;
using UnityEngine;
using Emerge.Core;

namespace Emerge.Communication
{
    /// <summary>
    /// This clock will sync to the master clock time.
    /// </summary>
    public class SyncedClockSlave : BaseSyncedClock, ISyncedClockSlave
    {
        private readonly ClockOffset offsetCalculator = new ClockOffset();
        private long? clockOffset;


        private ClockSyncState_Slave currentState = ClockSyncState_Slave.idle;
        private enum ClockSyncState_Slave
        {
            idle,
            awaitingSecondMasterTimestamp
        }


        #region Public

        public event Action OnClockSynced;
        public event Action OnClockStarted;
        public event Action OnClockStopped;
        public event Action OnClockReset;
        public event Action OnClockRestart;

        public bool IsSynced
        {
            get
            {
                return clockOffset.HasValue;
            }
        }

        public DateTime LastSyncDate
        {
            get;
            private set;
        } = DateTime.MinValue;

        public override long ClockTimeMilliseconds
        {
            get
            {
                return base.ClockTimeMilliseconds - (clockOffset ?? 0);
            }
        }

        #endregion


        #region Lifecycle

        public SyncedClockSlave(string clockId, IClockSyncDataTransfer clockMessanger) : base(clockId, clockMessanger) { }

        #endregion


        #region Overrides

        override protected void HandleDataReceived(UInt16 messageType, long timestamp)
        {
            if (currentState != ClockSyncState_Slave.idle && messageType == TactileMessageIdConstants.clockSyncAbort)
            {
                AbortAndResetSync();
            }
            else if (currentState == ClockSyncState_Slave.idle && messageType == TactileMessageIdConstants.clockSyncMasterStartSync)
            {
                HandleReceivedInitialMasterTimestamp(timestamp);
            }
            else if (currentState == ClockSyncState_Slave.awaitingSecondMasterTimestamp && messageType == TactileMessageIdConstants.clockSyncMasterSendingTimestamp)
            {
                offsetCalculator.SetTimestamp4Master(timestamp);
                ProcessOffset();
                currentState = ClockSyncState_Slave.idle;
            }
            else if (messageType == TactileMessageIdConstants.clockSyncStartClock)
            {
                stopwatch.Start();
                HandleReceivedInitialMasterTimestamp(timestamp); // start syncing
                MainThreadExecutor.Instance.Enqueue(OnClockStarted);
            }
            else if (messageType == TactileMessageIdConstants.clockSyncStopClock)
            {
                stopwatch.Stop();
                AdjustOffsetSoClockTimeIs(timestamp);
                MainThreadExecutor.Instance.Enqueue(OnClockStopped);
                MainThreadExecutor.Instance.Enqueue(OnClockSynced);
            }
            else if (messageType == TactileMessageIdConstants.clockSyncResetClock)
            {
                stopwatch.Reset();
                AdjustOffsetSoClockTimeIs(0);
                MainThreadExecutor.Instance.Enqueue(OnClockReset);
                MainThreadExecutor.Instance.Enqueue(OnClockSynced);
            }
            else if (messageType == TactileMessageIdConstants.clockSyncRestartClock)
            {
                stopwatch.Restart();
                HandleReceivedInitialMasterTimestamp(timestamp); // start syncing
                MainThreadExecutor.Instance.Enqueue(OnClockRestart);
            }
        }

        #endregion

        private long UnsyncedTimeMilliseconds
        {
            get
            {
                return base.ClockTimeMilliseconds;
            }
        }

        private void HandleReceivedInitialMasterTimestamp(long timestamp)
        {
            offsetCalculator.SetTimestamp1Master(timestamp);
            offsetCalculator.SetTimestamp2Slave(UnsyncedTimeMilliseconds);
            offsetCalculator.SetTimestamp3Slave(UnsyncedTimeMilliseconds);
            SendMesssageType(TactileMessageIdConstants.clockSyncSlaveRequestingTimestamp);
            currentState = ClockSyncState_Slave.awaitingSecondMasterTimestamp;
        }


        #region Helpers

        private void AbortAndResetSync()
        {
            currentState = ClockSyncState_Slave.idle;
            offsetCalculator.Reset();
        }

        private void ProcessOffset()
        {
            if (!offsetCalculator.TryCalculateSlaveClockOffset(out long offsetValue))
            {
                return;
            }

            clockOffset = offsetValue;
            LastSyncDate = DateTime.Now;

            MainThreadExecutor.Instance.Enqueue(OnClockSynced);
        }

        private void AdjustOffsetSoClockTimeIs(long time)
        {
            clockOffset = UnsyncedTimeMilliseconds - time;
        }

        #endregion
    }
}
