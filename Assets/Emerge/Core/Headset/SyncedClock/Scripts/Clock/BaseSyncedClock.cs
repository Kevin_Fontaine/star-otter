﻿/******************************************************************************
 * Copyright (C) Emerge Now, Inc. 2016-2019.                                  *
 * Emerge Now Inc proprietary and confidential.                               *
 *                                                                            *
 * Use subject to the terms of the Emerge Exploration Program Agreement       *
 * and/or terms of limited, non-exclusive, non-sublicensable license to       *
 * Emerge SDK between Emerge and you, your company or any other organization. *
 ******************************************************************************/
using System;
using System.Diagnostics;
using UnityEngine;
using Emerge.Core;

namespace Emerge.Communication
{
    /// <summary>
    /// This class holds all common logic between the master and slave synced clocks.
    /// </summary>
    abstract public class BaseSyncedClock
    {
        protected readonly Stopwatch stopwatch;
        private readonly string id;

        private readonly IClockSyncDataTransfer messanger;

        


        /// <summary>
        /// The synced clock time.
        /// On a master clock this will be the absolute time.
        /// On a slave clock this will be the synced time (if a sync has occured).
        /// </summary>
        virtual public long ClockTimeMilliseconds
        {
            get
            {
                return stopwatch.ElapsedMilliseconds;
            }
        }


        #region Abstract

        abstract protected void HandleDataReceived(UInt16 messageType, long timestamp);

        #endregion


        #region Lifecycle

        protected BaseSyncedClock(string clockId, IClockSyncDataTransfer clockMessanger)
        {
            id = clockId;
            messanger = clockMessanger;
            messanger.OnDataReceived += HandleMessageReceived;

            stopwatch = new Stopwatch();
            stopwatch.Start();
        }

        #endregion


        #region Event Handlers

        private void HandleMessageReceived(UInt16 messageType, string messageClockId, long timestamp)
        {
            if ((!string.IsNullOrEmpty(id) || !string.IsNullOrEmpty(messageClockId)) && messageClockId != id)
            {
                return;
            }

            HandleDataReceived(messageType, timestamp);
        }

        #endregion


        #region Protected

        /// <summary>
        /// Child classes should call this method to send a clock sync message.
        /// </summary>
        /// <param name="type">Message type</param>
        /// <returns>Whether the message could be sent.</returns>
        protected bool SendMesssageType(UInt16 type)
        {
            return SendMesssageType(type, stopwatch.ElapsedMilliseconds);
        }

        protected bool SendMesssageType(UInt16 type, long timestamp)
        {
            if (messanger.IsNullOrDestroyed())
            {
                return false;
            }

            return messanger.SendData(type, id, timestamp);
        }

        #endregion
    }
}
