﻿/******************************************************************************
 * Copyright (C) Emerge Now, Inc. 2016-2019.                                  *
 * Emerge Now Inc proprietary and confidential.                               *
 *                                                                            *
 * Use subject to the terms of the Emerge Exploration Program Agreement       *
 * and/or terms of limited, non-exclusive, non-sublicensable license to       *
 * Emerge SDK between Emerge and you, your company or any other organization. *
 ******************************************************************************/

using UnityEngine;

internal class ClockOffset
{
    private long? timestamp1Master;
    private long? timestamp2Slave;
    private long? timestamp3Slave;
    private long? timestamp4Master;

    internal void SetTimestamp1Master(long timestamp)
    {
        timestamp1Master = timestamp;
    }

    internal void SetTimestamp2Slave(long timestamp)
    {
        timestamp2Slave = timestamp;
    }

    internal void SetTimestamp3Slave(long timestamp)
    {
        timestamp3Slave = timestamp;
    }

    internal void SetTimestamp4Master(long timestamp)
    {
        timestamp4Master = timestamp;
    }

    internal bool CanCalculateOffset
    {
        get
        {
            return (
                timestamp1Master.HasValue && 
                timestamp2Slave.HasValue && 
                timestamp3Slave.HasValue && 
                timestamp4Master.HasValue
            );
        }
    }

    internal bool TryCalculateSlaveClockOffset(out long offset)
    {
        if (!CanCalculateOffset)
        {
            Debug.LogWarningFormat("ClockOffset: not enough info to calculate offset:" +
                "\ntimestamp1Master={0}" +
                "\ntimestamp2Slave={1}" +
                "\ntimestamp3Slave={2}" +
                "\ntimestamp4Master={3}", 
                timestamp1Master, 
                timestamp2Slave, 
                timestamp3Slave, 
                timestamp4Master
            );
            offset = 0;
            return false;
        }

        offset = (timestamp2Slave.Value - timestamp1Master.Value - timestamp4Master.Value + timestamp3Slave.Value) / 2;
        return true;
    }

    internal void Reset()
    {
        timestamp1Master = null;
        timestamp2Slave = null;
        timestamp3Slave = null;
        timestamp4Master = null;
    }
}
