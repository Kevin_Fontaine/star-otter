﻿/******************************************************************************
 * Copyright (C) Emerge Now, Inc. 2016-2019.                                  *
 * Emerge Now Inc proprietary and confidential.                               *
 *                                                                            *
 * Use subject to the terms of the Emerge Exploration Program Agreement       *
 * and/or terms of limited, non-exclusive, non-sublicensable license to       *
 * Emerge SDK between Emerge and you, your company or any other organization. *
 ******************************************************************************/
using System;

namespace Emerge.Communication
{
    public interface ISyncedClockSlave
    {
        event Action OnClockSynced;
        event Action OnClockStarted;
        event Action OnClockStopped;
        event Action OnClockReset;
        event Action OnClockRestart;

        long ClockTimeMilliseconds { get; }

        bool IsSynced { get; }

        DateTime LastSyncDate { get; }
    }
}
