﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Emerge.Communication
{
    public class UDPMessageReceiver : UDPMessageHandler
    {
        [SerializeField]
        TactileHardwareController tactileHardwareController;
        public override void HandleIncomingMessage(UInt16 messageId, byte[] msg)
        {
            if (messageId == TactileMessageIdConstants.tactileData)
            {
                tactileHardwareController.SendDataToTactileEngine(System.Text.Encoding.UTF8.GetString(msg));
            }
        }
    }
}