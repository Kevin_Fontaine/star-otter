﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace Emerge.Communication
{
    public class UDPMessageSender : MonoBehaviour
    {
        byte[] buffer;
        IUDPSender udpOut;
        // Start is called before the first frame update
        void Start()
        {
            udpOut = HeadsetUDPConnection.Instance;
            buffer = null;
        }

        public void SendMessageToHardware(string message)
        {
            if (message == String.Empty)
            {
                return;
            }
            if (udpOut == null)
            {
                udpOut = HeadsetUDPConnection.Instance;
            }
            buffer = Encoding.ASCII.GetBytes(message);
            
            udpOut.SendData(TactileMessageIdConstants.tactileData, buffer);
        }
        // Update is called once per frame
        void Update()
        {
            if(Input.GetKeyUp(KeyCode.M))
            {
                buffer = Encoding.ASCII.GetBytes("{\"action\":\"play\",\"data\":{\"tfx_id\":\"tfx1\",\"params\":{\"mode\":\"modify\",\"loop\":1},\"tpt_set\":[{\"pt_index\":0,\"pos\":[[-0.03,0.15,0]],\"beam\":1,\"amp\":99.684,\"time_pp\":4},{\"pt_index\":1,\"pos\":[[0,0.15,-0.027]],\"beam\":1,\"amp\":99.318,\"time_pp\":4},{\"pt_index\":2,\"pos\":[[-0.03,0.15,-0.027]],\"beam\":1,\"amp\":99.318,\"time_pp\":4}]}}");
                udpOut.SendData(TactileMessageIdConstants.tactileData, buffer);
            }
        }

        void OnApplicationQuit()
        {
            HeadsetUDPConnection.Instance.Disconnect();
        }
    }
}
