using Ceras;
using Ceras.Formatters;
using Ceras.Formatters.AotGenerator;

namespace Ceras.GeneratedFormatters
{
	public static class GeneratedFormatters
	{
		public static void UseFormatters(SerializerConfig config)
		{
			config.ConfigType<Ceras.GeneratedFormatters.UDPMessage>().CustomFormatter = new UDPMessageFormatter();
			config.ConfigType<Ceras.GeneratedFormatters.CustomData>().CustomFormatter = new CustomDataFormatter();
			config.ConfigType<Ceras.GeneratedFormatters.Hand>().CustomFormatter = new HandFormatter();
		}
	}

	internal class UDPMessageFormatter : IFormatter<Ceras.GeneratedFormatters.UDPMessage>
	{
		IFormatter<Ceras.GeneratedFormatters.CustomData> _customDataFormatter;
		IFormatter<Ceras.GeneratedFormatters.Hand> _handFormatter;
		IFormatter<bool> _boolFormatter;
		IFormatter<System.Collections.Generic.List<ushort>> _list_ushortFormatter;

		public void Serialize(ref byte[] buffer, ref int offset, Ceras.GeneratedFormatters.UDPMessage value)
		{
			_customDataFormatter.Serialize(ref buffer, ref offset, value.customData);
			_handFormatter.Serialize(ref buffer, ref offset, value.hand_L);
			_handFormatter.Serialize(ref buffer, ref offset, value.hand_R);
			_handFormatter.Serialize(ref buffer, ref offset, value.remoteHand_L);
			_handFormatter.Serialize(ref buffer, ref offset, value.remoteHand_R);
			_boolFormatter.Serialize(ref buffer, ref offset, value.testThis);
			_list_ushortFormatter.Serialize(ref buffer, ref offset, value.extraObjectPositionsTEMP);
		}

		public void Deserialize(byte[] buffer, ref int offset, ref Ceras.GeneratedFormatters.UDPMessage value)
		{
			_customDataFormatter.Deserialize(buffer, ref offset, ref value.customData);
			_handFormatter.Deserialize(buffer, ref offset, ref value.hand_L);
			_handFormatter.Deserialize(buffer, ref offset, ref value.hand_R);
			_handFormatter.Deserialize(buffer, ref offset, ref value.remoteHand_L);
			_handFormatter.Deserialize(buffer, ref offset, ref value.remoteHand_R);
			_boolFormatter.Deserialize(buffer, ref offset, ref value.testThis);
			_list_ushortFormatter.Deserialize(buffer, ref offset, ref value.extraObjectPositionsTEMP);
		}
	}

	internal class CustomDataFormatter : IFormatter<Ceras.GeneratedFormatters.CustomData>
	{
		IFormatter<bool> _boolFormatter;
		IFormatter<int> _intFormatter;
		IFormatter<float> _floatFormatter;

		public void Serialize(ref byte[] buffer, ref int offset, Ceras.GeneratedFormatters.CustomData value)
		{
			_boolFormatter.Serialize(ref buffer, ref offset, value.beatTrigger);
			_intFormatter.Serialize(ref buffer, ref offset, value.currentEffect);
			_floatFormatter.Serialize(ref buffer, ref offset, value.BPM);
			_floatFormatter.Serialize(ref buffer, ref offset, value.amplitude2);
			_floatFormatter.Serialize(ref buffer, ref offset, value.amplitude);
			_floatFormatter.Serialize(ref buffer, ref offset, value.beatTriggerTimeStamp);
		}

		public void Deserialize(byte[] buffer, ref int offset, ref Ceras.GeneratedFormatters.CustomData value)
		{
			_boolFormatter.Deserialize(buffer, ref offset, ref value.beatTrigger);
			_intFormatter.Deserialize(buffer, ref offset, ref value.currentEffect);
			_floatFormatter.Deserialize(buffer, ref offset, ref value.BPM);
			_floatFormatter.Deserialize(buffer, ref offset, ref value.amplitude2);
			_floatFormatter.Deserialize(buffer, ref offset, ref value.amplitude);
			_floatFormatter.Deserialize(buffer, ref offset, ref value.beatTriggerTimeStamp);
		}
	}

	internal class HandFormatter : IFormatter<Ceras.GeneratedFormatters.Hand>
	{
		IFormatter<bool> _boolFormatter;
		IFormatter<System.Collections.Generic.List<ushort>> _list_ushortFormatter;
		//IFormatter<System.Collections.Generic.List<uint>> _list_uintFormatter;

		public void Serialize(ref byte[] buffer, ref int offset, Ceras.GeneratedFormatters.Hand value)
		{
			_boolFormatter.Serialize(ref buffer, ref offset, value.active);
			_boolFormatter.Serialize(ref buffer, ref offset, value.avatarType);
			_list_ushortFormatter.Serialize(ref buffer, ref offset, value.positions);
			//_list_uintFormatter.Serialize(ref buffer, ref offset, value.rotations);
		}

		public void Deserialize(byte[] buffer, ref int offset, ref Ceras.GeneratedFormatters.Hand value)
		{
			_boolFormatter.Deserialize(buffer, ref offset, ref value.active);
			_boolFormatter.Deserialize(buffer, ref offset, ref value.avatarType);
			_list_ushortFormatter.Deserialize(buffer, ref offset, ref value.positions);
			//_list_uintFormatter.Deserialize(buffer, ref offset, ref value.rotations);
		}
	}
}
