﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Ceras;
using Ceras.GeneratedFormatters;
using Ceras.Formatters;

public class Serializer_Ceras : MonoBehaviour, ISerializer
{

    CerasSerializer ceras;

    public void Init()
    {
        var config = new SerializerConfig();

        config.Advanced.AotMode = AotMode.Enabled;
        //NOTE: this line crashes - CerasUnityFormatters.ApplyToConfig(config); // Formatters from the UnityExtension for Vector3 etc...
        GeneratedFormatters.UseFormatters(config);// Also add this line when creating your SerializerConfig if you use IL2CPP

        ceras = new CerasSerializer(config);
    }


    //NEED THESE HERE SO IL2CPP doesnt strip out the functionality. these do not need to be called, just written out here
    //NOTE:
    //Unity classes dont work atm - only primitives
    public void DummyFunctions()
    {
        var unused = new CerasSerializer();
        new CollectionFormatter<List<ulong>, ulong>(unused); // for:  "public List<ulong> positions;"
        new CollectionFormatter<List<uint>, uint>(unused); // for:  "public List<uint> rotations;"
        new CollectionFormatter<List<ushort>, ushort>(unused);

    }

    public byte[] Serialize<T>(T obj)
    {
        return ceras.Serialize<T>(obj);
    }

    //// Recycle the serialization buffer by keeping the reference to it around.
    //// Optionally we can even let Ceras create (or resize) the buffer for us.
    public void Serialize<T>(T obj, ref byte[] buffer)
    {
        ceras.Serialize<T>(obj, ref buffer);
    }

    public T Deserialize<T>(byte[] b)
    {
        return ceras.Deserialize<T>(b);
    }

    //recycle the message object, no GC
    public void Deserialize<T>(ref T receivedMessage, byte[] data)
    {
        ceras.Deserialize<T>(ref receivedMessage, data);
    }
}
