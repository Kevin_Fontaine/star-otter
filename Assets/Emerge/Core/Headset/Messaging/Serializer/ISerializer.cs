﻿using Emerge.NetworkMessaging;

public interface ISerializer
{
    void Init();
   
    byte[] Serialize<T>(T obj);
    void Serialize<T>(T obj, ref byte[] buffer);//no garbage version
    
    T Deserialize<T>(byte[] b);
    void Deserialize<T>(ref T receivedMessage, byte[] data); //no garbage version
}
