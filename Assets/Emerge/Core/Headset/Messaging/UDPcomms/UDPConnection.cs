﻿/******************************************************************************
 * Copyright (C) Emerge Now, Inc. 2016-2019.                                  *
 * Emerge Now Inc proprietary and confidential.                               *
 *                                                                            *
 * Use subject to the terms of the Emerge Exploration Program Agreement       *
 * and/or terms of limited, non-exclusive, non-sublicensable license to       *
 * Emerge SDK between Emerge and you, your company or any other organization. *
 ******************************************************************************/
using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Collections.Generic;
using UnityEngine;
using LiteNetLib;
using LiteNetLib.Utils;
using Emerge.Core;

namespace Emerge.Communication
{
    /// <summary>
    /// This class manages a basic UDP connection.
    /// It can be used on all target platforms.
    /// </summary>
    [RequireComponent(typeof(MainThreadExecutor))]
    public class UDPConnection : MonoBehaviour, IUDPConnection, IUDPReceiver, IUDPSender, INetEventListener
    {
        #region Constants

        private const int UPDATE_TIME = 15;
        private const int THREAD_SLEEP_PERIOD = 15;
        private const int DEFAULT_PORT = 5000;
        private const int AUTOCONNECTION_BODY = 1;
        private const string CONNECTION_KEY = "Emerge.Communication.UDPConnection.Default";

        #endregion

        /// <summary>
        /// This enum defines the behaviour this class should take for automatically connecting to peers.
        /// </summary>
        public enum AutoConnection
        {
            /// <summary>
            /// No automatic connection.
            /// </summary>
            None = 0,

            /// <summary>
            /// This will object will broadcast itself as available for a connection.
            /// </summary>
            Broadcast = 1,

            /// <summary>
            /// This class will listen for broadcasted available connections.
            /// It will try to connect when it finds one.
            /// </summary>
            Listen = 2
        }

        [SerializeField]
        private bool startOnAwake = true;

        /// <summary>
        /// If true, then the data received event will be invoked on the main thread.
        /// This will incur a slight delay, and is not recommended unless explicitly needed.
        /// If false, the data received event will be invoked on the connection background thread.
        /// All other events will always be invoked on the main thread.
        /// </summary>
        public bool dataEventMainThread = false;

        [SerializeField]
        private int myPort = DEFAULT_PORT;

        [SerializeField]
        protected int targetPort = DEFAULT_PORT;

        [Header("Autoconnection (For Testing Only)")]

        /// <summary>
        /// The autoconnect behaviour this object should use.
        /// </summary>
        [SerializeField]
        private AutoConnection autoConnect = AutoConnection.None;

        private NetManager netManager;
        private NetPeer connectedPeer;

        /// <summary>
        /// For thread safety, please lock this object while using it.
        /// </summary>
        private readonly NetDataWriter dataWriter = new NetDataWriter();

        /// <summary>
        /// This is the body of the autoconnection broadcast message.
        /// It is only used if autoconnection is enabled.
        /// </summary>
        private readonly byte[] autoConnectionMessage = new byte[] { AUTOCONNECTION_BODY };

        private Thread workerThread;
        private bool shouldExitWorkerThread;
        private MainThreadExecutor mainThread;

        #region Unity Methods

        protected virtual void OnDestroy()
        {
            shouldExitWorkerThread = true;

            if (netManager != null && netManager.IsRunning)
            {
                netManager.Stop();
            }
        }

        protected virtual void Awake()
        {
            mainThread = GetComponent<MainThreadExecutor>();
            netManager = new NetManager(this)
            {
                UpdateTime = UPDATE_TIME
            };

            if (startOnAwake)
            {
                StartRunning(myPort);
            }

            workerThread = new Thread(RunConnectionWork)
            {
                IsBackground = true
            };
            workerThread.Start();
        }

        private void RunConnectionWork()
        {
            while (true)
            {
                netManager.PollEvents();
                Thread.Sleep(THREAD_SLEEP_PERIOD);

                if (shouldExitWorkerThread)
                {
                    break;
                }
            }
        }

        protected virtual void Update()
        {
            if (!IsConnected && autoConnect == AutoConnection.Broadcast)
            {
                netManager.SendDiscoveryRequest(autoConnectionMessage, targetPort);
            }
        }

        #endregion


        #region IUDPConnection

        public event UDPPeerDelegate OnConnected;
        public event UDPPeerDelegate OnDisconnected;

        public bool IsConnected
        {
            get
            {
                return connectedPeer != null;
            }
        }

        public bool IsRunning
        {
            get
            {
                return netManager != null && netManager.IsRunning;
            }
        }

        public bool StartRunning(int port)
        {
            if (netManager.IsRunning)
            {
                Debug.LogWarningFormat("UDPConnection: start failed, already running on port '{0}'", myPort);
                return false;
            }

            myPort = port;
            switch (autoConnect)
            {
                case AutoConnection.Broadcast:
                    netManager.UnconnectedMessagesEnabled = true;
                    break;

                case AutoConnection.Listen:
                    netManager.DiscoveryEnabled = true;
                    break;
            }

            netManager.Start(port);
            return true;
        }

        public bool Connect(string address, int port)
        {
            if (!netManager.IsRunning)
            {
                Debug.LogWarning("UDPConnection: must be started before a connection can be made");
                return false;
            }

            if (connectedPeer != null)
            {
                Debug.LogWarning("UDPConnection: cannot establish new connection, already connected to peer");
                return false;
            }

            Debug.LogFormat("UDPConnection: attempting to connect to {0}:{1}", address, port);
            targetPort = port;
            netManager.Connect(address, port, CONNECTION_KEY);
            return true;
        }

        public void Disconnect()
        {
            netManager.DisconnectAll();

            if (connectedPeer != null)
            {
                NetPeer oldPeer = connectedPeer;
                connectedPeer = null;
                OnDisconnected?.Invoke(oldPeer.EndPoint.Address.ToString(), oldPeer.EndPoint.Port);
            }
        }

        #endregion


        #region IUDPReceiver

        public event UDPDataDelegate OnDataReceived;

        #endregion


        #region IUDPSender 

        public bool SendData(UInt16 messageId, byte[] data)
        {
            // We use ReliableUnordered because it has a larger size limit than Unreliable
            return SendData(messageId, data, UDPSendMethod.ReliableOrdered);
        }

        public bool SendData(UInt16 messageId, byte[] data, UDPSendMethod sendMethod)
        {
            if (connectedPeer == null)
            {
                return false;
            }

            byte[] finalMessage = Concatenate(messageId, data);
            lock (dataWriter)
            {
                dataWriter.Reset();
                dataWriter.Put(finalMessage);
                connectedPeer.Send(dataWriter, ConvertSendMethodEnum(sendMethod));
            }

            return true;
        }

        #endregion


        #region INetEventListener

        /*
         * All INetEventListener events are invoked on the background thread
         * (these events are called directly from netManager.PollEvents())
         */

        public void OnPeerConnected(NetPeer peer)
        {
            Debug.LogFormat("UDPConnection: connected to peer at '{0}:{1}'", peer.EndPoint.Address.ToString(), peer.EndPoint.Port);
            connectedPeer = peer;

            mainThread.Enqueue(() => 
            {
                OnConnected?.Invoke(peer.EndPoint.Address.ToString(), peer.EndPoint.Port);
            });
        }

        public void OnNetworkError(IPEndPoint endPoint, SocketError socketErrorCode)
        {
            Debug.LogWarningFormat("UDPConnection: network error: '{0}'", socketErrorCode);
        }

        public void OnNetworkReceiveUnconnected(IPEndPoint remoteEndPoint, NetPacketReader reader, UnconnectedMessageType messageType)
        {
            if (autoConnect == AutoConnection.None)
            {
                // Unconnected messages are only used to establish an automatic connection.
                return;
            }

            if (messageType == UnconnectedMessageType.DiscoveryRequest)
            {
                Debug.Log("UDPConnection: received discovery request. Sending discovery response.");
                lock (dataWriter)
                {
                    dataWriter.Reset();
                    dataWriter.Put(AUTOCONNECTION_BODY);
                    netManager.SendUnconnectedMessage(dataWriter, remoteEndPoint);
                }
            }
            else if (messageType == UnconnectedMessageType.BasicMessage && netManager.PeersCount == 0 && reader.GetInt() == AUTOCONNECTION_BODY)
            {
                Debug.LogFormat("UDPConnection: received discovery response. Connecting to: '{0}:{1}'", remoteEndPoint.Address, remoteEndPoint.Port);
                netManager.Connect(remoteEndPoint, CONNECTION_KEY);
            }
        }

        public void OnNetworkLatencyUpdate(NetPeer peer, int latency) { /* noop */ }

        public void OnConnectionRequest(ConnectionRequest request)
        {
            request.AcceptIfKey(CONNECTION_KEY);
        }

        public void OnPeerDisconnected(NetPeer peer, DisconnectInfo disconnectInfo)
        {
            Debug.LogFormat("UDPConnection: peer disconnected '{0}', info: '{1}'", peer.EndPoint, disconnectInfo.Reason);
            if (peer != connectedPeer)
            {
                return;
            }

            connectedPeer = null;

            if (OnDisconnected == null)
            {
                return;
            }

            mainThread.Enqueue(() =>
            {
                OnDisconnected?.Invoke(peer.EndPoint.Address.ToString(), peer.EndPoint.Port);
            });
        }

        public void OnNetworkReceive(NetPeer peer, NetPacketReader reader, DeliveryMethod deliveryMethod)
        {
            if (peer != connectedPeer || OnDataReceived == null)
            {
                return;
            }

            byte[] data = reader.GetRemainingBytes();
            if (!Parse(data, out UInt16 messageId, out byte[] contents))
            {
                return;
            }

            if (dataEventMainThread)
            {
                mainThread.Enqueue(() =>
                {
                    OnDataReceived?.Invoke(messageId, contents);
                });
            }
            else
            {
                OnDataReceived?.Invoke(messageId, contents);
            }
        }

        #endregion


        #region Helpers

        private static byte[] Concatenate(UInt16 messageId, byte[] messageContents)
        {
            byte[] idData = BitConverter.GetBytes(messageId);
            if (messageContents == null || messageContents.Length == 0)
            {
                return idData;
            }

            byte[] finalData = new byte[idData.Length + messageContents.Length];
            Buffer.BlockCopy(idData, 0, finalData, 0, idData.Length);
            Buffer.BlockCopy(messageContents, 0, finalData, idData.Length, messageContents.Length);
            return finalData;
        }

        private static bool Parse(byte[] completeMessage, out UInt16 messageId, out byte[] contents)
        {
            try
            {
                messageId = BitConverter.ToUInt16(completeMessage, 0);
                if (completeMessage.Length <= sizeof(UInt16))
                {
                    contents = null;
                    return true;
                }

                contents = new byte[completeMessage.Length - sizeof(UInt16)];
                Buffer.BlockCopy(completeMessage, sizeof(UInt16), contents, 0, contents.Length);
                return true;
            }
            catch (Exception e)
            {
                Debug.LogErrorFormat("UDPConnection: error parsing message: {0}", e);
                messageId = 0;
                contents = null;
                return false;
            }
        }

        /// <summary>
        /// Converts our public facing send method enum to its LiteNet equivalent.
        /// This lets us contain the LiteNet code inside this class.
        /// </summary>
        /// <param name="sendMethod">Send method enum</param>
        /// <returns>The LiteNet equivalent enum</returns>
        private static DeliveryMethod ConvertSendMethodEnum(UDPSendMethod sendMethod)
        {
            DeliveryMethod liteNetValue = DeliveryMethod.Unreliable;
            switch (sendMethod)
            {
                case UDPSendMethod.Unreliable:
                    liteNetValue = DeliveryMethod.Unreliable;
                    break;

                case UDPSendMethod.ReliableOrdered:
                    liteNetValue = DeliveryMethod.ReliableOrdered;
                    break;

                case UDPSendMethod.ReliableSequenced:
                    liteNetValue = DeliveryMethod.ReliableSequenced;
                    break;

                case UDPSendMethod.ReliableUnordered:
                    liteNetValue = DeliveryMethod.ReliableUnordered;
                    break;

                case UDPSendMethod.Sequenced:
                    liteNetValue = DeliveryMethod.Sequenced;
                    break;
            }

            return liteNetValue;
        }

        #endregion
    }
}
