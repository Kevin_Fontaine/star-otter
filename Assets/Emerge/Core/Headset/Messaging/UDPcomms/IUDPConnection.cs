﻿/******************************************************************************
 * Copyright (C) Emerge Now, Inc. 2016-2019.                                  *
 * Emerge Now Inc proprietary and confidential.                               *
 *                                                                            *
 * Use subject to the terms of the Emerge Exploration Program Agreement       *
 * and/or terms of limited, non-exclusive, non-sublicensable license to       *
 * Emerge SDK between Emerge and you, your company or any other organization. *
 ******************************************************************************/

namespace Emerge.Communication
{
    public delegate void UDPPeerDelegate(string address, int port);

    public interface IUDPConnection
    {
        event UDPPeerDelegate OnConnected;
        event UDPPeerDelegate OnDisconnected;

        bool IsConnected { get; }

        bool IsRunning { get; }

        bool StartRunning(int port);

        bool Connect(string address, int port);

        void Disconnect();
    } 
}
