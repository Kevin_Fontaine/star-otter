﻿/******************************************************************************
 * Copyright (C) Emerge Now, Inc. 2016-2019.                                  *
 * Emerge Now Inc proprietary and confidential.                               *
 *                                                                            *
 * Use subject to the terms of the Emerge Exploration Program Agreement       *
 * and/or terms of limited, non-exclusive, non-sublicensable license to       *
 * Emerge SDK between Emerge and you, your company or any other organization. *
 ******************************************************************************/
using Emerge.Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Emerge.Communication
{
    /// <summary>
    /// A UDP connection specifically for connecting to the headset.
    /// It can be used on all target platforms.
    /// This class acts like a Singleton but does not extend the Emerge.Core.Singleton class 
    /// because it inherits from a base class already.
    /// </summary>
    public class HeadsetUDPConnection : UDPConnection 
    {
        public static HeadsetUDPConnection Instance
        {
            get;
            private set;
        }

        #region UnityEvents 

        protected override void Awake()
        {
            base.Awake();
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Debug.LogWarningFormat("HeadsetUDPConnection: multiple instances found, using '{0}' as the singleton", Instance.gameObject.name);
            }
        }

        #endregion


        #region Public

        /// <summary>
        /// Connects to the given IP address using the headset target port.
        /// </summary>
        /// <param name="ip">Address</param>
        public void Connect(string ip)
        {
            Connect(ip, targetPort);
        }

        #endregion
    }
}
