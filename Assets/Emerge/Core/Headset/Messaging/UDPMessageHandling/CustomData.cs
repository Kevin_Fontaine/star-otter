﻿using Ceras.GeneratedFormatters;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ceras.GeneratedFormatters
{

    [System.Serializable]
    public class CustomData
    {
        public int currentEffect;
        public float BPM;

        public float amplitude;
        public float amplitude2;

        public bool beatTrigger;
        public float beatTriggerTimeStamp;
        public float beatTriggerLength;

        public bool onsetTrigger;
        public float onsetTriggerTimeStamp;
        public float onsetTriggerStrength;
    }
}
