﻿using Emerge.HandTracking;
using Emerge;
using Ceras.GeneratedFormatters;

public class MessageXfer_Hands : MessageXferHandler
{
    //NOTE: this should be automatic later on
    //      currently though:
    //      PC always has rigged since thats where hand tracking is happening
    //
    //      HMD local hands are primitive (5 fingers and palm)
    //      and HMD remote hands are rigged
    //
    // so data xfer goes like this: rigged -> primitive (for local hands)
    //                              rigged -> rigged (for remote hands)

    private readonly AvatarType destinationHandType = AvatarType.Primitive;
    private readonly AvatarType destinationRemoteHandType = AvatarType.Rigged;

    // Decoding hand helpers
    private UDPMessageHandDataSource localHandsDataSource;
    private UDPMessageHandDataSource remoteHandsDataSource;
    private SpaceTransformation spaceTransformation;


    private void Awake()
    {
#if UNITY_WSA
        spaceTransformation = gameObject.AddComponent<SpaceTransformation>();
        spaceTransformation.sourceLocalSpace = FindObjectOfType<HardwareMarkerCenter>().transform;
        spaceTransformation.destinationLocalSpace = FindObjectOfType<HardwareMarkerImageMarker>().transform;

        if (localHandsDataSource == null)
        {
            localHandsDataSource = new UDPMessageHandDataSource(EmergeHandDataSourceIds.UDP_LOCAL, true);
        }

        if (remoteHandsDataSource == null)
        {
            remoteHandsDataSource = new UDPMessageHandDataSource(EmergeHandDataSourceIds.UDP_REMOTE, false);
        }
        
        Emerge.Tracking.HandTracking.StartAllTracking();
#endif
    }

    public override void Init(ref UDPMessage m)
    {
        m.Init(destinationHandType, destinationRemoteHandType);
    }


    //===================================================================================
    //  ENCODE

    public override void Encode(UDPMessage message)
    {
        //Local hands----------------------------------- 
        if (Emerge.Tracking.HandTracking.HasDefaultDataSource && Emerge.Tracking.HandTracking.Default.HasData)
        {
            Emerge.Tracking.HandSet handSet = Emerge.Tracking.HandTracking.Default.MostRecentHandSet;
            EncodeHand(message.hand_L, handSet.left);
            EncodeHand(message.hand_R, handSet.right);
        }
        else
        {
            EncodeHand(message.hand_L, null);
            EncodeHand(message.hand_R, null);
        }

        //REMOTE hands-------------------------------------
        Emerge.Tracking.IHandDataSource remoteDataSource = Emerge.Tracking.HandTracking.DataSourceForId(EmergeHandDataSourceIds.REMOTE);
        if (!remoteDataSource.IsNullOrDestroyed() && remoteDataSource.HasData)
        {
            Emerge.Tracking.HandSet handSet = remoteDataSource.MostRecentHandSet;
            EncodeHand(message.remoteHand_L, handSet.left);
            EncodeHand(message.remoteHand_R, handSet.right);
        }
        else
        {
            EncodeHand(message.remoteHand_L, null);
            EncodeHand(message.remoteHand_R, null);
        }
    }


    private void EncodeHand(Hand messageData, Emerge.Tracking.Hand trackedData)
    {
        messageData.active = trackedData != null && trackedData.active;
        if (!messageData.active)
        {
            return;
        }

        messageData.StoreData(trackedData);
    }


    //===================================================================================
    //  DECODE

    public override void Decode(UDPMessage message)
    {
        if (localHandsDataSource != null)
        {
            localHandsDataSource.DecodeMessage(message.hand_L, message.hand_R, spaceTransformation);
        }

        if (remoteHandsDataSource != null)
        {
            remoteHandsDataSource.DecodeMessage(message.remoteHand_L, message.remoteHand_R, spaceTransformation);
        }
    }
}
