﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Emerge.NetworkMessaging;
using Ceras.GeneratedFormatters;

public abstract class MessageXferHandler : MonoBehaviour
{
    public virtual void Init(ref UDPMessage m) { }

    public virtual void Encode(UDPMessage m)
    {
        // empty
    }

    public virtual void Decode(UDPMessage m)
    {
        // empty
    }
}
