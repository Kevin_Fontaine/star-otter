﻿using UnityEngine;  
using Emerge.HandTracking;
using Emerge; 
using Ceras.GeneratedFormatters;

public class MessageXfer_Hands_Test : MessageXfer_Hands
{
    /*

#if !UNITY_WSA && UNITY_EDITOR
    public bool debug;
    //public List<Transform> debugObjects;

    public bool useLocalHandsToPuppetRemoteHands;


    [Header("mock hands for testing remote side in editor. ignored in builds")]
    public PuppetHand_riggedHand mock_puppetHand_local_L;
    public PuppetHand_riggedHand mock_puppetHand_local_R;
    public PuppetHand_riggedHand mock_puppetHand_remote_L;
    public PuppetHand_riggedHand mock_puppetHand_remote_R;
#endif




    //===================================================================================
    //  ENCODE

    public override void Encode(UDPMessage message)
    {


#if !UNITY_WSA && UNITY_EDITOR
        if (debug)
        {
            if (useLocalHandsToPuppetRemoteHands)
            {
                puppetHand_remote_L = AvatarManager.Instance.GetLocalAvatar().GetHand(Handedness.Left) as PuppetHand_riggedHand;
                puppetHand_remote_R = AvatarManager.Instance.GetLocalAvatar().GetHand(Handedness.Right) as PuppetHand_riggedHand;
            }
        }
#endif

        base.Encode(message);


#if !UNITY_WSA && UNITY_EDITOR
        if (debug)
        {
            if (useLocalHandsToPuppetRemoteHands)
            {
                puppetHand_remote_L = AvatarManager.Instance.GetRemoteAvatar().GetHand(Handedness.Left) as PuppetHand_riggedHand;
                puppetHand_remote_R = AvatarManager.Instance.GetRemoteAvatar().GetHand(Handedness.Right) as PuppetHand_riggedHand;
            }
        }
#endif

    }






    //===================================================================================
    //  DECODE

    public override void Decode(UDPMessage message)
    {


#if !UNITY_WSA && UNITY_EDITOR
        if (debug)
        {
            puppetHand_local_L = mock_puppetHand_local_L;
            puppetHand_local_R = mock_puppetHand_local_R;

            puppetHand_remote_L = mock_puppetHand_remote_L;
            puppetHand_remote_R = mock_puppetHand_remote_R;
        }
#endif

        base.Decode(message);


#if !UNITY_WSA && UNITY_EDITOR
        if (debug)
        {
            //set the hands back from the mock hands to the real ones for the next encoding
            puppetHand_local_L = AvatarManager.Instance.GetLocalAvatar().GetHand(Handedness.Left) as PuppetHand_riggedHand;
            puppetHand_local_R = AvatarManager.Instance.GetLocalAvatar().GetHand(Handedness.Right) as PuppetHand_riggedHand;

            puppetHand_remote_L = AvatarManager.Instance.GetRemoteAvatar().GetHand(Handedness.Left) as PuppetHand_riggedHand;
            puppetHand_remote_R = AvatarManager.Instance.GetRemoteAvatar().GetHand(Handedness.Right) as PuppetHand_riggedHand;
        }
#endif

    }



    */
}