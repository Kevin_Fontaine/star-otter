﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine; 
using Ceras.GeneratedFormatters;

public class MessageXfer_ExtraInfo : MessageXferHandler
{
    public bool testValue;

#if !UNITY_WSA && UNITY_EDITOR
    [Space(30)]
    [Header("Editor only debug/testing")]
    public bool debug; 
#endif
    public override void Encode(UDPMessage m)
    {
        m.testThis = testValue;
    }

    public override void Decode(UDPMessage m)
    {
        testValue = m.testThis;

#if !UNITY_WSA && UNITY_EDITOR
        if (debug) Debug.Log("decoded value = " + m.testThis);
#endif
    }




}
