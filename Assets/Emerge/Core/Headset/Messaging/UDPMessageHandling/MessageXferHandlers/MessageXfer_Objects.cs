﻿using Ceras.GeneratedFormatters;
using System.Collections.Generic;
using UnityEngine;
using static Emerge.NetworkMessaging.Compression.Position;

public class MessageXfer_Objects : MessageXferHandler
{

    [Header("Extra Objects to transfer")]
    public List<Transform> extraObjects;


#if !UNITY_WSA && UNITY_EDITOR
    [Space(30)]
    [Header("Editor only debug/testing")]
    public bool debug;
    public List<Transform> debugObjects;
#endif

    public override void Init(ref UDPMessage m)
    {
        //using a variable size list of objects for now, but to use a fixed list do this:
        //m.extraObjectPositionsTEMP = new Vector3[extraObjects.Count];
    }

    public override void Encode(UDPMessage message)
    { 
        message.extraObjectPositionsTEMP.Clear();

        for (int i = 0; i < extraObjects.Count; i++)
        {
            message.extraObjectPositionsTEMP.Add(CompressPosition( extraObjects[i].localPosition.x));
            message.extraObjectPositionsTEMP.Add(CompressPosition(extraObjects[i].localPosition.y));
            message.extraObjectPositionsTEMP.Add(CompressPosition(extraObjects[i].localPosition.z));
             
        }
    }


    public override void Decode(UDPMessage message)
    {




        if (extraObjects.Count > 0)
        {
            for (int i = 0; i < extraObjects.Count; i++)
            {

                Vector3 pos = new Vector3(DecompressPosition(message.extraObjectPositionsTEMP[i * 3]),
                                            DecompressPosition(message.extraObjectPositionsTEMP[(i * 3) + 1]),
                                            DecompressPosition(message.extraObjectPositionsTEMP[(i * 3) + 2]));
                
                Transform t;
#if !UNITY_WSA && UNITY_EDITOR
                if (debug)
                    t = debugObjects[i]; 
                else
#endif
                t = extraObjects[i];

                
                t.localPosition = pos;
                 
            }
        }

    }





}
