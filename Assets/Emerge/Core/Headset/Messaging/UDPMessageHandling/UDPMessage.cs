﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

using Ceras.Formatters.AotGenerator;
using Emerge.NetworkMessaging;


//using static Emerge.HandTracking.HandData;

namespace Ceras.GeneratedFormatters
{
    [MemberConfig(TargetMember.All)] //serialize all except those marked [Exclude]
    [GenerateFormatter]
    public class UDPMessage
    {
        //hands
        public Hand hand_L;
        public Hand hand_R;
        public Hand remoteHand_L;
        public Hand remoteHand_R;

        //extra info
        public bool testThis;

        //temp - custom class here because using child type derived from UDPMessage was too much of a PITA
        //this entire section should no longer be needed as we remove HMD comms for the SDK end users
        public CustomData customData;

        //extra objects
        public List<ushort> extraObjectPositionsTEMP = new List<ushort>();


        //allow message handlers to do custom setup
        public void Init(Emerge.AvatarType handType, Emerge.AvatarType remoteHandType)
        {
            hand_L = new Hand(); //parameter-less constructor is easier w/Ceras, so leaving it that way for now...
                                 //look into setting the TypeConfig to allow that in the future
            hand_R = new Hand();
            hand_L.Init(Compression.Conversion.ConvertAvatarTypeToBool(handType));
            hand_R.Init(Compression.Conversion.ConvertAvatarTypeToBool(handType));

            remoteHand_L = new Hand();
            remoteHand_R = new Hand();

            remoteHand_L.Init(Compression.Conversion.ConvertAvatarTypeToBool(remoteHandType));
            remoteHand_R.Init(Compression.Conversion.ConvertAvatarTypeToBool(remoteHandType));

            customData = new CustomData();
        }
    }


    public class Hand
    {
        public const int primitiveHandArraySize = 6; // 5 fingers + 1palm 
        public const int riggedHandArraySize = 24;//25; // TODO GET THIS FROM SOMEWHERE////////////

        //------------------------------ serialized data ---------------------------------//
        public bool avatarType; //the avatar type of the data being SENT. 
                                //that means that it can be reduced from a avatarType on the source machine
        public bool active;
        public List<ushort> positions;
        //public List<uint> rotations;



        [Exclude]
        public Vector3[] rawPositions;
        //[Exclude]
        //public Quaternion[] rawRotations;
        [Exclude]
        public int pointsCount;



        public void Init(bool handType)
        {
            avatarType = handType;

            switch (Compression.Conversion.ConvertBoolToAvatarType(handType))
            {
                case Emerge.AvatarType.Primitive:
                    pointsCount = primitiveHandArraySize;
                    break;

                case Emerge.AvatarType.Rigged:
                    pointsCount = riggedHandArraySize;
                    break;
            }

            rawPositions = new Vector3[pointsCount];
            //rawRotations = new Quaternion[pointsCount];
            positions = new List<ushort>(new ushort[pointsCount * 3]);
            //rotations = new List<uint>(new uint[pointsCount]);
        }

        public void StoreData(Emerge.Tracking.Hand h)
        {
            if (avatarType)
            {
                //---    primitive
                AddPrimitivePointsToArray(h, rawPositions);
            }
            else
            {
                //---    rigged
                AddAllPointsToArray(h, rawPositions);
            }

            Compression.Position.CompressPositionsList(rawPositions, ref positions);
        }

        private static void AddPrimitivePointsToArray(Emerge.Tracking.Hand hand, Vector3[] points)
        {
            points[0] = hand.palm.center;
            points[1] = hand.fingers.thumb.Tip;
            points[2] = hand.fingers.index.Tip;
            points[3] = hand.fingers.middle.Tip;
            points[4] = hand.fingers.ring.Tip;
            points[5] = hand.fingers.little.Tip;
        }

        private static void AddAllPointsToArray(Emerge.Tracking.Hand hand, Vector3[] points)
        {
            int i = 0;
            foreach (Vector3 point in hand)
            {
                points[i] = point;
                i++;
            }
        }

        /*
        public void StoreData(Emerge.HandTracking.HandData h, Transform root)
        {


            //Debug.Log("store data called ----------------- for " + avatarType + " pointsCount = " + pointsCount);
            if (avatarType == true)
            {
                //---    primitive
                //Debug.Log("rawPositions.Length = " + rawPositions.Length);
                h.GetCurrentPose_fingerTipsAndPalmOnly(ref rawPositions, root);
                rawRotations = h.GetCurrentPoseRotations_fingerTipsAndPalmOnly(root);

                //Debug.Log("primitive rawRotations.Length = " + rawRotations.Length);
            }
            else
            {
                //---    rigged
                h.GetCurrentPose(ref rawPositions);
                rawRotations = h.GetCurrentPoseRotations();
                //Debug.Log("rigged rawRotations.Length = " + rawRotations.Length);
            }

            //Debug.Log("rotations.Count = " + rotations.Count);
            for (int i = 0; i < pointsCount; i++)
                rotations[i] = Compression.Rotation.Compress(rawRotations[i]);
            
            Compression.Position.CompressPositionsList(rawPositions, ref positions);
        }
        */

        public void DecompressData(Emerge.Tracking.Hand hand, Emerge.SpaceTransformation transformation)
        {
            hand.active = active;
            if (!active)
            {
                return;
            }

            Compression.Position.DecompressPositionsList(positions, ref rawPositions);

            if (avatarType)
            {
                // primitive
                hand.palm.center = transformation.TransformFromSourceToDestinationLocalPoint(rawPositions[0]);
                hand.fingers.thumb.Tip = transformation.TransformFromSourceToDestinationLocalPoint(rawPositions[1]);
                hand.fingers.index.Tip = transformation.TransformFromSourceToDestinationLocalPoint(rawPositions[2]);
                hand.fingers.middle.Tip = transformation.TransformFromSourceToDestinationLocalPoint(rawPositions[3]);
                hand.fingers.ring.Tip = transformation.TransformFromSourceToDestinationLocalPoint(rawPositions[4]);
                hand.fingers.little.Tip = transformation.TransformFromSourceToDestinationLocalPoint(rawPositions[5]);
            }
            else
            {
                // rigged
                for (int i = 0; i <= Enum.GetValues(typeof(Emerge.Tracking.HandPointEnumerator.EnumerationPoint)).Cast<int>().Max(); i++)
                {
                    Vector3 transformedPoint = transformation.TransformFromSourceToDestinationLocalPoint(rawPositions[i]);
                    switch ((Emerge.Tracking.HandPointEnumerator.EnumerationPoint)i)
                    {
                        case Emerge.Tracking.HandPointEnumerator.EnumerationPoint.WristInner: hand.wrist.innerEdge = transformedPoint; break;
                        case Emerge.Tracking.HandPointEnumerator.EnumerationPoint.WristCenter: hand.wrist.center = transformedPoint; break;
                        case Emerge.Tracking.HandPointEnumerator.EnumerationPoint.WristOuter: hand.wrist.outerEdge = transformedPoint; break;
                        case Emerge.Tracking.HandPointEnumerator.EnumerationPoint.PalmCenter: hand.palm.center = transformedPoint; break;
                        case Emerge.Tracking.HandPointEnumerator.EnumerationPoint.ThumbBaseJoint: hand.fingers.thumb.BaseJoint = transformedPoint; break;
                        case Emerge.Tracking.HandPointEnumerator.EnumerationPoint.ThumbMidJoint: hand.fingers.thumb.MiddleJoint = transformedPoint; break;
                        case Emerge.Tracking.HandPointEnumerator.EnumerationPoint.ThumbTopJoint: hand.fingers.thumb.TopJoint = transformedPoint; break;
                        case Emerge.Tracking.HandPointEnumerator.EnumerationPoint.ThumbTip: hand.fingers.thumb.Tip = transformedPoint; break;
                        case Emerge.Tracking.HandPointEnumerator.EnumerationPoint.IndexBaseJoint: hand.fingers.index.BaseJoint = transformedPoint; break;
                        case Emerge.Tracking.HandPointEnumerator.EnumerationPoint.IndexMidJoint: hand.fingers.index.MiddleJoint = transformedPoint; break;
                        case Emerge.Tracking.HandPointEnumerator.EnumerationPoint.IndexTopJoint: hand.fingers.index.TopJoint = transformedPoint; break;
                        case Emerge.Tracking.HandPointEnumerator.EnumerationPoint.IndexTip: hand.fingers.index.Tip = transformedPoint; break;
                        case Emerge.Tracking.HandPointEnumerator.EnumerationPoint.MiddleBaseJoint: hand.fingers.middle.BaseJoint = transformedPoint; break;
                        case Emerge.Tracking.HandPointEnumerator.EnumerationPoint.MiddleMidJoint: hand.fingers.middle.MiddleJoint = transformedPoint; break;
                        case Emerge.Tracking.HandPointEnumerator.EnumerationPoint.MiddleTopJoint: hand.fingers.middle.TopJoint = transformedPoint; break;
                        case Emerge.Tracking.HandPointEnumerator.EnumerationPoint.MiddleTip: hand.fingers.middle.Tip = transformedPoint; break;
                        case Emerge.Tracking.HandPointEnumerator.EnumerationPoint.RingBaseJoint: hand.fingers.ring.BaseJoint = transformedPoint; break;
                        case Emerge.Tracking.HandPointEnumerator.EnumerationPoint.RingMidJoint: hand.fingers.ring.MiddleJoint = transformedPoint; break;
                        case Emerge.Tracking.HandPointEnumerator.EnumerationPoint.RingTopJoint: hand.fingers.ring.TopJoint = transformedPoint; break;
                        case Emerge.Tracking.HandPointEnumerator.EnumerationPoint.RingTip: hand.fingers.ring.Tip = transformedPoint; break;
                        case Emerge.Tracking.HandPointEnumerator.EnumerationPoint.PinkyBaseJoint: hand.fingers.little.BaseJoint = transformedPoint; break;
                        case Emerge.Tracking.HandPointEnumerator.EnumerationPoint.PinkyMidJoint: hand.fingers.little.MiddleJoint = transformedPoint; break;
                        case Emerge.Tracking.HandPointEnumerator.EnumerationPoint.PinkyTopJoint: hand.fingers.little.TopJoint = transformedPoint; break;
                        case Emerge.Tracking.HandPointEnumerator.EnumerationPoint.PinkyTip: hand.fingers.little.Tip = transformedPoint; break;
                    }
                }

                hand.palm.radius = Vector3.Magnitude(hand.palm.center - hand.wrist.center);
            }
        }
        
        /*
        public void DecompressData(Emerge.HandTracking.HandData h)
        {

            //Debug.Log("Setting the hand pose: " + h.gameObject.name + " positions.Count = " + positions.Count);

            Compression.Position.DecompressPositionsList(positions, ref rawPositions);

            h.SetHandPose(rawPositions);

            for (int i = 0; i < pointsCount; i++)
                rawRotations[i] = Compression.Rotation.Decompress(rotations[i]);
            h.SetHandPose(rawRotations);
        }
        */
    }
}
