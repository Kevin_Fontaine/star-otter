﻿/******************************************************************************
 * Copyright (C) Emerge Now, Inc. 2016-2019.                                  *
 * Emerge Now Inc proprietary and confidential.                               *
 *                                                                            *
 * Use subject to the terms of the Emerge Exploration Program Agreement       *
 * and/or terms of limited, non-exclusive, non-sublicensable license to       *
 * Emerge SDK between Emerge and you, your company or any other organization. *
 ******************************************************************************/
using System; 
using UnityEngine;

namespace Emerge.Communication
{
    public abstract class AbstractUDPMessageHandler : MonoBehaviour
    {
        protected virtual void OnDestroy()
        {
            if (HeadsetUDPConnection.Instance != null)
            {
                HeadsetUDPConnection.Instance.OnDataReceived -= HandleIncomingMessage;
            }
        }

        protected virtual void Start()
        {
            HeadsetUDPConnection.Instance.OnDataReceived += HandleIncomingMessage;
        }

        public virtual void HandleIncomingMessage(UInt16 messageId, byte[] msg)
        {
            // empty
        }
    }
}