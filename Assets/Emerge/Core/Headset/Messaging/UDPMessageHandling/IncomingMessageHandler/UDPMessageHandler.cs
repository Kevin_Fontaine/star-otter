﻿/******************************************************************************
 * Copyright (C) Emerge Now, Inc. 2016-2019.                                  *
 * Emerge Now Inc proprietary and confidential.                               *
 *                                                                            *
 * Use subject to the terms of the Emerge Exploration Program Agreement       *
 * and/or terms of limited, non-exclusive, non-sublicensable license to       *
 * Emerge SDK between Emerge and you, your company or any other organization. *
 ******************************************************************************/
using System; 

namespace Emerge.Communication
{
    public class UDPMessageHandler : AbstractUDPMessageHandler
    {
        public override void HandleIncomingMessage(UInt16 messageId, byte[] msg)
        {
            if (messageId != TactileMessageIdConstants.coreData)
            {
                return;
            }

            UDPMessagingHub.Instance.DecodeIncomingData(msg);
        }
    }
}
