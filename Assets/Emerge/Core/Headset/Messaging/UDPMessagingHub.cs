﻿
/******************************************************************************
 * Copyright (C) Emerge Now, Inc. 2016-2019.                                  *
 * Emerge Now Inc proprietary and confidential.                               *
 *                                                                            *
 * Use subject to the terms of the Emerge Exploration Program Agreement       *
 * and/or terms of limited, non-exclusive, non-sublicensable license to       *
 * Emerge SDK between Emerge and you, your company or any other organization. *
 ******************************************************************************/
using Ceras.GeneratedFormatters;
using System;
using UnityEngine;

namespace Emerge.Communication
{

    /// <summary>
    /// central hub for transfer of UDP messages between 2 platforms or devices
    /// 
    /// data is collected and serialized, and then deserialized and applied on the other device or platform
    /// 
    /// this includes avatar/hand positions, as well as positions of world objects and other custom state info
    /// that relies on low latency. Multiuser (Photon) RPC's should be used in cases where latency isnt critical and
    /// message reciept needs to be reliable.
    /// 
    /// the 2 steps (encode/decode) are kept in the same class to allow for easy testing and debugging on one device
    /// 
    /// </summary>
    // Dont make this inherit from Singleton<T> as that can only be run from Main Unity Thread
    public class UDPMessagingHub : MonoBehaviour 
    { 
        //TODO: make this just a list of MessageHandler modules
        // making it explicit for now so i can test each one first
        public MessageXfer_Hands hands;
        public MessageXfer_Objects objects;
        public MessageXfer_ExtraInfo extraInfo;

        //TODO: locate all of these in the scene at startup
        public MessageXferHandler[] messageHandlers;
#if !UNITY_WSA && UNITY_EDITOR
        [Space(20)]
        [Header("Copy mock data on Update() for testing proper transfer of data in editor")]
        public bool debug; //use local transfer to test that data is set up correctly
#endif


        IUDPSender udpOut;
        ISerializer serializer;
        UDPMessage message;
        UDPMessage receivedMessage;
        byte[] buffer;

        //message receive queue/thread
        readonly object receivedMessageLock = new object();
        bool hasNewMessage;

        public static UDPMessagingHub Instance;

        private void Awake()
        {
            if (!Instance)
                Instance = this;
            else
                Destroy(gameObject);
        }

        private void Start()
        { 
            udpOut = HeadsetUDPConnection.Instance;

            serializer = GetComponentInChildren<ISerializer>();

            serializer.Init();
             
            CreateNewUDPMessage(out message);
            CreateNewUDPMessage(out receivedMessage);

            buffer = null;
        }
         
        //allow the handlers to do a custom setup pass on the message before its used
        private void CreateNewUDPMessage(out UDPMessage msg)
        {
            msg = new UDPMessage();

            hands.Init(ref msg);
            objects.Init(ref msg);
            extraInfo.Init(ref msg);
             
            for (int i = 0; i < messageHandlers.Length; i++)
                messageHandlers[i].Init(ref msg);
        }





#if !UNITY_WSA
        private void Update()
        {

#if UNITY_EDITOR
            if (debug)
            {
                DoMockDataTransferForTesting();
                return;
            }
#endif

            EncodeAllData();

            udpOut.SendData(TactileMessageIdConstants.coreData, buffer);
        }
#endif

       




        //===================================================================================
        //main encode/decode functions
        //LOCAL/SENDER
        public void EncodeAllData()
        {
            hands.Encode(message);
            objects.Encode(message);
            extraInfo.Encode(message);

            for (int i = 0; i < messageHandlers.Length; i++)
                messageHandlers[i].Encode(message);


            //Debug.Log("Message Size in bytes: " + serializer.Serialize<UDPMessage>(message).Length);

            serializer.Serialize<UDPMessage>(message, ref buffer);

        }

        // REMOTE/RECEIVER side ---------------------------------------------------------------
        public void DecodeIncomingData(byte[] data)
        {
            try
            {
                lock (receivedMessageLock)
                {
                    serializer.Deserialize<UDPMessage>(ref receivedMessage, data); //Ceras allows recycling objects
                    hasNewMessage = true;
                }
            }
            catch { }
        }

        private void LateUpdate()
        {
            lock (receivedMessageLock)
            {
                if (!hasNewMessage)
                {
                    return;
                }

                hasNewMessage = false;
                for (int i = 0; i < messageHandlers.Length; i++)
                {
                    Decode(receivedMessage, messageHandlers[i]);
                }

                Decode(receivedMessage, hands);
                Decode(receivedMessage, objects);
                Decode(receivedMessage, extraInfo);
            }
        }

        private static void Decode(UDPMessage message, MessageXferHandler handler)
        {
            try
            {
                handler.Decode(message);
            }
            catch (Exception e)
            {
                Debug.LogErrorFormat("UDPMessagingHub: error decoding message: {0}", e);
            }
        }


        //===================================================================================
        // DEBUG -
        /// <summary>
        /// Test in editor using mock hands to verify data transfer before building to HL
        /// </summary>
        public void DoMockDataTransferForTesting()
        {
            EncodeAllData();

            DecodeIncomingData(buffer);
        }



    }
}