﻿/******************************************************************************
 * Copyright (C) Emerge Now, Inc. 2016-2019.                                  *
 * Emerge Now Inc proprietary and confidential.                               *
 *                                                                            *
 * Use subject to the terms of the Emerge Exploration Program Agreement       *
 * and/or terms of limited, non-exclusive, non-sublicensable license to       *
 * Emerge SDK between Emerge and you, your company or any other organization. *
 ******************************************************************************/

using Emerge.Core;

namespace Emerge.Communication
{
    public class NetworkClockSync : Singleton<NetworkClockSync>
    {
// TODO add network clock sync back, using new UDP functionality
/// Singleton type was updated while this whole class was commented. Update Instance references and initialization as needed.
/* 
        public static NetworkClockSync Instance;
        //
        public bool logToFile;
        //public LatencyTimer_FileLogging fileLogger;

        System.Diagnostics.Stopwatch stopwatch;

        private int frameCounter;

        public TextMesh text_Timer;
        public TextMesh text_Offset;

        //other timeout timers
        int networkTimeOut;


        //simple state - if things get complicated, expand this to proper state machine
        private ClockSyncState_Slave currentState;
        private enum ClockSyncState_Slave
        {
            idle, //waiting for initial timestamp

            sendDelayRequest, //initial timestamp received, now send the delay request
            awaitingDelayRequestResponse, //delay request sent, waiting for response from master
            exchangeSuccess,
            abort
        }

        internal bool ShouldResync()
        {
            if (numberOfSyncAttempts == 0 || !commsConnected)
                //make sure the auto-sync has run - triggered a second or 2 after initial UDP connection
                //this would only fire if we are trying to resync while thats happening
                return false;

            if (!successfulSync) //sync may have failed during the initial auto-sync
                return true;

            Debug.Log("ShouldResync() called");

            Debug.Log("time since last sync = " + GetTimeSinceLastSync() + " resync at 300,000");
            return (GetTimeSinceLastSync() > 300000); //every 5 minutes, reset

        }

        private ClockSyncState_Master currentState_master;
        private enum ClockSyncState_Master
        {
            idle,
            sendingInitialTimestamp,
            waitForDelayRequest,
            delayRequestReceived,
            abort
        }

        private enum SyncMessages
        {
            none,
            initialTimeStampFromMaster,
            delayRequest,
            abort
        }

        private void Awake()
        {
            //          for reference... so far no file logging needed
            //fileLogger = GetComponent<LatencyTimer_FileLogging>();
            //    if (logToFile)
            //        fileLogger.Log(frameNumber.ToString() + ", " + currentTime.ToString());


            currentState = ClockSyncState_Slave.idle;

            currentState_master = ClockSyncState_Master.idle;

            Instance = this;
            SetupTimer();
        }

        //   TIMER
        //=====================================================
        internal void SetupTimer()
        {
            stopwatch = new System.Diagnostics.Stopwatch();
            stopwatch.Start();
        }

        //[ContextMenu("TimerReset")]
        internal void TimerReset()
        {
            stopwatch.Reset();
            stopwatch.Start();
        }

        internal float GetCurrentTime()
        {
            return stopwatch.ElapsedMilliseconds;
        }

        //   DATA
        //=====================================================
        //slave (hololens) should have all these by the end of the exchange
        float timeStamp1_master;
        float timeStamp2_slave;
        float timeStamp3_slave;
        float timeStamp4_master;

        //float ms_difference;
        //float sm_difference;

        //HOLOLENS
        public float myClockOffset;


        public float GetCurrentSyncedTime()
        {
            //hololens should return this. PC side so far just has an offset of 0, but i need to clean all that up...
            return GetCurrentTime() - myClockOffset;
        }
        private bool successfulSync; //if we have synced at least once successfully
        private float lastSyncedTime;
        public float GetTimeSinceLastSync()
        {
            return GetCurrentTime() - lastSyncedTime;
        }

        private int numberOfSyncAttempts;
        public void InitiateSync()
        {
            Debug.Log("InitiateSync() called");
            //PC ONLY
#if !UNITY_WSA
            if (!commsConnected)
            {
                Debug.Log("Comms not connected!");
                return;
            }
            numberOfSyncAttempts++;
            //start the sync exchange. * hololens must be connected via UDP comms already *
            currentState_master = ClockSyncState_Master.sendingInitialTimestamp;
#endif
        }

#if !UNITY_WSA
        public Action<bool> UDPCommsConnectionStatusChanged;
        SimpleUDP_sender udpSender;
        private void CommsConnected(bool b)
        {
            commsConnected = b;
            UDPCommsConnectionStatusChanged.Dispatch(b);

            if (b)
            {
                //start auto-connection process
                StartCoroutine(Connected());
            }
            else
            {
                //comms disconnected - reset all auto sync props
                numberOfSyncAttempts = 0;
            }
        }


        IEnumerator Connected()
        {
            Debug.Log("Start auto connect coroutine");
            yield return new WaitForSeconds(2f);
            InitiateSync();
        }
#endif


        private void Update()
        {

            //Debug.Log("timer time = " + GetCurrentTime());

            //DisplayTimerProperties();

            text_Timer.text = "" + (GetCurrentTime() - myClockOffset);
            text_Offset.text = "" + myClockOffset;


            //PC ONLY
#if !UNITY_WSA
            if (udpSender == null)
                udpSender = FindObjectOfType<SimpleUDP_sender>();


            if (udpSender.IsConnected())
            {
                if (commsConnected == false)
                    CommsConnected(true);
            }
            else
            {
                if (commsConnected == true)
                    CommsConnected(false);
            }



            if (Input.GetKeyDown(KeyCode.T))
            {
                InitiateSync();
            }

            if (currentState_master == ClockSyncState_Master.waitForDelayRequest) //waiting for response on PC
            {
                //Debug.Log("waiting For Delay Request message...");
                //float timeStamp;
                bool messageReceived = false;
                byte[][] s = udpSender.GetMessages();
                //Debug.Log("udp messages length = " + s.Length);
                if (s.Length > 0)
                    messageReceived = true;

                for (int i = 0; i < s.Length; i++)
                {
                    //Debug.Log("byte array " + i + " = array of " + s[i].Length + " bytes");

                    //if (BitConverter.IsLittleEndian)
                    //{
                    //    Debug.Log("CONVERT / REVERSE");
                    //    Array.Reverse(s[i]); // Convert big endian to little endian
                    //}
                    //timeStamp = BitConverter.ToSingle(s[i], 0);

                    //Debug.Log("succesful parsed value: timeStamp = " + timeStamp);
                    //break;
                }


                if (messageReceived)
                {
                    Debug.Log("UDP message received");
                    currentState_master = ClockSyncState_Master.delayRequestReceived;
                    networkTimeOut = 0;
                }


            }

            if (currentState_master == ClockSyncState_Master.delayRequestReceived)
            {
                //messages are sending via realtime comms. time out after a few seconds

                //do time out timer here, call abort after x seconds
                //abort: 
                //currentState_master = ClockSyncState_Master.abort;
                networkTimeOut++;
                if (networkTimeOut > 100)
                    currentState_master = ClockSyncState_Master.idle;

            }

#endif



        }



        // PC 
        //piggyback on the realtime UDP messages going out to Hololens
        //======================================================================
        // PC side -> send message to HL
        private int sendAttempt;
        public bool commsConnected;

        internal Vector3 CreateUDPSendVector3()
        {
            if (!commsConnected)
                return Vector3.zero;

            if (currentState_master == ClockSyncState_Master.sendingInitialTimestamp)
            {
                currentState_master = ClockSyncState_Master.waitForDelayRequest;
                sendAttempt++;
                return new Vector3((int)SyncMessages.initialTimeStampFromMaster, sendAttempt, GetCurrentTime());
            }
            else if (currentState_master == ClockSyncState_Master.delayRequestReceived)
            {
                //send  delay back to hololens
                //Debug.Log("Sending delayrequest back to hololens, " + (int)SyncMessages.delayRequest);
                sendAttempt++;
                return new Vector3((int)SyncMessages.delayRequest, sendAttempt, GetCurrentTime());
            }
            else if (currentState_master == ClockSyncState_Master.abort)
            {
                //send  abort messages to hololens
                sendAttempt++;
                return new Vector3((int)SyncMessages.abort, sendAttempt, 0);
            }
            else
            {
                return Vector3.zero;
            }
        }

        //HOLOLENS SIDE - receive the initial timestamp
        //=======================================================================
        internal void ReceiveVector(Vector3 vector3)
        {
            int state = (int)vector3.x;
            if (currentState != ClockSyncState_Slave.idle) //any state besides idle will accept an abort message (abort sets us back to idle)
            {
                if (state == (int)SyncMessages.abort)
                {
                    this.AbortAndReset();
                    return;
                }
            }

            if (currentState == ClockSyncState_Slave.idle)
            {

                if (state == (int)SyncMessages.initialTimeStampFromMaster)
                {
                    //first time stamp from master
                    timeStamp1_master = vector3.z;
                    Debug.Log("GOT TIMESTAMP: " + timeStamp1_master + " on attempt#: " + (int)vector3.y);
                    timeStamp2_slave = GetCurrentTime();

                    SendDelayRequestToMaster();
                    Debug.Log("currentState switched to ClockSyncState_Slave.awaitingDelayRequestResponse");
                    currentState = ClockSyncState_Slave.awaitingDelayRequestResponse;
                    return;
                }
            }

            if (currentState == ClockSyncState_Slave.awaitingDelayRequestResponse)
            {
                if (state == (int)SyncMessages.delayRequest)
                {
                    //delay request time stamp from master
                    timeStamp4_master = vector3.z;
                    Debug.Log("GOT DELAY REQUEST TIMESTAMP: " + timeStamp4_master + " on attempt#: " + (int)vector3.y);

                    ProcessOffset();

                    Debug.Log("SYNC COMPLETE!");
                    currentState = ClockSyncState_Slave.idle;
                    return;
                }
            }
        }

        //
        private void ProcessOffset()
        {
            lastSyncedTime = GetCurrentTime();
            successfulSync = true;
            myClockOffset = (timeStamp2_slave - timeStamp1_master - timeStamp4_master + timeStamp3_slave) / 2;
            Debug.Log("timeStamp1_master = " + timeStamp1_master);
            Debug.Log("timeStamp1_master = " + timeStamp2_slave);
            Debug.Log("timeStamp1_master = " + timeStamp3_slave);
            Debug.Log("timeStamp1_master = " + timeStamp4_master);

            Debug.Log("Processing Offset. Offset = " + myClockOffset);
        }


        private void AbortAndReset()
        {
            Debug.Log("ABORT received, setting state back to idle");
            currentState = ClockSyncState_Slave.idle;
            //reset all props
            timeStamp1_master = 0;
            timeStamp2_slave = 0;
            timeStamp3_slave = 0;
            timeStamp4_master = 0;

        }

        //Hololens UDP send
        private void SendDelayRequestToMaster()
        {
            // send UDP message to PC with our current Time
            //NOTE!! TODO: not checking if valid ip, no safety check if we are connected or not. 
            //assuming HL UDP socket is already connected

            timeStamp3_slave = GetCurrentTime();

            UDPCommunication u = FindObjectOfType<UDPCommunication>();
            byte[] b = BitConverter.GetBytes(GetCurrentTime());

            //byte[] messageBytes = new byte[message.GetCoordCount() * 3 * 4];
            //Buffer.BlockCopy(message.GetFlattenedCoords(), 0, messageBytes, 0, message.GetCoordCount() * 3 * 4);
            //byte[] toSend = BitConverter.GetBytes(messageBytes.Length);

            u.SendUDPMessage(u.externalIP, u.externalPort, b);
        }
        //=======================================================================





        //utils
        public static void DisplayTimerProperties()
        {
            // Display the timer frequency and resolution.
            if (System.Diagnostics.Stopwatch.IsHighResolution)
            {
                Debug.Log("Operations timed using the system's high-resolution performance counter.");
            }
            else
            {
                Debug.Log("Operations timed using the DateTime class.");
            }

            long frequency = System.Diagnostics.Stopwatch.Frequency;
            Debug.Log("  Timer frequency in ticks per second = " + frequency);
            long nanosecPerTick = (1000L * 1000L * 1000L) / frequency;
            Debug.Log("  Timer is accurate within " + nanosecPerTick + " nanoseconds");
        }
        */
    }
}
