﻿using System;
using UnityEngine;
using System.Collections.Generic;

using SlimMath;

namespace Emerge.NetworkMessaging
{


    public static class Compression
    {
        public static class Conversion
        {
            public static Emerge.AvatarType ConvertBoolToAvatarType(bool b)
            {
                if (b)
                    return AvatarType.Primitive;
                else
                    return AvatarType.Rigged;
            }

            public static bool ConvertAvatarTypeToBool(Emerge.AvatarType type)
            {
                return type == AvatarType.Primitive;
            }
        }



        public static class Position
        {
            
            public static ushort CompressPosition(float f)
            {
                return HalfUtilities.Pack(f);
            }

            public static float DecompressPosition(ushort u)
            {
                return HalfUtilities.Unpack(u);
            }

            public static void CompressPositionsList(Vector3[] rawPositions, ref List<ushort> final)
            {
                for (int i = 0; i < rawPositions.Length; i++)
                {
                    final[i * 3] = CompressPosition(rawPositions[i].x);
                    final[(i * 3) + 1] = CompressPosition(rawPositions[i].y);
                    final[(i * 3) + 2] = CompressPosition(rawPositions[i].z);
                }
            }

            public static void DecompressPositionsList(List<ushort> final, ref Vector3[] rawPositions)
            {
                for (int i = 0; i < rawPositions.Length; i++)
                { 
                    Vector3 pos = new Vector3(DecompressPosition(final[i * 3]),
                                                DecompressPosition(final[(i * 3) + 1]),
                                                DecompressPosition(final[(i * 3) + 2]));

                    rawPositions[i] = pos;
                }
            }


            //TODO: this compression is too lossy - for us since we have a limited hand tracking space and are using meters,
            //our range can be compressed down to -5 to 5. (handtracking active area is only -.5 to .5)

            public static ulong EncodeVector3ToULong(Vector3 v)
            {
                //Vectors must stay within the -320.00 to 320.00 range per axis - no error handling is coded here
                //Adds 32768 to get numbers into the 0-65536 range rather than -32768 to 32768 range to allow unsigned
                //Multiply by 100 to get two decimal place
                ulong xcomp = (ulong)(Mathf.RoundToInt((v.x * 100f)) + 32768);
                ulong ycomp = (ulong)(Mathf.RoundToInt((v.y * 100f)) + 32768);
                ulong zcomp = (ulong)(Mathf.RoundToInt((v.z * 100f)) + 32768);
                //Debug.Log ("comps " + xcomp + " " + ycomp + " " + zcomp);
                return xcomp + ycomp * 65536 + zcomp * 4294967296;
            }
            public static Vector3 DecodeVector3FromULong(ulong i)
            {
                //Debug.Log ("ulong " +i);
                //Get the leftmost bits first. The fractional remains are the bits to the right.
                // 1024 is 2 ^ 10 - 1048576 is 2 ^ 20 - just saving some calculation time doing that in advance
                ulong z = (ulong)(i / 4294967296);
                ulong y = (ulong)((i - z * 4294967296) / 65536);
                ulong x = (ulong)(i - y * 65536 - z * 4294967296);
                //Debug.Log (x + " " + y + " " + z);
                // subtract 512 to move numbers back into the -512 to 512 range rather than 0 - 1024
                return new Vector3(((float)x - 32768f) / 100f, ((float)y - 32768f) / 100f, ((float)z - 32768f) / 100f);
            }
        }

        public static class Rotation
        {
            private const float Minimum = -1.0f / 1.414214f; // note: 1.0f / sqrt(2)
            private const float Maximum = +1.0f / 1.414214f;

            public static uint Compress(Quaternion rotation)
            {
                float absX = Mathf.Abs(rotation.x),
                    absY = Mathf.Abs(rotation.y),
                    absZ = Mathf.Abs(rotation.z),
                    absW = Mathf.Abs(rotation.w);

                var largestComponent = new LargestComponent(ComponentType.X, absX);
                if (absY > largestComponent.Value)
                {
                    largestComponent.Value = absY;
                    largestComponent.ComponentType = ComponentType.Y;
                }
                if (absZ > largestComponent.Value)
                {
                    largestComponent.Value = absZ;
                    largestComponent.ComponentType = ComponentType.Z;
                }
                if (absW > largestComponent.Value)
                {
                    largestComponent.Value = absW;
                    largestComponent.ComponentType = ComponentType.W;
                }

                float a, b, c;
                switch (largestComponent.ComponentType)
                {
                    case ComponentType.X:
                        if (rotation.x >= 0)
                        {
                            a = rotation.y;
                            b = rotation.z;
                            c = rotation.w;
                        }
                        else
                        {
                            a = -rotation.y;
                            b = -rotation.z;
                            c = -rotation.w;
                        }
                        break;
                    case ComponentType.Y:
                        if (rotation.y >= 0)
                        {
                            a = rotation.x;
                            b = rotation.z;
                            c = rotation.w;
                        }
                        else
                        {
                            a = -rotation.x;
                            b = -rotation.z;
                            c = -rotation.w;
                        }
                        break;
                    case ComponentType.Z:
                        if (rotation.z >= 0)
                        {
                            a = rotation.x;
                            b = rotation.y;
                            c = rotation.w;
                        }
                        else
                        {
                            a = -rotation.x;
                            b = -rotation.y;
                            c = -rotation.w;
                        }
                        break;
                    case ComponentType.W:
                        if (rotation.w >= 0)
                        {
                            a = rotation.x;
                            b = rotation.y;
                            c = rotation.z;
                        }
                        else
                        {
                            a = -rotation.x;
                            b = -rotation.y;
                            c = -rotation.z;
                        }
                        break;
                    default:
                        // Should never happen!
                        throw new ArgumentOutOfRangeException("Unknown rotation component type: " +
                                                              largestComponent.ComponentType);
                }

                float normalizedA = (a - Minimum) / (Maximum - Minimum),
                    normalizedB = (b - Minimum) / (Maximum - Minimum),
                    normalizedC = (c - Minimum) / (Maximum - Minimum);

                uint integerA = (uint)Mathf.FloorToInt(normalizedA * 1024.0f + 0.5f),
                    integerB = (uint)Mathf.FloorToInt(normalizedB * 1024.0f + 0.5f),
                    integerC = (uint)Mathf.FloorToInt(normalizedC * 1024.0f + 0.5f);

                return (((uint)largestComponent.ComponentType) << 30) | (integerA << 20) | (integerB << 10) | integerC;
            }

            public static Quaternion Decompress(uint compressedRotation)
            {
                var largestComponentType = (ComponentType)(compressedRotation >> 30);
                uint integerA = (compressedRotation >> 20) & ((1 << 10) - 1),
                    integerB = (compressedRotation >> 10) & ((1 << 10) - 1),
                    integerC = compressedRotation & ((1 << 10) - 1);

                float a = integerA / 1024.0f * (Maximum - Minimum) + Minimum,
                    b = integerB / 1024.0f * (Maximum - Minimum) + Minimum,
                    c = integerC / 1024.0f * (Maximum - Minimum) + Minimum;

                Quaternion rotation;
                switch (largestComponentType)
                {
                    case ComponentType.X:
                        // (?) y z w
                        rotation.y = a;
                        rotation.z = b;
                        rotation.w = c;
                        rotation.x = Mathf.Sqrt(1 - rotation.y * rotation.y
                                                   - rotation.z * rotation.z
                                                   - rotation.w * rotation.w);
                        break;
                    case ComponentType.Y:
                        // x (?) z w
                        rotation.x = a;
                        rotation.z = b;
                        rotation.w = c;
                        rotation.y = Mathf.Sqrt(1 - rotation.x * rotation.x
                                                   - rotation.z * rotation.z
                                                   - rotation.w * rotation.w);
                        break;
                    case ComponentType.Z:
                        // x y (?) w
                        rotation.x = a;
                        rotation.y = b;
                        rotation.w = c;
                        rotation.z = Mathf.Sqrt(1 - rotation.x * rotation.x
                                                   - rotation.y * rotation.y
                                                   - rotation.w * rotation.w);
                        break;
                    case ComponentType.W:
                        // x y z (?)
                        rotation.x = a;
                        rotation.y = b;
                        rotation.z = c;
                        rotation.w = Mathf.Sqrt(1 - rotation.x * rotation.x
                                                   - rotation.y * rotation.y
                                                   - rotation.z * rotation.z);
                        break;
                    default:
                        // Should never happen!
                        throw new ArgumentOutOfRangeException("Unknown rotation component type: " +
                                                              largestComponentType);
                }

                return rotation;
            }

            private enum ComponentType : uint
            {
                X = 0,
                Y = 1,
                Z = 2,
                W = 3
            }

            private struct LargestComponent
            {
                public ComponentType ComponentType;
                public float Value;

                public LargestComponent(ComponentType componentType, float value)
                {
                    ComponentType = componentType;
                    Value = value;
                }
            }
        }
    }

}