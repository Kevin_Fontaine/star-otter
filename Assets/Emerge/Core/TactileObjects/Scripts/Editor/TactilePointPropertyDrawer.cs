/******************************************************************************
Copyright (C) Emerge Now, Inc. 2016-2019. 
Emerge Now Inc proprietary and confidential.

Use subject to the terms of the Emerge Exploration Program Agreement
and/or terms of limited, non-exclusive, non-sublicensable license to 
Emerge SDK between Emerge and you, your company or any other organization.
******************************************************************************/


using UnityEditor;
using UnityEngine;

using Emerge.Core;


[CustomPropertyDrawer(typeof(TactilePointData))]
class TactilePointDrawer : PropertyDrawer
{
    public int rows = 3; 
    public float topOffset = 10f;

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    { 
         return base.GetPropertyHeight(property, label) * rows + topOffset; 
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);


        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 1;

        // Calculate rects
        float height = EditorGUIUtility.singleLineHeight; 
        float yPos = position.y + topOffset;
        Rect idRect = new Rect(position.x, yPos, position.width * 0.9f, height);
        Rect tppRect = new Rect(position.x, yPos + 16, position.width * 0.9f, height);
        Rect ampRect = new Rect(position.x, yPos + 32, position.width * 0.9f, height);
        
        EditorGUI.PropertyField(idRect, property.FindPropertyRelative("id"), new GUIContent("ID"));
        EditorGUI.Slider(tppRect, property.FindPropertyRelative("timePerPoint"), 2.5f, 10f, new GUIContent("Time-per-point"));
        EditorGUI.Slider(ampRect, property.FindPropertyRelative("amplitude"), 0, 100f, new GUIContent("Amplitude"));

        
        //for reference - grabbing value - saving this because it took forever to find
        //fieldInfo.GetValue(property.serializedObject.targetObject)

        // Set indent back to what it was
        EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty();
    }
}


