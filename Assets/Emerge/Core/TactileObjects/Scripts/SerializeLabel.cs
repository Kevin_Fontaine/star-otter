﻿/******************************************************************************
 * Copyright (C) Emerge Now, Inc. 2016-2019.                                  *
 * Emerge Now Inc proprietary and confidential.                               *
 *                                                                            *
 * Use subject to the terms of the Emerge Exploration Program Agreement       *
 * and/or terms of limited, non-exclusive, non-sublicensable license to       *
 * Emerge SDK between Emerge and you, your company or any other organization. *
 ******************************************************************************/
// CleanPass

using System;

namespace Emerge.Core
{
    [AttributeUsage(AttributeTargets.Property)]
    public class SerializeLabel : Attribute
    {
        public readonly string label;

        public SerializeLabel(string label)
        {
            this.label = label;
        }
    }
}