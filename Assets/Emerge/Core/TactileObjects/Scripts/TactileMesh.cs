﻿/******************************************************************************
 * Copyright (C) Emerge Now, Inc. 2016-2019.                                  *
 * Emerge Now Inc proprietary and confidential.                               *
 *                                                                            *
 * Use subject to the terms of the Emerge Exploration Program Agreement       *
 * and/or terms of limited, non-exclusive, non-sublicensable license to       *
 * Emerge SDK between Emerge and you, your company or any other organization. *
 ******************************************************************************/

using Emerge.Communication;
using Emerge.MeshInteraction;
using System;
using System.Collections.Generic; 
using UnityEngine;

namespace Emerge.Core
{ 
    /// <summary>
    ///     Emerge Object that uses MeshInteraction components to calculate
    ///     which parts of a mesh or animated mesh are being touched
    /// </summary>
    public class TactileMesh : AbstractTactileBehavior
    {
        // ToDo: Review and remove
        [HelpBox("Temp flag to let us know if runtime instantiated or not. Will be fixed later... \n" +
                 "If this is checked, Awake() will not fire until manually called in code")]
        private MeshInteraction.MeshInteraction meshInteraction;

        private bool initialized;

        private readonly int beamformUpdateFrequency = 3;
         
        // Manually track touch using the mesh interaction. If any points found, we are touching.
        private bool meshPointsFound;

        // Delay sending updates - limit to sending every 3 frames to give hardware time
        // to run thru all the points
        private int beamformUpdateCount;

        // ToDo: Remove magic number
        private readonly int maxTactilePoints = 100;

        // ToDo: We should have a more generalized Pool class that works with Generics.
        private Stack<TactilePointDto> tactilePointDtoPool; 

        // Temp results, temp storage before adding to final 
        public MeshInteractionResults tempResults = new MeshInteractionResults();

        // Final merged results, OR single hand results  
        public MeshInteractionResults finalResults = new MeshInteractionResults();
 
        // public TactileMeshDto TactileMeshDto { get; private set; }
           
        public List<TactilePointDto> TactilePointDtos { get; set; }
         
        public static string StrengthString = "strength";
        [Range(0, 100.0f)]
        [SerializeField]
        private float strength = 100.0f;
        public float Strength
        {
            get => strength;
            set => strength = value;
        }


        #region unityEvents

        public override void Awake()
        {
            base.Awake();

            // TactileMeshDto = new TactileMeshDto(TactileMethod.mesh); 
            TactilePointDtos = new List<TactilePointDto>();

            meshInteraction = GetComponentInChildren<MeshInteraction.MeshInteraction>();
            meshInteraction.autoUpdate = false;
            meshInteraction.autoClearDebugPoints = false;

            tactilePointDtoPool = new Stack<TactilePointDto>();
            InitializeTactilePointDtoPool();
             
            initialized = true;
        }

        public override void Update()
        {
            if (!initialized) { return; }

            // Collider/Trigger detection first, for early out 

            base.Update();

            // Use the collider as an early out to avoid mesh calculations - if hands are not in the collider, exit

            bool collisionIsActive = CollisionIsActive; 
            if (CollisionPhaseIsBeginOrEnd)
            {
                meshInteraction.Pause(collisionIsActive);

                // Reset mesh interaction props - assume collider is larger than mesh and its entered/exited first --
                // so this prepares the correct state
                meshPointsFound = false;

                // ToDo: verify 
                // Even if touch is true, exit to allow one frame here for the meshInteraction to enable and get some results
                // just in case - test to make sure 
                return;
            }

            if (!collisionIsActive) { return; }

            // One or both hands are touching. Figure out which hands,
            // and send those to processor for testing against mesh collision 

            finalResults.Reset();

            if (HandCollisionStatus == HandCollisionStatus.Both)
            {
                tempResults.Reset();

                meshInteraction.debug?.Clear();

                meshInteraction.RunMeshInteractionProcessingCycle(tempResults, Handedness.Left);
                meshInteraction.RunMeshInteractionProcessingCycle(finalResults, Handedness.Right);

                // NOTE: if tempresults -> 'meshPointsFoundThisFrame' is false, the points list will not be merged

                MeshInteractionResults.CombineResults(tempResults, finalResults);
            }
            else
            {
                meshInteraction?.debug?.Clear();
                Handedness h = HandCollisionStatus == HandCollisionStatus.Left ? Handedness.Left : Handedness.Right;
                meshInteraction.RunMeshInteractionProcessingCycle(finalResults, h);
            }

            // After processing, check to see if any mesh points are found/intersected and update meshPointsFound bool
            UpdateMeshInteractionTouchedStatus(finalResults);

            if (meshPointsFound)
            {
                // ToDo: expose this variable in Config file or somewhere 
                // Beamform every third Update 
                // Update, set to 1 so we send every frame.
                if (++beamformUpdateCount == beamformUpdateFrequency)
                {
                    beamformUpdateCount = 0; 

                    if (finalResults.positions.Length > 0)
                    {
                        SetTactileDataPointDtosFromCoordinates(finalResults.positions);
                        // Each point does this now that TactileMeshDto doesn't extend TactilePointDto
                        // TactileMeshDto.TactilePointMode = TactilePointMode.Modify;
                    } 
                } 
            }
        }

        #endregion

        #region public

        /// <summary>
        ///     ToDo: populate this
        /// </summary>
        /// <param name="meshInteraction"></param>
        public void RuntimeInit(MeshInteraction.MeshInteraction meshInteraction)
        {
            if (meshInteraction is null) throw new ArgumentNullException(nameof(MeshInteraction.MeshInteraction));

            this.meshInteraction = meshInteraction;
            initialized = true;
        }

        public override IDto[] GetDtos()
        { 
            IDto[] dtos = new IDto[TactilePointDtos.Count];

            int i;
            int len = TactilePointDtos.Count;
            for ( i = 0; i < len; i++ )
            {
                dtos[i] = TactilePointDtos[i];
            }

            return dtos;
        }

        public override void ClearDtos()
        {  
            ReturnAllTactilePointDtosToPool(); 
        }

        #endregion

        #region private

        private void UpdateMeshInteractionTouchedStatus(MeshInteractionResults results)
        {
            if (results is null) throw new ArgumentNullException(nameof(MeshInteractionResults));

            meshPointsFound = results.MeshPointsFoundThisFrame;
        }
        
        private void InitializeTactilePointDtoPool()
        {
            for (int i = 0; i < maxTactilePoints; i++)
            {
                TactilePointDto tactilePointDto = new TactilePointDto(TactileMethod.point);
                tactilePointDtoPool.Push(tactilePointDto);
            }
        }

        public void ReturnTactilePointDtoToPool(TactilePointDto tactilePointDto)
        {
            tactilePointDtoPool.Push(tactilePointDto);
        }

        public TactilePointDto GetTactilePointDtoFromPool()
        {
            if (tactilePointDtoPool.Count == 0)
            {
                Debug.Log("TactilePointDto Pool was empty.");

                return null;
            }

            return tactilePointDtoPool.Pop();
        }

        private void ReturnAllTactilePointDtosToPool()
        {
            foreach (TactilePointDto tactilePointDto in TactilePointDtos )
            {
                ReturnTactilePointDtoToPool(tactilePointDto);
            }

            TactilePointDtos.Clear();
        }
         
        public void SetTactileDataPointDtosFromCoordinates(Vector3[] coordinates)
        {
            if (coordinates is null) { throw new ArgumentNullException(nameof(coordinates)); }
             
            // ToDo: review for optimal limit checking.
            int len = coordinates.Length <= maxTactilePoints ? coordinates.Length : maxTactilePoints;
            for (int i = 0; i < len; i++)
            {
                Vector3 coord = coordinates[i];

                // Final chance to clobber any 0, -1, 0 coords that slipped through the net
                if (coord == Vector3.down)
                {
                    // Debug.Log("removed a Vector3.down tactilePoint");
                    continue;
                }

                TactilePointDto tactilePointDto = GetTactilePointDtoFromPool();
                if (tactilePointDto != null)
                {
                    tactilePointDto.Position = coordinates[i];
                    tactilePointDto.Amplitude = strength;
                    TactilePointDtos.Add(tactilePointDto);
                }
            }

            // Debug.Log($"Mesh Tactile Points: {TactilePointDtos.Count}");
            // ToDo: verify removal. why do this right after adding them? moved above. 
            // ReturnAllTactilePointDtosToPool(); 
        }
        #endregion
    }
}
