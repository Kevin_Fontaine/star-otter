﻿/******************************************************************************
 * Copyright (C) Emerge Now, Inc. 2016-2019.                                  *
 * Emerge Now Inc proprietary and confidential.                               *
 *                                                                            *
 * Use subject to the terms of the Emerge Exploration Program Agreement       *
 * and/or terms of limited, non-exclusive, non-sublicensable license to       *
 * Emerge SDK between Emerge and you, your company or any other organization. *
 ******************************************************************************/
// CleanPass

using Emerge.Communication;
using Emerge.HandTracking; 
using System;
using System.Collections.Generic;
using System.Linq;
using SimpleJSON;
using UnityEngine;
using UnityEngineInternal;

namespace Emerge.Core
{
    public class TactileManager : Singleton<TactileManager>
    { 
        /// <summary>
        /// Dispatched when a new emerge object is added.
        /// </summary>
        public Action<AbstractTactileBehavior> OnEmergeObjectAdded { get; set; }

        /// <summary>
        /// Dispatched when an emerge object is removed.
        /// </summary>
        public Action<AbstractTactileBehavior> OnEmergeObjectRemoved { get; set; }
         
        // ToDo: To what does this comment refer? Can we remove it? 
        ///     Attempt to track game state in a way that does not require LEAP
        ///     - pair network objects (visual representation) with their LEAP counterparts
        ///     NOTE: this is currently a scene object, which means that only the MasterClient owns it!
        /// 
        public List<AbstractTactileBehavior> EmergeObjects { get; private set; }

        private TactileHardwareController beamformerHardwareConnection;
        private UDPMessageSender udpMessageSender;

        private List<IDto> tactileEffectsCompose; 

        // Track the historical number of EmergeObjects added.
        private int numberOfEmergeObjectsAdded;

        private List<IDto> EffectDtos = new List<IDto>(); 
        private List<IDto> PointDtos = new List<IDto>();
        private readonly string offString = "stop";
        // A *blankk* Dto used for implementing the Solo Point Amplification effect
        private TactilePointDto tactilePointSpacerDto;

        private TactileConfig tactileConfig;

        private bool runningOnXRPlatform = false;

        #region unityEvents 
        void Awake()
        { 
            EmergeObjects = new List<AbstractTactileBehavior>();
            tactileEffectsCompose = new List<IDto>();
            tactileConfig = TactileConfig.Instance;
            tactilePointSpacerDto = new TactilePointDto(TactileMethod.point);
            tactilePointSpacerDto.Amplitude = 0;
            tactilePointSpacerDto.Position = Vector3.zero;
             
            numberOfEmergeObjectsAdded = 0;

            // ToDo: consider Singletons or ServiceLocator for these
        }

        private void Start()
        {
            beamformerHardwareConnection = FindObjectOfType<TactileHardwareController>();
            udpMessageSender = FindObjectOfType<UDPMessageSender>();
            runningOnXRPlatform = (TactileConfig.Instance.Mode == Mode.XRPlatforms);
        }

        public void Update()
        {
            // Currently they operate separately and the last to send is the one that wins.

            // If none of our attempts to send tactile data to the beamformer are successful, disable beamforming. 
 
            // Send Compose messages before Collision-triggered messages.
            // After Compose messates are sent   
             
            bool useBeamforming = SendTactileDataFromEffectsComposeList();
               
            tactileEffectsCompose.Clear();

            useBeamforming = SendTactileDataFromCollisions() || useBeamforming;
             
            if (!useBeamforming)
            { StopBeamforming(); } 
        } 

        private void OnApplicationQuit()
        {
            StopBeamforming();
        }
        
        #endregion

        #region private
        internal void StopBeamforming()
        {
            if (runningOnXRPlatform)
            {
                if (udpMessageSender != null)
                {
                    string sendOffString = JsonCreator.Instance.BuildJSONToSend(offString).ToString();
                    udpMessageSender.SendMessageToHardware(JsonCreator.Instance.BuildJSONToSend(offString).ToString());
                }
            }
            else
            {
                beamformerHardwareConnection.StopBeamforming(JsonCreator.Instance.BuildJSONToSend(offString));
            }
        }

        // Remote hands is a special case EmergeObject that has skinned mesh tactility
        // attached/detached at runtime. Only set up for 2 players, everything is structured for only 2 atm. 
        internal void AddTactileMeshToRemoteHands(Transform transform, PuppetHand leftHand, PuppetHand rightHand)
        {
            if (transform is null) throw new ArgumentNullException(nameof(transform));
            if (leftHand is null) throw new ArgumentNullException(nameof(leftHand));
            if (rightHand is null) throw new ArgumentNullException(nameof(rightHand));

            //GetComponent<EmergeObjectRemoteHandInitializer>().SetupHands(transform, leftHand, rightHand);
        }
           
        /// <summary>
        /// Gathers ITactileData references for all collisions and sends them to the beamformer.
        /// </summary>
        /// <returns>True if beamforming should continue, false if beamforming should cease.</returns>
        private bool SendTactileDataFromCollisions()
        {
            bool verifyBeamforming = false; 

            // Reset Dto Lists at beginning of each frame
            //MeshDtos.Clear();
            EffectDtos.Clear();
            PointDtos.Clear();

            // Find all Tactile Behaviors that are colliding with a Hand
            var collidingTactileBehaviors = GetAllTactileBehaviorsWithAnActiveCollisionPhase();
            if (collidingTactileBehaviors?.Count > 0)
            { 
                foreach (AbstractTactileBehavior tactileBehavior in collidingTactileBehaviors)
                {
                    // A TactileBehavior may have more than one IDto object (tactileMeshDtos have multiple tactilePointDtos) 
                    AppendDtosToListByTactileBehaviorType(tactileBehavior);
                     
                    // We've used the Dto for this emergeObject so clear its Dtos
                    tactileBehavior.ClearDtos();

                    // Set VerifyBeamforming so this method will return true to indicate that at least one 
                    // beamforming Json was sent.
                    verifyBeamforming = true;
                }
                 
                // Send all gathered Json objects 
                SendTactileEffectsCollisionJson();
                SendTactilePointsCollisionJson(); 
            } 

            return verifyBeamforming;
        }

        private void SendTactilePointsCollisionJson()
        {
            if (PointDtos.Count == 0) { return; }

            if (PointDtos.Count == 1 && tactileConfig.UseSoloPointAmplification)
            {
                 PointDtos.Add(tactilePointSpacerDto);
            }

            JSONObject tactilePointsTransmissionJson = JsonCreator.Instance.GetTactilePointsTransmissionJson(PointDtos);

            // Don't route through WrapJsonForTransmission. It uses a JSONArray instead of JSONObject
            // Instead, the GetTactilePointsTransmissionJson prepares the full node for transmission instead
            // of just returning the data node
            //string actionValue = ActionType.play.ToString("G");
            //JSONObject wrappedJson = JsonCreator.Instance.WrapJsonForTransmission(actionValue, tactilePointsJson); 
            //beamformerHardwareConnection.SendJson(wrappedJson);
            if (runningOnXRPlatform)
            {
                if (udpMessageSender != null)
                {
                    udpMessageSender.SendMessageToHardware(tactilePointsTransmissionJson.ToString());
                }
            }
            else
            {
                beamformerHardwareConnection.SendJson(tactilePointsTransmissionJson);
            }
        }

        private void SendTactileEffectsCollisionJson()
        {
            foreach (TactileEffectDto dto in EffectDtos)
            { 
                JSONObject jsonData;
                // pick a method type to send
                if ( dto.ActionType == ActionType.play ||
                     dto.ActionType == ActionType.composeandplay )
                {  
                    jsonData = JsonCreator.Instance.GetTactileEffectActionTypePlayJson(dto);
                }
                else
                {
                    // This means a Hand collided with a TactileEffect while it had an ActionType set to Play, Pause, Stop or None
                    return;
                }
                  
                string actionValue = ActionType.play.ToString("G");
                JSONObject wrappedJson = JsonCreator.Instance.WrapJsonForTransmission(actionValue, jsonData);
                if (runningOnXRPlatform)
                {
                    if (udpMessageSender != null)
                    {
                        udpMessageSender.SendMessageToHardware(wrappedJson.ToString());
                    }
                }
                else
                {
                    beamformerHardwareConnection.SendJson(wrappedJson);
                }
            }
        }

        /// <summary>
        /// Gathers ITactileData references that need to be Generated and sends them to the beamformer. 
        /// </summary>
        /// <returns>True if beamforming should continue, false if beamforming should cease.</returns>
        private bool SendTactileDataFromEffectsComposeList()
        { 
            var verifyBeamforming = false;
             
            foreach(IDto dto in tactileEffectsCompose)
            {
                JSONObject json = JsonCreator.Instance.CreateComposeTactileEffectJson(dto as TactileEffectDto);
                if (runningOnXRPlatform)
                {
                    if (udpMessageSender != null)
                    {
                        udpMessageSender.SendMessageToHardware(json.ToString());
                    }
                }
                else
                {
                    beamformerHardwareConnection.SendJson(json);
                }

                verifyBeamforming = true;
            }

            return verifyBeamforming;
        } 

        private List<AbstractTactileBehavior> GetAllTactileBehaviorsWithAnActiveCollisionPhase()
        {
            List<AbstractTactileBehavior> collidingEmergeObjects = EmergeObjects.FindAll( (AbstractTactileBehavior emergeObject) => emergeObject.CollisionIsActive); 
               
            return collidingEmergeObjects;
        }

        private void AppendDtosToListByTactileBehaviorType(AbstractTactileBehavior tactileBehavior)
        {
            Type tactileBehaviorType = tactileBehavior.GetType();

            if (tactileBehaviorType == typeof(TactileMesh) || tactileBehaviorType == typeof(TactilePoint))
            {
                AddDtosToList(tactileBehavior.GetDtos(), ref PointDtos); 
            }
            else if (tactileBehaviorType == typeof(TactileEffect))
            {
                // Check if the TactileEffects is not-none.
                // During collision sustain we don't want to send anything, so here we don't add it to the list if it is none.  
                // Effects only return one IDTO in the array
                IDto[] dtos = tactileBehavior.GetDtos();
                TactileEffectDto tactileEffectDto = (TactileEffectDto)dtos[0];
                if (tactileEffectDto.ActionType != ActionType.none)
                {
                    AddDtosToList(dtos, ref EffectDtos);
                }
            }
        }

        private void AddDtosToList(IDto[] array, ref List<IDto> list)
        {
            foreach (IDto dto in array)
            {
                list.Add(dto);
            }
        }
 
        private List<AbstractTactileBehavior> GetAllEmergeObjectsWithCollisionEnd()
        {
            List<AbstractTactileBehavior> collidingEmergeObjects = EmergeObjects.FindAll((AbstractTactileBehavior emergeObject) => emergeObject.CollisionPhase == CollisionPhase.CollisionEnd);

            return collidingEmergeObjects;
        } 

        #endregion

        #region public 
        /// <summary>
        /// Get Emerge Mesh object based on the identifier.
        /// </summary>
        /// <param name="identifier">Identifier property of the BaseEmergeObject requested.</param>
        /// <returns>MeshEmergeObject</returns>
        public TactileMesh GetEmergeMeshObject(string identifier)
        {
            if (identifier is null) throw new ArgumentNullException(nameof(identifier));

            return (TactileMesh)EmergeObjects.FirstOrDefault(item => item.Identifier == identifier);
        }

        /// <summary>
        /// Returns the base Emerge object behaviour based on the Id property. 
        /// </summary>
        /// <param name="id">Id property of the BaseEmergeObject requested.</param>
        /// <returns>BaseEmergeObject</returns>
        public AbstractTactileBehavior GetTactileBehavior(int id)
        {
            return EmergeObjects.FirstOrDefault(item => item.Id == id);
        }
         
        /// <summary>
        /// Add a new BaseEmergeObject to the manager. This is called when a new BaseEmergeObject is instantiated.
        /// </summary>
        /// <param name="emergeObject">BaseEmergeObject to add.</param>
        public void AddEmergeObject(AbstractTactileBehavior emergeObject)
        {
            if (emergeObject is null) { throw new System.ArgumentNullException(nameof(emergeObject)); }
              
            emergeObject.Id = ++numberOfEmergeObjectsAdded; 
            EmergeObjects.Add(emergeObject); 
            
            OnEmergeObjectAdded?.Invoke(emergeObject);   
        } 

        /// <summary>
        /// Remove emerge object from the manager. This is called when emerge object is destroyed
        /// </summary>
        /// <param name="id">Id property of the BaseEmergeObject to remove.</param>
        public void RemoveEmergeObject(int id)
        {
            if (EmergeObjects == null)
            {
                return;
            }
            // TODO : Change EmergeObjects to dictionary rather than list, emerge object ID should be unique
            AbstractTactileBehavior emergeObject = EmergeObjects.FirstOrDefault(item => item.Id == id);
            if (emergeObject != null)
            {
                OnEmergeObjectRemoved?.Invoke(emergeObject);
                EmergeObjects.Remove(emergeObject);
            }
        }

        /// <summary>
        /// Add TactileData to queue of TactileData objects that need to be generated on next Update.
        /// Set the TactileEffectActionType here. Anything that sends tactileData here is not responsible for
        /// knowing that it should first set the TactileEffectActionType property;
        /// </summary>
        /// <param name="tactileData">The ITactileData object to add to the queue.</param>
        public void AddTactileDataToEffectsGenerateQueue(IDto tactileData)
        {
            Debug.Log("AddTactileDataToEffectsGenerateQueue()");
            if (tactileData is null) throw new ArgumentNullException(nameof(tactileData));

            tactileEffectsCompose?.Add( tactileData );
        }

        #endregion
    }
}
