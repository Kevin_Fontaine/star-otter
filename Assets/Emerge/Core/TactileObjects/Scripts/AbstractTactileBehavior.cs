﻿/******************************************************************************
 * Copyright (C) Emerge Now, Inc. 2016-2019.                                  *
 * Emerge Now Inc proprietary and confidential.                               *
 *                                                                            *
 * Use subject to the terms of the Emerge Exploration Program Agreement       *
 * and/or terms of limited, non-exclusive, non-sublicensable license to       *
 * Emerge SDK between Emerge and you, your company or any other organization. *
 ******************************************************************************/
// CleanPass

using System;
using System.Collections.Generic;
using Emerge.Communication;
using Leap.Unity.Interaction;
using UnityEngine;

namespace Emerge.Core
{
    /// <summary>
    ///     Attach this component to bring tactile behaviour to a GameObject.
    ///     There are a few methods of Emerge object behaviour
    ///     1. Assign Tactile animations to a game object - uses one of Emerge's tactile animations
    ///     2. Use a Mesh to bring tactility - uses the verts in the mesh to determine the tactile points
    ///     3. Use BaseEmergeObject to specify independent coordinates of tactile sensation.
    /// </summary> 
    [Serializable]
    public abstract class AbstractTactileBehavior : MonoBehaviour
    {
        // This string must match the field below. It is a companion string lookup value used by an Editor script to find the field. 
        public static readonly string IdentifierString = "identifier"; 
        [SerializeField]
        [HideInInspector]
        private string identifier = "default"; 
        public string Identifier
        {
            get => identifier;
            set => identifier = value;
        }
        public NetworkObject NetworkObjectMultiUserComponent { get; set; }
        public Action<AbstractTactileBehavior> CollisionPhaseChanged { get; set; }
        public InteractionBehaviour InteractionBehaviour { get; protected set; }
        public BaseHandCollision PerObjectCollisionDetection { get; protected set; }
        public CollisionPhase CollisionPhase { get; protected set; }
        public Action<IDto> Init { get; set; }

        /// <summary>
        /// ToDo: specify how this Id is used.
        /// </summary> 
        public int Id { get; set; } 
         
        /// <summary>
        ///     Determine if the collision phase changed during the most recent Update.
        /// </summary>
        /// <returns>True if the collision began or stopped on the most recent Update.</returns>
        public bool CollisionPhaseIsBeginOrEnd =>
            CollisionPhase == CollisionPhase.CollisionBegin ||
            CollisionPhase == CollisionPhase.CollisionEnd;

        /// <summary>
        ///     Determine if CollisionPhase is either CollisionBegin or CollisionSustain.
        ///     This replaces the isTouched property that was used earlier.
        /// </summary>
        /// <returns>True if CollisionPhase is either CollisionBegin or CollisionSustain.</returns>
        public bool CollisionIsActive
        {
            get
            {
                bool active = CollisionPhase == CollisionPhase.CollisionBegin || CollisionPhase == CollisionPhase.CollisionSustain || CollisionPhase == CollisionPhase.CollisionEnd;
                  
                return active;
            }
        }

        public bool CollisionHasEnded =>
            CollisionPhase == CollisionPhase.CollisionEnd;

        protected CollisionPhase PrevCollisionPhase; 
        protected TactileManager TactileManager;
        protected HandCollisionStatus HandCollisionStatus;

        protected ActionType CurrentAction;
          
        #region public
        public virtual void Awake()
        {
            TactileManager = TactileManager.Instance;
            // Network object (photon) to sync transform across PCs
            NetworkObjectMultiUserComponent = GetComponent<NetworkObject>();
            if(NetworkObjectMultiUserComponent == null) { NetworkObjectMultiUserComponent = GetComponentInChildren<NetworkObject>(); }
            // NOTE: only required by Ownership manager - 
            // are we getting rid of that in favor of John's new system? in that case we can get rid of this here...
            InteractionBehaviour = GetComponent<InteractionBehaviour>(); 
            if(TryGetComponent(out BaseHandCollision baseHandCollision))
            {
                PerObjectCollisionDetection = baseHandCollision;
            } 
            else 
            {
                Debug.LogError("A BaseHandCollision object is required with AbstractTactileBehavior");
            };
              
            PrevCollisionPhase = CollisionPhase.None;
            CollisionPhase = CollisionPhase.None;
             
            TactileManager?.AddEmergeObject(this);
             
            NetworkObjectMultiUserComponent?.SetInteractableObjectRoot(transform);
        }

        public virtual void Update()
        {
            DetectCollisions();
        } 

        /// <summary>
        ///     Override this to retrieve this class' ITactileData object.
        /// </summary>
        /// <returns></returns>
        public virtual IDto[] GetDtos()
        {
            return null;
        }

        public virtual List<IDto> GetDTOs()
        {
            List<IDto> dtos = new List<IDto>();

            return dtos;
        }

        public abstract void ClearDtos();
       
        #endregion 

        #region protected 
        // TODO: need to make NetworkObject component optional.... currently only required by OwnershipManager
        protected void SetupPhotonObjects()
        {
            if (NetworkObjectMultiUserComponent == null)
            {
                NetworkObjectMultiUserComponent = gameObject.AddComponent<NetworkObject>();
            }

            NetworkObjectMultiUserComponent?.SetInteractableObjectRoot(transform);
        }
         
        protected virtual void DetectCollisions()
        {
            UpdateHandCollisionStatus();
            DispatchCollisionStatusChangedEvent();
        }

        protected void UpdateHandCollisionStatus()
        { 
            HandCollisionStatus = PerObjectCollisionDetection.DetectHandCollision();

            if (HandCollisionStatus != HandCollisionStatus.None)
            {
                // Collision is active

                switch (PrevCollisionPhase)
                {
                    case CollisionPhase.None:
                    case CollisionPhase.CollisionEnd:
                    {
                        // If the collision status on the previous update was either None or CollisionEnd and now there is a collision,
                        // this is the first Update with a collision so we set CollisionPhase to CollisionBegin
                        CollisionPhase = CollisionPhase.CollisionBegin;
                    }
                        break;
                         
                    default:
                    {
                        // CollisionBegin was active for one Update and now we set it to CollisionSustain.
                        // Catch any other cases and apply default. 
                        CollisionPhase = CollisionPhase.CollisionSustain;
                    }
                        break;
                }
            }
            else
            {
                // No Collision is active 
                 
                switch (PrevCollisionPhase)
                {
                    case CollisionPhase.CollisionBegin:
                    case CollisionPhase.CollisionSustain:
                    {
                        // If Collision Status on the previous update was either CollisionBegin or CollisionSustain, 
                        // and now there is no collision, set CollisionPhase to CollisionEnd for one Update
                        CollisionPhase = CollisionPhase.CollisionEnd; 
                    }
                        break;
                         
                    default:
                    {
                        // CollisionEnd was active on the prior frame and now we set CollisionPhase to None.
                        // Catch any other cases and apply default.
 
                        CollisionPhase = CollisionPhase.None; 
                    }
                        break;
                }
            } 
        }
        
        /// <summary>
        /// Dispatch on any change, even from Begin to Sustain and End to None. 
        /// </summary>
        protected void DispatchCollisionStatusChangedEvent()
        {
            if (PrevCollisionPhase != CollisionPhase)
            { 
                CollisionPhaseChanged?.Invoke(this);
            }

            PrevCollisionPhase = CollisionPhase;
        }
        #endregion

        #region private 
        private void OnDestroy()
        {
            ClearSubscriptions();
              
            TactileManager.Instance?.RemoveEmergeObject(Id);
        }

        // Bulk removal of methods registered to listen for Actions broadcast from this class. 
        private void ClearSubscriptions()
        { 
            CollisionPhaseChanged = delegate { };
        }

        private void OnDisable()
        {
            PrevCollisionPhase = CollisionPhase.None;
            CollisionPhase = CollisionPhase.None;
        } 


        #endregion 
    }
}
