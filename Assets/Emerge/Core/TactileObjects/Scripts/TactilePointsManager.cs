﻿/******************************************************************************
 * Copyright (C) Emerge Now, Inc. 2016-2019.                                  *
 * Emerge Now Inc proprietary and confidential.                               *
 *                                                                            *
 * Use subject to the terms of the Emerge Exploration Program Agreement       *
 * and/or terms of limited, non-exclusive, non-sublicensable license to       *
 * Emerge SDK between Emerge and you, your company or any other organization. *
 ******************************************************************************/

using Emerge.Communication;
using System;
using UnityEngine;

namespace Emerge.Core
{ 
    // Considered for updating Mesh beamforming to use tactilePoints but this may not be necessary. 
    public class TactilePointsManager : MonoBehaviour
    {
        //protected bool active;

        //[SerializeField]
        //protected TactilePointMode tactilePointMode = TactilePointMode.Override;

        //[SerializeField]
        //protected Transform[] coordinateTransforms;

        //[HideInInspector] 
        //protected TactilePointData[] tactilePoints;

        //private TactilePoint tactilePoint;

        /// <summary>
        /// Creates a list of empty TactilePoint objects the size of your Transform[] list ("CustomPointsGenerator.objects")
        /// </summary>
        /// <param name="message">the custom points portion of the BeamformerMessage</param>
        //public virtual void Init(ref TactilePointDto message)
        //{
        //    tactilePoints = new TactilePointData[coordinateTransforms.Length];
        //    message.tactilePoints.Clear();

        //    for (int i = 0; i < coordinateTransforms.Length; i++)
        //    {
        //        tactilePoints[i] = new TactilePointData(coordinateTransforms[i].position) {id = i};

        //        // ToDo: Test this - since we are keeping all points it might make sense
        //        message.tactilePoints.Add(tactilePoints[i]);  
        //    }
        //    message.tactilePointMode = tactilePointMode;

        //    active = false;
        //    tactilePoint = GetComponent<TactilePoint>();
        //    tactilePoint.CollisionPhaseChanged += CollisionPhaseChangedEventHandler;
        //}

        //private void CollisionPhaseChangedEventHandler( AbstractTactileBehavior emergeObject )
        //{
        //    active = emergeObject.CollisionIsActive;
        //}

        //public virtual void TouchStatusChanged(bool isTouched)
        //{
        //    if (isTouched)
        //        StartAnimation();
        //    else
        //        StopAnimation();
        //}
         
        //protected virtual void StartAnimation()
        //{
        //    active = true;
        //}

        //protected virtual void StopAnimation()
        //{
        //    active = false;
        //}

        // Update and ready to send to hardware 
        //public virtual void UpdatePoints(ref TactilePointDto tactileData)
        //{
        //    if (!active) return;

        //    //simple default behaviour to be overridden - assign the 'point' property for each object
        //    //this will simple beam all objects in your Transform[] list
        //    for (int i = 0; i < coordinateTransforms.Length; i++)
        //    {
        //        if (coordinateTransforms[i].gameObject.activeSelf)
        //        {
        //            tactileData.tactilePoints[i].beam = true;
        //            tactileData.tactilePoints[i].SetPoint(coordinateTransforms[i].position);
        //        }
        //        else
        //        {
        //            tactileData.tactilePoints[i].beam = false;
        //        } 
        //    }
             
        //    //EXAMPLE - move to another class
        //    ////base.UpdatePoints();
        //    //message.points.Clear();
        //    //message.overridePreviousMessage = true;
        //    //message.points.Add(new TactilePoint(transform.position, true, 0, 4.5f, 100));
        //    //message.points.Add(new TactilePoint(transform.position + transform.right * 0.065f + transform.forward * -0.075f, true, 1, 5.0f, 50));
        //    //message.points.Add(new TactilePoint(transform.position + transform.right * -0.03f + transform.forward * 0.03f, true, 2, 3.5f, 100));
        //    //message.points.Add(new TactilePoint(transform.position + transform.right * -0.03f, true, 3, 3.5f, 33));
        //    //message.points.Add(new TactilePoint(transform.position + transform.right * 0.03f, true, 4, 3.5f, 66)); 
        //}
    }
}
