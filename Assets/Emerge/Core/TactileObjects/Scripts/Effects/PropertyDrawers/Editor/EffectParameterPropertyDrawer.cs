﻿using UnityEditor;
using UnityEngine;
using Emerge.Core;
using EditorHelperUtils;
using Emerge.Core.Shared.Utils;
using static Emerge.Core.EffectConstants;

[CustomPropertyDrawer(typeof(AbstractModifiedPrimitive), true)]
class EffectParameterDrawer : PropertyDrawer
{ 
    public int rows = 2;
    public float topOffset = 10f;
    public float bottomOffset = 10f;

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return base.GetPropertyHeight(property, label) * rows + topOffset;
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);


        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 1;

        // Calculate rects
        float height = EditorGUIUtility.singleLineHeight;
        float yPos = position.y + topOffset;
        Rect idRect = new Rect(position.x, yPos, position.width * 0.9f, height);
        Rect valueRect = new Rect(position.x, yPos + 16, position.width * 0.9f, height);
         
        System.Type t = EditorHelper.GetType(property);

        //draw: description
        //---------------------------------------------------------------------------------
        string desc = property.FindPropertyRelative(nameof(ModifiedFloat.description)).stringValue;

        if (desc != EffectConstants.NoneString)
            EditorGUI.HelpBox(idRect, desc, MessageType.Info);
        else
        {
            valueRect = idRect;
            rows = 1; //remove the description row
        }
        //alternative text box
        //EditorGUI.PropertyField(idRect, property.FindPropertyRelative(nameof(ModifiedFloat.description)), new GUIContent(desc));
         
        //draw: values of different types
        //---------------------------------------------------------------------------------
        if (t == typeof(ModifiedFloat))
        {
            ModifiedFloat myTarget = fieldInfo.GetValue(property.serializedObject.targetObject) as ModifiedFloat;

            EditorGUI.BeginChangeCheck();

            EditorGUI.Slider(valueRect, property.FindPropertyRelative(nameof(ModifiedFloat.currentValue)), myTarget.Min, myTarget.Max, label);

            if (EditorGUI.EndChangeCheck())
                myTarget.ValueChanged();
        }
        else if (t == typeof(ModifiedInt))
        {
            ModifiedInt myTarget = fieldInfo.GetValue(property.serializedObject.targetObject) as ModifiedInt;

            EditorGUI.BeginChangeCheck();

            EditorGUI.IntSlider(valueRect, property.FindPropertyRelative(nameof(ModifiedInt.currentValue)), myTarget.Min, myTarget.Max, label);

            if (EditorGUI.EndChangeCheck())
                myTarget.ValueChanged();
        }
        else if (t == typeof(ModifiedBool))
        {
            EditorGUI.BeginChangeCheck();
            EditorGUI.PropertyField(valueRect, property.FindPropertyRelative(nameof(ModifiedBool.currentValue)), label);
            if (EditorGUI.EndChangeCheck())
            {
                ModifiedBool myTarget = fieldInfo.GetValue(property.serializedObject.targetObject) as ModifiedBool;
                myTarget.ValueChanged();
            }
        }
        else
        {
            // unable to find type
            EditorGUI.PropertyField(idRect, property.FindPropertyRelative(nameof(ModifiedFloat.description)), new GUIContent("TYPE NOT SUPPORTED"));
        }


        // Set indent back to what it was
        EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty();
    }
}


