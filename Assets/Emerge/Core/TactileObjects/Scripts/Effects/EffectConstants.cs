﻿using System;
using Emerge.Core.Shared.Utils;

namespace Emerge.Core
{
    public static class EffectConstants
    {
        public static readonly string NoneString = "";
        public static readonly string PointTimeString = "Time That Each Point is Activated [ms]";
        public static readonly string FrameRepsString =
            "Number of Times Each Animation Segment is Repeated";

        public static readonly int AmplitudeMin = 0;
        public static readonly int AmplitudeMax = 100;
        public static readonly float MinTimePerPoint = 2.25f;

        //defaults
        public static readonly int frameRepDefault = 4;
        public static readonly float pointTimeDefault = 7f;

        public static ModifiedInt DefaultAmplitude()
        {
            return new ModifiedInt(_min: AmplitudeMin,
                _max: AmplitudeMax,
                _default: AmplitudeMax,
                EffectConstants.NoneString);
        }

        //TPP------------------------
        public static ModifiedFloat DefaultTimePerPoint(float _overrideDefault, Action _changeCallback)
        {
            return new ModifiedFloat(_min: MinTimePerPoint,
                _max: 10f,
                _default: _overrideDefault,
                EffectConstants.PointTimeString,
                _changeCallback);
        }

        public static ModifiedFloat DefaultTimePerPoint(Action _changeCallback)
        {
            return DefaultTimePerPoint(pointTimeDefault, _changeCallback);
        }

        //frame reps------------------------
        public static ModifiedInt DefaultFrameReps(float _overrideDefault, Action _changeCallback)
        {
            return new ModifiedInt(_min: 1,
                _max: 20,
                _default: 4,
                EffectConstants.FrameRepsString,
                _changeCallback);
        }

        public static ModifiedInt DefaultFrameReps(Action _changeCallback)
        {
            return DefaultFrameReps(frameRepDefault, _changeCallback);
        } 
    }
}