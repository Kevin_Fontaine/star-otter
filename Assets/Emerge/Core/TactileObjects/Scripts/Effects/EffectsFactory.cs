﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Emerge.Communication
{
    using Core;
    using SimpleJSON;
    using System;

    [System.Serializable]
    public class EffectsFactory
    {
        public TactileEffectType effectType = TactileEffectType.basic_plane_wave;

        //make sure the variable names here match the type, needed for reflection.
        public BasicPlaneWaveProperties BasicPlaneWaveProperties;
        public CustomPlaneWaveProperties CustomPlaneWaveProperties;
        public SimpleCircularWaveProperties SimpleCircularWaveProperties;
        public CustomCircularWaveProperties CustomCircularWaveProperties;
        public CustomZGridProperties CustomZGridProperties;
        public ExpandingSphereProperties ExpandingSphereProperties;
        public ShakingSphereProperties ShakingSphereProperties;
        public SpiralGridProperties SpiralGridProperties;
        public FurProperties FurProperties;
        public RandomWalkProperties RandomWalkProperties;

        public AbstractEffectProperties CreateEffect()
        {
            return GetEffect(effectType);
        }

        public System.Type GetEffectDerivedType(TactileEffectType effect)
        {
            return GetEffect(effect).GetType();
        }

        public AbstractEffectProperties GetCurrentEffect()
        {
            return GetEffect(effectType);
        }

        private AbstractEffectProperties GetEffect(TactileEffectType effect)
        {
            switch (effect)
            {
                case TactileEffectType.basic_plane_wave:
                {
                    return BasicPlaneWaveProperties;
                }  
                case TactileEffectType.custom_plane_wave:
                    return CustomPlaneWaveProperties;
                case TactileEffectType.simple_circular_wave:
                    return SimpleCircularWaveProperties;
                case TactileEffectType.custom_circular_wave:
                    return CustomCircularWaveProperties;
                case TactileEffectType.custom_z_grid:
                    return CustomZGridProperties;
                case TactileEffectType.expanding_sphere:
                    return ExpandingSphereProperties;
                case TactileEffectType.shaking_sphere:
                    return ShakingSphereProperties;
                case TactileEffectType.spiral_grid:
                    return SpiralGridProperties;
                case TactileEffectType.fur:
                    return FurProperties;
                case TactileEffectType.random_walk:
                    return RandomWalkProperties;
                default:
                    return BasicPlaneWaveProperties;
            }
        }
    }
}