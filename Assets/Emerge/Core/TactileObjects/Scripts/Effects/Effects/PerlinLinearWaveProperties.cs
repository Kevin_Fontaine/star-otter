using System;
using Emerge.Core.Shared.Utils;

namespace Emerge.Core
{
    using SimpleJSON;
    using static EffectConstants;

    [Serializable]
    public class PerlinLinearWaveProperties : AbstractEffectProperties
    {
        //{'Length':14.0, 'Width':5.0, 'Zspacing':2.0 , 'Xspacing':2.0, 'Curvature':np.Inf, 'Theta':0.0, 'frameReps':4, 'TimePerPoint':7.0, 'amp':100, 'PositionNoiseAmplitude':2, 'PositionNoiseFrequency':1, 'IntensityNoisePercentage':0, 'IntensityNoiseFrequency':1}
        public ModifiedFloat Length;
        public ModifiedFloat Width;
        public ModifiedFloat Zspacing;
        public ModifiedFloat Xspacing;
        public ModifiedFloat Curvature;
        public ModifiedFloat Theta;
        public ModifiedInt AnimationSegmentRepetitions;
        public ModifiedFloat PositionNoiseAmplitude;
        public ModifiedInt PositionNoiseFrequency;
        public ModifiedInt IntensityNoisePercentage;
        public ModifiedInt IntensityNoiseFrequency;
        public ModifiedFloat TimePerPoint;
        public ModifiedInt Amplitude;

        public PerlinLinearWaveProperties()
        {
            tactileEffectType = TactileEffectType.perlin_linear_wave;
            Length = new ModifiedFloat(_min: 1f, _max: 15f, _default: 8f,
            "Length the Wave Will Travel [cm]", SetDirty); //14.0f;?
            Width = new ModifiedFloat(_min: 1f, _max: 15f, _default: 5f,
            "Width of the Travelling Wave [cm]", SetDirty); //5.0f;?
            Zspacing = new ModifiedFloat(_min: 2f, _max: 10f, _default: 2f,
            "Spacing of Wave Segments [cm]", SetDirty); //2.0f;
            Xspacing = new ModifiedFloat(_min: 2f, _max: 10f, _default: 2f,
            "Spacing of Points in Each Wave Line [cm]", SetDirty); //2.0f;
            Curvature = new ModifiedFloat(_min: 0.1f, _max: 360f, _default: 360f,
            "Curvature in Y Direction of the Wave", SetDirty); //360f;
            Theta = new ModifiedFloat(_min: 0, _max: 360f, _default: 360f,
            "Direction of the Travelling Wave [Degrees]", SetDirty); //360.0f;
            AnimationSegmentRepetitions = new ModifiedInt(_min: 1, _max: 10, _default: 4,
                EffectConstants.FrameRepsString, SetDirty); //4;
            PositionNoiseAmplitude = new ModifiedFloat(_min: 0f, _max: 15f, _default: 2f,
                EffectConstants.NoneString, SetDirty); //4;
            PositionNoiseFrequency = new ModifiedInt(_min: 0, _max: 40, _default: 1,
                EffectConstants.NoneString, SetDirty); //4;
            IntensityNoisePercentage = new ModifiedInt(_min: 0, _max: 50, _default: 0,
                EffectConstants.NoneString, SetDirty); //4;
            IntensityNoiseFrequency = new ModifiedInt(_min: 0, _max: 40, _default: 1,
                EffectConstants.NoneString, SetDirty);

            TimePerPoint = EffectConstants.DefaultTimePerPoint(SetDirty);
            Amplitude = new ModifiedInt(_min: AmplitudeMin, _max: AmplitudeMax, _default: AmplitudeMax, EffectConstants.NoneString, SetDirty);
        }

        public override JSONObject GetDataAsJSONObject()
        {
            JSONObject array = new JSONObject();
            array.Add("length", Length.Value);
            array.Add("width", Width.Value);
            array.Add("yspacing", Zspacing.Value);
            array.Add("xspacing", Xspacing.Value);
            array.Add("curvature", Curvature.Value);
            array.Add("theta", Theta.Value);
            array.Add("frameReps", AnimationSegmentRepetitions.Value);
            array.Add("yNoiseAmp", PositionNoiseAmplitude.Value);
            array.Add("yNoiseFreq", PositionNoiseFrequency.Value);
            array.Add("ampNoisePercentage", IntensityNoisePercentage.Value);
            array.Add("ampNoiseFreq", IntensityNoiseFrequency.Value);
            array.Add("pointTime", TimePerPoint.Value);
            array.Add("amp_factor", Amplitude.Value / 100.0f);
            return array;
        }
    }
}
