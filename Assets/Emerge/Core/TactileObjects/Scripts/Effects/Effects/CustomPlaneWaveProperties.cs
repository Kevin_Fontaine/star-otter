using System;
using System.Collections;
using System.Collections.Generic;
using Emerge.Core.Shared.Utils;
using UnityEngine;

namespace Emerge.Core
{
    using SimpleJSON;
    using static EffectConstants;

    [Serializable]
    public class CustomPlaneWaveProperties : AbstractEffectProperties
    {
        public ModifiedFloat Length;
        public ModifiedFloat Width;
        public ModifiedFloat ZSpacing;
        public ModifiedFloat XSpacing;
        public ModifiedFloat Curvature;
        public ModifiedFloat Theta;
        public ModifiedInt AnimationSegmentRepetitions;
        public ModifiedFloat TimePerPoint;
        public ModifiedInt Amplitude;

        public CustomPlaneWaveProperties()
        {
            tactileEffectType = TactileEffectType.custom_plane_wave;
            Length = new ModifiedFloat(_min: 1f, _max: 15f, _default: 8f,
            "Length the Wave Will Travel [cm]", SetDirty); //14.0f;?
            Width = new ModifiedFloat(_min: 1f, _max: 15f, _default: 5f,
            "Width of the Travelling Wave [cm]", SetDirty); //5.0f;?
            ZSpacing = new ModifiedFloat(_min: 2f, _max: 10f, _default: 2f,
            "Spacing of Wave Segments [cm]", SetDirty); //2.0f;
            XSpacing = new ModifiedFloat(_min: 2f, _max: 10f, _default: 2f,
            "Spacing of Points in Each Wave Line [cm]", SetDirty); //2.0f;
            Curvature = new ModifiedFloat(_min: 0.1f, _max: 360f, _default: 360f,
            "Curvature in Y Direction of the Wave", SetDirty); //360f;
            Theta = new ModifiedFloat(_min: 0, _max: 360f, _default: 360f,
            "Direction of the Travelling Wave [Degrees]", SetDirty); //360.0f;
            AnimationSegmentRepetitions = new ModifiedInt(_min: 1, _max: 10, _default: 4,
                EffectConstants.FrameRepsString, SetDirty); //4;
            Amplitude = new ModifiedInt(_min: AmplitudeMin, _max: AmplitudeMax, _default: AmplitudeMax, EffectConstants.NoneString, SetDirty);
            TimePerPoint = EffectConstants.DefaultTimePerPoint(SetDirty);
        }
        public override JSONObject GetDataAsJSONObject()
        {
            JSONObject array = new JSONObject();
            array.Add("length", Length.Value);
            array.Add("width", Width.Value);
            array.Add("yspacing", ZSpacing.Value);
            array.Add("xspacing", XSpacing.Value);
            array.Add("curvature", Curvature.Value);
            array.Add("theta", Theta.Value);
            array.Add("frameReps", AnimationSegmentRepetitions.Value);
            array.Add("pointTime", TimePerPoint.Value);
            array.Add("amp_factor", Amplitude.Value / 100.0f);
            return array;
        }
    }
}
