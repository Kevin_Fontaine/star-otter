using System;
using Emerge.Core.Shared.Utils;

namespace Emerge.Core
{
    using SimpleJSON;

    [Serializable]
    public class PerlinCircleSetProperties : AbstractEffectProperties
    {
        //{'TimePerPoint' : 7.0, 'amp' : 100, 'curvature' = 3.0, 'SliceAngle' : 360, 'dS' : 0,'RadiusNoiseAmplitude' : 2, 'RadiusNoiseFrequency' : 6}
        public ModifiedInt NumberofPoints;
        public ModifiedFloat RadiusNoiseAmplitude;
        public ModifiedInt RadiusNoiseFrequency;
        public ModifiedFloat SliceAngle;
        public ModifiedFloat Radius;
        public ModifiedFloat TimePerPoint;
        public ModifiedInt Amplitude;
        public PerlinCircleSetProperties()
        {
            tactileEffectType = TactileEffectType.perlin_circle_set;
            NumberofPoints = new ModifiedInt(_min: 6, _max: 25, _default: 9, "Number of Points in Circle", SetDirty); //5.0f;
            RadiusNoiseAmplitude = new ModifiedFloat(_min: 0f, _max: 15f, _default: 2f, "Noise Amplitude For Position", SetDirty); //5.0f;
            RadiusNoiseFrequency = new ModifiedInt(_min: 0, _max: 40, _default: 1, "Noise Frequency For Position", SetDirty); //5.0f;
            SliceAngle = new ModifiedFloat(_min: 60, _max: 360f, _default: 360f, "Angle of Slice of Circle [Degrees]", SetDirty); //5.0f;
            Radius = new ModifiedFloat(_min: 0.1f, _max: 10f, _default: 2f, "Radius of Circle [cm]", SetDirty); //5.0f;
            TimePerPoint = new ModifiedFloat(_min: 2.25f, _max: 10f, _default: 7f, EffectConstants.NoneString, SetDirty); //7.0f;
            Amplitude = new ModifiedInt(_min: 0, _max: 100, _default: 100, EffectConstants.NoneString); //5.0f;
        }
        public override JSONObject GetDataAsJSONObject()
        {
            JSONObject array = new JSONObject();
            array.Add("nPoints", NumberofPoints.Value);
            array.Add("rNoiseAmp", RadiusNoiseAmplitude.Value);
            array.Add("rNoiseFreq", RadiusNoiseFrequency.Value);
            array.Add("angAcceptance", SliceAngle.Value);
            array.Add("R", Radius.Value); 
            array.Add("pointTime", TimePerPoint.Value);
            array.Add("amp_factor", Amplitude.Value / 100.0f);
            return array;
        }/*

        public void Reset()
        {
            wavespeed.Reset();
            intensity.Reset();
            TimePerPoint.Reset();
            Amplitude.Reset();
        }*/
    }
}
