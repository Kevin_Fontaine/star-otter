﻿/******************************************************************************
 * Copyright (C) Emerge Now, Inc. 2016-2019.                                  *
 * Emerge Now Inc proprietary and confidential.                               *
 *                                                                            *
 * Use subject to the terms of the Emerge Exploration Program Agreement       *
 * and/or terms of limited, non-exclusive, non-sublicensable license to       *
 * Emerge SDK between Emerge and you, your company or any other organization. *
 ******************************************************************************/

using System;
using Emerge.Core.Shared.Utils;

namespace Emerge.Core
{
    using SimpleJSON;
    using static EffectConstants;

    //Class for defining the changeable properties for the heart pulse emotion effect 
    [Serializable]
    public class HeartPulseEmotionEffectProperties : AbstractEffectProperties
    {
        public ModifiedInt FrameReps;
        public ModifiedInt Amplitude;

        //Constructor to setup the class and set default values
        public HeartPulseEmotionEffectProperties()
        {
            tactileEffectType = TactileEffectType.pulsingHeart;
            FrameReps = new ModifiedInt(_min: 0, _max: 8, _default: 3, "Repetitions per frame", SetDirty);
            Amplitude = new ModifiedInt(_min: AmplitudeMin, _max: AmplitudeMax, _default: AmplitudeMax, EffectConstants.NoneString, SetDirty);
        }

        //Method to override the JSON data being transmitted using the properties specific to this effect
        public override JSONObject GetDataAsJSONObject()
        {
            JSONObject array = new JSONObject();
            array.Add("frameReps", FrameReps.Value);
            array.Add("amp_factor", Amplitude.Value / 100.0f);

            return array;
        }
    }
}
