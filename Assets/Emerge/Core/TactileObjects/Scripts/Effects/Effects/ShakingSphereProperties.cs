﻿using System;
using Emerge.Core.Shared.Utils;

namespace Emerge.Core
{
    using SimpleJSON;
    using static EffectConstants;

    [Serializable]
    public class ShakingSphereProperties : AbstractEffectProperties
    {
        //'Length' : 4.0, 'Width' : 4.0, 'XSpacing' : 1.0, 'ZSpacing' : 1.0, 'SphereRadius' : 4.0, 'NumberOfMoves' : 10, 'MovementDistance' : 1.0, 'amp' : 100, 'poEffectParameter_IntTime' : 5.0, 'frameReps' : 5
        public ModifiedFloat Length;
        public ModifiedFloat Width;
        public ModifiedFloat XSpacing;
        public ModifiedFloat ZSpacing;
        public ModifiedFloat SphereRadius;
        public ModifiedInt NumberOfMoves;
        public ModifiedFloat MovementDistance;
        public ModifiedFloat TimePerPoint;
        public ModifiedInt AnimationSegmentRepetitions;
        public ModifiedInt Amplitude;
        public ShakingSphereProperties()
        {
            tactileEffectType = TactileEffectType.shaking_sphere;
            Length = new ModifiedFloat(_min: 1f, _max: 4f, _default: 3f, "Length of Sphere Section [cm]", SetDirty); //4.0f;
            Width = new ModifiedFloat(_min: 1f, _max: 4f, _default: 3f, "Width of Sphere Section [cm]", SetDirty); //4.0f;
            XSpacing = new ModifiedFloat(_min: 1f, _max: 4f, _default: 1f, "Spacing in Width Direction [cm]", SetDirty); //1.0f;
            ZSpacing = new ModifiedFloat(_min: 1f, _max: 4f, _default: 1f, "Spacing in Length Direction [cm]", SetDirty); //1.0f;
            SphereRadius = new ModifiedFloat(_min: 10f, _max: 20f, _default: 15f, "Radius of Sphere Section [cm]", SetDirty); //4.0f;
            NumberOfMoves = new ModifiedInt(_min: 1, _max: 5, _default: 5, "Number of Distinct Shaking Moves", SetDirty); //10;
            MovementDistance = new ModifiedFloat(_min: 1f, _max: 5f, _default: 1f, "Movement From the Center For Each Shake", SetDirty); //1.0f;            
            TimePerPoint = EffectConstants.DefaultTimePerPoint(SetDirty);
            AnimationSegmentRepetitions = new ModifiedInt(_min: 1, _max: 6, _default: 5, EffectConstants.FrameRepsString, SetDirty); //5;
            Amplitude = new ModifiedInt(_min: AmplitudeMin, _max: AmplitudeMax, _default: AmplitudeMax, EffectConstants.NoneString, SetDirty);
        }

        public override JSONObject GetDataAsJSONObject()
        {
            JSONObject array = new JSONObject();
            array.Add("length", Length.Value);
            array.Add("width", Width.Value);
            array.Add("xspacing", XSpacing.Value);
            array.Add("zspacing", ZSpacing.Value);
            array.Add("Rmax", SphereRadius.Value);
            array.Add("nMoves", NumberOfMoves.Value);
            array.Add("rMove", MovementDistance.Value);
            array.Add("amp_factor", Amplitude.Value / 100.0f);
            array.Add("pointTime", TimePerPoint.Value);
            array.Add("frameReps", AnimationSegmentRepetitions.Value);
            return array;
        }
    }
}
