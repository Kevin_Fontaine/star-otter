﻿using System;
using Emerge.Core.Shared.Utils;

namespace Emerge.Core
{
    using SimpleJSON;
    using static EffectConstants;

    [Serializable]
    public class SpiralGridProperties : AbstractEffectProperties
    {
        //'length' : 4.0, 'width' : 4.0, 'xspacing' : 1.0, 'yspacing' : 1.0, 'Rmax' : 4.0, 'nMoves' : 10, 'rMove' : 1.0, 'amp' : 100, 'poEffectParameter_IntTime' : 5.0, 'frameReps' : 5
        public ModifiedFloat SpiralFactor;
        public ModifiedFloat MinimumRadius;
        public ModifiedFloat MaximumRadius;
        public ModifiedInt NumberOfPositions;
        public ModifiedInt NumberOfGridPoints;
        public ModifiedFloat Spacing;
        public ModifiedFloat TimePerPoint;
        public ModifiedInt AnimationSegmentRepetitions;
        public ModifiedInt Amplitude;

        public SpiralGridProperties()
        {
            tactileEffectType = TactileEffectType.spiral_grid;
            SpiralFactor = new ModifiedFloat(_min: 0.1f, _max: 10f, _default: .5f, "Spiral Factor", SetDirty); //4.0f;
            MinimumRadius = new ModifiedFloat(_min: 0.1f, _max: 4.9f, _default: 1f, "Final Distance From Center [cm]", SetDirty); //4.0f;
            MaximumRadius = new ModifiedFloat(_min: 5f, _max: 10f, _default: 5f, "Initial Distance From Center [cm]", SetDirty); //1.0f;
            NumberOfPositions = new ModifiedInt(_min: 1, _max: 20, _default: 10, "Number of Positions for the Grid", SetDirty); //1.0f;
            NumberOfGridPoints = new ModifiedInt(_min: 1, _max: 16, _default: 9, "Number of Points in the Grid", SetDirty); //4.0f;
            Spacing = new ModifiedFloat(_min: 0.1f, _max: 5f, _default: 0.5f, "Spacing of Points in the Grid [cm]", SetDirty); //0.5;            
            TimePerPoint = EffectConstants.DefaultTimePerPoint(SetDirty);
            AnimationSegmentRepetitions = new ModifiedInt(_min: 1, _max: 4, _default: 2, EffectConstants.FrameRepsString, SetDirty); //5;
            Amplitude = new ModifiedInt(_min: AmplitudeMin, _max: AmplitudeMax, _default: AmplitudeMax, EffectConstants.NoneString, SetDirty);
        }

        public override JSONObject GetDataAsJSONObject()
        {
            JSONObject array = new JSONObject();
            array.Add("a", SpiralFactor.Value);
            array.Add("rMin", MinimumRadius.Value);
            array.Add("rMax", MaximumRadius.Value);
            array.Add("nFrames", NumberOfPositions.Value);
            array.Add("nGrid", NumberOfGridPoints.Value);
            array.Add("spac", Spacing.Value);
            array.Add("amp_factor", Amplitude.Value / 100.0f);
            array.Add("pointTime", TimePerPoint.Value);
            array.Add("frameReps", AnimationSegmentRepetitions.Value);
            return array;
        }
    }
}
