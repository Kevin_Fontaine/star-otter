using System;
using Emerge.Core.Shared.Utils;


namespace Emerge.Core
{
    using SimpleJSON;
    using static EffectConstants;

    [Serializable]
    public class nPolygonProperties : AbstractEffectProperties
    {
        //{'TimePerPoint':7.0, 'amp':100, 'NumberOfSides':3, 'curvature'=2.0, 'PointsPerSide':1}
        public ModifiedInt NumberOfSides;
        public ModifiedFloat CenterToCornerDistance;
        public ModifiedInt PointsPerSide;
        public ModifiedFloat TimePerPoint;
        public ModifiedInt Amplitude;

        public nPolygonProperties()
        {
            tactileEffectType = TactileEffectType.nPolygon;
            NumberOfSides = new ModifiedInt(_min: 3, _max: 8, _default: 3, "Number of Side For Polygon", SetDirty); //5.0f;
            CenterToCornerDistance = new ModifiedFloat(_min: 0, _max: 10f, _default: 2, "Radius of Polygon", SetDirty); //5.0f;
            PointsPerSide = new ModifiedInt(_min: 0, _max: 30, _default: 1, "Additional Points Per Side", SetDirty); //5.0f;
            TimePerPoint = EffectConstants.DefaultTimePerPoint(SetDirty);
            Amplitude = new ModifiedInt(_min: AmplitudeMin, _max: AmplitudeMax, _default: AmplitudeMax, EffectConstants.NoneString, SetDirty);
        }

        public override JSONObject GetDataAsJSONObject()
        {
            JSONObject array = new JSONObject();
            array.Add("n", NumberOfSides.Value);
            array.Add("R", CenterToCornerDistance.Value);
            array.Add("sidePoints", PointsPerSide.Value);
            array.Add("pointTime", TimePerPoint.Value);
            array.Add("amp_factor", Amplitude.Value / 100.0f);
            return array;
        }
    }
}
