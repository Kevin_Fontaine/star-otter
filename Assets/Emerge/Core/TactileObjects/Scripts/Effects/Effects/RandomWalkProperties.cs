﻿using System;
using Emerge.Core.Shared.Utils;

namespace Emerge.Core
{
    using SimpleJSON;
    using static EffectConstants;

    [Serializable]
    public class RandomWalkProperties : AbstractEffectProperties
    {
        //'length' : 4.0, 'width' : 4.0, 'xspacing' : 1.0, 'yspacing' : 1.0, 'Rmax' : 4.0, 'nMoves' : 10, 'rMove' : 1.0, 'amp' : 100, 'poEffectParameter_IntTime' : 5.0, 'frameReps' : 5
        public ModifiedInt NumberOfSteps;
        public ModifiedFloat XRange;
        public ModifiedFloat ZRange;
        public ModifiedFloat PointSpacing;
        public ModifiedFloat AngularStepRange;
        public ModifiedInt NumberOfPaths;
        public ModifiedFloat TimePerPoint;
        public ModifiedInt AnimationSegmentRepetitions;
        public ModifiedInt Amplitude;
        public RandomWalkProperties()
        {
            tactileEffectType = TactileEffectType.random_walk;
            NumberOfSteps = new ModifiedInt(_min: 0, _max: 100, _default: 50, "Number of Steps Each Path Takes", SetDirty); //100;
            XRange = new ModifiedFloat(_min: 1f, _max: 20f, _default: 5f, "Width Range of Walker", SetDirty); //14.0f;
            ZRange = new ModifiedFloat(_min: 1f, _max: 20f, _default: 5f, "Length Range of Walker", SetDirty); //5.0f;
            PointSpacing = new ModifiedFloat(_min: .1f, _max: 1f, _default: 0.5f, "Spacing Between Steps [cm]", SetDirty); //1.5f;
            AngularStepRange = new ModifiedFloat(_min: 45f, _max: 90f, _default: 60f, "Range in Which Walker Can Turn [Degrees]", SetDirty); //2.0f;
            NumberOfPaths = new ModifiedInt(_min: 1, _max: 2, _default: 2, "Number of Walkers", SetDirty); //360f;            
            TimePerPoint = EffectConstants.DefaultTimePerPoint(SetDirty);
            AnimationSegmentRepetitions = new ModifiedInt(_min: 1, _max: 4, _default: 4, EffectConstants.FrameRepsString, SetDirty); //5;
            Amplitude = new ModifiedInt(_min: AmplitudeMin, _max: AmplitudeMax, _default: AmplitudeMax, EffectConstants.NoneString, SetDirty);
        }

        public override JSONObject GetDataAsJSONObject()
        {
            JSONObject array = new JSONObject();
            array.Add("nSteps", NumberOfSteps.Value);
            array.Add("xRange", XRange.Value);
            array.Add("yRange", ZRange.Value);
            array.Add("spacing", PointSpacing.Value);
            array.Add("angRange", AngularStepRange.Value);
            array.Add("NumberOfPaths", NumberOfPaths.Value);
            array.Add("frameReps", AnimationSegmentRepetitions.Value);
            array.Add("pointTime", TimePerPoint.Value);
            array.Add("amp_factor", Amplitude.Value / 100.0f);
            return array;
        }
    }
}
