﻿using System;
using Emerge.Core.Shared.Utils;

namespace Emerge.Core
{
    using SimpleJSON;
    using static EffectConstants;

    [Serializable]
    public class CrawlingBugProperties : AbstractEffectProperties
    { 
        public ModifiedFloat Speed;
        public ModifiedFloat TimePerPoint;
        public ModifiedInt Amplitude;

        public CrawlingBugProperties()
        {
            tactileEffectType = TactileEffectType.crawling_bug;
            Speed = new ModifiedFloat(_min: 0, _max: 10f, _default: 5f, "The Speed of the Bugs Legs", SetDirty); //5.0f;
            Amplitude = new ModifiedInt(_min: AmplitudeMin, _max: AmplitudeMax, _default: AmplitudeMax, EffectConstants.NoneString, SetDirty);
            TimePerPoint = EffectConstants.DefaultTimePerPoint(SetDirty); 
        }

        public override JSONObject GetDataAsJSONObject()
        {
            JSONObject array = new JSONObject();
            array.Add("speed", Speed.Value);
            array.Add("pointTime", TimePerPoint.Value);
            array.Add("amp_factor", Amplitude.Value / 100.0f);
            return array;
        } 
    }
}
