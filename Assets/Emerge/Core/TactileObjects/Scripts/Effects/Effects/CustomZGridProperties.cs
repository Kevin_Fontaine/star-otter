﻿using System;
using Emerge.Core.Shared.Utils;

namespace Emerge.Core
{
    using SimpleJSON;
    using static EffectConstants;

    [Serializable]
    public class CustomZGridProperties : AbstractEffectProperties
    {
        //'XDimension' : 8.0, 'ZDimension' :  6.0, 'XSpacing' : 2.5, 'ZSpacing' : 2.5, 'YStep' : 0.8, 'frameReps' : 5, 'poEffectParameter_IntTime' : 5.0, 'amp' : 100
        public ModifiedFloat XDimension;
        public ModifiedFloat ZDimension;
        public ModifiedFloat XSpacing;
        public ModifiedFloat ZSpacing;
        public ModifiedFloat YStep;
        public ModifiedInt AnimationSegmentRepetitions;
        public ModifiedFloat TimePerPoint;
        public ModifiedInt Amplitude;

        public CustomZGridProperties()
        {
            tactileEffectType = TactileEffectType.custom_z_grid;
            XDimension = new ModifiedFloat(_min: 0.1f, _max: 15f, _default: 8f, "Dimension of Points in X [cm]", SetDirty); // 8.0f;
            ZDimension = new ModifiedFloat(_min: 0.1f, _max: 15f, _default: 6f, "Dimension of Points in Z [cm]", SetDirty); //6.0f;
            XSpacing = new ModifiedFloat(_min: 2f, _max: 5f, _default: 2.5f, "Spacing of Points in X [cm]", SetDirty); //2.5f;
            ZSpacing = new ModifiedFloat(_min: 2f, _max: 5f, _default: 2.5f, "Spacing of Points in Z [cm]", SetDirty); //2.5f;
            YStep = new ModifiedFloat(_min: 0.1f, _max: 5f, _default: 0.8f, "Step in Y Direction Between Two Grids [cm]", SetDirty); //0.8f;
            Amplitude = new ModifiedInt(_min: AmplitudeMin, _max: AmplitudeMax, _default: AmplitudeMax, EffectConstants.NoneString, SetDirty);
            TimePerPoint = EffectConstants.DefaultTimePerPoint(SetDirty);
            AnimationSegmentRepetitions = new ModifiedInt(_min: 1, _max: 10, _default: 3, EffectConstants.FrameRepsString, SetDirty); //5;
        }

        public override JSONObject GetDataAsJSONObject()
        {
            JSONObject array = new JSONObject();
            array.Add("xdim", XDimension.Value);
            array.Add("ydim", ZDimension.Value);
            array.Add("xspac", XSpacing.Value);
            array.Add("yspac", ZSpacing.Value);
            array.Add("zStep", YStep.Value);
            array.Add("frameReps", AnimationSegmentRepetitions.Value);
            array.Add("pointTime", TimePerPoint.Value);
            array.Add("amp_factor", Amplitude.Value / 100.0f);
            return array;
        }
    }
}