﻿using System;
using Emerge.Core.Shared.Utils;

namespace Emerge.Core
{
    using SimpleJSON;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using static EffectConstants;

    [Serializable]
    public class BasicPlaneWaveProperties : AbstractEffectProperties
    {
        public ModifiedFloat Wavespeed;
        public ModifiedFloat Intensity;
        public ModifiedInt Amplitude;

        public BasicPlaneWaveProperties()
        {
            tactileEffectType = TactileEffectType.basic_plane_wave;
            Wavespeed = new ModifiedFloat(_min: 0, _max: 10f, _default: 5f, "The Speed of The Travelling Wave [Arbitrary Units]", SetDirty); //5.0f;
            Intensity = new ModifiedFloat(_min: 0, _max: 10f, _default: 5f, "The Tactile Intensity", SetDirty); //5.0f;
            Amplitude = new ModifiedInt(_min: AmplitudeMin, _max: AmplitudeMax, _default: AmplitudeMax, EffectConstants.NoneString, SetDirty);
        }

        public override JSONObject GetDataAsJSONObject()
        {
            JSONObject array = new JSONObject();
            array.Add("wavespeed", Wavespeed.Value);
            array.Add("intensity", Intensity.Value); 
            array.Add("amp_factor", Amplitude.Value / 100.0f);

            return array;
        } 
    }
}
