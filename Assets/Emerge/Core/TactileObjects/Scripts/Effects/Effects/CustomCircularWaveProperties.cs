using System;
using System.Collections;
using System.Collections.Generic;
using Emerge.Core.Shared.Utils;
using UnityEngine;
using SimpleJSON;

namespace Emerge.Core
{
    using static EffectConstants;
    [Serializable]
    public class CustomCircularWaveProperties : AbstractEffectProperties
    {
        //'NumberOfRings' : 5, 'MaximumRadius' : 5.0, 'MinimumSpacing' : 1.0, 'SliceAngle' : 360, 'Curvature' : np.Inf, 'Direction' : False, 'frameReps' : 4, 'TimePerPoint' : 7.0, 'amp' : 100
        public ModifiedInt NumberOfRings;
        public ModifiedFloat MaximumRadius;
        public ModifiedFloat MinimumSpacing;
        public ModifiedInt SliceAngle;
        public ModifiedFloat Curvature;
        public ModifiedBool Direction;
        public ModifiedInt AnimationSegmentRepetitions;
        public ModifiedFloat TimePerPoint;
        public ModifiedInt Amplitude;
        public CustomCircularWaveProperties()
        {
            tactileEffectType = TactileEffectType.custom_circular_wave;
            NumberOfRings = new ModifiedInt(_min: 1, _max: 8, _default: 5,
            "The Number of Rings Included in the Wave", SetDirty); //5;
            MaximumRadius = new ModifiedFloat(_min: 1.0f, _max: 10f, _default: 5f,
                "Maximum Radius of the Outer Ring [cm]", SetDirty); //5.0f;
            MinimumSpacing = new ModifiedFloat(_min: 1f, _max: 10f, _default: 2f,
                "Minimum Spacing Between Points For Single Circle [cm]", SetDirty); //1.0f;
            SliceAngle = new ModifiedInt(_min: 45, _max: 360, _default: 360,
                "Angle Covered By The Effect [Degrees]", SetDirty); //360;
            Curvature = new ModifiedFloat(_min: 0.1f, _max: 360f, _default: 360f,
                "Curvature Creates a Dome Shape", SetDirty); //100.0f;
            Direction = new ModifiedBool(_default: false, "Direction of Wave [True = Inside to Outside]", SetDirty); //false;
            AnimationSegmentRepetitions = new ModifiedInt(_min: 1, _max: 3, _default: 1, EffectConstants.FrameRepsString, SetDirty); //4;
            Amplitude = new ModifiedInt(_min: AmplitudeMin, _max: AmplitudeMax, _default: AmplitudeMax, EffectConstants.NoneString, SetDirty);
            TimePerPoint = EffectConstants.DefaultTimePerPoint(SetDirty);
        }
        public override JSONObject GetDataAsJSONObject()
        {
            JSONObject array = new JSONObject();
            array.Add("nRings", NumberOfRings.Value);
            array.Add("maxR", MaximumRadius.Value);
            array.Add("minSpacing", MinimumSpacing.Value);
            array.Add("angAcceptance", SliceAngle.Value);
            array.Add("curvature", Curvature.Value);
            array.Add("OutIn", Direction.Value);
            array.Add("frameReps", AnimationSegmentRepetitions.Value);
            array.Add("pointTime", TimePerPoint.Value);
            array.Add("amp_factor", Amplitude.Value / 100.0f);
            return array;
        }
    }
}
