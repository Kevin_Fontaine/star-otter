﻿using System;
using Emerge.Core.Shared.Utils;

namespace Emerge.Core
{
    using SimpleJSON;
    using static EffectConstants;

    [Serializable]
    public class CircleSetProperties : AbstractEffectProperties
    {
        //{'TimePerPoint':7.0, 'amp':100, 'curvature':3.0, 'SliceAngle':360, 'dS':0}
        public ModifiedInt NumberOfPoints;
        public ModifiedFloat Radius;
        public ModifiedFloat SliceAngle;
        public ModifiedFloat TimePerPoint;
        public ModifiedInt Amplitude;

        public CircleSetProperties()
        {
            tactileEffectType = TactileEffectType.circle_set;
            NumberOfPoints = new ModifiedInt(_min: 6, _max: 25, _default: 9, "Number of Points in the Circle", SetDirty); //5.0f;
            Radius = new ModifiedFloat(_min: 0.1f, _max: 10f, _default: 2f, "Radius of Circle [cm]", SetDirty); //100.0f;
            SliceAngle = new ModifiedFloat(_min: 60f, _max: 360f, _default: 360f, "Angle of Slice of Circle [Degrees]", SetDirty); //5.0f;
            Amplitude = new ModifiedInt(_min: AmplitudeMin, _max: AmplitudeMax, _default: AmplitudeMax, EffectConstants.NoneString, SetDirty);
            TimePerPoint = EffectConstants.DefaultTimePerPoint(SetDirty); 
        }

        public override JSONObject GetDataAsJSONObject()
        {
            JSONObject array = new JSONObject();
            array.Add("nPoints", NumberOfPoints.Value);
            array.Add("R", Radius.Value); 
            array.Add("angAcceptance", SliceAngle.Value);
            array.Add("pointTime", TimePerPoint.Value);
            array.Add("amp_factor", Amplitude.Value / 100.0f);
            return array;
        }
    }
}
