﻿using System;
using Emerge.Core.Shared.Utils;

namespace Emerge.Core
{
    using SimpleJSON;
    using static EffectConstants;

    [Serializable]
    public class ExpandingSphereProperties : AbstractEffectProperties
    {
        //'Length' : 5.0, 'Width' : 5.0, 'XSpacing' : 2.5, 'ZSpacing' : 2.5, 'MinimumRadius' : 5.0, 'MaximumRadius' : 10.0, 'NumberOfRings' : 5, 'amp' : 100, 'poEffectParameter_IntTime' : 5.0, 'frameReps' : 5
        public ModifiedFloat Length;
        public ModifiedFloat Width;
        public ModifiedFloat XSpacing;
        public ModifiedFloat ZSpacing;
        public ModifiedFloat MinimumRadius;
        public ModifiedFloat MaximumRadius;
        public ModifiedInt NumberOfSegments;
        public ModifiedInt Amplitude;
        public ModifiedFloat TimePerPoint;
        public ModifiedInt AnimationSegmentRepetitions;

        public ExpandingSphereProperties()
        {
            tactileEffectType = TactileEffectType.expanding_sphere;
            Length = new ModifiedFloat(_min: 0.1f, _max: 10f, _default: 5f, "Length of Sphere Section [cm]", SetDirty); //5.0f;
            Width = new ModifiedFloat(_min: 0.1f, _max: 10f, _default: 5f, "Width of Sphere Section [cm]", SetDirty); //5.0f;
            XSpacing = new ModifiedFloat(_min: 2, _max: 5f, _default: 2.5f, "Spacing in Width Direction [cm]", SetDirty); //2.5f;
            ZSpacing = new ModifiedFloat(_min: 2f, _max: 5f, _default: 2.5f, "Spacing in Length Direction [cm]", SetDirty); //2.5f;
            MinimumRadius = new ModifiedFloat(_min: 0.1f, _max: 5f, _default: 5f, "Starting Radius of Sphere Section [cm]", SetDirty); //5.0f;
            MaximumRadius = new ModifiedFloat(_min: 5.1f, _max: 25f, _default: 10f, "Ending Radius of Sphere Section [cm]", SetDirty); //10.0f;
            NumberOfSegments = new ModifiedInt(_min: 2, _max: 10, _default: 5, "The Number of Animation Segments Between the Minimum and Maximum Radius", SetDirty); //5;
            AnimationSegmentRepetitions = new ModifiedInt(_min: 1, _max: 7, _default: 5, EffectConstants.FrameRepsString, SetDirty); //5;
            Amplitude = new ModifiedInt(_min: AmplitudeMin, _max: AmplitudeMax, _default: AmplitudeMax, EffectConstants.NoneString, SetDirty);
            TimePerPoint = EffectConstants.DefaultTimePerPoint(SetDirty);
        }

        public override JSONObject GetDataAsJSONObject()
        {
            JSONObject array = new JSONObject();
            array.Add("length", Length.Value);
            array.Add("width", Width.Value);
            array.Add("xspacing", XSpacing.Value);
            array.Add("yspacing", ZSpacing.Value);
            array.Add("Rmin", MinimumRadius.Value);
            array.Add("Rmax", MaximumRadius.Value);
            array.Add("NumberOfRings", NumberOfSegments.Value);
            array.Add("amp_factor", Amplitude.Value / 100.0f);
            array.Add("pointTime", TimePerPoint.Value);
            array.Add("frameReps", AnimationSegmentRepetitions.Value);
            return array;
        }
    }
}
