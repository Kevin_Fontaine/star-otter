﻿using System;
using Emerge.Core.Shared.Utils;

namespace Emerge.Core
{
    using SimpleJSON;
    using static EffectConstants;

    [Serializable]
    public class FurProperties : AbstractEffectProperties
    {
        public ModifiedFloat Length;
        public ModifiedFloat Width;
        public ModifiedFloat XSpacing;
        public ModifiedFloat ZSpacing;
        public ModifiedFloat Curvature;
        public ModifiedFloat TimePerPoint;
        public ModifiedInt AnimationSegmentRepetitions;
        public ModifiedInt Amplitude;

        public FurProperties()
        {
            tactileEffectType = TactileEffectType.fur;
            Length = new ModifiedFloat(_min: 0.1f, _max: 20f, _default: 14f, "Length of the Fur [cm]", SetDirty); //14.0f;
            Width = new ModifiedFloat(_min: 0.1f, _max: 20f, _default: 5f, "Width of the Fur [cm]", SetDirty); //5.0f;
            XSpacing = new ModifiedFloat(_min: 1f, _max: 5f, _default: 1.5f, "Spacing in Length Direction [cm]", SetDirty); //1.5f;
            ZSpacing = new ModifiedFloat(_min: 1f, _max: 5f, _default: 2.0f, "Spacing in Width Direction [cm]", SetDirty); //2.0f;
            Curvature = new ModifiedFloat(_min: 0.1f, _max: 360f, _default: 360f, "Curvature of Fur", SetDirty); //360f;
            Amplitude = new ModifiedInt(_min: AmplitudeMin, _max: AmplitudeMax, _default: AmplitudeMax, EffectConstants.NoneString, SetDirty);
            TimePerPoint = EffectConstants.DefaultTimePerPoint(SetDirty);
            AnimationSegmentRepetitions = EffectConstants.DefaultFrameReps(SetDirty);
        }

        public override JSONObject GetDataAsJSONObject()
        {
            JSONObject array = new JSONObject();
            array.Add("length", Length.Value);
            array.Add("width", Width.Value);
            array.Add("zspacing", ZSpacing.Value);
            array.Add("xspacing", XSpacing.Value);
            array.Add("curvature", Curvature.Value);
            array.Add("frameReps", AnimationSegmentRepetitions.Value);
            array.Add("point", TimePerPoint.Value);
            array.Add("amp_factor", Amplitude.Value / 100.0f);
            return array;
        }
    }
}