using System;
using Emerge.Core.Shared.Utils;

namespace Emerge.Core
{
    using SimpleJSON;
    using static EffectConstants;

    [Serializable]
    public class SimpleCircularWaveProperties : AbstractEffectProperties
    {
        public ModifiedFloat Wavespeed;
        public ModifiedFloat SliceAngle;
        public ModifiedFloat Intensity;
        public ModifiedBool Direction;
        public ModifiedInt Amplitude;
        public SimpleCircularWaveProperties()
        {
            tactileEffectType = TactileEffectType.simple_circular_wave;
            Wavespeed = new ModifiedFloat(_min: 0f, _max: 10f, _default: 5f, "Speed of Wave", SetDirty); //5.0f;
            SliceAngle = new ModifiedFloat(_min: 45f, _max: 360f, _default: 360f, "Angle of Pie Slice", SetDirty); //360.0f;
            Intensity = new ModifiedFloat(_min: 0f, _max: 10f, _default: 5f, "Intensity of Wave", SetDirty); //5.0f;
            Direction = new ModifiedBool(_default: false, "Direction of Wave", SetDirty); //false;            
            Amplitude = new ModifiedInt(_min: AmplitudeMin, _max: AmplitudeMax, _default: AmplitudeMax, EffectConstants.NoneString, SetDirty);
        }

        public override JSONObject GetDataAsJSONObject()
        {
            JSONObject array = new JSONObject();
            array.Add("wavespeed", Wavespeed.Value);
            array.Add("angle", SliceAngle.Value);
            array.Add("intensity", Intensity.Value);
            array.Add("OutIn", Direction.Value);
            array.Add("amp_factor", Amplitude.Value / 100.0f);
            return array;
        }
    }
}
