﻿using System;

namespace Emerge.Core
{
    [Serializable]
    public abstract class AbstractModifiedPrimitive
    { 
        protected Action OnVariableChange;

        protected void RegisterAction(Action t)
        {
            if (t != null) { OnVariableChange += t; }
        }

        public void ValueChanged()
        {
            OnVariableChange.Dispatch();
        }
    } 
}

