﻿/******************************************************************************
 * Copyright (C) Emerge Now, Inc. 2016-2019.                                  *
 * Emerge Now Inc proprietary and confidential.                               *
 *                                                                            *
 * Use subject to the terms of the Emerge Exploration Program Agreement       *
 * and/or terms of limited, non-exclusive, non-sublicensable license to       *
 * Emerge SDK between Emerge and you, your company or any other organization. *
 ******************************************************************************/
// CleanPass

using UnityEditor;

namespace Emerge.Core
{
    [CustomEditor(typeof(TactileEffect), true)]
    [CanEditMultipleObjects]
    public class EffectEmergeObjectEditor : AbstractTactileBehaviorEditor
    { 
        protected TactileEffect TactileEffect;
        protected SerializedObject SerializedEffectEmergeObject; 

        void OnEnable()
        {
            TactileEffect = (TactileEffect)target;
            SerializedEffectEmergeObject = new SerializedObject(TactileEffect); 
        }  

        public override void OnInspectorGUI()
        {  
            base.OnInspectorGUI();

            SerializedEffectEmergeObject.Update(); 

            DrawPropertiesExcluding(SerializedEffectEmergeObject, new string[] { "m_Script" });
            SerializedEffectEmergeObject.ApplyModifiedProperties();
        } 
    }
}