﻿using UnityEditor;
using UnityEditorInternal;
using UnityEngine;
 
using Emerge.Core; 

[CustomEditor(typeof(TactilePointCollection))]
public class TactilePointCollectionEditor : Editor
{
    private SerializedProperty tactilePoints;
    private ReorderableList reorderableList;
    private TactilePointCollection targetTactilePointCollection;

    private void OnEnable()
    {
        //Find the list in our ScriptableObject script.
        tactilePoints = serializedObject.FindProperty(TactilePointCollection.TactilePointsString);

        //Create an instance of our reorderable list.
        reorderableList = new ReorderableList(serializedObject: serializedObject, elements: tactilePoints, draggable: true, displayHeader: true,
            displayAddButton: true, displayRemoveButton: true);

        //Set up the method callback to draw our list header
        reorderableList.drawHeaderCallback = DrawHeaderCallback;

        //Set up the method callback to draw each element in our reorderable list
        reorderableList.drawElementCallback = DrawElementCallback;

        //Set the height of each element.
        reorderableList.elementHeightCallback += ElementHeightCallback;

        //Set up the method callback to define what should happen when we add a new object to our list.
        reorderableList.onAddCallback += OnAddCallback;
        //Set up the method callback to define what should happen when we add the object is removed from list.
        reorderableList.onRemoveCallback += OnRemoveCallback;
        //Set up the method callback to define what should happen when an object in list is changed or the order is shuffled.
        // m_ReorderableList.onChangedCallback += OnChangedCallback;
    }

    private void OnChangedCallback(ReorderableList list)
    { 
        throw new System.NotImplementedException();
    }

    /// <summary>
    /// Draws the header for the reorderable list
    /// </summary>
    /// <param name="rect"></param>
    private void DrawHeaderCallback(Rect rect)
    {
        EditorGUI.LabelField(rect, "Tactile Points", EditorStyles.boldLabel);
    }

    /// <summary>
    /// This methods decides how to draw each element in the list
    /// </summary>
    /// <param name="rect"></param>
    /// <param name="index"></param>
    /// <param name="isactive"></param>
    /// <param name="isfocused"></param>
    private void DrawElementCallback(Rect rect, int index, bool isactive, bool isfocused)
    {
        // Right justify 
        // https://forum.unity.com/threads/inspector-create-field-on-the-right.386991/
         
        // Get the element we want to draw from the list.
        SerializedProperty element = reorderableList.serializedProperty.GetArrayElementAtIndex(index);
        rect.y += 2;
         
        // Debug.Log($"{rect.x}, {EditorGUIUtility.currentViewWidth}");
        EditorGUI.PropertyField(
            position: new Rect(rect.x, rect.y, width: EditorGUIUtility.currentViewWidth - rect.x - 15.0f, height: EditorGUIUtility.singleLineHeight),
            property: element, 
            label: GUIContent.none, 
            includeChildren: true); 
    }

    /// <summary>
    /// Calculates the height of a single element in the list.
    /// This is extremely useful when displaying list-items with nested data.
    /// </summary>
    /// <param name="index"></param>
    /// <returns></returns>
    private float ElementHeightCallback(int index)
    {
        //Gets the height of the element. This also accounts for properties that can be expanded, like structs.
        float propertyHeight =
            EditorGUI.GetPropertyHeight(reorderableList.serializedProperty.GetArrayElementAtIndex(index), true);

        float spacing = EditorGUIUtility.singleLineHeight / 2;

        return propertyHeight + spacing;
    }

    /// <summary>
    /// Defines how a new list element should be created and added to our list.
    /// </summary>
    /// <param name="list"></param>
    private void OnAddCallback(ReorderableList list)
    {
        int index = list.serializedProperty.arraySize;
        list.serializedProperty.arraySize++;
        list.index = index; 
        // Make the fresh element null (otherwise the last item in the list is duplicated)
        SerializedProperty element = list.serializedProperty.GetArrayElementAtIndex(index);
        if (element.objectReferenceValue != null) { element.objectReferenceValue = null; }
        
        // Debug.Log("added " + list.index);
    }

    /// <summary>
    /// Defines how an element should be removed from a list.
    /// </summary>
    /// <param name="list"></param>
    private void OnRemoveCallback(ReorderableList list)
    {
        // Debug.Log("removed " + list.index);
        int index = list.index;
        SerializedProperty element = list.serializedProperty.GetArrayElementAtIndex(index);
        if (element != null)
        {
            TactilePoint tp = (element.serializedObject.targetObject as TactilePointCollection).TactilePoints[index];
            if (tp != null)
            {
                tp.IsMemberOfTactilePointCollection = false;
            }
            if (element.objectReferenceValue != null)
            {
                Debug.Log(element.objectReferenceValue.name);
            }
        }

        // Avoid doubling delete calls with null check instead
        // https://answers.unity.com/questions/555724/serializedpropertydeletearrayelementatindex-leaves.html
        var elementProperty = list.serializedProperty.GetArrayElementAtIndex(index);
        if (elementProperty.objectReferenceValue != null) { elementProperty.objectReferenceValue = null; }

        list.serializedProperty.DeleteArrayElementAtIndex(index);   
    }

    /// <summary>
    /// Draw the Inspector Window
    /// </summary>
    public override void OnInspectorGUI()
    { 
        DrawPropertiesExcluding(serializedObject, new string[] { TactilePointCollection.StrengthString });

        if (targetTactilePointCollection == null)
            targetTactilePointCollection = (TactilePointCollection) target;
 
        serializedObject.Update();

        reorderableList.DoLayoutList();

        DrawPropertiesExcluding(serializedObject, new string[] { "m_Script" });

        serializedObject.ApplyModifiedProperties();

        targetTactilePointCollection.UpdateTactilePointMembers();
    }
}
