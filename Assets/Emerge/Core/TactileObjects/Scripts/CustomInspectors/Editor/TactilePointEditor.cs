﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Emerge.Core
{
    [CustomEditor(typeof(TactilePoint), true)]
    [CanEditMultipleObjects]
    public class TactilePointEditor : AbstractTactileBehaviorEditor
    {
        private TactilePoint targetTactilePoint;
        private SerializedProperty strength;
        private SerializedProperty isMemberOfTactilePointCollectionString;
          
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if (targetTactilePoint == null)
                targetTactilePoint = (TactilePoint) target;

            //EditorGUILayout.ObjectField("Script", MonoScript.FromMonoBehaviour((TactilePoint)target),
            //    typeof(TactilePoint), false); 
            //GUI.enabled = true;

            //serializedObject.Update();
                 
            if (targetTactilePoint.IsMemberOfTactilePointCollection)
            { 
                GUI.enabled = false;
            }
            else
            { 
                GUI.enabled = true;
            }

            // debug manual override
            // targetTactilePoint.IsMemberOfTactilePointCollection = false;

            DrawPropertiesExcluding(serializedObject, new string[] {"m_Script"});

            // ToDo: Call ApplyModifiedProperties twice?
            serializedObject.ApplyModifiedProperties();
            serializedObject.Update();
            serializedObject.ApplyModifiedProperties();
        }
    }
}