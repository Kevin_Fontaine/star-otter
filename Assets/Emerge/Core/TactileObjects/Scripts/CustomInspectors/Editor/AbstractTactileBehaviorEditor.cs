﻿/******************************************************************************
 * Copyright (C) Emerge Now, Inc. 2016-2019.                                  *
 * Emerge Now Inc proprietary and confidential.                               *
 *                                                                            *
 * Use subject to the terms of the Emerge Exploration Program Agreement       *
 * and/or terms of limited, non-exclusive, non-sublicensable license to       *
 * Emerge SDK between Emerge and you, your company or any other organization. *
 ******************************************************************************/
// CleanPass

using System;
using JetBrains.Annotations;
using UnityEditor;
using UnityEngine;

namespace Emerge.Core
{
    [CustomEditor(typeof(AbstractTactileBehavior), true)]
    [CanEditMultipleObjects]
    public class AbstractTactileBehaviorEditor : Editor
    {
        private AbstractTactileBehavior tactileBehavior;
        private SerializedProperty id;
          
        private static readonly string TactileBehaviorIdString = "Tactile Id";
        private static readonly string HelpBoxString =
            "Optional string to identify this Tactile Behavior. Query via TactileManager.";

        public override void OnInspectorGUI()
        {  
            if (tactileBehavior == null)
                tactileBehavior = (AbstractTactileBehavior)target;

            GUI.enabled = false;
            EditorGUILayout.ObjectField("Script", MonoScript.FromMonoBehaviour((AbstractTactileBehavior)target),
                typeof(AbstractTactileBehavior), false);
            GUI.enabled = true;

            serializedObject.Update();

            EditorGUILayout.HelpBox(HelpBoxString, MessageType.None);

            if (id == null) { id = serializedObject.FindProperty(AbstractTactileBehavior.IdentifierString); }
            // If we found an identifier, display the TactileBehaviorIdString.
            if (id != null) { EditorGUILayout.PropertyField(id, new GUIContent(TactileBehaviorIdString)); }

            DrawHorizontalLineInEditor(new Color(0.4f, 0.4f, 0.4f), GetHorizontalLineGUIStyle());

            serializedObject.ApplyModifiedProperties();
        }

        protected void DrawHorizontalLineInEditor([NotNull] GUIStyle guiStyle)
        {
            DrawHorizontalLineInEditor(Color.grey, guiStyle);
        }

        protected void DrawHorizontalLineInEditor(Color color, [NotNull] GUIStyle guiStyle)
        {
            if (guiStyle is null) throw new ArgumentNullException(nameof(guiStyle));

            var c = GUI.color;
            GUI.color = color;
            GUILayout.Box(GUIContent.none, guiStyle);
            GUI.color = c;
        }

        protected GUIStyle GetHorizontalLineGUIStyle()
        {
            var horizontalLineGuiStyle = new GUIStyle();
            horizontalLineGuiStyle.normal.background = EditorGUIUtility.whiteTexture;
            horizontalLineGuiStyle.margin = new RectOffset(10, 10, 4, 4);
            horizontalLineGuiStyle.fixedHeight = 1;

            return horizontalLineGuiStyle;
        }
    } 
}
