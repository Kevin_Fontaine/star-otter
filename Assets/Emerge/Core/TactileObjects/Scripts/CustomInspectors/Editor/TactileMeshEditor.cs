﻿using UnityEditor;
using UnityEngine;

namespace Emerge.Core
{
    [CustomEditor(typeof(TactileMesh), true)]
    [CanEditMultipleObjects]
    public class TactileMeshEditor : AbstractTactileBehaviorEditor
    {
        private TactileMesh tactileMeshTarget; 

        private void OnEnable()
        {
            tactileMeshTarget = (TactileMesh)target;
        }

        public override void OnInspectorGUI()
        { 
            base.OnInspectorGUI();

            if (tactileMeshTarget == null) { tactileMeshTarget = (TactileMesh) target; }

            if (tactileMeshTarget?.gameObject.GetComponent<BaseHandCollision>() == null)
            {
                // Instead of using RequireComponent, we use this warning message
                // This way the addition of a TactileMesh component won't cause a BaseHandCollision component
                // to be added automatically.

                EditorGUILayout.HelpBox("An adjacent ColliderToHandCollision or CustomAreaToHandCollision component is required.", MessageType.Error);
            } 

            GUI.enabled = true;
             
            //DrawPropertiesExcluding(serializedObject, new string[] { "m_Script" });
            EditorGUILayout.PropertyField(serializedObject.FindProperty(TactileMesh.StrengthString));

            // ToDo: Call ApplyModifiedProperties twice?
            serializedObject.ApplyModifiedProperties();
            serializedObject.Update();
            serializedObject.ApplyModifiedProperties();
        }
    }
}
