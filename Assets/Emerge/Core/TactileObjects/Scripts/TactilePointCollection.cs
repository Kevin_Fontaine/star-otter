﻿using System;
using Emerge.Core;
using Emerge.Core.Utils;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TactilePointCollection : MonoBehaviour
{
 
    public static string TactilePointsString = "tactilePoints"; 
    [SerializeField]
    [HideInInspector]
    private List<TactilePoint> tactilePoints; 
    public List<TactilePoint> TactilePoints { get { return tactilePoints; } set { tactilePoints = value; } }

    public static string StrengthString = "strength";
    [SerializeField, Range(0, 100.0f)]
    private float strength;

    public float Strength
    {
        get => strength;
        set => strength = value;
    }


    void Update()
    {
        UpdateTactilePointMembers();
    }

    public void UpdateTactilePointMembers()
    {
        if (tactilePoints == null) { return; }

        foreach (TactilePoint tp in tactilePoints)
        {
            if (tp != null)
            {
                tp.Strength = strength;
                tp.TactilePointCollection = this;
                tp.IsMemberOfTactilePointCollection = true;
            }
        }
    }
}  