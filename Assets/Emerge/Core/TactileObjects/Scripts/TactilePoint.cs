﻿/******************************************************************************
 * Copyright (C) Emerge Now, Inc. 2016-2019.                                  *
 * Emerge Now Inc proprietary and confidential.                               *
 *                                                                            *
 * Use subject to the terms of the Emerge Exploration Program Agreement       *
 * and/or terms of limited, non-exclusive, non-sublicensable license to       *
 * Emerge SDK between Emerge and you, your company or any other organization. *
 ******************************************************************************/

using System;
using UnityEngine;
using Emerge.Communication;

namespace Emerge.Core
{
    /// <summary>
    /// Emerge Object for passing custom points directly to beamformer.
    /// When used as an EmergeObject, your custom points will be transmitted
    /// when hands touch the Trigger Collider
    /// </summary>
    [Serializable]
    public class TactilePoint : AbstractTactileBehavior
    {
        private TactilePointDto tactilePointDto;

        protected bool active;

        public static string StrengthString = "strength";
        [Range(0, 100.0f)] [SerializeField] private float strength = 100.0f;

        public float Strength
        {
            get => strength;
            set => strength = value;
        }

        public static string IsMemberOfTactilePointCollectionString = "isMemberOfTactilePointCollection";
        [SerializeField] [HideInInspector] private bool isMemberOfTactilePointCollection;

        public bool IsMemberOfTactilePointCollection
        {
            get => isMemberOfTactilePointCollection;
            set => isMemberOfTactilePointCollection = value;
        }

        public static string TactilePointCollectionString = "tactilePointCollection";
        private TactilePointCollection tactilePointCollection;

        public TactilePointCollection TactilePointCollection
        {
            get => tactilePointCollection;
            set => tactilePointCollection = value;
        }

        public override void Awake()
        {
            base.Awake();

            tactilePointDto = new TactilePointDto(TactileMethod.point);

            // ToDo: confirm removal and delete
            // tactilePointDto.TactilePointMode = tactilePointMode;

            active = false;

            CollisionPhaseChanged += CollisionPhaseChangedEventHandler;
        }

        private void CollisionPhaseChangedEventHandler(AbstractTactileBehavior emergeObject)
        {
            active = emergeObject.CollisionIsActive;
        }

        public virtual void TouchStatusChanged(bool isTouched)
        {
            if (isTouched)
                StartAnimation();
            else
                StopAnimation();
        }

        protected virtual void StartAnimation()
        {
            active = true;
        }

        protected virtual void StopAnimation()
        {
            active = false;
        }

        public virtual void UpdatePoint()
        {
            if (!active || tactilePointDto == null) return;

            if (gameObject.activeSelf)
            {
                tactilePointDto.Beam = true;
                tactilePointDto.Position = transform.position;
                tactilePointDto.Amplitude = Strength;
            }
            else
            {
                tactilePointDto.Beam = false;
            }
        }

        public override void Update()
        {
            base.Update();

            UpdatePoint();
        }

        public override IDto[] GetDtos()
        {
            if (tactilePointDto == null) return null;

            return new IDto[] {tactilePointDto};
        }

        public override void ClearDtos()
        {
            // empty
        }
    }
}