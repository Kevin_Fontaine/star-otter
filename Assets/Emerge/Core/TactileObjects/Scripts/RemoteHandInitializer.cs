﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Emerge.MeshInteraction;
using Emerge.Core;
using Emerge.HandTracking;

public class RemoteHandInitializer : MonoBehaviour
{
    public Transform prefabToInstantiate;
    public Transform emergeObjectPrefabToInstantiate;

    private GameObject debugMirrorMesh;
    public Material debugMaterial;

    public bool createDebugMesh;


    void Start()
    {
        if (createDebugMesh)
        {
            debugMirrorMesh = new GameObject("debug mirror mesh");
            debugMirrorMesh.AddComponent<MeshFilter>();
            MeshRenderer m = debugMirrorMesh.AddComponent<MeshRenderer>();
            m.material = debugMaterial;
        }
    }


    private void SetupNewHandObject(Transform remoteHandPrefabInstance, PuppetHand h, Emerge.Handedness which)
    { 
        Transform meshInteractionPrefabInstance = Instantiate(prefabToInstantiate);
        meshInteractionPrefabInstance.name = "MeshInteraction Remote Hand - " + which;

        MeshInteractionSetupData d = new MeshInteractionSetupData();
        d.autoLocateMesh = false;

        //Avatar has 2 hands. so find the skinned mesh renderer on the particular hand that we were passed 
        SkinnedMeshRenderer smr = h.transform.GetComponentInChildren<SkinnedMeshRenderer>();
        d.skinnedMeshRendererToUse = smr;

        d.rootTransform = remoteHandPrefabInstance;
        d.modelTransform = remoteHandPrefabInstance.parent;//this is the same as AvatarManager.Instance.GetRemoteRoot();
        d.meshInteractionType = MeshInteractionType.skinnedMesh;

        MeshInteraction mi = meshInteractionPrefabInstance.GetComponent<MeshInteraction>();
        mi.Init(d);

        //setup debug object
        if (createDebugMesh)
        {
            mi.mesh_source.debugMode = true;
            mi.mesh_source.animationDataDebugMesh = debugMirrorMesh;
        }


        //===================================================================================== EMERGE OBJECT CREATION

        Transform emergeObjectPrefabInstance = Instantiate(emergeObjectPrefabToInstantiate);
        emergeObjectPrefabInstance.name = "EmergeObject Remote Hand - " + which;
        TactileMesh eomb = emergeObjectPrefabInstance.GetComponent<TactileMesh>();
        eomb.RuntimeInit(mi);


        //----- parent to hierarchy - this should take care of destroy()ing when players leave, but need to test...

        //find interactable object root
        //GameObject root = EmergeCore.Instance.GetInteractableObjectRoot();

        //Debug.Log("h.GetHandTransforms(Emerge.HandParts.PalmCenter) = " + h.GetHandTransforms(Emerge.HandParts.PalmCenter).name);


        //Transform parent = h.transform.parent; //root.transform; //d.rootTransform;
        Transform palmCenter = h.GetHandTransforms(Emerge.HandParts.PalmCenter); //null always??

        emergeObjectPrefabInstance.parent = palmCenter; //attach to center knuckle/palm center, because collider
        meshInteractionPrefabInstance.parent = h.transform.parent; //this is easier to find when its at the root

        emergeObjectPrefabInstance.localPosition = Vector3.zero;
        emergeObjectPrefabInstance.localRotation = Quaternion.identity;
         
    }


    public void SetupHands(Transform avatarRoot, PuppetHand left, PuppetHand right)
    {

        SetupNewHandObject(avatarRoot, left, Emerge.Handedness.Left);
        SetupNewHandObject(avatarRoot, right, Emerge.Handedness.Right);
    }


    //internal void DestroyHandObject(Transform t)
    //{
    //    //NOTE: as long as the objects above are moved into the remote hand hierarchy after creation, they will be 
    //    //destroyed automatically along with the hand itself.
    //}
}

