﻿/******************************************************************************
 * Copyright (C) Emerge Now, Inc. 2016-2019.                                  *
 * Emerge Now Inc proprietary and confidential.                               *
 *                                                                            *
 * Use subject to the terms of the Emerge Exploration Program Agreement       *
 * and/or terms of limited, non-exclusive, non-sublicensable license to       *
 * Emerge SDK between Emerge and you, your company or any other organization. *
 ******************************************************************************/

using System;
using UnityEngine;
using Emerge.Tracking;
using Emerge.HandTracking;

namespace Emerge.Core
{ 
    /// <summary>
    /// Information about avatar hand collision with an object that has a Trigger Collider
    /// </summary>
    public class ColliderToHandCollision : BaseHandCollision 
    {
        bool[] leftHandStatus;
        bool[] rightHandStatus;
        private int totalHandParts;
        void Awake()
        {
            totalHandParts = Enum.GetValues(typeof(HandColliderType)).Length * 2; 
            //leftHandStatus = new bool[Enum.GetValues(typeof(HandParts)).Length];
            leftHandStatus = new bool[totalHandParts];
            rightHandStatus = new bool[totalHandParts];
        }

        private void OnDisable()
        {
            if (!Emerge.Tracking.HandTracking.Default.IsNullOrDestroyed())
            {
                Emerge.Tracking.HandTracking.Default.OnHandInactive -= HandleHandInactive;
            }

            ClearLeftHandCollision();
            ClearRightHandCollision();
        }

        private void OnEnable()
        {
            Emerge.Tracking.HandTracking.Default.OnHandInactive += HandleHandInactive;
        }

        /// <summary>
        /// Fire the collision routine and figure out which, if any, hands are colliding
        /// </summary>
        /// <returns>Enum indicating None, Left, Right, or Both hands colliding</returns>
        public override HandCollisionStatus DetectHandCollision()
        {
            HandCollisionStatus handsColliding;

            bool left = CheckListForCollisions(ref leftHandStatus);
            bool right = CheckListForCollisions(ref rightHandStatus);

            if (left & !right) { handsColliding = HandCollisionStatus.Left; }
            else if (!left & right) { handsColliding = HandCollisionStatus.Right; }
            else if (left & right) { handsColliding = HandCollisionStatus.Both; }
            else { handsColliding = HandCollisionStatus.None; }

            return handsColliding;
        }

        /// <summary>
        /// Check the results list for either hand to see if any parts are colliding.
        /// </summary>
        /// <param name="handList">Left or Right hand results list.</param>
        /// <returns>True if any part in the list is colliding.</returns>
        public bool CheckListForCollisions(ref bool[] handList)
        {
            for (int i = 0; i < totalHandParts; i++)
            {
                if (handList[i])
                    return true;
            }

            return false;
        }


        public void ClearLeftHandCollision()
        {
            for (int i = 0; i < leftHandStatus.Length; i++)
            {
                leftHandStatus[i] = false;
            }
        }


        public void ClearRightHandCollision()
        {
            for (int i = 0; i < rightHandStatus.Length; i++)
            {
                rightHandStatus[i] = false;
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            HandleCollisionEvent(other, true);
        }

        private void OnTriggerExit(Collider other)
        {
            HandleCollisionEvent(other, false);
        }

        private void HandleCollisionEvent(Collider other, bool didEnter)
        {
            InteractionComponent handComponent = other.GetComponent<InteractionComponent>();
            if (handComponent == null || handComponent.HandModel == null)
            {
                return;
            }

            if (handComponent.HandModel.chirality == Chirality.Left)
            {
                leftHandStatus[(int)handComponent.type] = didEnter;
            }
            else
            {
                rightHandStatus[(int)handComponent.type] = didEnter;
            }
        }


        private void HandleHandInactive(IHandDataSource dataSource, Hand hand, Chirality chirality)
        {
            switch (chirality)
            {
                case Chirality.Left:
                    ClearLeftHandCollision();
                    break;

                case Chirality.Right:
                    ClearRightHandCollision();
                    break;

                default:
                    break;
            }
        }


        /*
        //AvatarTrackingEventHandler interface
        //-----------------------------------------------------
        public void OnLeftHandActive() { }

        public void OnLeftHandInActive()
        {
            ClearLeftHandCollision();
        }

        public void OnRightHandActive() { }

        public void OnRightHandInActive()
        {
            ClearRightHandCollision();
        }
        */
    }
}


