﻿/******************************************************************************
 * Copyright (C) Emerge Now, Inc. 2016-2019.                                  *
 * Emerge Now Inc proprietary and confidential.                               *
 *                                                                            *
 * Use subject to the terms of the Emerge Exploration Program Agreement       *
 * and/or terms of limited, non-exclusive, non-sublicensable license to       *
 * Emerge SDK between Emerge and you, your company or any other organization. *
 ******************************************************************************/

using UnityEngine;

namespace Emerge.Core
{ 
    /// <summary>
    /// Defines a collision area (box or sphere) and detects hand objects inside
    /// relies on testing against a hand-only layer and hand colliders/triggers 
    /// having HandProperty attached to them with more info about them (which hand, which part of the hand, etc)
    /// </summary>
    public class BaseHandCollision : MonoBehaviour
    {  
        /// <summary>
        /// Fire the collision routine and figure out which, if any, hands are colliding.
        /// </summary>
        /// <returns>Enum indicating None, Left, Right, or Both hands colliding.</returns>
        public virtual HandCollisionStatus DetectHandCollision()
        {
            HandCollisionStatus handsColliding = HandCollisionStatus.None;

            return handsColliding;
        } 
    }
}
