﻿/******************************************************************************
 * Copyright (C) Emerge Now, Inc. 2016-2019.                                  *
 * Emerge Now Inc proprietary and confidential.                               *
 *                                                                            *
 * Use subject to the terms of the Emerge Exploration Program Agreement       *
 * and/or terms of limited, non-exclusive, non-sublicensable license to       *
 * Emerge SDK between Emerge and you, your company or any other organization. *
 ******************************************************************************/

using System;
using System.Collections.Generic;
using Emerge.Communication; 
using UnityEngine;

namespace Emerge.Core
{
    public class DynamicCountTactilePointsManager : MonoBehaviour
    {
        protected bool active;

        [SerializeField]
        protected TactilePointMode tactilePointMode = TactilePointMode.Override;
         
        //[HideInInspector] 
        //protected Stack<TactilePointData> tactilePoints;
                 
        // ToDo: make a shared pool class (like LeapMotion's)
        // private Stack<TactilePoint> tactilePointPool;

        // ToDo: Not editable for now
        private int maxCountTactilePoints = 19;

        /// <summary>
        /// Creates a list of empty TactilePoint objects the size of your Transform[] list ("CustomPointsGenerator.objects")
        /// </summary>
        /// <param name="message">the custom points portion of the BeamformerMessage</param>
        public virtual void Init(ref TactilePointDto message)
        {
            //tactilePoints = new Stack<TactilePointData>();
            // tactilePointPool = new Stack<TactilePoint>();

            //message.tactilePoints.Clear(); 

            // InitializeTactilePoints();

            // message.TactilePointMode = tactilePointMode;
        }

        //private void InitializeTactilePool()
        //{  
        //    // Populate the pool of TactilePoints to draw from. We reuse them instead of generating them new each Update. 
        //    for (int i = 0; i < maxCountTactilePoints; i++)
        //    {
        //        tactilePointPool.Push(new TactilePoint(Vector3.zero) {id = i});
        //    }
        //}

        //private void InitializeTactilePoints()
        //{
        //    // Populate the pool of TactilePoints to draw from. We reuse them instead of generating them new each Update. 
        //    for (int i = 0; i < maxCountTactilePoints; i++)
        //    {
        //        tactilePoints.Push(new TactilePoint(Vector3.zero) {beam = true});
        //    }
        //}

        //private void RecycleAllTactilePoints()
        //{
        //    foreach (TactilePoint tactilePoint in tactilePoints)
        //    {
        //        tactilePointPool.Push(tactilePoint);
        //    }

        //    tactilePoints.Clear();
        //}
         
        //public virtual void TouchStatusChanged(bool isTouched)
        //{
        //    if (isTouched)
        //        StartAnimation();
        //    else
        //        StopAnimation();
        //}

        ////animation control
        ////-----------------------------------------------
        //protected virtual void StartAnimation()
        //{
        //    active = true;
        //}

        //protected virtual void StopAnimation()
        //{
        //    active = false;
        //}

        // Update and ready to send to hardware 
        // In this dynamic version the coordinates of each TactilePoint in tactileData.points should be updated prior to passing it to this method.
        public virtual void UpdatePoints(ref TactilePointDto tactileData)
        {
            if (tactileData is null) throw new ArgumentNullException(nameof(tactileData));

            if (!active) return;

            // RecycleAllTactilePoints();
            // ResetAllTactilePoints();
            //tactilePoints.Clear();

            // int len = tactileData.tactilePoints.Count; 
            //for (int i = 0; i < len; i++)
            //{
            //    tactileData.tactilePoints[i].beam = true;
            //    tactileData.tactilePoints[i].SetPoint(points[i].position);

            //    TactilePoint tactilePoint = tactilePointPool.Pop();
            //    tactilePoints.Push(tactilePoint);
            //}

            //EXAMPLE - move to another class
            ////base.UpdatePoints();
            //message.points.Clear();
            //message.overridePreviousMessage = true;
            //message.points.Add(new TactilePoint(transform.position, true, 0, 4.5f, 100));
            //message.points.Add(new TactilePoint(transform.position + transform.right * 0.065f + transform.forward * -0.075f, true, 1, 5.0f, 50));
            //message.points.Add(new TactilePoint(transform.position + transform.right * -0.03f + transform.forward * 0.03f, true, 2, 3.5f, 100));
            //message.points.Add(new TactilePoint(transform.position + transform.right * -0.03f, true, 3, 3.5f, 33));
            //message.points.Add(new TactilePoint(transform.position + transform.right * 0.03f, true, 4, 3.5f, 66)); 
        }
    }
}
