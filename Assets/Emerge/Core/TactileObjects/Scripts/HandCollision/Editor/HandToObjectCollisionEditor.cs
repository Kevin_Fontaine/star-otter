﻿/******************************************************************************
 * Copyright (C) Emerge Now, Inc. 2016-2019.                                  *
 * Emerge Now Inc proprietary and confidential.                               *
 *                                                                            *
 * Use subject to the terms of the Emerge Exploration Program Agreement       *
 * and/or terms of limited, non-exclusive, non-sublicensable license to       *
 * Emerge SDK between Emerge and you, your company or any other organization. *
 ******************************************************************************/
// CleanPass

using Emerge.Core;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(CustomAreaToHandCollision))]
[ExecuteInEditMode]
public class HandToObjectCollisionEditor : Editor
{
    private CustomAreaToHandCollision noColliderHandToObjectCollision;

    void OnEnable()
    { 
        noColliderHandToObjectCollision = target as CustomAreaToHandCollision;
    }

    public override void OnInspectorGUI()
    { 
        base.OnInspectorGUI();

        serializedObject.Update();

        EditorGUILayout.BeginHorizontal();
        if (noColliderHandToObjectCollision.collisionAreaType == CollisionType.Box)
        {
            var extentsProperty = serializedObject.FindProperty(CustomAreaToHandCollision.EditorExtentsName); 
            EditorGUILayout.PropertyField(extentsProperty); 
        }
        else if(noColliderHandToObjectCollision.collisionAreaType == CollisionType.Sphere)
        {
            var radiusProperty = serializedObject.FindProperty(CustomAreaToHandCollision.EditorRadiusName); 
            EditorGUILayout.PropertyField(radiusProperty);  
        }
        EditorGUILayout.EndHorizontal(); 

        serializedObject.ApplyModifiedProperties();
    }
}
