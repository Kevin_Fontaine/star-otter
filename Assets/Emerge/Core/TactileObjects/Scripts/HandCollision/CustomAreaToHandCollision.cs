﻿/******************************************************************************
 * Copyright (C) Emerge Now, Inc. 2016-2019.                                  *
 * Emerge Now Inc proprietary and confidential.                               *
 *                                                                            *
 * Use subject to the terms of the Emerge Exploration Program Agreement       *
 * and/or terms of limited, non-exclusive, non-sublicensable license to       *
 * Emerge SDK between Emerge and you, your company or any other organization. *
 ******************************************************************************/
// CleanPass

using System;
using UnityEngine;
using Emerge.Tracking;
using Emerge.HandTracking;

namespace Emerge.Core
{
    /// \deprecated Defines a collision area (box or sphere) and detects hand objects inside the area.
    /// <summary>
    /// Deprecated - Use ColliderToHandCollision instead. This will be removed in a future update.
    /// Defines a collision area (box or sphere) and detects hand objects inside the area.
    /// It relies on testing against a hand-only layer and hand colliders/triggers 
    /// having HandProperty attached to them with more info about them (which hand, which part of the hand, etc.).
    /// </summary>
    [Obsolete("Deprecated - Use ColliderToHandCollision instead. This will be removed in a future update.")]
    public class CustomAreaToHandCollision : BaseHandCollision
    {
        [Tooltip("Set to 'Hand' layermask if you want hand-to-object collision")] [SerializeField]
        private LayerMask mask;
        public LayerMask Mask
        {
            get => mask;
            set => mask = value;
        }

        // Detection area properties
        public CollisionType collisionAreaType;
         
        [SerializeField]
        private Vector3 center;
        public Vector3 Center
        {
            get => center;
            set => center = value;
        }
         
        [SerializeField]
        [HideInInspector]
        private Vector3 extents;
        public Vector3 Extents
        {
            get => extents;
            set => extents = value;
        }
        
        [SerializeField]
        [HideInInspector]
        private float radius;
        public float Radius
        {
            get => radius;
            set => radius = value;
        }

        private int totalHandParts;
        private bool[] leftHandStatus;
        private bool[] rightHandStatus; 
        private Collider[] results;
         
#if UNITY_EDITOR
        public static string EditorExtentsName => nameof(extents);
        public static string EditorRadiusName => nameof(radius);
#endif

        #region UnityEvents

        private void Awake()
        {
            totalHandParts = Enum.GetValues(typeof(HandColliderType)).Length * 2;
            leftHandStatus = new bool[totalHandParts];
            rightHandStatus = new bool[totalHandParts];
             
            // If the user left it set to Nothing assume they want Hand
            if (Mask == 0)
            {
                Mask = 1 << LayerMask.NameToLayer("Hand");
            }

            results = new Collider[totalHandParts * 2]; 
        }

        private void OnDrawGizmos()
        {
            if (collisionAreaType == CollisionType.Box) 
            { 
                Gizmos.DrawWireCube(transform.position + Center, Extents); 
            }
            else
            {
                Gizmos.DrawWireSphere(transform.position + Center, Radius);
            }
        } 
        #endregion
         
        #region public

        /// <summary>
        /// Fire the collision routine and figure out which, if any, hands are colliding.
        /// </summary>
        /// <returns>Enum indicating None, Left, Right, or Both hands colliding</returns>
        public override HandCollisionStatus DetectHandCollision()
        {
            HandCollisionStatus handsColliding;

            UpdateCollisionInfo();

            bool left = CheckListForCollisions(ref leftHandStatus);
            bool right = CheckListForCollisions(ref rightHandStatus);

            if (left & !right) { handsColliding = HandCollisionStatus.Left; }
            else if (!left & right) { handsColliding = HandCollisionStatus.Right; }
            else if (left & right) { handsColliding = HandCollisionStatus.Both; }
            else { handsColliding = HandCollisionStatus.None; }

            return handsColliding;
        }

        /// <summary>
        /// Check the results list for either hand to see if any parts are colliding.
        /// </summary>
        /// <param name="handList">Left or Right hand results list.</param>
        /// <returns>True if any part in the list is colliding.</returns>
        public bool CheckListForCollisions(ref bool[] handList)
        {
            for (int i = 0; i < totalHandParts; i++)
            {
                if (handList[i])
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Update the collision information.
        /// </summary>
        public void UpdateCollisionInfo()
        {
            //clear the lists
            for (int i = 0; i < totalHandParts; i++)
            {
                leftHandStatus[i] = false;
                rightHandStatus[i] = false;
            } 

            int totalResults = 0;

            if (collisionAreaType == CollisionType.Box)
            {
                Vector3 halfExtents = Extents * 0.5f;
                totalResults = Physics.OverlapBoxNonAlloc(transform.position + Center, halfExtents, results, transform.rotation, Mask, QueryTriggerInteraction.Collide);
            }
            else if (collisionAreaType == CollisionType.Sphere)
            {
                totalResults = Physics.OverlapSphereNonAlloc(transform.position + Center, Radius, results, Mask, QueryTriggerInteraction.Collide);
            }

            for (int i = 0; i < totalResults; i++)
            {
                InteractionComponent handComponent = results[i].GetComponent<InteractionComponent>();
                if (handComponent == null || handComponent.HandModel == null)
                {
                    continue;
                }

                if (handComponent.HandModel.chirality == Chirality.Left)
                {
                    leftHandStatus[(int)handComponent.type] = true;
                }
                else
                {
                    rightHandStatus[(int)handComponent.type] = true;
                }
            }
        } 
        #endregion
    }
}
