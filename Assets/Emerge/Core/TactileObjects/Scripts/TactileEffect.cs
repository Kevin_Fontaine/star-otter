﻿/******************************************************************************
 * Copyright (C) Emerge Now, Inc. 2016-2019.                                  *
 * Emerge Now Inc proprietary and confidential.                               *
 *                                                                            *
 * Use subject to the terms of the Emerge Exploration Program Agreement       *
 * and/or terms of limited, non-exclusive, non-sublicensable license to       *
 * Emerge SDK between Emerge and you, your company or any other organization. *
 ******************************************************************************/
// CleanPass

using Emerge.Communication;
using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;

namespace Emerge.Core
{
    /// <summary>
    /// Emerge Object for passing custom points directly to beamformer.
    /// When used as an EmergeObject, your custom points will be transmitted
    /// when hands touch the Trigger Collider
    /// </summary>
    public class TactileEffect : AbstractTactileBehavior
    {
        [SerializeField]
        private AbstractEffectProperties activeEffect;
       
        private List<AbstractEffectProperties> effects;
        private int activeInstanceId;
        private string defaultEffectId = "1";
        private ISyncedClockMaster syncedClock;

        //Tactile effect transform track
        Vector3? prevPos = null;
        Quaternion? prevRot = null;
        Vector3? prevScale = null;
        readonly float rotationAngleThreshold = 0.1f;

        public long EffectTimeMilliseconds
        { 
            get
            {
                if (syncedClock.IsNullOrDestroyed())
                {
                    return 0;
                }

                return syncedClock.ClockTimeMilliseconds;
            }
        }

        [SerializeField] 
        private string effectId;    
        public string EffectId
        {
            get => effectId;
            set => effectId = value;
        }

        public TactileEffectDto TactileEffectData { get; private set; } 
 
        #region UnityEvents

        public override void Awake()
        { 
            base.Awake();

            if (effects == null)
            {
                effects = new List<AbstractEffectProperties>();
            }

            if (string.IsNullOrEmpty(EffectId))
            {
                EffectId = defaultEffectId;
            }
             
            TactileEffectData = new TactileEffectDto(TactileMethod.effects, activeEffect, EffectId);
        }

        void Start()
        { 
            syncedClock = new SyncedClockMaster(Identifier, ClockSyncMessageHub.Instance);  
        }

        public void OnEnable()
        {
            UpdateTransforms(); 
        }

        /// <summary>
        /// Override this function to update the transforms of tactile animation (position, rotation).
        /// </summary>
        public override void Update()
        {
            base.Update();

            if(activeEffect == null)
            {
                return;
            }

            // Local control of tactileData update 
            UpdateTransforms();

            //Check Transform values to see if any of them are dirty
            CheckForDirtyTransformVals(); 

            // If any properties were dirtied or the activeEffect was changed
            if (TactileEffectData.GetEffectProperty().IsDirty ||
                activeEffect?.GetInstanceID() != activeInstanceId )
            {
                // Debug.Log($"IsDirty: {isDirty}, uniqueId: {uniqueId}, guid: {guid}, instanceId: {instanceId}");

                activeInstanceId = activeEffect.GetInstanceID();

                TactileEffectData.ActionType = ActionType.composeandplay;
                 
                TactileManager.AddTactileDataToEffectsGenerateQueue(TactileEffectData);
                
                // todo: verify we can return here. 2020/05/19
                // would it be possible to have a *dirtied* change and a CollisionEnd on the same frame and 
                // returning here would prevent the effect from being stopped? 
                return;
            }
             
            if (CollisionPhase == CollisionPhase.CollisionBegin)
            {
                // If an effect was touched and released it may have been stopped so we need to re-compose it.  
                TactileEffectData.ActionType = ActionType.composeandplay;
                TactileManager.AddTactileDataToEffectsGenerateQueue(TactileEffectData);
            }
            else if(CollisionPhase == CollisionPhase.CollisionEnd)
            {
                TactileEffectData.ActionType = ActionType.pause;
            }
            else if (CollisionPhase == CollisionPhase.CollisionSustain)
            {
                // Don't send anything on collision sustain. 
                TactileEffectData.ActionType = ActionType.none;
            }
        }

        // Called when the script is loaded or a value is changed in the Inspector (Called in the editor only). 
        public void OnValidate()
        {
            // Debug.Log("OnValidate()");
            if ( TactileEffectData != null )
            {  
                TactileManager.AddTactileDataToEffectsGenerateQueue(TactileEffectData);
            }
        }
        #endregion

        #region private

        private void UpdateTransforms()
        {
            TactileEffectData.SetRootPosition(transform.position);
            TactileEffectData.SetRootScale(transform.localScale);
            TactileEffectData.SetRootRotation(transform.rotation);
            // ToDo: Optimize: these should only happen once when the effect is initialized.
            TactileEffectData.SetEffectID(EffectId); 
            TactileEffectData.SetEffect(activeEffect);  
        }

        //Check if any of the transform values got changed over the last frame, if so Set the AbstractEffectProperties to dirty
        private void CheckForDirtyTransformVals()
        {
            bool isDirty = false;

            if(prevPos != null) //Check Postion
            {
                if (transform.position != prevPos)
                    isDirty = true;
            }
            if (prevRot != null && !isDirty) //Check Rotation
            {
                if (Mathf.Abs(Quaternion.Angle(transform.rotation,prevRot.Value)) > rotationAngleThreshold)
                    isDirty = true;
            }
            if (prevScale != null && !isDirty) //Check Scale
            {
                if (transform.localScale != prevScale)
                    isDirty = true;
            }

            if (isDirty)
                TactileEffectData.GetEffectProperty().SetDirty();  //Set the effect to dirty if transform values were changed

            prevPos = transform.position;
            prevRot = transform.rotation;
            prevScale = transform.localScale;
        }
        #endregion

        #region public
        /// <summary>
        /// Gets the active Effect.
        /// </summary>
        /// <returns>The Active Effect.</returns>
        public AbstractEffectProperties GetCurrentEffect()
        {
            return activeEffect;
        }

        /// <summary>
        /// Sets the active Effect.
        /// </summary>
        /// <param name="abstractEffectProperties">ToDo: stub</param>
        public void SetCurrentEffect([NotNull] AbstractEffectProperties abstractEffectProperties )
        {
            if (abstractEffectProperties is null) throw new ArgumentNullException(nameof(abstractEffectProperties));

            activeEffect = abstractEffectProperties;
            if (!syncedClock.IsNullOrDestroyed())
            {
                syncedClock.RestartClock();
            }
        }
 
        /// <summary>
        /// Retrieves the Tactile Data object.
        /// </summary>
        /// <returns>This EmergeObject's ITactileData subclass.</returns>
        public override IDto[] GetDtos()
        {
            // Debug.Log($"GetTactileData will return tactile data: {tactileCustomPointsData.GetDataAsJSONObject()}");

            return new IDto[]{ TactileEffectData };
        }

        public override void ClearDtos()
        {
            // empty
        }
        #endregion 
    } 
}