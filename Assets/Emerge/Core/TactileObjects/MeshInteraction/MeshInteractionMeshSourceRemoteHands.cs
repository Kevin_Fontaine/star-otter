﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Emerge.Core;
using Unity.Mathematics;
using Emerge.HandTracking;

namespace Emerge.MeshInteraction
{

    public class MeshInteractionMeshSourceRemoteHands : MeshInteractionMeshSource
    {
        

        //[Header("Use Remote Hands")]
        ////TODO: how will we handle the collision box that enables mesh interaction via EmergeObject now?
        ////the remote hands are not EmergeObjects but still need to be enabled for touch
        //public bool useRemoteHandsAsMeshSource;
        //public Transform remoteHand;

        //public RemoteHandType remoteHandType;

        //public enum RemoteHandType
        //{
        //    skinnedMesh,
        //    jointPositions
        //}


        

        //AvatarController remoteAvatarController;

        //private void GetRemoteHands()
        //{
        //    //AvatarController
        //    remoteAvatarController = AvatarManager.Instance.GetRemoteAvatar();
        //    Debug.Log("AvatarController remoteAvatarController = " + remoteAvatarController.gameObject.name);
        //    HandData d = remoteAvatarController.leftHand.currentHandData;
        //    for (int i = 0; i < d.all.Length; i++)
        //    {

        //    }
        //}





        //internal override void Setup(MeshInteraction_Processor p)
        //{
        //    processor = p;

        //    //NOTE: for first testing i'll use a ref to a dummy hand model in the scene
        //    // in the future, we should get the mesh from the AVatarManager? not sure yet how to deal with
        //    // multiple hands * multiple users and other issues.
        //    if (useRemoteHandsAsMeshSource)
        //    {
        //        //GetRemoteHands(); //implement later

        //        //for now, get the remote hand ref


        //        //use hand transforms instead of verts?
        //        if (remoteHandType == RemoteHandType.skinnedMesh)
        //        {
        //            skinnedMeshRenderer = gameObject.GetComponentInChildren<SkinnedMeshRenderer>();
        //            modelTransform = skinnedMeshRenderer.transform.root; //NOTE: need to look into this - whats the chain that xforms skinned mesh verts. using root of the skinned mesh seems to work, although with an offset
        //            m_Mesh = skinnedMeshRenderer.sharedMesh;
        //            isSkinnedMesh = true;
        //            Debug.Log("MeshInteraction_MeshSource found puppet hand to use...");

        //        }
        //        else
        //        {
        //            Mesh m = new Mesh();
        //            HandData h = new HandData();
        //            List<Vector3> joints = new List<Vector3>(h.all.Length);

        //            m.SetVertices(joints);
        //            m_Mesh = m;
        //        }
        //    }

 
        //}


        //internal void UpdateMeshDataWithRemoteHands()
        //{

        //}


        //private void OnDrawGizmos()
        //{
        //    if (useRemoteHandsAsMeshSource)
        //    {
        //        if (remoteAvatarController == null)
        //            return;

        //        HandData d = remoteAvatarController.leftHand.currentHandData;
        //        Vector3 size = Vector3.one * 0.01f;
        //        for (int i = 0; i < d.all.Length; i++)
        //        {
        //            Gizmos.DrawCube(d.all[i].position, size);
        //        }

        //        HandData d2 = remoteAvatarController.rightHand.currentHandData;
        //        for (int i = 0; i < d.all.Length; i++)
        //        {
        //            Gizmos.DrawCube(d.all[i].position, size);
        //        }

        //    }
        //}



        


        //internal override void UpdateSkinnedMeshVerts()
        //{
        //    if (useRemoteHandsAsMeshSource)
        //    {

        //        if (remoteHandType == RemoteHandType.jointPositions)
        //        {
        //            //only update the remote hand positions to use for detection
        //            Vector3[] positions = remoteAvatarController.leftHand.currentHandData.GetCurrentPoseAsFlatVector3List();
        //            processor.FillVertsList(ref positions);
        //            return;


        //        }
        //        else
        //        {
        //            //update the actual skinned mesh of the dummy object - this is temporary, as we should
        //            //eventually just use the remote avatar - see Notes on this at the top
        //            Vector3[] positions = remoteAvatarController.leftHand.currentHandData.GetCurrentPoseAsFlatVector3List();
        //            remoteAvatarController.leftHand.currentHandData.SetHandPose(positions);
        //        }

        //    }



        //    base.UpdateSkinnedMeshVerts();

        //}

 
    }
}
