﻿/******************************************************************************
 * Copyright (C) Emerge Now, Inc. 2016-2019.                                  *
 * Emerge Now Inc proprietary and confidential.                               *
 *                                                                            *
 * Use subject to the terms of the Emerge Exploration Program Agreement       *
 * and/or terms of limited, non-exclusive, non-sublicensable license to       *
 * Emerge SDK between Emerge and you, your company or any other organization. *
 ******************************************************************************/
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Emerge.Communication;

namespace Emerge.MeshInteraction
{
    using Core;
    /// <summary>
    /// Data process is responsible for Filtering the tactile points selected by mesh interaction process
    /// This class also determines the tactile texture associated with each tactile point
    /// </summary>
    public class MeshInteractionDataPostProcessor : AbstractMeshInteractionDataPostProcessor
    {
    }
}