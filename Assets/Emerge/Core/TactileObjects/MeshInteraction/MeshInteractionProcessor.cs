﻿/******************************************************************************
 * Copyright (C) Emerge Now, Inc. 2016-2019.                                  *
 * Emerge Now Inc proprietary and confidential.                               *
 *                                                                            *
 * Use subject to the terms of the Emerge Exploration Program Agreement       *
 * and/or terms of limited, non-exclusive, non-sublicensable license to       *
 * Emerge SDK between Emerge and you, your company or any other organization. *
 ******************************************************************************/

using System.Collections.Generic;
using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;
using Unity.Mathematics;
using UnityEngine;
using static Emerge.MeshInteraction.MeshInteractionUtils;
using Emerge.Core;

namespace Emerge.MeshInteraction
{
    /// <summary>
    ///    Mesh Interaction Processor selects the tactile points based on the interaction of the hand with the 3d mesh
    ///    It uses a grid of 9 'sensor' points on the palm and 2 'sensor' points at the tip and knuckle of each finger
    ///    
    ///The Visual Debug portion uses a pool of prefabs to place a visual marker at each point in a set of data points being processed.
    ///NOTE: This visual debug can be very slow and should be turned off at runtime
    ///
    ///You can set the color and size of the prefab set to each point in the set.
    ///The sets of points processed are as follows:
    ///- all intersected points on the mesh
    ///- potential tactile points corresponding to each 'sensor' point in the hand
    ///- final set of pruned tactile points
    ///- POI (average centerpoint) used as part of the pruning process
    /// </summary>  
    public class MeshInteractionProcessor : AbstractMeshInteractionProcessor
    {
    }
}