﻿/******************************************************************************
 * Copyright (C) Emerge Now, Inc. 2016-2019.                                  *
 * Emerge Now Inc proprietary and confidential.                               *
 *                                                                            *
 * Use subject to the terms of the Emerge Exploration Program Agreement       *
 * and/or terms of limited, non-exclusive, non-sublicensable license to       *
 * Emerge SDK between Emerge and you, your company or any other organization. *
 ******************************************************************************/
using System.Collections.Generic;
using System.Collections;
using System.Collections.ObjectModel;
using UnityEngine;
using System;
using EasyEditor;

namespace Emerge.MeshInteraction
{
    using HandTracking;

    public class MeshInteractionHandTransformLocator : AbstractMeshInteractionHandTransformLocator
    {
    }
}
