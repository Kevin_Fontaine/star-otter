﻿ 
using UnityEditor;
using UnityEngine;
using System.Collections;
using EasyEditor;

namespace Emerge.MeshInteraction
{

    [Groups("Components", "Debugging")]
    [CustomEditor(typeof(MeshInteraction))]
    public class MeshInteractionEditor : EasyEditorBase
    {

    }
}