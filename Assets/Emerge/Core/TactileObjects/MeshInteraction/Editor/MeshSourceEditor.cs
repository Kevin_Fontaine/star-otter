﻿ 
using UnityEditor;
using UnityEngine;
using System.Collections;
using EasyEditor;

namespace Emerge.MeshInteraction
{
    [Groups("Debug")]
    [CustomEditor(typeof(MeshInteractionMeshSource))]
    public class MeshSourceEditor : EasyEditorBase
    {
    }
}