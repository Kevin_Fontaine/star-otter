﻿using EasyEditor;
using UnityEditor;

namespace Emerge.MeshInteraction
{
    [Groups("DebugTransforms")]
    [CustomEditor(typeof(MeshInteractionHandTransformLocator))]
    public class HandTransformLocatorEditor : EasyEditorBase
    {

    }
}
