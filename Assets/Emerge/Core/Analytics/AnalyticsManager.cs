﻿/******************************************************************************
 * Copyright (C) Emerge Now, Inc. 2016-2019.                                  *
 * Emerge Now Inc proprietary and confidential.                               *
 *                                                                            *
 * Use subject to the terms of the Emerge Exploration Program Agreement       *
 * and/or terms of limited, non-exclusive, non-sublicensable license to       *
 * Emerge SDK between Emerge and you, your company or any other organization. *
 ******************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Wrapper class for AbstractAnalyticsManager
public class AnalyticsManager : AbstractAnalyticsManager
{
    public static AnalyticsManager instance;

    private void Awake() //Follow singleton pattern for instance
    {
        if (instance)
            Destroy(gameObject);
        else
            instance = this;
    }
}
