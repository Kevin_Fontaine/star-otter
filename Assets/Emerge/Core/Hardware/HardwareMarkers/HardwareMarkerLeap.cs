﻿using UnityEngine;

namespace Emerge
{
    /// <summary>
    /// HandTrackingSource locates this component's transform via discovery of this class
    /// </summary>
    public class HardwareMarkerLeap : AbstractHardwareMarkerLeap { }
}
