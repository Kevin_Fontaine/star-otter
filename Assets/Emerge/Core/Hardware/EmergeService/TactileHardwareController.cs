﻿/******************************************************************************
 * Copyright (C) Emerge Now, Inc. 2016-2019.                                  *
 * Emerge Now Inc proprietary and confidential.                               *
 *                                                                            *
 * Use subject to the terms of the Emerge Exploration Program Agreement       *
 * and/or terms of limited, non-exclusive, non-sublicensable license to       *
 * Emerge SDK between Emerge and you, your company or any other organization. *
 ******************************************************************************/
// CleanPass

using SimpleJSON;
using UnityEngine;

namespace Emerge.Communication
{
    public class TactileHardwareController : AbstractTactileHardwareController
    {
        private void OnApplicationPause(bool pause)
        {
            if(pause)
            {
                SendDataToTactileEngine("{\"action\":\"stop\",\"data\":\"{}\"}");
            }
        }
        private void OnApplicationQuit()
        {
            SendDataToTactileEngine("{\"action\":\"stop\",\"data\":\"{}\"}");   
        }
    }
}