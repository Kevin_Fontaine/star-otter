﻿/******************************************************************************
 * Copyright (C) Emerge Now, Inc. 2016-2019.                                  *
 * Emerge Now Inc proprietary and confidential.                               *
 *                                                                            *
 * Use subject to the terms of the Emerge Exploration Program Agreement       *
 * and/or terms of limited, non-exclusive, non-sublicensable license to       *
 * Emerge SDK between Emerge and you, your company or any other organization. *
 ******************************************************************************/

using Microsoft.Win32;
using SimpleJSON;
using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using UnityEngine;

namespace Emerge.Communication
{
    /// <summary>
    /// Establishes a connection and sends data to the Emerge service
    /// </summary>
    public class SocketController : AbstractSocketController
    { }    
}