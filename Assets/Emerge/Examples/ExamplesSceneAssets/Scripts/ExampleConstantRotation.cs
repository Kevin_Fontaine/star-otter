﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExampleConstantRotation : MonoBehaviour
{
    [SerializeField] 
    private Transform target;

    [SerializeField] 
    private Vector3 rotationAxis;

    [SerializeField]
    private float anglesPerSecond;

    void Update()
    {
        if (target != null)
        {
            target.Rotate(rotationAxis, anglesPerSecond * Time.deltaTime, Space.Self);
        }
    }
}
