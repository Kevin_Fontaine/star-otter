﻿using UnityEngine;

/// <summary>
/// Cycles the Strength value for a TactilePointCollection from minStrength to maxStrength
/// with a cycle period of animateStrengthCyclesPerSecond.
/// </summary>
public class ExampleTactilePointCollectionStrengthPhaseCycler : MonoBehaviour
{
    [SerializeField]
    private TactilePointCollection tactilePointCollection;

    [SerializeField]
    [Range(0, 100.0f)]
    private float minStrength = 100.0f;

    [SerializeField]
    [Range(0, 100.0f)]
    private float maxStrength = 100.0f;

    [SerializeField]
    private bool animateStrength;

    [SerializeField]
    [Range(0, 3.0f)]
    public float animateStrengthCyclesPerSecond;

    private float TwoPi = 2.0f * Mathf.PI;
    
    void Update()
    {
        if (tactilePointCollection != null && animateStrength)
        {
            if (maxStrength > minStrength)
            {
                float hold = minStrength;
                minStrength = maxStrength;
                minStrength = hold;
            }

            float deltaStrength = maxStrength - minStrength;
              
            tactilePointCollection.Strength = minStrength + ((Mathf.Sin(Time.time * animateStrengthCyclesPerSecond * TwoPi) + 1.0f) * 0.5f) * deltaStrength;  
        }
    }
}
