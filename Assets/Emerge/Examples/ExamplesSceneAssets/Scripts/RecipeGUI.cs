﻿/******************************************************************************
 * Copyright (C) Emerge Now, Inc. 2016-2019.                                  *
 * Emerge Now Inc proprietary and confidential.                               *
 *                                                                            *
 * Use subject to the terms of the Emerge Exploration Program Agreement       *
 * and/or terms of limited, non-exclusive, non-sublicensable license to       *
 * Emerge SDK between Emerge and you, your company or any other organization. *
 ******************************************************************************/
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

using SimpleJSON;

namespace Emerge.Core
{
    public class RecipeGUI : Singleton<RecipeGUI>
    { 
        // recipe data
        public Dictionary<string, GameObject> recipes = new Dictionary<string, GameObject>();
        private string current_recipe;
         
        // gui
        private GUIStyle label_style;
        private GUIStyle button_style;
        private Rect point_window;
        private Rect recipe_window;
        private Rect minimized;
        private bool isMinimized = false;
        private Vector2 resolution;
        private Vector2 scroll_position;
        private bool hideAllAtStart = true;
        // recipe dictionary
         
        void Awake()
        {
            // initialize gui vars
            point_window = new Rect(Screen.width - 300, 20, 280, 50);
            recipe_window = new Rect(Screen.width - 300, 20, 280, 200);
            minimized = new Rect(20, 20, 280, 45);
            resolution = new Vector2(Screen.width, Screen.height);
            scroll_position = new Vector2(0, 0);

            current_recipe = null; 
        }

        private void Start()
        {
            Invoke("HideObjects", 2.0f);
        }

        void HideObjects()
        {
            if (hideAllAtStart)
            {
                // Get list of Interactable Objects
                GameObject InteractableObject = GameObject.Find("InteractableObjects");
                foreach (Transform child in InteractableObject.transform)
                {
                    recipes[child.name] = child.gameObject;
                    child.gameObject.SetActive(false);
                }
            }
        }

        private void OnGUI()
        {
            // GUIStyle must be called in OnGUI
            label_style = new GUIStyle("label");
            //label_style.font = (Font)Resources.Load("Ubuntu_Mono/UbuntuMono-Regular");
            label_style.alignment = TextAnchor.MiddleCenter;
            label_style.normal.textColor = Color.white;
            label_style.fontSize = 13;

            button_style = new GUIStyle("button");
            //button_style.font = (Font)Resources.Load("Ubuntu_Mono/UbuntuMono-Regular");
            button_style.normal.textColor = Color.white;
            button_style.fontSize = 13;

            if (resolution.x != Screen.width || resolution.y != Screen.height)
            {
                recipe_window.x = 20;
                minimized.x = 20;
                point_window.x = Screen.width / 2 - 130;
                resolution.x = Screen.width;
                resolution.y = Screen.height;
            }

            if (!isMinimized)
            {
                recipe_window = GUILayout.Window(2, recipe_window, DisplayRecipes, "Recipes");
            }
            else
            {
                minimized = GUILayout.Window(3, minimized, HideRecipes, "Recipes");
            }
        }

        private void DisplayRecipes(int windowID)
        { 
            scroll_position = GUILayout.BeginScrollView(scroll_position, false, false, GUIStyle.none, GUI.skin.verticalScrollbar);
            // create a button for each recipe file
            foreach (string recipe in recipes.Keys)
            {
                // if selected, purge previous recipe and load new recipe
                if (GUILayout.Button(recipe, button_style))
                {
                    if (current_recipe != null && recipe == current_recipe)
                    {
                        recipes[recipe].SetActive(false);
                        current_recipe = null;
                    }
                    else if (current_recipe != null)
                    {
                        recipes[current_recipe].SetActive(false);
                        recipes[recipe].SetActive(true);
                        current_recipe = recipe;
                    }
                    else
                    {
                        recipes[recipe].SetActive(true);
                        current_recipe = recipe;
                    }
                }
            }
            GUILayout.EndScrollView();
            if (GUI.Button(new Rect(recipe_window.width - 20, 2, 15, 15), "-"))
            {
                isMinimized = true;
            }
        }


        // Make the contents of the window
        private void HideRecipes(int windowID)
        {
            GUILayout.BeginHorizontal();
            if (GUI.Button(new Rect(minimized.width - 20, 2, 15, 15), "+"))
            {
                isMinimized = false;
            }
            GUILayout.EndHorizontal();
        }
    }
}