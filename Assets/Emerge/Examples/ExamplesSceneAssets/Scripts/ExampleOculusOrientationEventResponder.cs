﻿using UnityEngine;

public class ExampleOculusOrientationEventResponder : MonoBehaviour
{
    [SerializeField]
    private TactileHardwareVisualizer tactileHardwareVisualizer;

    void Start()
    {
        AnchorSource.Instance.OnOculusOrientationLocked += HandleOculusOrientationLocked;
        AnchorSource.Instance.OnOculusOrientationUnlocked += HandleOculusOrientationUnlocked;
    }

    private void HandleOculusOrientationUnlocked()
    {
        tactileHardwareVisualizer?.ShowHideHardwareConfiguration(true);
    }

    private void HandleOculusOrientationLocked()
    {
        tactileHardwareVisualizer?.ShowHideHardwareConfiguration(false);
    } 
}
