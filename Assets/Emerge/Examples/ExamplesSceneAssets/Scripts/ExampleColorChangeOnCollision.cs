﻿using Emerge.Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Emerge.Core
{
    public class ExampleColorChangeOnCollision : MonoBehaviour
    {
        private Renderer renderer;
        private AbstractTactileBehavior tactileBehavior;

        void Start()
        {
            renderer = GetComponent<Renderer>();
            tactileBehavior = GetComponent<AbstractTactileBehavior>();

            tactileBehavior.CollisionPhaseChanged += HandleCollisionChanged;
        }

        private void HandleCollisionChanged(AbstractTactileBehavior obj)
        {
            if (tactileBehavior.CollisionIsActive)
            {
                renderer.material.color = Color.green;
            }
            else
            {
                renderer.material.color = Color.red;
            }
        } 
    }
}
