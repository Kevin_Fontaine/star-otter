﻿using System.Collections;
using System.Collections.Generic;
using Leap.Unity.Interaction;
using UnityEngine;

public class DebugAutoRotate : MonoBehaviour
{
    public Vector3 eulerRotationPerSecond; 
 
    void Update()
    { 
        transform.Rotate(eulerRotationPerSecond * Time.deltaTime);
    }
}
