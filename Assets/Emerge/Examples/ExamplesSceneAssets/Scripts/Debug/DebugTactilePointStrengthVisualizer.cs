﻿using System.Collections;
using System.Collections.Generic;
using Emerge.Core;
using UnityEngine;

public class DebugTactilePointStrengthVisualizer : MonoBehaviour
{
    public Transform visual;
    public TactilePoint tactilePoint;
      
    private Renderer pointRenderer;

    void Start()
    { 
        pointRenderer = visual.GetComponent<Renderer>();
    }

    private void Update()
    {
        float colorScale = tactilePoint.Strength / 100.0f;

        pointRenderer.material.color = new Color(1.0f - colorScale, 1.0f - colorScale, colorScale);
    }
}
