﻿using Emerge.Core;
using UnityEngine;

public class DebugTactilePointCollisionVisualizer : MonoBehaviour
{
    public Transform visual;
    public TactilePoint tactilePoint;

    private Renderer contactRenderer;
    
    void Start()
    {
        tactilePoint.CollisionPhaseChanged += HandleCollisionPhaseChanged;
        contactRenderer = visual.GetComponent<Renderer>();
    }

    private void HandleCollisionPhaseChanged(AbstractTactileBehavior obj)
    {
        if (obj.CollisionIsActive)
        {
            contactRenderer.material.color = Color.green;
        }
        else
        {
            contactRenderer.material.color = Color.red;
        }
    }
}
