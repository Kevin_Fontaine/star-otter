﻿using System.Collections;
using System.Collections.Generic;
using Emerge.Core;
using UnityEngine;

public class DebugTactilePointsScatter : MonoBehaviour
{
    [SerializeField]
    private List<TactilePoint> tactilePoints;
    public List<TactilePoint> TactilePoints { get; set; }

    [Range(0, 0.05f)] 
    public float radiusScale;

    private Dictionary<Emerge.Core.TactilePoint, Vector3> unitLocalPositions;

    void Start()
    {
        unitLocalPositions = new Dictionary<TactilePoint, Vector3>();

        foreach (Emerge.Core.TactilePoint tp in tactilePoints)
        {
            float angle = (Random.value * 360.0f) * Mathf.Deg2Rad;
            float x = Mathf.Cos(angle);
            float z = Mathf.Sin(angle); 

            Vector3 unitLocalPosition = new Vector3(x, 0, z);
            unitLocalPositions.Add(tp, unitLocalPosition);
        }
    }

    void Update()
    {
        foreach (Emerge.Core.TactilePoint tp in tactilePoints)
        {
            Vector3 placementLocalPosition;
            unitLocalPositions.TryGetValue(tp, out placementLocalPosition);
            tp.transform.localPosition = placementLocalPosition * radiusScale + Vector3.up * 0.15f;
        }
    }
}
