﻿using UnityEngine;

public class ExampleHardwareToggle : MonoBehaviour
{
    [Header("Press H Key to Show/Hide Hardware.")]
    [SerializeField]
    private TactileHardwareVisualizer tactileHardwareVisualizer;
     
    public void Start()
    { 
        // empty
    }

    public void Update()
    {  
        // Test Show/Hide hardware from Editor using keyboard
        if (Input.GetKeyDown(KeyCode.H))
        { 
            tactileHardwareVisualizer?.ShowHideHardwareConfiguration(!tactileHardwareVisualizer.HardwareConfiguration.gameObject.activeInHierarchy);
        }
    } 
}
