﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Emerge.Core;

public class ExampleEmergeObjectAccess : MonoBehaviour
{

    // AnimationEmergeObject obj;
    TactileMesh tactileMeshObj;

    const string animationObjectName = "spiral";
    const string meshObjectName = "bunny";
   

    void Start()
    {
        //Object will be added to Emerge Object Manager only when it becomes active in scene 
        // obj = EmergeObjectsManager.Instance.GetEmergeAnimationObject(animationObjectName);
        tactileMeshObj = TactileManager.Instance.GetEmergeMeshObject(meshObjectName); 
    }

    
    void Update()
    { 
        //if(Input.GetKeyUp(KeyCode.A))
        //{
        //    if (obj != null)
        //    {
        //        Debug.Log("Success : Found Emerge Object Method !!!!");
        //        Debug.Log("Tactile Method = Animation");
        //        Debug.Log("Tactile Animation " + obj.GetTactileAnimation());
        //        Debug.Log("Tactile ObjectID " + obj.Id);
        //    }
        //}
        //if (obj == null)
        //{
        //    obj = EmergeObjectsManager.Instance.GetEmergeAnimationObject(animationObjectName);
        //    if (obj != null)
        //    {
        //        Debug.Log("Success : Found Emerge Object Method !!!!");
        //        Debug.Log("Tactile Method = Animation");
        //        Debug.Log("Tactile Animation " + obj.GetTactileAnimation());
        //        Debug.Log("Tactile ObjectID " + obj.Id);
        //    }
        //}
        if (tactileMeshObj == null)
        {
            tactileMeshObj = TactileManager.Instance.GetEmergeMeshObject(meshObjectName);
            if (tactileMeshObj != null)
            {
                Debug.Log("Success : Found Emerge Object Method !!!!");
                Debug.Log("Tactile Method = Mesh");
                Debug.Log("Tactile ObjectID " + tactileMeshObj.Id);
            }
        }
    }
}
