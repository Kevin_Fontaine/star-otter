﻿using Emerge.Core;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class ExampleOnScreenCollisionStatusDisplay : MonoBehaviour
{
    [SerializeField] 
    private AbstractTactileBehavior tactileBehavior;

    [SerializeField] 
    private Text text;

    private float lastCollisionBeginTime;
    private float lastCollisionSustainTime;
    private float lastCollisionEndTime;

    private void Start()
    {
        tactileBehavior.CollisionPhaseChanged += HandleCollisionPhaseChanged;
    }

    private void HandleCollisionPhaseChanged(AbstractTactileBehavior obj)
    {
        if (tactileBehavior.CollisionPhase == CollisionPhase.CollisionBegin)
        {
            lastCollisionBeginTime = Time.time;
        }
        else if (tactileBehavior.CollisionPhase == CollisionPhase.CollisionSustain)
        {
            lastCollisionSustainTime = Time.time;
        }
        else if (tactileBehavior.CollisionPhase == CollisionPhase.CollisionEnd)
        {
            lastCollisionEndTime = Time.time;
        } 
    }

    void Update()
    {
        if (text != null && tactileBehavior != null)
        {
            StringBuilder displayString = new StringBuilder();
            
            displayString.Append("Active Collision: ");
            displayString.Append(tactileBehavior.CollisionIsActive.ToString());
            displayString.Append("\n");

            displayString.Append("Last Collision Begin: ");
            displayString.Append(lastCollisionBeginTime.ToString("F3"));
            displayString.Append("\n");

            displayString.Append("Last Collision Sustain: ");
            displayString.Append(lastCollisionSustainTime.ToString("F3"));
            displayString.Append("\n");

            displayString.Append("Last Collision End: ");
            displayString.Append(lastCollisionEndTime.ToString("F3"));
            displayString.Append("\n");

            text.text = displayString.ToString();
        }
    }
}
