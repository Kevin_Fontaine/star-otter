﻿/******************************************************************************
 * Copyright (C) Emerge Now, Inc. 2016-2019.                                  *
 * Emerge Now Inc proprietary and confidential.                               *
 *                                                                            *
 * Use subject to the terms of the Emerge Exploration Program Agreement       *
 * and/or terms of limited, non-exclusive, non-sublicensable license to       *
 * Emerge SDK between Emerge and you, your company or any other organization. *
 ******************************************************************************/
// CleanPass

using System.Collections;
using Emerge.Core;
using UnityEngine;

// Disables warning about private fields that marked SerializeField but appear to be never assigned. 
#pragma warning disable 649

[RequireComponent(typeof (TactileEffect))]
public class ExampleCustomEffectsController : MonoBehaviour
{
    private bool alternate;

    [SerializeField]
    private TactileEffect tactileEffect;

    [SerializeField]
    private BasicPlaneWaveProperties basicPlaneWaveProperties;
     
    [SerializeField]
    private ExpandingSphereProperties expandingSphereProperties;

    private int localAmplitude = 100; 
    private float localWaveSpeed = 1;

    private void Start()
    {
        StartCoroutine("AlternateEffects");

        tactileEffect.SetCurrentEffect(basicPlaneWaveProperties);
    }

    private void Update()
    {  
        if (tactileEffect.GetCurrentEffect().GetTactileType() == TactileEffectType.basic_plane_wave)
        { 
            basicPlaneWaveProperties.Wavespeed.Value = localWaveSpeed;
            basicPlaneWaveProperties.Amplitude.Value = localAmplitude;
        }
        else { expandingSphereProperties.Amplitude.Value = localAmplitude; }

        localWaveSpeed += 1.0f;
        localAmplitude += 1;
        if (localWaveSpeed > 10.0f) { localWaveSpeed = 0.0f; }
        if (localAmplitude > 100) { localAmplitude = 0; }
    }

    private IEnumerator AlternateEffects()
    {
        while (true)
        {
            yield return new WaitForSeconds(5);

            alternate = !alternate;
            if (alternate) { tactileEffect.SetCurrentEffect(basicPlaneWaveProperties); }
            else { tactileEffect.SetCurrentEffect(expandingSphereProperties); }
        }
    }
}