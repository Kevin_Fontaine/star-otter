﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Emerge.Communication;

public class ExampleOwnershipTransfer : MonoBehaviour
{ 
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.O))
        {
            OwnershipManager.Instance.TakeOwnership();
        }
    }
}
