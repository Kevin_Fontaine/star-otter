﻿using System.Collections.Generic;
using Emerge.Core;
using UnityEngine;
 
public class ExampleSnakeWave : MonoBehaviour
{
    public List<TactilePoint> tactilePoints;

    [Range(0, 100.0f)]
    public float Strength = 100.0f;

    [Range(0, 10.0f)]
    public float AnimateCyclesPerSecond;

    private float TwoPi = 2.0f * 3.14159f;

    private void Start()
    {
        if (tactilePoints == null || tactilePoints.Count == 0)
        {
            tactilePoints = new List<TactilePoint>();

            TactilePoint[] tactilePointsArray = transform.GetComponentsInChildren<TactilePoint>();
            foreach (TactilePoint tactilePoint in tactilePointsArray)
            {
                tactilePoints.Add(tactilePoint);
            }
        }
    }

    void Update()
    {
        float value = Time.time * AnimateCyclesPerSecond * TwoPi;

        int len = tactilePoints.Count;
        for (int i = 0; i < len; i++)
        {
            TactilePoint tactilePoint = tactilePoints[i];

            tactilePoint.Strength = Strength;

            float x = -0.05f + (0.015f * i);
            float y = 0.14f;
            float z = -0.05f + ((Mathf.Sin(value + TwoPi / len * i) + 1.0f) / 2.0f) * 0.05f;
            tactilePoint.transform.position = new Vector3(x,y,z);
        }
    }
}
