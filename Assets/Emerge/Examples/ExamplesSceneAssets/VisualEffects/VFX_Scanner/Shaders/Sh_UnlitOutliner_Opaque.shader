// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Haptics/Sh_UnlitOutliner_Opaque"
{
	Properties
	{
		_MainTexture("MainTexture", 2D) = "white" {}
		_Grad_Radial_B("Grad_Radial_B", 2D) = "white" {}
		_ExponentRadialOffset("ExponentRadialOffset", Float) = 0
		_ExponentTopOffset("ExponentTopOffset", Float) = 0
		_ExponentBottomOffset("ExponentBottomOffset", Float) = 0
		_BottomOffsetMultiplier("BottomOffsetMultiplier", Float) = 0
		_TopOffsetMultiplier("TopOffsetMultiplier", Float) = 0
		_RadialOffsetMultiplier("RadialOffsetMultiplier", Float) = 0
		_Speed("Speed", Float) = 0
		_RadialOffsetSinOffset("RadialOffsetSinOffset", Float) = 0
		_RadialOffsetSinInput("RadialOffsetSinInput", Float) = 0
		_TopOffsetSinTimeOffset("TopOffsetSinTimeOffset", Float) = 0
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
	}
	
	SubShader
	{
		Tags { "RenderType"="Transparent" "Queue"="Transparent" }
		LOD 100
		CGINCLUDE
		#pragma target 3.0
		ENDCG
		Blend One One
		Cull Back
		ColorMask RGBA
		ZWrite Off
		ZTest LEqual
		Offset 0 , 0
		
		

		Pass
		{
			Name "Unlit"
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_instancing
			#include "UnityCG.cginc"
			#include "UnityShaderVariables.cginc"


			struct appdata
			{
				float4 vertex : POSITION;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				float4 ase_texcoord : TEXCOORD0;
				float4 ase_texcoord1 : TEXCOORD1;
				float3 ase_normal : NORMAL;
				float4 ase_color : COLOR;
			};
			
			struct v2f
			{
				float4 vertex : SV_POSITION;
				float4 ase_color : COLOR;
				float4 ase_texcoord : TEXCOORD0;
				UNITY_VERTEX_OUTPUT_STEREO
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			uniform sampler2D _Grad_Radial_B;
			uniform float _ExponentBottomOffset;
			uniform float _BottomOffsetMultiplier;
			uniform float _ExponentRadialOffset;
			uniform float _RadialOffsetMultiplier;
			uniform float _Speed;
			uniform float _RadialOffsetSinInput;
			uniform float _RadialOffsetSinOffset;
			uniform float _ExponentTopOffset;
			uniform float _TopOffsetMultiplier;
			uniform float _TopOffsetSinTimeOffset;
			uniform sampler2D _MainTexture;
			uniform float4 _MainTexture_ST;
			
			v2f vert ( appdata v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
				UNITY_TRANSFER_INSTANCE_ID(v, o);

				float2 uv95 = v.ase_texcoord * float2( 1,1 ) + float2( 0,0 );
				float2 uv45 = v.ase_texcoord1 * float2( 1,1 ) + float2( 0,0 );
				float4 tex2DNode87 = tex2Dlod( _Grad_Radial_B, float4( uv45, 0, 0.0) );
				float mulTime110 = _Time.y * _Speed;
				float2 uv122 = v.ase_texcoord.xy * float2( 1,1 ) + float2( 0,0 );
				
				o.ase_color = v.ase_color;
				o.ase_texcoord.xy = v.ase_texcoord.xy;
				
				//setting value to unused interpolator channels and avoid initialization warnings
				o.ase_texcoord.zw = 0;
				
				v.vertex.xyz += ( ( ( 1.0 - uv95.y ) * pow( tex2DNode87.r , _ExponentBottomOffset ) * float3(0,1,0) * _BottomOffsetMultiplier ) + ( pow( tex2DNode87.r , _ExponentRadialOffset ) * v.ase_normal * _RadialOffsetMultiplier * sin( ( ( frac( ( tex2DNode87.r + mulTime110 ) ) * _RadialOffsetSinInput ) - _RadialOffsetSinOffset ) ) ) + ( float3( 0,0,0 ) * float3( 0,0,0 ) * uv122.y * pow( tex2DNode87.r , _ExponentTopOffset ) * float3(0,1,0) * _TopOffsetMultiplier * sin( ( ( frac( ( tex2DNode87.r + mulTime110 + _TopOffsetSinTimeOffset ) ) * _RadialOffsetSinInput ) - _RadialOffsetSinOffset ) ) ) );
				o.vertex = UnityObjectToClipPos(v.vertex);
				return o;
			}
			
			fixed4 frag (v2f i ) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID(i);
				fixed4 finalColor;
				float2 uv_MainTexture = i.ase_texcoord.xy * _MainTexture_ST.xy + _MainTexture_ST.zw;
				
				
				finalColor = ( i.ase_color.a * tex2D( _MainTexture, uv_MainTexture ) );
				return finalColor;
			}
			ENDCG
		}
	}
	CustomEditor "ASEMaterialInspector"
	
	
}
/*ASEBEGIN
Version=16200
-1227;760;1220;1069;2284.156;865.2081;1.764999;True;True
Node;AmplifyShaderEditor.RangedFloatNode;111;-2676.332,-43.76251;Float;False;Property;_Speed;Speed;8;0;Create;True;0;0;False;0;0;0.75;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;45;-2886.272,-256.1664;Float;False;1;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleTimeNode;110;-2473.332,-45.76251;Float;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;87;-2608.149,-274.9775;Float;True;Property;_Grad_Radial_B;Grad_Radial_B;1;0;Create;True;0;0;False;0;fe2be4cc1ff9b1746894ca30bc8bfe03;fe2be4cc1ff9b1746894ca30bc8bfe03;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;134;-2538.358,347.2746;Float;False;Property;_TopOffsetSinTimeOffset;TopOffsetSinTimeOffset;11;0;Create;True;0;0;False;0;0;0.33;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;105;-1768.758,-105.2763;Float;False;1509.774;798.9183;Radial Offset;10;101;102;98;100;99;112;113;114;115;117;;1,1,1,1;0;0
Node;AmplifyShaderEditor.SimpleAddOpNode;132;-2230.742,281.674;Float;False;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;109;-2252.332,-117.7625;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;114;-1748.944,198.5852;Float;False;Property;_RadialOffsetSinInput;RadialOffsetSinInput;10;0;Create;True;0;0;False;0;0;12;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.FractNode;133;-2096.564,281.9148;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FractNode;61;-2118.154,-117.5217;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;120;-1772.514,728.8707;Float;False;1509.667;766.2907;TopOffset;9;127;126;125;123;122;121;131;130;129;;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;117;-1559.88,317.0746;Float;False;Property;_RadialOffsetSinOffset;RadialOffsetSinOffset;9;0;Create;True;0;0;False;0;0;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;129;-1499.137,921.5124;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;4;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;104;-1770.527,-912.2779;Float;False;1509.667;766.2907;BottomOffset;7;92;95;96;91;94;97;93;;1,1,1,1;0;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;113;-1450.339,68.34613;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;4;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;121;-1299.365,1313.623;Float;False;Property;_ExponentTopOffset;ExponentTopOffset;3;0;Create;True;0;0;False;0;0;6.41;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;130;-1295.767,1006.991;Float;False;2;0;FLOAT;0;False;1;FLOAT;0.5;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;95;-1210.847,-862.2779;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;92;-1297.378,-327.5255;Float;False;Property;_ExponentBottomOffset;ExponentBottomOffset;4;0;Create;True;0;0;False;0;0;6.5;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;115;-1246.969,153.8248;Float;False;2;0;FLOAT;0;False;1;FLOAT;0.5;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;100;-1240.358,-15.65379;Float;False;Property;_ExponentRadialOffset;ExponentRadialOffset;2;0;Create;True;0;0;False;0;0;-0.02;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;125;-891.4759,1147.209;Float;False;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SinOpNode;131;-1085.548,923.9836;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;122;-903.7236,802.1598;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.Vector3Node;126;-704.6968,1199.765;Float;False;Constant;_Vector2;Vector 2;3;0;Create;True;0;0;False;0;0,1,0;0,1,0;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RangedFloatNode;102;-877.3609,375.9938;Float;False;Property;_RadialOffsetMultiplier;RadialOffsetMultiplier;7;0;Create;True;0;0;False;0;0;0.0015;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.Vector3Node;94;-702.7102,-441.3837;Float;False;Constant;_Vector1;Vector 1;3;0;Create;True;0;0;False;0;0,1,0;0,1,0;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RangedFloatNode;123;-673.5786,1380.161;Float;False;Property;_TopOffsetMultiplier;TopOffsetMultiplier;6;0;Create;True;0;0;False;0;0;5.62;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;97;-671.592,-260.9874;Float;False;Property;_BottomOffsetMultiplier;BottomOffsetMultiplier;5;0;Create;True;0;0;False;0;0;0.1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;96;-977.1812,-815.4379;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SinOpNode;112;-1036.75,70.81731;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;91;-889.4893,-493.9401;Float;False;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;99;-863.4764,-42.53672;Float;False;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.NormalVertexDataNode;98;-875.5317,224.474;Float;False;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;93;-429.8604,-583.4805;Float;False;4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SamplerNode;1;263.6796,-875.5071;Float;True;Property;_MainTexture;MainTexture;0;0;Create;True;0;0;False;0;b9c4358eb8c4e9e4396959d24c50831c;83af6ef698d138b4698011796c27a462;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;127;-431.8472,1057.668;Float;False;7;7;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT3;0,0,0;False;5;FLOAT;0;False;6;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.VertexColorNode;42;406.8891,-1115.051;Float;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;101;-427.9837,-55.2763;Float;False;4;4;0;FLOAT;0;False;1;FLOAT3;0,0,0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleAddOpNode;103;124.6332,-304.4843;Float;False;3;3;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;43;728.6531,-873.3477;Float;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;16;1298.531,-539.0609;Half;False;True;2;Half;ASEMaterialInspector;0;1;Haptics/Sh_UnlitOutliner_Opaque;0770190933193b94aaa3065e307002fa;0;0;Unlit;2;True;4;1;False;-1;1;False;-1;0;5;False;-1;1;False;-1;True;0;False;-1;0;False;-1;True;False;True;0;False;-1;True;True;True;True;True;0;False;-1;True;False;255;False;-1;255;False;-1;255;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;True;2;False;-1;True;3;False;-1;True;True;0;False;-1;0;False;-1;True;2;RenderType=Transparent=RenderType;Queue=Transparent=Queue=0;True;2;0;False;False;False;False;False;False;False;False;False;False;False;0;;0;0;Standard;0;2;0;FLOAT4;0,0,0,0;False;1;FLOAT3;0,0,0;False;0
WireConnection;110;0;111;0
WireConnection;87;1;45;0
WireConnection;132;0;87;1
WireConnection;132;1;110;0
WireConnection;132;2;134;0
WireConnection;109;0;87;1
WireConnection;109;1;110;0
WireConnection;133;0;132;0
WireConnection;61;0;109;0
WireConnection;129;0;133;0
WireConnection;129;1;114;0
WireConnection;113;0;61;0
WireConnection;113;1;114;0
WireConnection;130;0;129;0
WireConnection;130;1;117;0
WireConnection;115;0;113;0
WireConnection;115;1;117;0
WireConnection;125;0;87;1
WireConnection;125;1;121;0
WireConnection;131;0;130;0
WireConnection;96;0;95;2
WireConnection;112;0;115;0
WireConnection;91;0;87;1
WireConnection;91;1;92;0
WireConnection;99;0;87;1
WireConnection;99;1;100;0
WireConnection;93;0;96;0
WireConnection;93;1;91;0
WireConnection;93;2;94;0
WireConnection;93;3;97;0
WireConnection;127;2;122;2
WireConnection;127;3;125;0
WireConnection;127;4;126;0
WireConnection;127;5;123;0
WireConnection;127;6;131;0
WireConnection;101;0;99;0
WireConnection;101;1;98;0
WireConnection;101;2;102;0
WireConnection;101;3;112;0
WireConnection;103;0;93;0
WireConnection;103;1;101;0
WireConnection;103;2;127;0
WireConnection;43;0;42;4
WireConnection;43;1;1;0
WireConnection;16;0;43;0
WireConnection;16;1;103;0
ASEEND*/
//CHKSM=0E3D01BF351031E09638DA71D9B374CE20FB7FFC