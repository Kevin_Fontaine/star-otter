// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Haptics/TEMP_UnlitDissolve_01"
{
	Properties
	{
		_ColorWide("Color Wide", Color) = (0,0,0,0)
		_PixelTiles_RandomLuminance("PixelTiles_RandomLuminance", 2D) = "white" {}
		_TimeSpeed("Time Speed", Float) = 0
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
	}
	
	SubShader
	{
		Tags { "RenderType"="Transparent" "Queue"="Transparent" }
		LOD 100
		CGINCLUDE
		#pragma target 3.0
		ENDCG
		Blend One One , One One
		Cull Off
		ColorMask RGBA
		ZWrite Off
		ZTest LEqual
		Offset 0 , 0
		
		

		Pass
		{
			Name "Unlit"
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_instancing
			#include "UnityCG.cginc"
			#include "UnityShaderVariables.cginc"


			struct appdata
			{
				float4 vertex : POSITION;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				float4 ase_texcoord : TEXCOORD0;
			};
			
			struct v2f
			{
				float4 vertex : SV_POSITION;
				float4 ase_texcoord : TEXCOORD0;
				UNITY_VERTEX_OUTPUT_STEREO
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			uniform float4 _ColorWide;
			uniform sampler2D _PixelTiles_RandomLuminance;
			uniform float4 _PixelTiles_RandomLuminance_ST;
			uniform float _TimeSpeed;
			
			v2f vert ( appdata v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
				UNITY_TRANSFER_INSTANCE_ID(v, o);

				o.ase_texcoord.xy = v.ase_texcoord.xy;
				
				//setting value to unused interpolator channels and avoid initialization warnings
				o.ase_texcoord.zw = 0;
				
				v.vertex.xyz +=  float3(0,0,0) ;
				o.vertex = UnityObjectToClipPos(v.vertex);
				return o;
			}
			
			fixed4 frag (v2f i ) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID(i);
				fixed4 finalColor;
				float2 uv_PixelTiles_RandomLuminance = i.ase_texcoord.xy * _PixelTiles_RandomLuminance_ST.xy + _PixelTiles_RandomLuminance_ST.zw;
				float mulTime42 = _Time.y * _TimeSpeed;
				float clampResult47 = clamp( (0.0 + (abs( sin( frac( ( tex2D( _PixelTiles_RandomLuminance, uv_PixelTiles_RandomLuminance ).r + mulTime42 ) ) ) ) - 0.75) * (1.0 - 0.0) / (0.9 - 0.75)) , 0.0 , 1.0 );
				float4 temp_output_44_0 = ( _ColorWide * clampResult47 );
				
				
				finalColor = temp_output_44_0;
				return finalColor;
			}
			ENDCG
		}
	}
	CustomEditor "ASEMaterialInspector"
	
	
}
/*ASEBEGIN
Version=16200
-1382;-1;1374;1130;888.6965;265.6932;1;True;False
Node;AmplifyShaderEditor.RangedFloatNode;43;-601.143,616.8654;Float;False;Property;_TimeSpeed;Time Speed;18;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;38;-585.6172,368.5009;Float;True;Property;_PixelTiles_RandomLuminance;PixelTiles_RandomLuminance;17;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleTimeNode;42;-349.1426,568.8655;Float;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;39;-119.3432,427.1363;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FractNode;45;68.95477,417.0859;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SinOpNode;48;94.25996,547.2269;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.AbsOpNode;49;270.1909,596.632;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCRemapNode;46;390.6917,278.5099;Float;False;5;0;FLOAT;0;False;1;FLOAT;0.75;False;2;FLOAT;0.9;False;3;FLOAT;0;False;4;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;4;-207.5,-304;Float;False;Property;_ColorWide;Color Wide;1;0;Create;True;0;0;False;0;0,0,0,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ClampOpNode;47;675.0737,352.0155;Float;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;25;-129.4895,-485.1503;Float;False;Property;_ColorNarrow;Color Narrow;2;0;Create;True;0;0;False;0;0,0,0,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;14;535.5632,-314.4263;Float;False;3;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;32;-122.634,-943.684;Float;False;Property;_ColorCore;Color Core;3;0;Create;True;0;0;False;0;0,0,0,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;3;102.5,-131;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.ClampOpNode;33;-165.4208,-1137.701;Float;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.ClampOpNode;9;-109.9333,-97.23798;Float;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;35;731.7272,-165.6632;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;20;-863.1859,-474.0224;Float;False;Property;_NarrowOutMax;Narrow Out Max;11;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;5;-796.933,-54.23796;Float;False;Property;_WideInMin;Wide In Min;4;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;28;-743.8699,-1183.828;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;21;-750.7254,-725.2943;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;34;153.4225,-1152.062;Float;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.TFHCRemapNode;22;-469.1868,-791.0226;Float;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;0;False;4;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;19;-908.1859,-566.0226;Float;False;Property;_NarrowOutMin;Narrow Out Min;10;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;6;-780.933,43.76202;Float;False;Property;_WideInMax;Wide In Max;5;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;37;887.7272,-34.66324;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;30;-901.3303,-1024.556;Float;False;Property;_CoreOutMin;Core Out Min;14;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;18;-963.1859,-644.0226;Float;False;Property;_NarrowInMax;Narrow In Max;9;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;1;-861.933,-292.238;Float;True;Property;_GradientCells;GradientCells;0;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;7;-725.933,121.762;Float;False;Property;_WideOutMin;Wide Out Min;6;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;26;-956.3303,-1102.556;Float;False;Property;_CoreInMax;Core In Max;13;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;29;-856.3303,-932.556;Float;False;Property;_CoreOutMax;Core Out Max;15;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCRemapNode;31;-462.3313,-1249.556;Float;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;0;False;4;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;8;-680.933,213.762;Float;False;Property;_WideOutMax;Wide Out Max;7;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;24;146.5669,-693.5284;Float;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.TFHCRemapNode;2;-286.9334,-103.238;Float;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;0;False;4;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;27;-972.3303,-1200.556;Float;False;Property;_CoreInMin;Core In Min;12;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;17;-979.1859,-742.0226;Float;False;Property;_NarrowInMin;Narrow In Min;8;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;36;449.7272,-145.6632;Float;False;Property;_ShutoffGradients;Shut off Gradients;16;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;44;732.2008,96.39631;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;10;-568.4725,-37.5097;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ClampOpNode;23;-172.2763,-679.1675;Float;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;16;1101.958,36.73546;Float;False;True;2;Float;ASEMaterialInspector;0;1;Haptics/TEMP_UnlitDissolve_01;0770190933193b94aaa3065e307002fa;0;0;Unlit;2;True;4;1;False;-1;1;False;-1;4;1;False;-1;1;False;-1;True;0;False;-1;0;False;-1;True;False;True;2;False;-1;True;True;True;True;True;0;False;-1;True;False;255;False;-1;255;False;-1;255;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;True;2;False;-1;True;3;False;-1;True;True;0;False;-1;0;False;-1;True;2;RenderType=Transparent=RenderType;Queue=Transparent=Queue=0;True;2;0;False;False;False;False;False;False;False;False;False;False;False;0;;0;0;Standard;0;2;0;FLOAT4;0,0,0,0;False;1;FLOAT3;0,0,0;False;0
WireConnection;42;0;43;0
WireConnection;39;0;38;1
WireConnection;39;1;42;0
WireConnection;45;0;39;0
WireConnection;48;0;45;0
WireConnection;49;0;48;0
WireConnection;46;0;49;0
WireConnection;47;0;46;0
WireConnection;14;0;24;0
WireConnection;14;1;3;0
WireConnection;14;2;34;0
WireConnection;3;0;4;0
WireConnection;3;1;9;0
WireConnection;33;0;31;0
WireConnection;9;0;2;0
WireConnection;35;0;14;0
WireConnection;35;1;36;0
WireConnection;28;0;27;0
WireConnection;28;1;26;0
WireConnection;21;0;17;0
WireConnection;21;1;18;0
WireConnection;34;0;33;0
WireConnection;34;1;32;0
WireConnection;22;0;1;1
WireConnection;22;1;17;0
WireConnection;22;2;21;0
WireConnection;22;3;19;0
WireConnection;22;4;20;0
WireConnection;37;0;35;0
WireConnection;37;1;44;0
WireConnection;31;0;1;1
WireConnection;31;1;27;0
WireConnection;31;2;28;0
WireConnection;31;3;30;0
WireConnection;31;4;29;0
WireConnection;24;0;23;0
WireConnection;24;1;25;0
WireConnection;2;0;1;1
WireConnection;2;1;5;0
WireConnection;2;2;10;0
WireConnection;2;3;7;0
WireConnection;2;4;8;0
WireConnection;44;0;4;0
WireConnection;44;1;47;0
WireConnection;10;0;5;0
WireConnection;10;1;6;0
WireConnection;23;0;22;0
WireConnection;16;0;44;0
ASEEND*/
//CHKSM=ABCA2ACB534BD3DA6AC7E99F5E3CCDE40A4AC4DB