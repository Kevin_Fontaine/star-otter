// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Haptics/Sh_OutlineVertOffset"
{
	Properties
	{
		_Color0("Color 0", Color) = (0,0,0,0)
		_InteriorOffset("Interior Offset", Float) = 0
		_SineFrequency("Sine Frequency", Float) = 3
		_InteriorSpeed("InteriorSpeed", Float) = 0
		_Remap_InMin("Remap_InMin", Float) = 0
		_Remap_InOffset("Remap_InOffset", Float) = 0
		_RandTile72("RandTile72", 2D) = "white" {}
		_ExteriorOffset("Exterior Offset", Float) = 0
		_ExteriorOffsetUVScale("ExteriorOffsetUVScale", Vector) = (0,0,0,0)
		_ExteriorOffsetUVSpeed("ExteriorOffsetUVSpeed", Vector) = (0,0,0,0)
		_ExteriorTiling("Exterior Tiling", Vector) = (0,0,0,0)
	}
	
	SubShader
	{
		Tags { "RenderType"="Opaque" "Queue"="Transparent" }
		LOD 100
		CGINCLUDE
		#pragma target 3.0
		ENDCG
		Blend One One , One One
		Cull Off
		ColorMask RGBA
		ZWrite Off
		ZTest LEqual
		Offset 0 , 0
		
		

		Pass
		{
			Name "Unlit"
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_instancing
			#include "UnityCG.cginc"
			#include "UnityShaderVariables.cginc"


			struct appdata
			{
				float4 vertex : POSITION;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				float3 ase_normal : NORMAL;
				float4 ase_texcoord : TEXCOORD0;
				float4 ase_color : COLOR;
			};
			
			struct v2f
			{
				float4 vertex : SV_POSITION;
				float4 ase_texcoord : TEXCOORD0;
				UNITY_VERTEX_OUTPUT_STEREO
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			uniform float _InteriorOffset;
			uniform float _InteriorSpeed;
			uniform float _SineFrequency;
			uniform sampler2D _RandTile72;
			uniform float2 _ExteriorOffsetUVSpeed;
			uniform float2 _ExteriorOffsetUVScale;
			uniform float _ExteriorOffset;
			uniform float2 _ExteriorTiling;
			uniform float _Remap_InMin;
			uniform float _Remap_InOffset;
			uniform float4 _Color0;
			
			v2f vert ( appdata v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
				UNITY_TRANSFER_INSTANCE_ID(v, o);

				float2 uv13 = v.ase_texcoord * float2( 1,1 ) + float2( 0,0 );
				float2 uv43 = v.ase_texcoord.xy * float2( 1,1 ) + float2( 0,0 );
				float2 panner49 = ( _Time.y * _ExteriorOffsetUVSpeed + ( _ExteriorOffsetUVScale * uv43 ));
				
				o.ase_texcoord.xy = v.ase_texcoord.xy;
				
				//setting value to unused interpolator channels and avoid initialization warnings
				o.ase_texcoord.zw = 0;
				
				v.vertex.xyz += ( ( v.ase_normal * _InteriorOffset * ( ( sin( ( ( _Time.y * _InteriorSpeed ) + ( uv13.x * _SineFrequency ) ) ) + 1.0 ) / 2.0 ) * v.ase_color.r ) + ( tex2Dlod( _RandTile72, float4( panner49, 0, 0.0) ).r * v.ase_normal * v.ase_color.g * _ExteriorOffset ) );
				o.vertex = UnityObjectToClipPos(v.vertex);
				return o;
			}
			
			fixed4 frag (v2f i ) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID(i);
				fixed4 finalColor;
				float2 uv35 = i.ase_texcoord.xy * _ExteriorTiling + float2( 0,0 );
				float temp_output_59_0 = ( 1.0 - uv35.y );
				float clampResult34 = clamp( (0.0 + (( frac( ( 1.0 - uv35.x ) ) * frac( uv35.x ) * ( temp_output_59_0 * temp_output_59_0 ) ) - _Remap_InMin) * (1.0 - 0.0) / (( _Remap_InMin + _Remap_InOffset ) - _Remap_InMin)) , 0.0 , 1.0 );
				
				
				finalColor = ( clampResult34 * _Color0 );
				return finalColor;
			}
			ENDCG
		}
	}
	CustomEditor "ASEMaterialInspector"
	
	
}
/*ASEBEGIN
Version=16200
-1356;1;1349;1135;2817.205;710.7933;2.241021;True;False
Node;AmplifyShaderEditor.RangedFloatNode;26;-1782.174,414.433;Float;False;Property;_InteriorSpeed;InteriorSpeed;3;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;15;-1600.339,645.9888;Float;False;Property;_SineFrequency;Sine Frequency;2;0;Create;True;0;0;False;0;3;3;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.Vector2Node;60;-2174.803,-344.3424;Float;False;Property;_ExteriorTiling;Exterior Tiling;10;0;Create;True;0;0;False;0;0,0;0,0;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.TextureCoordinatesNode;13;-1640.991,497.6689;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleTimeNode;11;-1863.474,297.7809;Float;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;14;-1388.452,627.9355;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;24;-1580.957,358.5394;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;35;-1916.572,-351.7813;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TextureCoordinatesNode;43;-1840.657,945.5479;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.OneMinusNode;47;-1619.527,-390.5486;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.Vector2Node;51;-1858.017,821.4111;Float;False;Property;_ExteriorOffsetUVScale;ExteriorOffsetUVScale;8;0;Create;True;0;0;False;0;0,0;0,0;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.SimpleAddOpNode;16;-1112.459,495.2603;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;59;-1597.789,-235.2497;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;31;-1241.675,-134.1844;Float;False;Property;_Remap_InOffset;Remap_InOffset;5;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;61;-1472.998,-115.5305;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleTimeNode;46;-1735.657,1160.548;Float;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.FractNode;48;-1441.527,-378.5487;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FractNode;38;-1444.256,-290.7397;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;30;-1249.409,-209.9731;Float;False;Property;_Remap_InMin;Remap_InMin;4;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.Vector2Node;54;-1889.017,1071.411;Float;False;Property;_ExteriorOffsetUVSpeed;ExteriorOffsetUVSpeed;9;0;Create;True;0;0;False;0;0,0;0,0;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.SinOpNode;23;-962.0295,497.6632;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;52;-1555.017,909.4111;Float;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleAddOpNode;32;-1028.229,-177.4922;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;36;-1254.011,-307.7536;Float;False;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;21;-819.0292,508.9468;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.PannerNode;49;-1392.017,917.4111;Float;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.VertexColorNode;3;-789.3829,754.3879;Float;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;42;-655.1444,1034.077;Float;False;Property;_ExteriorOffset;Exterior Offset;7;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;9;-862.7312,411.0367;Float;False;Property;_InteriorOffset;Interior Offset;1;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCRemapNode;29;-868.004,-306.3118;Float;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;0;False;4;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;39;-992.0032,948.4562;Float;True;Property;_RandTile72;RandTile72;6;0;Create;True;0;0;False;0;21c9def4c5027c947a3fa8ae2e0fd77f;21c9def4c5027c947a3fa8ae2e0fd77f;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleDivideOpNode;22;-700.0289,509.9468;Float;False;2;0;FLOAT;0;False;1;FLOAT;2;False;1;FLOAT;0
Node;AmplifyShaderEditor.NormalVertexDataNode;27;-866.6106,276.3216;Float;False;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;8;-446.0529,400.0942;Float;False;4;4;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;40;-416.5623,934.4516;Float;False;4;4;0;FLOAT;0;False;1;FLOAT3;0,0,0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ColorNode;2;-664.8685,-187.7357;Float;False;Property;_Color0;Color 0;0;0;Create;True;0;0;False;0;0,0,0,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ClampOpNode;34;-667.211,-308.7525;Float;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;41;-133.4882,476.9424;Float;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;33;-174.6347,-307.2302;Float;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;0;148.4671,49.5763;Float;False;True;2;Float;ASEMaterialInspector;0;1;Haptics/Sh_OutlineVertOffset;0770190933193b94aaa3065e307002fa;0;0;Unlit;2;True;4;1;False;-1;1;False;-1;4;1;False;-1;1;False;-1;True;0;False;-1;0;False;-1;True;False;True;2;False;-1;True;True;True;True;True;0;False;-1;True;False;255;False;-1;255;False;-1;255;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;True;2;False;-1;True;3;False;-1;True;True;0;False;-1;0;False;-1;True;2;RenderType=Opaque=RenderType;Queue=Transparent=Queue=0;True;2;0;False;False;False;False;False;False;False;False;False;False;False;0;;0;0;Standard;0;2;0;FLOAT4;0,0,0,0;False;1;FLOAT3;0,0,0;False;0
WireConnection;14;0;13;1
WireConnection;14;1;15;0
WireConnection;24;0;11;0
WireConnection;24;1;26;0
WireConnection;35;0;60;0
WireConnection;47;0;35;1
WireConnection;16;0;24;0
WireConnection;16;1;14;0
WireConnection;59;0;35;2
WireConnection;61;0;59;0
WireConnection;61;1;59;0
WireConnection;48;0;47;0
WireConnection;38;0;35;1
WireConnection;23;0;16;0
WireConnection;52;0;51;0
WireConnection;52;1;43;0
WireConnection;32;0;30;0
WireConnection;32;1;31;0
WireConnection;36;0;48;0
WireConnection;36;1;38;0
WireConnection;36;2;61;0
WireConnection;21;0;23;0
WireConnection;49;0;52;0
WireConnection;49;2;54;0
WireConnection;49;1;46;0
WireConnection;29;0;36;0
WireConnection;29;1;30;0
WireConnection;29;2;32;0
WireConnection;39;1;49;0
WireConnection;22;0;21;0
WireConnection;8;0;27;0
WireConnection;8;1;9;0
WireConnection;8;2;22;0
WireConnection;8;3;3;1
WireConnection;40;0;39;1
WireConnection;40;1;27;0
WireConnection;40;2;3;2
WireConnection;40;3;42;0
WireConnection;34;0;29;0
WireConnection;41;0;8;0
WireConnection;41;1;40;0
WireConnection;33;0;34;0
WireConnection;33;1;2;0
WireConnection;0;0;33;0
WireConnection;0;1;41;0
ASEEND*/
//CHKSM=59611F89936848F1BE3B441439DFD1E5E3148C82