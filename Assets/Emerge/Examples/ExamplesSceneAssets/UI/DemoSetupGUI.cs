﻿/******************************************************************************
 * Copyright (C) Emerge Now, Inc. 2016-2019.                                  *
 * Emerge Now Inc proprietary and confidential.                               *
 *                                                                            *
 * Use subject to the terms of the Emerge Exploration Program Agreement       *
 * and/or terms of limited, non-exclusive, non-sublicensable license to       *
 * Emerge SDK between Emerge and you, your company or any other organization. *
 ******************************************************************************/
using UnityEngine;
using Photon.Pun;
using Emerge.Communication;
namespace Emerge.Core
{
    public class DemoSetupGUI : MonoBehaviour
    {
        public Rect windowRect = new Rect(0, 0, 400, 400);
        string myIPAddress;

        TactileHardwareController beamformerHardwareConnection;
        SocketController beamformerCommunication;

        void OnGUI()
        {
            GUI.color = Color.gray;
            // Register the window.
            windowRect = GUI.Window(0, windowRect, DoMyWindow, "Emerge Settings");
        } 

        bool validate = true;
        bool maximize = false;
        // Make the contents of the window
        void DoMyWindow(int windowID)
        {
            // Make a very long rect that is 20 pixels tall.
            // This will make the window be resizable by the top
            // title bar - no matter how wide it gets.
            GUI.DragWindow(new Rect(0, 0, windowRect.width, 20));
            GUILayout.BeginHorizontal();
            if (GUI.Button(new Rect(windowRect.width - 50, 20, 20, 20), "-"))
            {
                maximize = false;
                validate = true;
            }
            if (GUI.Button(new Rect(windowRect.width - 70, 20, 20, 20), "+"))
            {
                maximize = true;
                validate = true;
            }
            GUILayout.EndHorizontal();

            float width = windowRect.width;// position.width;

            if (myIPAddress == null)
            {
                myIPAddress = IPAddressUtils.GetIP();
                if (myIPAddress == "no ipv4 available")
                {
                    Debug.LogWarning("Cant find a ipv4 address for this machine");
                }
            }

            //Photon Server Setting setup
            //=====================================================
            photonSettings = GUILayout.Toggle(photonSettings, "Network Setting", GUILayout.ExpandWidth(false));

            if (photonSettings)
            {
                GUILayout.BeginHorizontal(GUILayout.Width(width * 0.9f));

                GUILayout.BeginVertical();

                //DisplayPhotonServerSetting();

                GUILayout.EndVertical();

                GUILayout.EndHorizontal();
            }

            GUILayout.Space(10f);

            //Beamformer setup
            //=====================================================
            showBeamformer = GUILayout.Toggle(showBeamformer, "Emerge Service Comms", GUILayout.ExpandWidth(false));

            if (showBeamformer)
            {
                GUILayout.BeginHorizontal(GUILayout.Width(width * 0.5f));

                GUILayout.BeginVertical();

                DisplayBeamformerSection();

                GUILayout.EndVertical();

                GUILayout.EndHorizontal();
            }

            GUILayout.Space(10f);

            //Offset sliders
            //=====================================================
            //DisplayOffsetSliders();

            //GUILayout.Space(10f);

            GUILayout.BeginHorizontal();

            showDevices = GUILayout.Toggle(showDevices, "Device Connections and Remote Commands", GUILayout.ExpandWidth(false));

            GUILayout.EndHorizontal();
            //showDevices = EditorGUILayout.Foldout(showDevices, "Device Connections and Remote Commands", true);
            if (showDevices)
            {
                //LEAP PCs
                //=====================================================
                GUILayout.BeginHorizontal(GUILayout.Width(width * 0.5f));

                GUILayout.BeginVertical();

                DisplayLEAPPCGUI(1);

                GUILayout.Space(30f);

                GUILayout.EndVertical();
                GUILayout.BeginVertical();

                DisplayLEAPPCGUI(2);

                GUILayout.Space(30f);

                GUILayout.EndVertical();
                GUILayout.EndHorizontal();


                //HOLOLENSES
                //=====================================================
                GUILayout.BeginHorizontal(GUILayout.Width(width));
                //have seen a bug where a hololens is turned off during demo and remains stored in memory here preventing you connecting another one. this button will clear stored hololens and allow re-initiating a UDP connection.
                if (GUILayout.Button("Clear all stored hololens connections and reset"))
                {
                    HeadsetUDPConnection.Instance.Disconnect();
                    Hololens_currentlyBroadcastingUDPto = null;
                }
                GUILayout.EndHorizontal();

                GUILayout.BeginHorizontal(GUILayout.Width(width * 0.5f));

                GUILayout.BeginVertical();
                DisplayHOLOLENSGUI(1);
                GUILayout.EndVertical();

                //GUILayout.EndHorizontal();
                //GUILayout.BeginHorizontal(GUILayout.Width(width * 0.5f));
                GUILayout.BeginVertical();
                DisplayHOLOLENSGUI(2);
                GUILayout.EndVertical();

                GUILayout.EndHorizontal();
            }
            if (validate)
            {
                if (!maximize)
                {
                    windowRect.width = 400;
                    windowRect.height = 50;
                }
                else
                {
                    windowRect.width = 400;
                    windowRect.height = 700;
                }
                validate = false;
            }
        }

        //===============================================================
        // PHOTON SERVER
        bool editPhotoServerSettings = false;
        private void DisplayPhotonServerSetting()
        {
            GUILayout.BeginVertical();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Photon Server Address : ");
            if (!editPhotoServerSettings)
            {
                GUILayout.Label(TactileConfig.Instance.PhotonServerAddress);
            }
            else
            {
                string photonServerAddress = GUILayout.TextField(TactileConfig.Instance.PhotonServerAddress);
                if (GUI.changed)
                {
                    TactileConfig.Instance.PhotonServerAddress = photonServerAddress;
                }
            }
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Seat Number : ");
            if (!editPhotoServerSettings)
            {
                GUILayout.Label(TactileConfig.Instance.SeatNumber.ToString());
            }
            else
            {
                int seatNumber = int.Parse(GUILayout.TextField(TactileConfig.Instance.SeatNumber.ToString()));
                if (GUI.changed)
                {
                    TactileConfig.Instance.SeatNumber = seatNumber;
                }
            }
            GUILayout.EndHorizontal();

            GUILayout.EndVertical();

            GUILayout.BeginVertical();
            GUILayout.EndVertical();
        }

        //===============================================================
        // BEAMFORMER 
        string beamformerStatusString = "Beamformer comms:";
      

        private void DisplayBeamformerSection()
        {
            if (beamformerHardwareConnection == null)
            {
                beamformerHardwareConnection = FindObjectOfType<TactileHardwareController>();
            }

            if (beamformerCommunication == null)
            {
                beamformerCommunication = FindObjectOfType<SocketController>();
            }



            if (beamformerHardwareConnection != null && beamformerCommunication != null)
            {

                if (beamformerHardwareConnection.CommunicationEnabled == false)
                {
                    beamformerStatusString = "Beamformer comms status: disabled";

                    if (GUILayout.Button("Turn ON beamformer connection"))
                    {
                        SetBeamformerCommEnabled(true);
                        string ip = IPAddressUtils.GetIP();
                        SetBeamformerIP(ip);
                    }
                }
                else
                {
                    beamformerStatusString = "Beamformer comms status: enabled and listening at \n" + beamformerCommunication.GetHostIP();
                    if (GUILayout.Button("Turn OFF beamformer connection"))
                    {
                        SetBeamformerCommEnabled(false);
                    }
                }

                GUILayout.Label(beamformerStatusString);

            }
            else
            {
                GUILayout.Label("No beamformer connection found! make sure youre in the right scene");
            }
        }


        //setting serialized properties
        public void SetBeamformerIP(string ip)
        {
            beamformerCommunication.host = ip;
        }


        public void SetBeamformerCommEnabled(bool b)
        {
            beamformerHardwareConnection.CommunicationEnabled = b;
        }

        //===============================================================
        // SECTION TOGGLES
        bool showDevices = true;
        bool showBeamformer;
        bool photonSettings = true;
       

        //===============================================================
        // LEAP 
        public void DisplayLEAPPCGUI(int num)
        {
            GUIStyle boldStyle = new GUIStyle();
            boldStyle.fontStyle = FontStyle.Bold;

            GUIStyle whiteStyle = new GUIStyle();
            whiteStyle.normal.textColor = new Color(1.0f, 1.0f, 1.0f);

            // !Communication.NetworkPlayers.Exists || 
            if (Communication.NetworkPlayers.Instance.LEAPPlayers.Count < num)
            {
                GUILayout.Label("LEAP PC " + num, boldStyle);
                GUILayout.Label("not connected", whiteStyle);

                return;
            }



            LEAPNetworkPlayer p = Communication.NetworkPlayers.Instance.LEAPPlayers[num - 1];
            //string name = PhotonNetwork.playerList[p.photonView.ownerId].NickName;

            string name = p.photonView.Owner.NickName;

            GUILayout.Label("LEAP PC " + name, boldStyle);

            string text;

            if (p.AmILocal())
            {
                GUILayout.Label("LOCAL", whiteStyle);

                text = "IP: " + myIPAddress + "\n";// + "SocketServer connected: " + socketServer.IsClientConnected();

            }
            else
            {
                GUILayout.Label("REMOTE", whiteStyle);

                text = "IP: " + "XXX.XXX.XXX.XXX" + "\n" +
              "-";
            }

            GUILayout.TextField(text);

        }


        //===============================================================
        // HOLOLENS 

        public void DisplayHOLOLENSGUI(int num)
        {
            GUIStyle wordWrapStyle = new GUIStyle();
            wordWrapStyle.wordWrap = true;
            wordWrapStyle.normal.textColor = new Color(1.0f, 1.0f, 1.0f);

            GUIStyle boldStyle = new GUIStyle();
            boldStyle.fontStyle = FontStyle.Bold;
            boldStyle.normal.textColor = new Color(1.0f, 1.0f, 1.0f);

            GUIStyle whiteStyle = new GUIStyle();
            whiteStyle.normal.textColor = new Color(1.0f, 1.0f, 1.0f);

            // !Communication.NetworkPlayers.Exists || 
            if (Communication.NetworkPlayers.Instance.HololensPlayers.Count < num)
            {
                return;
            }

            HololensNetworkPlayer p = Communication.NetworkPlayers.Instance.HololensPlayers[num - 1];
            //string name = PhotonNetwork.playerList[p.photonView.ownerId].NickName;

            string name = p.photonView.Owner.NickName;

            GUILayout.Label("HOLOLENS" + num, boldStyle);
            GUILayout.TextArea("Name: " + name, wordWrapStyle, GUILayout.Width(windowRect.width * 0.5f));
            GUILayout.TextArea("NOTE: can only detect connections for HL that connect to this PC.");

            //-----------------------------------
            //      UDP code testing
            //-----------------------------------

            if (!HeadsetUDPConnection.Instance.IsConnected)
            {
                if (GUILayout.Button("Connect to this Hololens"))
                {
                    GetHololensIP(p);
                }
            }
            else
            {
                if (Hololens_currentlyBroadcastingUDPto == p)
                {

                    if (GUILayout.Button("Stop sending data to Hololens"))
                    {
                        HeadsetUDPConnection.Instance.Disconnect();
                        Hololens_currentlyBroadcastingUDPto = null;
                    }

                    AdditionalConnectedHololensUI(p);
                }


            }
        }

        private void AdditionalConnectedHololensUI(HololensNetworkPlayer p)
        {
            GUILayout.Space(10f);
            if (GUILayout.Button("Toggle hand graphics"))
            {
                ToggleHandGraphics(p);
            }
        }

        


        //RPCs to hololens
        //===============================================================================================
        public void ToggleHandGraphics(HololensNetworkPlayer HL_player)
        {
            HL_player.ToggleHandGraphics(HL_player.photonView.Owner);
        }

        public void ResetTimer(HololensNetworkPlayer HL_player)
        {
            HL_player.ResetTimer(HL_player.photonView.Owner);
        }


        private HololensNetworkPlayer Hololens_currentlyBroadcastingUDPto;
        public void GetHololensIP(HololensNetworkPlayer HL_player)
        {
            string myIP = IPAddressUtils.GetIP();
            byte[] myIP_asByteArray = IPAddressUtils.ConvertIPAddressStringtoBytes(myIP);

            HL_player.SendHololensIP(HL_player.photonView.Owner, PhotonNetwork.LocalPlayer.ActorNumber, myIP_asByteArray);

            Hololens_currentlyBroadcastingUDPto = HL_player;
        }

        //UTILS
        private LEAPNetworkPlayer FindLocalLeapPlayer()
        {
            foreach (LEAPNetworkPlayer leapPlayer in Communication.NetworkPlayers.Instance.LEAPPlayers)
            {
                if (leapPlayer.AmILocal())
                {
                    return leapPlayer;
                    //text = "IP: " + myIPAddress  ;

                }
            }
            return null;

        }

    }
}