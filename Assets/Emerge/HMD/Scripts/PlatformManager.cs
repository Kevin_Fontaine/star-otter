﻿/******************************************************************************
 * Copyright (C) Emerge Now, Inc. 2016-2019.                                  *
 * Emerge Now Inc proprietary and confidential.                               *
 *                                                                            *
 * Use subject to the terms of the Emerge Exploration Program Agreement       *
 * and/or terms of limited, non-exclusive, non-sublicensable license to       *
 * Emerge SDK between Emerge and you, your company or any other organization. *
 ******************************************************************************/
using UnityEngine;

namespace Emerge.Core
{
    public class PlatformManager : MonoBehaviour
    {
        [Tooltip("Hologram representing platform.")]
        public GameObject platformHologram;
        [Tooltip("Display platform on start.")]
        public bool showPlatform;

        private bool previousPlatformState;
        private BoxCollider col;

        void Start()
        {
            if (!platformHologram)
            {
                Debug.LogError("Please attach a 3D GameObject for visualizing the platformHologram.");
            }

            // Grab the collider that's on the same object as this script.
            col = platformHologram.GetComponent<BoxCollider>();
            UpdatePlatformView();
            previousPlatformState = showPlatform;
        }

        public void OnShowPlatform()
        {
            showPlatform = true;
            previousPlatformState = false;
            UpdatePlatformView();
        }

        public void OnHidePlatform()
        {
            showPlatform = false;
            previousPlatformState = true;
            UpdatePlatformView();
        }

        private void UpdatePlatformView()
        {
            if (col)
            {
                col.enabled = showPlatform;
            }
            platformHologram.SetActive(showPlatform);
        }

        public bool IsPlatformVisible()
        {
            return showPlatform;
        }

        public bool WasPlatformPreviouslyVisible()
        {
            return previousPlatformState;
        }

        public void SetPlatformPreviousVisibilityState(bool state)
        {
            previousPlatformState = state;
        }
    }
}