﻿using UnityEngine;

namespace Emerge.Core
{
    public class TargetTrackingLink : MonoBehaviour
    {
        public Vector3 offset;

        private bool isLinked = false;

        private void OnEnable()
        {
            ARCameraManager.OnVuforiaTrackingEnabled += SetLinkActive;
            ARCameraManager.OnVuforiaTrackingDisabled += SetLinkInactive;
        }

        private void OnDisable()
        {
            ARCameraManager.OnVuforiaTrackingEnabled -= SetLinkActive;
            ARCameraManager.OnVuforiaTrackingDisabled -= SetLinkInactive;
        }

        // Update is called once per frame
        void Update()
        {
            if (isLinked)
            {
                UpdateTransform();
            }
        }

        private void SetLinkActive()
        {
            isLinked = true;
        }

        private void SetLinkInactive()
        {
            isLinked = false;
        }

        private void UpdateTransform()
        {
            Quaternion targetRotation = new Quaternion(0, transform.rotation.y, 0, transform.rotation.w);
            Vector3 newPosition = transform.position + offset;
            transform.position = newPosition;
            transform.rotation = targetRotation;
        }
    }
}