﻿/******************************************************************************
 * Copyright (C) Emerge Now, Inc. 2016-2019.                                  *
 * Emerge Now Inc proprietary and confidential.                               *
 *                                                                            *
 * Use subject to the terms of the Emerge Exploration Program Agreement       *
 * and/or terms of limited, non-exclusive, non-sublicensable license to       *
 * Emerge SDK between Emerge and you, your company or any other organization. *
 ******************************************************************************/
//using UnityEngine.XR.WSA.Persistence;// HoloToolkit.Unity;
using System.Collections;
using UnityEngine;

namespace Emerge.Core
{
    public class EmergeAnchorManager : Singleton<EmergeAnchorManager>
    { 
        public float waitForAnchorStoreTime = 10;

        [Header("Sound Clips")]
        public AudioClip anchorSaved;
        public AudioClip anchorRemoved;
        //private WorldAnchorManager worldAnchorManager;
        private EmergeAnchor[] anchors;

        void Start()
        {
            //worldAnchorManager = WorldAnchorManager.Instance;
            //if (!worldAnchorManager)
            //{
            //    //Debug.LogError("This script expects that you have a WorldAnchorManager component in your scene.");
            //}

            anchors = FindObjectsOfType(typeof(EmergeAnchor)) as EmergeAnchor[];

            // We want to load the anchor on this object, but we need the AnchorStore to be ready first
            StartCoroutine(CheckForAnchorStoreReady());
        }

        public void SaveAnchor(GameObject objectToAnchor)
        {
            //if (worldAnchorManager.AnchorStore != null)
            {
                //worldAnchorManager.AttachAnchor(objectToAnchor, objectToAnchor.name);

                PlaySound(anchorSaved);
            }
        }

        public void RemoveAnchor(GameObject objectToUnanchor)
        {
            //if (worldAnchorManager.AnchorStore != null)
            {
                //worldAnchorManager.RemoveAnchor(objectToUnanchor);

                PlaySound(anchorRemoved);
            }
        }

        public void SaveAllAnchors()
        {
            for (int i = 0; i < anchors.Length; i++)
            {
                //worldAnchorManager.AttachAnchor(anchors[i].gameObject, anchors[i].name);
            }

            PlaySound(anchorSaved);
        }

        private IEnumerator CheckForAnchorStoreReady()
        {
            float startTime = Time.time;

            // We'll check once a second for AnchorStore ready during the allotted waitForAnchorStoreTime
            //while (Time.time < startTime + waitForAnchorStoreTime && worldAnchorManager.AnchorStore == null)
            {
                yield return new WaitForSeconds(1);
            }
            AnchorStoreReady();
        }

        private void AnchorStoreReady()
        {
            SaveAllAnchors(); // This also attaches any existing anchors found in Anchor Store per HTK code
        }

        private void PlaySound(AudioClip sound)
        {
            AudioSource source = GetComponent<AudioSource>();
            if (!source || !sound)
            {
                return;
            }

            source.clip = sound;
            source.Play();
        }
    }
}