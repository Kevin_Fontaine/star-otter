﻿/******************************************************************************
 * Copyright (C) Emerge Now, Inc. 2016-2019.                                  *
 * Emerge Now Inc proprietary and confidential.                               *
 *                                                                            *
 * Use subject to the terms of the Emerge Exploration Program Agreement       *
 * and/or terms of limited, non-exclusive, non-sublicensable license to       *
 * Emerge SDK between Emerge and you, your company or any other organization. *
 ******************************************************************************/
using UnityEngine;

namespace Emerge.Core
{
    public class EmergeAnchor : MonoBehaviour
    {

        public bool anchorByVuforia = true;
        public GameObject imageTarget;
        public Vector3 offset;

        private bool isLinked = false;
        private EmergeAnchorManager eam;
        Vector3 OriginalPosition = Vector3.zero;

        private void Start()
        {
            OriginalPosition = transform.position;
            eam = EmergeAnchorManager.Instance;
        }

        private void OnEnable()
        {
            if (anchorByVuforia)
            {
                ARCameraManager.OnVuforiaTrackingEnabled += RemoveAnchor;
                ARCameraManager.OnVuforiaTrackingDisabled += SaveAnchor;
            }
        }

        private void OnDisable()
        {
            if (anchorByVuforia)
            {
                ARCameraManager.OnVuforiaTrackingEnabled -= RemoveAnchor;
                ARCameraManager.OnVuforiaTrackingDisabled -= SaveAnchor;
            }
        }

        public void SaveAnchor()
        {
#if UNITY_WSA
            isLinked = false;

            if (GetComponent<UnityEngine.XR.WSA.WorldAnchor>())
            {
                return;
            }

            eam.SaveAnchor(gameObject);
#endif
        }

        public void RemoveAnchor()
        {
#if UNITY_WSA
            isLinked = true;
            if (!GetComponent<UnityEngine.XR.WSA.WorldAnchor>())
            {
                return;
            }

            eam.RemoveAnchor(gameObject);
#endif
        }

        // Update is called once per frame
        void Update()
        {
            if (isLinked)
            {
                UpdateTransform();
            }
        }

        private void UpdateTransform()
        {
            Quaternion targetRotation = new Quaternion(0, imageTarget.transform.rotation.y, 0, imageTarget.transform.rotation.w);
            //transform.Translate(imageTarget.transform.position + offset, imageTarget.transform);
            var curOffset = imageTarget.transform.rotation * OriginalPosition;
            Vector3 newPosition = curOffset + imageTarget.transform.position;
            transform.position = newPosition;
            transform.rotation = imageTarget.transform.rotation;// targetRotation;
                                                                //transform.LookAt(imageTarget.transform.forward);
        }
    }
}