﻿using System;
using System.Collections;
using System.Collections.Generic; 
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Manages the Cube with the Oculus Hands base functionalities
/// When the Left Index touches the cube, it becomes blue;
/// When the Right Index touches the cube, it become green;
/// When the Left hand pinches the middle finger, it becomes red;
/// </summary>
public class OculusAnchoringDataSource : MonoBehaviour
{
    private Transform rightController;
    private bool attach = false;
    private Vector3 offsetRotFromController = new Vector3(-50f, 180f, 180f);
    //private Vector3 offsetPosFromController = new Vector3(0.1f, 0.115f, -0.056f * 0.5f); // On surface, bottom left corner 
    private Vector3 offsetPosFromController = new Vector3(0.094f, 0.1267f, -0.0291f); // 06/2020 Silver SG1, On surface, bottom left corner 
    private Transform workspace;
    private Transform tactileHardware;
    private GameObject trackingIndicator;
    /// <summary>
    /// Start
    /// </summary>
    void Start()
    {
        //TODO : convert to generic way of getting the gameobject instead of hardcoding the name of the controller
        rightController = GameObject.Find("RightControllerAnchor").transform;
        //TODO : convert to event based system, everytime the anchor position changes, send an event to all listening gameobjects
        workspace = GameObject.Find("Workspace").transform;
        tactileHardware = GameObject.Find("TactileHardware").transform;
        trackingIndicator = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        trackingIndicator.name = "Tracking Indicator";
        trackingIndicator.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
    }
    /// <summary>
    /// Update
    /// </summary>
    void Update()
    {
        if (OVRInput.GetUp(OVRInput.RawButton.X, OVRInput.Controller.LTouch))
        {
            attach = !attach;
            trackingIndicator?.SetActive(attach);

            // attach means unlocked, orientation is happening
            if (attach)
            {
                if (AnchorSource.Instance.OnOculusOrientationUnlocked != null)
                { 
                    AnchorSource.Instance?.OnOculusOrientationUnlocked();
                }
            }
            else
            {
                if (AnchorSource.Instance.OnOculusOrientationLocked != null)
                {
                    AnchorSource.Instance?.OnOculusOrientationLocked();
                }
            }
        }

        if (attach)
        {
            Vector3 pos = Vector3.zero;
            tactileHardware.position = rightController.position;
            tactileHardware.rotation = rightController.rotation *
                                       Quaternion.Euler(offsetRotFromController.x, offsetRotFromController.y, offsetRotFromController.z);
            //transform.position += transform.rotation * new Vector3(-0.1f, -0.109f, 0.056f);
            tactileHardware.position += tactileHardware.right * offsetPosFromController.x + tactileHardware.forward * offsetPosFromController.y + tactileHardware.up * offsetPosFromController.z;
            workspace.position = tactileHardware.position;
            workspace.rotation = tactileHardware.rotation;
            trackingIndicator.transform.position = tactileHardware.position;
            trackingIndicator.transform.rotation = tactileHardware.rotation;
        }
    }
}