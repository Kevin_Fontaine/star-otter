﻿using System.Collections;
using System.Collections.Generic;
using Emerge;
using Emerge.Tracking;
using System;
using UnityEngine;
using Emerge.Core;

public class AnchorSource : Singleton<AnchorSource>
{
    public Action OnOculusOrientationLocked;
    public Action OnOculusOrientationUnlocked;

    void Start()
    {
        SetSource(); 
    }

    private void SetSource()
    {
#if UNITY_EDITOR
        if (TactileConfig.Instance.EditorHandTrackingSource == HandHardware.Oculus && TactileConfig.Instance.EditorAnchoringMethod == Anchoring.OculusControllers)
        {
            AddOculusAnchor();
        }
#else
        if (TactileConfig.Instance.TargetPlatform == Platforms.Oculus_Quest)
        {
            if (TactileConfig.Instance.EditorAnchoringMethod == Anchoring.OculusControllers)
            {
                AddOculusAnchor();
            }
        }
#endif
    }
    private void AddOculusAnchor()
    {
        GameObject child = new GameObject("Oculus");
        child.transform.SetParent(transform);
        OculusAnchoringDataSource oculusTrackingDataSource = child.AddComponent<OculusAnchoringDataSource>();
    } 
}
