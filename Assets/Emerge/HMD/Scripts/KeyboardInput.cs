﻿/******************************************************************************
 * Copyright (C) Emerge Now, Inc. 2016-2019.                                  *
 * Emerge Now Inc proprietary and confidential.                               *
 *                                                                            *
 * Use subject to the terms of the Emerge Exploration Program Agreement       *
 * and/or terms of limited, non-exclusive, non-sublicensable license to       *
 * Emerge SDK between Emerge and you, your company or any other organization. *
 ******************************************************************************/
using UnityEngine;

namespace Emerge.Core
{
    public class KeyboardInput : MonoBehaviour
    {

        [SerializeField]
        ARCameraManager arCameraManager = null;

        [SerializeField]
        PlatformManager platformManager = null;

        [SerializeField]
        AdjustmentHelper incrementalMove = null;

        [SerializeField]
        EmergeAnchorManager emergeAnchorManager = null;

        bool enableTracking = false;
        bool showPlatform = true;

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyUp(KeyCode.T) || Input.GetKeyUp(KeyCode.Space))
            {
                enableTracking = !enableTracking;
                if (enableTracking)
                {
                    arCameraManager.EnableARCamera();
                }
                else
                {
                    arCameraManager.DisableARCamera();
                }
            }

            if (Input.GetKeyUp(KeyCode.P))
            {
                showPlatform = !showPlatform;
                if (showPlatform)
                {
                    platformManager.OnShowPlatform();
                }
                else
                {
                    platformManager.OnHidePlatform();
                }
            }

            if (Input.GetKeyUp(KeyCode.Period))
            {
                incrementalMove.Move(4);
            }
            if (Input.GetKeyUp(KeyCode.Comma))
            {
                incrementalMove.Move(5);
            }
            if (Input.GetKeyUp(KeyCode.RightArrow))
            {
                incrementalMove.Move(1);
            }
            if (Input.GetKeyUp(KeyCode.LeftArrow))
            {
                incrementalMove.Move(3);
            }
            if (Input.GetKeyUp(KeyCode.UpArrow))
            {
                incrementalMove.Move(0);
            }
            if (Input.GetKeyUp(KeyCode.DownArrow))
            {
                incrementalMove.Move(2);
            }

            if (Input.GetKeyUp(KeyCode.S))
            {
                emergeAnchorManager.SaveAllAnchors();
            }

        }
    }
}