﻿/******************************************************************************
 * Copyright (C) Emerge Now, Inc. 2016-2019.                                  *
 * Emerge Now Inc proprietary and confidential.                               *
 *                                                                            *
 * Use subject to the terms of the Emerge Exploration Program Agreement       *
 * and/or terms of limited, non-exclusive, non-sublicensable license to       *
 * Emerge SDK between Emerge and you, your company or any other organization. *
 ******************************************************************************/
using UnityEngine;

namespace Emerge.Core
{
    public class AdjustmentHelper : MonoBehaviour
    {
        public float incrementPositionBy = 0.0025f;
        public float incrementRotationBy = 0.25f;

        public enum MoveDirections
        {
            // actions associated with moving a model the determined increment
            // Unity does not currently allow using select box to choose public enums so we need to rely on indexing
            Forward, // index 0
            Right, // index 1
            Backward, // index 2
            Left, // index 3
            Up, // index 4
            Down, // index 5
            RotateRight, // index 6
            RotateLeft // index 7
        }

        private EmergeAnchor emergeAnchor;

        private void Start()
        {
            if (GetComponent<EmergeAnchor>())
            {
                emergeAnchor = GetComponent<EmergeAnchor>();
            }
        }

        public void Move(int moveDirectionIndex)
        {
            // We need to remove any anchors prior, otherwise they will override our move attempt
            if (emergeAnchor)
            {
                emergeAnchor.RemoveAnchor();
            }

            MoveDirections direction = (MoveDirections)moveDirectionIndex;
            MoveModel(direction);
        }

        private void MoveModel(MoveDirections direction)
        {
            switch (direction)
            {
                case MoveDirections.Forward:
                    emergeAnchor.offset += transform.forward * incrementPositionBy;
                    break;
                case MoveDirections.Right:
                    emergeAnchor.offset += transform.right * incrementPositionBy;
                    break;
                case MoveDirections.Backward:
                    emergeAnchor.offset += transform.forward * -incrementPositionBy;
                    break;
                case MoveDirections.Left:
                    emergeAnchor.offset += transform.right * -incrementPositionBy;
                    break;
                case MoveDirections.Up:
                    emergeAnchor.offset += transform.up * incrementPositionBy;
                    break;
                case MoveDirections.Down:
                    emergeAnchor.offset += transform.up * -incrementPositionBy;
                    break;
                case MoveDirections.RotateRight:
                    transform.rotation *= Quaternion.AngleAxis(incrementRotationBy, Vector3.up);
                    break;
                case MoveDirections.RotateLeft:
                    transform.rotation *= Quaternion.AngleAxis(-incrementRotationBy, Vector3.up);
                    break;
            }
            transform.position = emergeAnchor.offset;
        }
    }
}