﻿/******************************************************************************
 * Copyright (C) Emerge Now, Inc. 2016-2019.                                  *
 * Emerge Now Inc proprietary and confidential.                               *
 *                                                                            *
 * Use subject to the terms of the Emerge Exploration Program Agreement       *
 * and/or terms of limited, non-exclusive, non-sublicensable license to       *
 * Emerge SDK between Emerge and you, your company or any other organization. *
 ******************************************************************************/
using UnityEngine;

namespace Emerge.Core
{
    public class ARCameraManager : Singleton<ARCameraManager>
    {
        public delegate void VuforiaTrackingEnabled();
        public static event VuforiaTrackingEnabled OnVuforiaTrackingEnabled;

        public delegate void VuforiaTrackingDisabled();
        public static event VuforiaTrackingDisabled OnVuforiaTrackingDisabled;

        public GameObject aRCamera;

        [Header("Sound Clips")]
        public AudioClip cameraOn;
        public AudioClip cameraOff;

        private bool camActive = false;

        void Start()
        {
            if (!aRCamera)
            {
                Debug.LogError("Please attach a target tracking GameObject to the AR Camera field.");
            }

            aRCamera.SetActive(camActive);
        }

        public void EnableARCamera()
        {
            if (camActive)
            {
                return; // Camera tracking is already enabled. Nothing to do here
            }

            camActive = true;
            UpdateCameraState();

            if (OnVuforiaTrackingEnabled != null)
            {
                OnVuforiaTrackingEnabled();
            }

            PlaySound(cameraOn);
        }

        public void DisableARCamera()
        {

            if (!camActive)
            {
                return; // Camera tracking is already disabled. Nothing to do here
            }

            camActive = false;
            UpdateCameraState();

            if (OnVuforiaTrackingDisabled != null)
            {
                OnVuforiaTrackingDisabled();
            }

            PlaySound(cameraOff);
        }

        private void UpdateCameraState()
        {
            aRCamera.SetActive(camActive);
        }

        private void PlaySound(AudioClip sound)
        {
            AudioSource source = GetComponent<AudioSource>();
            if (!source || !sound)
            {
                return;
            }

            source.clip = sound;
            source.Play();
        }
    }
}