3 types of objects:

occluders: eg the fingers. use a depthMask shader (grabbed the one from the HL toolkit, can use any other DepthMask shader, Vuforia has one too)
			This object will be drawn just after regular opaque objects, and will prevent subsequent objects from being drawn behind it. 

occludee: eg the ball. these objects must be at a renderqueue of 3000 (transparent) + 20. if needed, attach the RenderQueue script to them to force it

standard: objects in the scene that should remain unaffected, eg the background, platform, etc.
			need a renderQueue of Geometry, so they are drawn BEFORE the 2 types above